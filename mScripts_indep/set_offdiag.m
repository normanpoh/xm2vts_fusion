function sigma = set_offdiag(sigma, value)
index = find(eye(size(sigma))==0);
sigma(index) = value;