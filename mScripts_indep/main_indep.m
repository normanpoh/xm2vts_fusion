addpath ../mScripts
addpath ../mScripts_zoo

load_scores

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear cfg

%define intramodal fusions only
cfg.p{1}=[];
cfg.p{1}{1} = [ 1 2 7 9]; %face
cfg.p{1}{2} = [3 4 5]; %speech
cfg.p{2}{2} = [2 3 4]; %speech

p=1;
%[gmm] =  fusion_gmm_indep(data{p}, [1 2], [1],[],1);

%%
figure(1); set(gca,'fontsize', 14);
[com{1}] =  fusion_gmm(data{p}, [1 2], [1],[],1);
draw_empiric(data{p}.dset{2,1},data{p}.dset{2,2});
bayesS = com{1}.bayesS;
for k=1:2,
  sigma_ = set_offdiag(bayesS(k).sigma, 0);
  bayesS(k).sigma = sigma_;b
end;
%print('-djpeg95','Pictures/main_indep_1.jpg');
xlabel('Face classifier 1');
ylabel('Face classifier 2');
print('-depsc2','Pictures/main_indep_1.eps');

figure(2); set(gca,'fontsize', 14);
com{2} = fusion_gmm(data{p}, [1 2], [1],bayesS,1);
draw_empiric(data{p}.dset{2,1},data{p}.dset{2,2});
xlabel('Face classifier 1');
ylabel('Face classifier 2');
print('-depsc2','Pictures/main_indep_2.eps');
%%
figure(3); set(gca,'fontsize', 14);
[com{3}] =  fusion_gmm(data{p}, [1 2], [4],[],1);
draw_empiric(data{p}.dset{2,1},data{p}.dset{2,2});
xlabel('Face classifier 1');
ylabel('Face classifier 2');
print('-depsc2','Pictures/main_indep_3.eps');
%%
figure(4); set(gca,'fontsize', 14);
for i=1:3,
  wer(com{i}.dset{2,1},com{i}.dset{2,2},[],2,[],i);
end;
wer(com{2}.dset{2,1},com{2}.dset{2,2},[],2,[],2);
%print('-djpeg95','Pictures/main_indep_det.jpg');
legend('Full Bayes (Gaussian)', 'Naive Bayes','Full Bayes (GMM)');
print('-depsc2','Pictures/main_indep_det.eps');
%%
draw_theory_bayesS(gmm.bayesS);

for p=1:2,
  for row = 1:length(cfg.p{p}),
    cfg.p{p}(row,:)
  end;
end;