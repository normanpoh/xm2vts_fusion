function [out, epc_cost] = fusion_clientd_check(expe, chosen, method, beta)

%this operation operates on one dimension only!
n_samples = 11;
epc_range=[0.1 0.9];
%n_samples = 13;
%epc_range=[0 1];

if nargin<2|length(chosen)==0,
  chosen = 1;
end;

if nargin<4,
  beta = 0.5;
end;

method = lower(method);

%get the model label
model_ID = unique(expe.label{1,1});

%calculate global param
param = VR_analysis(expe.dset{1,1}(:,chosen),expe.dset{1,2}(:,chosen));

%go through each id
for d=1:2,for k=1:2,
    out.dset{d,k} = [];%zeros(size(expe.dset{d,k}));
  end;
end;

%perform client-dependent normalisation
for id=1:size(model_ID,1),
  
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      tmp{d,k} = expe.dset{d,k}(index{d,k},chosen);
    end;
  end;
  
  paramj = VR_analysis(tmp{1,1}, tmp{1,2});
  
  switch method
  case 'orig'
    A=1;
    B=0;
  case 'f-norm',
    term = (beta * (paramj.mu_C - paramj.mu_I) + (1-beta) * (param.mu_C - param.mu_I));
    %term = (beta(i)*paramj.mu_C  + (1-beta(i))*param.mu_C) - paramj.mu_I;
    A = 1/term;
    B = paramj.mu_I;
    C = 1;
    out.beta = beta;
  case 'z-norm'
    A = 1/paramj.sigma_I;
    B = paramj.mu_I;
    C = 1;
  case 'z-shift'
    A = 1;
    B = paramj.mu_I;
    C = 1;
  case 'eer-shift'
    B = (paramj.mu_I * paramj.sigma_C + paramj.mu_C * paramj.sigma_I) / (paramj.sigma_I + paramj.sigma_C);
    A = 1;
  case 'f-norm-simple'
    term = (beta * paramj.mu_C + (1-beta) * param.mu_C) - paramj.mu_I;
    %term = (beta(i)*paramj.mu_C  + (1-beta(i))*param.mu_C) - paramj.mu_I;
    A = 1/term;
    B = paramj.mu_I;
    C = 1;
    out.beta = beta;
  otherwise
    error('option does not exist!');
  end;
  %compute the fused scores
  for d=1:2,for k=1:2,
      out.dset{d,k}(index{d,k},:) = A * (tmp{d,k} - B);
    end;
  end;
end;

%calculate the dispersion
[out.epc.dev, out.epc.eva, epc_cost]  = epc(out.dset{1,1}, out.dset{1,2}, out.dset{2,1}, out.dset{2,2}, n_samples,epc_range);

