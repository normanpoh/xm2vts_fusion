function bayesS = get_system_bayesS(local)

for k=1:2
  bayesS(k).mu =[];
end;
%estimate the UI-client distribution
for id=1:length(local),
  for k=1:2,
    bayesS(k).mu = [bayesS(k).mu  local{id}.bayesS(k).mu];
    bayesS(k).sigma(:,:,id) = local{id}.bayesS(k).sigma;
  end;
end;
for k=1:2,
  bayesS(k).weight = ones(length(local),1) / length(local);
end;
