function [com,epc_cost ] = fusion_lr(expe, chosen, draw)
% use [com, com_gmm] = fusion_rbf(expe, chosen, 'nonorm') for no normalisation

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
  chosen = [1:2];
end;

if (nargin < 3),
  draw = 0;
end;

%make chosen
for d=1:2,for k=1:2,
    expe.dset{d,k} = expe.dset{d,k}(:,chosen);
  end;
end;

%standard procedure
data = [expe.dset{1,1}; expe.dset{1,2}];
labels = [ zeros(size(expe.dset{1,1},1),1);ones(size(expe.dset{1,2},1),1)];

%this glmfit output responses between 0 and 1 (logistic regression)
[bl,dl,sl] = glmfit(data,[labels ones(length(data),1)],'binomial');

%project the fused scores:
for d=1:2,
  for k=1:2,
    com.dset{d,k} = glmval(bl, expe.dset{d,k},'logit');
  end;
end;

com.bl = bl;
com.dl = dl;
%com.sl = sl;


[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

%surface fitting
if (draw),
  [xtesta1,xtesta2]=meshgrid( ...
      linspace(min(data(:,1)), max(data(:,1)), 100), ...
      linspace(min(data(:,2)), max(data(:,2)), 100) );
  [na,nb]=size(xtesta1);
  xtest1=reshape(xtesta1,1,na*nb);
  xtest2=reshape(xtesta2,1,na*nb);
  xtest=[xtest1;xtest2]';
  
  ypred = glmval(bl,xtest,'logit');
  
  ypredmat=reshape(ypred,na,nb);
  hold off
  contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
  hold on
  thrd = wer(com.dset{1,1}, com.dset{1,2});
  [cs,h]=contour(xtesta1,xtesta2,ypredmat,[thrd],'k');
  clabel(cs,h);colorbar;
  %plot(raw{1}(:,1), raw{1}(:,2), 'ro');
  %plot(raw{2}(:,1), raw{2}(:,2), 'b+');
  %colorbar;
  draw_empiric(expe.dset{1,1},expe.dset{1,2});
end;

