%multimodal
cfg{1}.p{1}=[];
for t1= [ 1 2 7 9],
  for t2 = [3 4 5],
    cfg{1}.p{1} = [cfg{1}.p{1}; t1 t2];
  end;
end;
cfg{1}.p{2} = [1 2; 1 3; 1 4];

%intramodal
t1 = [1 2 7 9];
t2 = [3 4 5];
cfg{2}.p{1} = [nchoosek(t1, 2); nchoosek(t2, 2)];
cfg{2}.p{2} = nchoosek([2 3 4],2);

syslabel_{1} = {syslabel{1:9}};
syslabel_{2} = {syslabel{10:13}};

clear  myout com
t=0;
for c=1:2,
  for p=1:2,
    for r = 1:size(cfg{c}.p{p},1),
      t=t+1;
      chosen = cfg{c}.p{p}(r,:);
      fprintf('experiment %d: c=%d, p=%d, r=%d combining %s and %s\n',t,c,p,r, ...
	      syslabel_{p}{chosen(1)},syslabel_{p}{chosen(2)}  );
      
      for d=1:2, for k=1:2,
	  expe.dset{d,k} = data{p}.dset{d,k}(:,chosen);
	end;
      end;
      expe.label = data{p}.label;
      
      %user-indep
      [com{1}, epc_cost] = fusion_gmm(expe, [1 2]);
      
      %user-spec: Gauss (by regression)
      [com{2}] = fusion_gauss_client(expe, [1:2], [], 0.5,'ca(mu)-qda');
      [com{3}] = fusion_gauss_client(expe, [1:2], [], 1,'ca(mu)-qda');
      
      %constrained map adaptation (full adaptation for the impostor distribution)
      [com{4}] = fusion_gauss_client(expe, [1:2], [], [0.5 0.5],'map-reynolds');
      [com{5}] = fusion_gauss_client(expe, [1:2], [], [0.5 0],'map-reynolds');
      
      %compensated
      for d=1:2, for k=1:2,
	  tmp.dset{d,k}= [com{1}.dset{d,k} com{2}.dset{d,k} com{3}.dset{d,k} com{4}.dset{d,k} com{5}.dset{d,k} ];
	end;
      end;
      tmp.label = expe.label;
      
      [com{6}] = fusion_wsum_brute_nonorm(tmp, [1 2],[0.5 0.5]);
      [com{7}] = fusion_wsum_brute_nonorm(tmp, [1 3],[0.5 0.5]);
      [com{8}] = fusion_wsum_brute_nonorm(tmp, [1 4],[0.5 0.5]);
      [com{9}] = fusion_wsum_brute_nonorm(tmp, [1 5],[0.5 0.5]);
      
      %a posteriori perf on the eva set
      for t_=1:9,
      %for t_=[5 9],
	[wer_min, tmp_, tmp_, FAR, FRR] = wer(com{t_}.dset{2,1},com{t_}.dset{2,2});
	[deg,myout{t}.polar(t_,:)] = DET2polar(FAR,FRR,[],[]);
      end;
      
      %a priori perf eva.
      for t_=1:9,
	%for t_=[5 9],
	myout{t}.res{t_} = com{t_}.epc;
      end;
      
    end;
  end;
end;

save main_compen_03_true_map_rerun.mat myout epc_cost

signs={'bo-','bs-','bd-','g^-','gv-', 'r*--', 'rp--', 'k*-', 'kp-'};

leg = {'UI','US Reg. (\gamma^C_1=0.5)','US Reg. (\gamma^C_1=1)'};
leg = {leg{:}, 'US MAP (\gamma^C_1=\gamma^C_2=0.5)', 'US MAP (\gamma^C_1=0.5,\gamma^C_2=0)'};
leg = {leg{:},  'Reg. com (\gamma^C_1=0.5)','Reg. com (\gamma^C_1=1)'};
leg = {leg{:},  'MAP com (\gamma^C_1=\gamma^C_2=0.5)','MAP com (\gamma^C_1=0.5,\gamma^C_2=0)'};

mylist = [1:9];
mylist = [1 3 4];
%mylist = [1 2 4];
%mylist = [1:5];
index=find(deg==45); %46;
for t=1:27,
  cla; hold on; set(gca,'fontsize',16);
  for t_=mylist,
    [nFAR,nFRR] = polar2DET(myout{t}.polar(t_,:));
    myeer(t,t_) = (normcdf(nFAR(index)) + normcdf(nFRR(index)) )/2;
    plot(nFAR,nFRR,signs{t_},'markersize',10);
  end;
  tit=sprintf('%d',t);
  title(tit);
  h=legend(leg{mylist});
  set(h,'fontsize',12);
  fname = sprintf('Pictures/main_compen_DET_%02d_simple.eps',t);
  Make_DET(0.10);
  pause;
  %print('-depsc2',fname);
end;

myeer=myeer*100;

cla; hold on;
plot(myeer(:,1),myeer(:,3),'+');
plot([0,2.5],[0,2.5],'k');

cla; hold on;
plot(myeer(:,2),myeer(:,3),'+');
plot([0,2.5],[0,2.5],'k');

expe_list{1}= 1:15; %multimodal
expe_list{2}= 16:21; %face
expe_list{3}= 22:27; %speech
mylist=1:9,

for c=1:3,
  for m=mylist,
    [cfg{c}.res{m},pNI,pNC] = epc_global_custom(myout, m, expe_list{c});
  end;
end;

mylist = [1 3 4];
mylist=[1 2 4 6 8];

lwidth=[ 1 1 1 1 1 2 2 2 2];

mylist = [1 2 4 6 8];
%mylist = [1 3 4 7 8];
for c=1:3,
  figure(c);
  plot_all_epc(epc_cost(:,1),leg,signs, cfg{c}, mylist,lwidth, 14,1,0);
  h = legend(leg{mylist});set(h, 'fontsize',12);
  
  fname = sprintf('Pictures/main_compen_DET_all_c%d.eps',c);
  pause;
  print('-depsc2',fname);
  
  plot_all_epc(epc_cost(:,1),leg,signs, cfg{c}, mylist,lwidth, 14,1,1);
  h = legend(leg{mylist});set(h, 'fontsize',12);
  
  fname = sprintf('Pictures/main_compen_EPC_all_c%d.eps',c);
  pause;
  print('-depsc2',fname);
end;

