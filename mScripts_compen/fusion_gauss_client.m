function [com, epc_cost] = fusion_gauss_client(expe, chosen, param, reliance, method)
%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
        chosen = [1:2];
end;

if (nargin < 4 | length(reliance)==0),
  reliance = 0; %use common client mu
end;

if (nargin < 5),
  method = 'ca(mu)-qda';
end;

%go through each id
for d=1:2,for k=1:2,
    com.dset{d,k} = zeros(size(expe.dset{d,k},1),1);
  end;
end;
for d=1:2,for k=1:2,
    expe.dset{d,k} = expe.dset{d,k}(:,chosen);
  end;
end;

%get global param
if (nargin < 3|length(param)==0),
  param = VR_analysis(expe.dset{1,1},expe.dset{1,2});
end;

%get the model label
model_ID = unique(expe.label{1,1});

local = get_user_bayesS(expe, 1:length(chosen), 1);

%perform client-dependent processing
%fprintf(1, 'Processed ID\n');

for id=1:size(model_ID,1),
  %fprintf(1,'%d ',id);
  bayesS = local{id}.bayesS;

  for d=1:2,
    for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      tmp{id}.dset{d,k} = expe.dset{d,k}(index{d,k},1:length(chosen));
    end;
  end;
  
  switch lower(method)
  case{'cd-qda'}
    %client-dependent quadratic discriminant analysis
    %do nothing
  case{'ca(mu)-qda'}
    %client-adapted MU quadratic discriminant analysis
    bayesS(2).mu = reliance * bayesS(2).mu + (1-reliance) * param.mu_C';
    bayesS(2).sigma = param.cov_C;
  case{'cd-lda'}
    %client-dependent linear discriminant analysi
    bayesS(1).sigma = (bayesS(1).sigma + bayesS(2).sigma)/2;
    bayesS(2).sigma = bayesS(2).sigma;
  case{'ca(mu)-lda'}
    %client-independent linear discriminant analysis
    bayesS(2).mu = reliance * bayesS(2).mu + (1-reliance) * param.mu_C';
    bayesS(2).sigma = param.cov_C;
    bayesS(1).sigma = (bayesS(1).sigma + bayesS(2).sigma)/2;
    bayesS(2).sigma = bayesS(2).sigma;
   
   case{'map-reynolds'}
    %client
    bayesS(2).mu = reliance(1) * bayesS(2).mu + (1-reliance(1)) * param.mu_C';

    bayesS(2).sigma =  (1-reliance(2)) * ( param.cov_C + param.mu_C'*param.mu_C) + ...
	reliance(2) * (bayesS(2).mean_square) ...
	- reliance(2)/reliance(1) * bayesS(2).mu*bayesS(2).mu';

    %impostor
    %bayesS(1).mu = bayesS(1).mu
    bayesS(1).sigma =  bayesS(1).mean_square - bayesS(1).mu*bayesS(1).mu';
    
    %check for positive definiteness
    for t_=1:2,
      [R,p] = chol(bayesS(t_).sigma);
      if p>0
	warning('not a positive definite matrix');
	bayesS(t_).sigma = covfixer2(bayesS(t_).sigma);
      end;
    end;

   otherwise
    error('Option does not exist');
  end;
  [tcmp{id}] = fusion_gmm(tmp{id}, 1:length(chosen), 1,bayesS,0,1); %no epc
  %draw_empiric(tmp{id}.dset{2,1},tmp{id}.dset{2,2});
  
  for d=1:2,for k=1:2,
      %combine the fused scores
        com.dset{d,k}(index{d,k},1) = tcmp{id}.dset{d,k};
    end;
  end;

  %saving the output
  %com.CD_bayesS{id} = local{id}.bayesS;
  %fprintf(1, '.%d', id);
  fprintf(1, '.');
end;

com.reliance = reliance;
%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

   
