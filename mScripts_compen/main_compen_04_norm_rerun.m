n_samples = 11;
epc_range = [0.1 0.9];

get_fnorm_scores;
get_znorm_scores;

clear myout com;

leg = {'gmm','US-qda','mean(gmm,US-qda)','LR(gmm,US-qda)','no-norm','z-norm','f-norm'};
%syslabel={'GMM,F','GMM,F','GMM,S','GMM,S','GMM,S',...
%	  'MLP,F','MLPi,F','MLP,F','MLPi,F','GMM,F',...
%	  'GMM,S','GMM,S','GMM,S'}; 
systype = [1 1 2 2 2  1 1 1 1 0  2 2 2];
syslabel={'(F,DCTs,GMM)', '(F,DCTb,GMM)', '(S,LFCC,GMM)', '(S,PAC,GMM)', '(S,SSC,GMM)',...
	  '(F,DCTs,MLP)','(F,DCTs,iMLP)','(F,DCTb,MLP)','(F,DCTb,iMLP)', '(F,DCTb,GMM)',...
	  '(S,LFCC,GMM)', '(S,PAC,GMM)', '(S,SSC,GMM)'}; 
  
t=0;
for p=1:2,
  for r = 1:size(data{p}.dset{d,k},2),
    t=t+1;
    %data{p}.dset{d,k}(:,r);
    [com{1}, epc_cost] = fusion_gmm(data{p}, r);
    [com{2}] = fusion_gauss_client(data{p}, r, [], 0.5,'ca(mu)-qda');

    %save the data!
    for m=1:2,
      for d=1:2, for k=1:2,
          ndata{m}{p}.dset{d,k}(:,r) = com{m}.dset{d,k};
        end;
      end;
    end;
    
    for d=1:2, for k=1:2,
        com{3}.dset{d,k} =data{p}.dset{d,k}(:,r);
        com{4}.dset{d,k} =zdata{p}.dset{d,k}(:,r);
        com{5}.dset{d,k} =fdata{p}.dset{d,k}(:,r);
      end;
    end;
    for m=3:5,
      [tmp.epc.dev, tmp.epc.eva, epc_cost]  = epc(com{m}.dset{1,1}, com{m}.dset{1,2}, com{m}.dset{2,1}, com{m}.dset{2,2}, n_samples,epc_range);
      com{m}.epc = tmp.epc;
    end;
    
    %a posteriori perf on the eva set
    for m=1:5,
      myout{t}.res{m} = com{m}.epc;
      [wer_min, tmp_, tmp_, FAR, FRR] = wer(com{m}.dset{2,1},com{m}.dset{2,2});
      myout{t}.eer(m) = wer_min;
      [deg,myout{t}.polar(m,:)] = DET2polar(FAR,FRR,[],[]);
    end;

    fprintf(1,'\n');
  end;
end;

save main_compen_04.mat myout
load main_compen_04.mat myout

eer_=[];
for t=1:length(myout),
  eer_=[eer_;myout{t}.eer];
end;
leg={'llr','us-llr','baseline','z-norm','f-norm'};

eer_=eer_*100;
cla; hold on;
plot(eer_(:,1),eer_(:,5),'+');
plot([0 7],[0 7],'k--');

cla; hold on;
plot(eer_(:,5),eer_(:,2),'+');
plot([0 7],[0 7],'k--');

eer_change = (eer_ ./ repmat(eer_(:,3),1,5) - 1)*100;
%leg_={'(a) face','(b) speech'};

selected=[4,5,2]; 
for s=1:2,
  figure(s);set(gca,'fontsize',16);
  s_ = find(systype==s);
  boxplot(eer_change(s_,selected));
  set(gca,'xticklabel',{leg{selected}});
  %xlabel(leg_{s});
  xlabel('');
  ylabel('relative change of EER (%)');
  {syslabel{s_}}
  eer_change(s_,selected)
  fname = sprintf('Pictures/main_compen_04_eer_change_%d.eps',s);
  print('-depsc2',fname);
end;

eer_=eer_*100;
for i=1:length(eer_),
  fprintf(1,'%d & %s & %2.2f & %2.2f & %2.2f & %2.2f \\\\\n',i,syslabel{i},eer_(i,3),eer_(i,4),eer_(i,5),eer_(i,2));
end;


mlist = [3 4 5 2];
range=1:3:91;
signs = {'bo-','rx--','gs-','kd--','k*-'};
for t=1:length(myout),
  cla; hold on;
  for m=1:length(mlist),
    [FAR,FRR]=polar2DET(myout{t}.polar(mlist(m),:));
    plot(FAR(range),FRR(range),signs{mlist(m)},'markersize',10);
  end;
  Make_DET;
  tit = sprintf('%d %s',t,syslabel{t});
  title(tit);
  legend({leg{mlist}});
  fname = sprintf('Pictures/main_compen_04_DET_%02d.eps',t);
  print('-depsc2',fname);
end;

epc_cost = linspace(0.1,0.9,11)';

my_elist{1} =  [1 2 7 9 10];
my_elist{2} = find(systype==2);

{syslabel{my_elist{1}}}
{syslabel{my_elist{2}}}

mylist=1:5;
for i=1:2,
  for m=mylist,
    [mycfg{i}.res{m},pNI,pNC] = epc_global_custom(myout, m, my_elist{i});
  end;
end;
signs = {'bx-','rd--','b*-','k+-','ks--','r^-','kh--','kv-','k^--','ks-','o--'};
lwidth = [ 1 2 1 2 2];

for i=1:2,
  figure(1);
  %mylist=[1:7];mylist=[2,5:7];
  mylist=[1 4 5 2];
  plot_all_epc(epc_cost(:,1),leg,signs, mycfg{i}, mylist,lwidth, 14,1,0);
  fname = sprintf('Pictures/main_compen04_norm%d_DET_all_.eps',i);
  pause;
  print('-depsc2',fname);
  figure(2);
  plot_all_epc(epc_cost(:,1),leg,signs, mycfg{i}, mylist,lwidth, 14,1,1);
  fname = sprintf('Pictures/main_compen04_norm%d_EPC_all_.eps',i);
  pause;
  print('-depsc2',fname);
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%now perform multimodal fusion experiments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg.p{1}=[];
for t1= [ 1 2 7 9],
  for t2 = [3 4 5],
    cfg.p{1} = [cfg.p{1}; t1 t2];
  end;
end;
cfg.p{2} = [1 2; 1 3; 1 4];

t=0;
for p=1:2,
  fprintf(1,'protocol %d\n',p);
  for r=1:length(cfg.p{p}),
    t=t+1;
    fprintf(1, '%d: %s\n',t,[syslabel{cfg.p{p}(r,1)} '+' syslabel{cfg.p{p}(r,2)}] );
  end;
end;

%prepare the data

for p=1:2,
  ndata{3}{p}.dset=zdata{p}.dset;
  ndata{4}{p}.dset=fdata{p}.dset;
end;

leg = {'llr', 'us-llr', 'z-norm', 'f-norm'};
t=0;
for p=1:2,
  for r=1:length(cfg.p{p}),
    fprintf(1,'protocol %d, r %d\n',p,r);
     t=t+1;
    %sum rule fusion
    chosen=cfg.p{p}(r,:);
    for m=1:4,
      
      for d=1:2, for k=1:2,
          com{m}.dset{d,k} = sum(ndata{m}{p}.dset{d,k}(:,chosen),2);
        end;
      end;
      
      [tmp.epc.dev, tmp.epc.eva, epc_cost]  = epc(com{m}.dset{1,1}, com{m}.dset{1,2}, com{m}.dset{2,1}, com{m}.dset{2,2}, n_samples,epc_range);
      myout_fuse{t}.res{m} = tmp.epc;
      
      %a posteriori perf on the eva set
      [wer_min, tmp_, tmp_, FAR, FRR] = wer(com{m}.dset{2,1},com{m}.dset{2,2});
      myout_fuse{t}.eer(m) = wer_min;
      [deg,myout_fuse{t}.polar(m,:)] = DET2polar(FAR,FRR,[],[]);
    end;%m


  end;%r
end;%p

eer_=[];
for t=1:length(myout_fuse),
  eer_=[eer_;myout_fuse{t}.eer];
end;
eer_change = (eer_ ./ repmat(eer_(:,1),1,4) - 1)*100;

save main_compen04_fusion.mat myout_fuse ndata

t=0;
for p=1:2,
  fprintf(1,'protocol %d\n',p);
  for r=1:length(cfg.p{p}),
    t=t+1;
    fprintf(1, '%d: %s %2.2f %2.2f %2.2f\n',t,[syslabel{cfg.p{p}(r,1)} '+' syslabel{cfg.p{p}(r,2)}], eer_change(t,2),eer_change(t,3),eer_change(t,4));
  end;
end;

