for p=1:2,
  for d=1:2, for k=1:2,
      fdata{p}.dset{d,k} = zeros(size(data{p}.dset{d,k}));
    end;
  end;
end;

for p=1:2,
  fdata{p}.label = data{p}.label;
  for b=1:size(data{p}.dset{1,1},2),
    for d=1:2, for k=1:2,
        expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    expe.label = data{p}.label;
    
    %[tmp, epc_cost] = fusion_clientd_check(expe, 1, 'f-norm');
    [tmp, epc_cost] = fusion_clientd_check(expe, 1, 'f-norm-simple');
    
    for d=1:2, for k=1:2,
        fdata{p}.dset{d,k}(:,b) = tmp.dset{d,k};
      end;
    end;
  end;
end;