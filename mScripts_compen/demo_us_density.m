
load main_compen04_fusion

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p=1; b=3;
n_users = 200;
d=2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p=1; b=3;
n_users = 20;
d=2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p=1; b=3;
n_users = 200;
d=1;


leg = {'llr', 'us-llr', 'z-norm', 'f-norm'};

clear pdf_
for m=1:4,
  ndata{m}{p}.label = data{p}.label;
  min_ = min( [ndata{m}{p}.dset{1,1};ndata{m}{p}.dset{1,2}]);
  max_ = max( [ndata{m}{p}.dset{1,1};ndata{m}{p}.dset{1,2}]);
  axis_ = [min_(b) max_(b)];
  
  scores = linspace(min_(b),max_(b),100)';
  [param{m},size_dset{p}] = cal_clientd_param(ndata{m}{p},3);
  
  figure(m);
  cla; hold on;
  for j=1:n_users,
    if m==4,
      scale = 8;
    else
      scale = 1;
    end;
    pdf_(:,1) = cmvnpdf(scores,param{m}.mu{d,1}(j),param{m}.sigma{d,1}(j)/scale);
    pdf_(:,2) = cmvnpdf(scores,param{m}.mu{d,2}(j),param{m}.sigma{d,2}(j)/scale);
    plot(scores,pdf_(:,1),'r-');
    plot(scores,pdf_(:,2),'b-');
  end;
  xlabel('scores');
  ylabel('likelihood');
  axis tight;
end;

for m=1:4,
  figure(m);
  axis_=axis; axis_(3)=0;axis(axis_);
  set(gca,'fontsize',16);
  xlabel('scores');
  ylabel('likelihood');
end;

for m=1:4,
  figure(m);
  %fname = sprintf('Pictures/main_compen04_demo_us_density_m%d_p%d_b%d_d%d_users%d.eps',m,p,b,d,n_users);
  %print('-depsc2',fname);
  fname = sprintf('Pictures/main_compen04_demo_us_density_m%d_p%d_b%d_d%d_users%d.eps',m,p,b,d,n_users);
  print('-depsc2',fname);
end;


d=2;
for m=1:4,
  for k=1:2
    tmp = [var(param{m}.mu{d,k}) var(ndata{m}{p}.dset{d,k})];
    var_(m,:,k) = [tmp(2)-tmp(1) tmp(1)] / tmp(2);
  end;
end;
%draw user-gauss
figure;set(gca,'fontsize',20);
draw_empiric(small.dset{2,1}, small.dset{2,2}, 1);
hold on;
get_user_bayesS(small, [1 2], 2, 1);
print('-depsc2','demo_var_03_user_gauss.eps');
