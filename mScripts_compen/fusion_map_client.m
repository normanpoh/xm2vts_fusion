function [com, epc_cost] = fusion_map_client(expe, chosen, reliance)
%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
  chosen = [1:2];
end;

%get the model label
model_ID = unique(expe.label{1,1});

if (nargin < 3 | length(reliance)==0),
  reliance = 1/length(model_ID); %use equal for client and impostor distributions
  reliance = [reliance reliance];
elseif length(reliance)==1,
  reliance = [reliance reliance];
end;

%go through each id
for d=1:2,for k=1:2,
    com.dset{d,k} = zeros(size(expe.dset{d,k},1),1);
  end;
end;
for d=1:2,for k=1:2,
    expe.dset{d,k} = expe.dset{d,k}(:,chosen);
  end;
end;

local = get_user_bayesS(expe, 1:length(chosen), 1);

%perform client-dependent processing
fprintf(1, 'Processed ID\n');

bayesS = get_system_bayesS(local);

for id=1:size(model_ID,1),
  %fprintf(1,'%d ',id);

  for d=1:2,
    for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      tmp{id}.dset{d,k} = expe.dset{d,k}(index{d,k},1:length(chosen));
    end;
  end;
  
  %calculate likelihoods

  %update the weight parameters
  for k=1:2,
    bayesS(k).weight = zeros(length(model_ID),1);
    bayesS(k).weight(id)=reliance(k);
    others=setdiff(1:length(model_ID),id);
    bayesS(k).weight(others)= (1-reliance(k))/length(others);
  end;
  
  for d=1:2,for k=1:2,
      c=1;tmp1 = gmmb_pdf(tmp{id}.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
      c=2;tmp2 = gmmb_pdf(tmp{id}.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
      com.dset{d,k}(index{d,k},1) = log(tmp2+realmin)-log(tmp1+realmin);
    end;
  end;
  
  fprintf(1, '.%d', id);
end;

com.reliance = reliance;
%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

   
