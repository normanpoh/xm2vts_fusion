cd /home/learning/norman/xm2vts_fusion/mScripts_compen
addpath ../mScripts
initialise;

for p=1:2,
  [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
  [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
  if p==1,
    for d=1:2,for k=1:2,
        data{p}.dset{d,k} = [ data{p}.dset{d,k} ...
          tanh_inv(data{p}.dset{d,k}(:,4)) tanh_inv(data{p}.dset{d,k}(:,5))];
        order = [1 2 3 6 7 8 4 9 5 10];
        data{p}.dset{d,k}(:,[1:10])= data{p}.dset{d,k}(:, order);
        
      end;
    end;
    %rearrange
  end;
  %delete the first one
  for d=1:2,for k=1:2,
      data{p}.dset{d,k}(:,1) = [];
    end;
  end;  
end;
%the new systems are as follows
syslabel={'GMM,F','GMM,F','GMM,S','GMM,S','GMM,S',...
	  'MLP,F','MLPi,F','MLP,F','MLPi,F','GMM,F',...
	  'GMM,S','GMM,S','GMM,S'}; 
systype = [1 1 2 2 2  1 1 1 1 1   2 2 2];
	   
clear cfg

save main_compen_01_gauss_rerun.mat myout epc_cost
load main_compen_01_gauss_rerun.mat myout epc_cost

%define the experimental protocols
cfg.p{1}=[];
for t1= [ 1 2 7 9],
  for t2 = [3 4 5],
    cfg.p{1} = [cfg.p{1}; t1 t2];
  end;
end;
cfg.p{2} = [1 2; 1 3; 1 4];

%get_znorm_scores

leg = {'gmm','US Reg. (\gamma^C_1=0.5)','US Reg. (\gamma^C_1=1)', ...
       'US-gmm','Reg. com (\gamma^C_1=0.5)','Reg. com (\gamma^C_1=1)','mean(gmm,US-gmm)','mix-mean(all,fix)'};
signs = {'bx-','bd--','b*-','b+-','rs--','r^-','kh--','kv-','k^--','ks-','o--'};
lwidth = [ 1 1 1 1 2 2 2 2];

clear  myout com
t=0;
for p=1:2,
  for r = 1:size(cfg.p{p},1),
    %p=1;r=1;
    t=t+1;
    chosen = cfg.p{p}(r,:);

    for d=1:2, for k=1:2,
        expe.dset{d,k} = data{p}.dset{d,k}(:,chosen);
      end;
    end;
    expe.label = data{p}.label;
    
    %user-indep
    [com{1}, epc_cost] = fusion_gmm(expe, [1 2]);

    %user-spec: Gauss
    [com{2}] = fusion_gauss_client(expe, [1:2], [], 0.5,'ca(mu)-qda');

    [com{3}] = fusion_gauss_client(expe, [1:2], [], 1,'ca(mu)-qda');
    %the MAP solutions
    %     r p(y|C,j=ID) + (1-r) p(y|I,j \ne ID)  
    %log ------------------------------------
    %     r p(y|I,j=ID) + (1-r) p(y|I,j \ne ID)  
    
    %usual MAP
    [com{4}] = fusion_map_client(expe, [1:2], [0.5 0.5]);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % UNUSED CLASSIFIERS
    %weigh likelihood: 
    %     r p(y|c,j) + (1-r) p(y|C)  
    %log ----------------------------
    %              p(y|I,j)
    %[com{3}] = fusion_gmm_client(expe, [1:2], [2 5], 0.5);
    %
    %constrained MAP
    %[com{3}] = fusion_map_client(expe, [1:2], [0.5 1]);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %compensated
    for d=1:2, for k=1:2,
	tmp.dset{d,k}= [com{1}.dset{d,k} com{2}.dset{d,k} com{3}.dset{d,k} com{4}.dset{d,k} ];
      end;
    end;
    tmp.label = expe.label;

    [com{5}] = fusion_wsum_brute_nonorm(tmp, [1 2],[0.5 0.5]);
    [com{6}] = fusion_wsum_brute_nonorm(tmp, [1 3],[0.5 0.5]);
    [com{7}] = fusion_wsum_brute_nonorm(tmp, [1 4],[0.5 0.5]);
    [com{8}] = fusion_wsum_brute_nonorm(tmp, [1 2 4],[1/2 1/4 1/4]);

    if (1==1),
      %myout{t}.param = VR_analysis(tmp.dset{2,1},tmp.dset{2,2});
      for d=1:2, for k=1:2,
	  mytmp = corrcoef(tmp.dset{d,k});
	  myout{t}.corr(d,k,1) = mytmp(1,2); %must be somewhat low
	  myout{t}.corr(d,k,2) = mytmp(2,3); %must be high
	  myout{t}.corr(d,k,3) = mytmp(2,4); 
	  myout{t}.corr(d,k,4) = mytmp(1,4); 
	end;
      end;
    end;
    
    for i=1:length(com),   
      myout{t}.res{i} = com{i}.epc;
    end;
  end;
end;


expe_list= 1:15;
mylist=1:8,
for m=mylist,
  [cfg.res{m},pNI,pNC] = epc_global_custom(myout, m, expe_list);
end;

mylist=[1 2 4 5 7];
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1,0);
fname = sprintf('Pictures/main_compen_DET_all.eps');
print('-depsc2',fname);
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1,1);
fname = sprintf('Pictures/main_compen_EPC_all.eps');
print('-depsc2',fname);

mylist=[1 2 5];
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1,0);
fname = sprintf('Pictures/main_compen_DET_all_simple.eps');
print('-depsc2',fname);
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1,1);
fname = sprintf('Pictures/main_compen_EPC_all_simple.eps');
print('-depsc2',fname);

for t=1:15,
  plot_all_epc(epc_cost(:,1),leg,signs, myout{t}, mylist,lwidth, 16,1,0);
  pause;
end;

%to verify the sensitivity to gamma
mylist=[2 3 5 6];
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1,0);
fname = sprintf('Pictures/main_compen_DET_gamma_sens.eps');
print('-depsc2',fname);
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1,1);
fname = sprintf('Pictures/main_compen_EPC_gamma_sens.eps');
print('-depsc2',fname);

%to verify the over all performance
mylist=[1 2 4 5 7];
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1,0);
fname = sprintf('Pictures/main_compen_DET_all_gmm.eps');
print('-depsc2',fname);
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1,1);
fname = sprintf('Pictures/main_compen_EPC_all_gmm.eps');
print('-depsc2',fname);



clear tmp;
mytmp = [];
for k=1:2
  for d=1:2,
    tmp=[];
    for t=1:15,
      tmp=[tmp myout{t}.corr(d,k)];
    end;
  end;
  mytmp = [mytmp tmp'];
end;

subplot(2,1,1);set(gca,'fontsize',16)
boxplot(mytmp,'orientation','horizontal')
a={'Impostor','Client'}
axis([0.5 1 .5 2.5]);
set(gca,'yticklabel',a);
xlabel('Correlation');
ylabel('');
fname = sprintf('Pictures/main_compen_corr.eps');
print('-depsc2',fname);