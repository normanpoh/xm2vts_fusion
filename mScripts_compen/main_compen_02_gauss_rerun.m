clear  myout com
t=0;
for p=1:2,
  for r = 1:size(cfg.p{p},1),
    %p=1;r=1;
    t=t+1;
    chosen = cfg.p{p}(r,:);

    for d=1:2, for k=1:2,
        expe.dset{d,k} = data{p}.dset{d,k}(:,chosen);
      end;
    end;
    expe.label = data{p}.label;
    
    %user-indep
    [com{1}, epc_cost] = fusion_gmm(expe, [1 2]);

    %user-spec: Gauss
    [com{2}] = fusion_gauss_client(expe, [1:2], [], 0.5,'ca(mu)-qda');

    [com{3}] = fusion_gauss_client(expe, [1:2], [], 1,'ca(mu)-qda');

    %compensated
    for d=1:2, for k=1:2,
	tmp.dset{d,k}= [com{1}.dset{d,k} com{2}.dset{d,k} com{3}.dset{d,k} ];
      end;
    end;
    tmp.label = expe.label;

    [com{4}] = fusion_wsum_brute_nonorm(tmp, [1 2],[0.5 0.5]);

    [com{5}] = fusion_wsum_brute_nonorm(tmp, [1 3],[0.5 0.5]);

    %a posteriori perf on the eva set
    for t_=1:5,
      [wer_min, tmp_, tmp_, FAR, FRR] = wer(com{t_}.dset{2,1},com{t_}.dset{2,2});
      [deg,myout{t}.polar(t_,:)] = DET2polar(FAR,FRR,[],[]);
    end;
  end;
end;

signs={'bo-','bs-','bd-', 'r*--', 'rp--'};

leg = {'UI','US (\gamma^C_1=0.5)','US (\gamma^C_1=1)','compensated(\gamma^C_1=0.5)','compensated(\gamma^C_1=1)'};
mylist = [1 3 5];
%mylist = [1 2 4];
%mylist = [1:5];
index=find(deg==45); %46;
for t=1:15,
  cla; hold on; set(gca,'fontsize',16);
  for t_=mylist,
    [nFAR,nFRR] = polar2DET(myout{t}.polar(t_,:));
    myeer(t,t_) = (normcdf(nFAR(index)) + normcdf(nFRR(index)) )/2;
    plot(nFAR,nFRR,signs{t_},'markersize',10);
  end;
  tit=sprintf('%d',t);
  title(tit);
  legend(leg{mylist});
  fname = sprintf('Pictures/main_compen_DET_%02d_simple.eps',t);
  Make_DET(0.10);
  pause;
  print('-depsc2',fname);
end;

myeer=myeer*100;

cla; hold on;
plot(myeer(:,1),myeer(:,3),'+');
plot([0,2.5],[0,2.5],'k');

cla; hold on;
plot(myeer(:,2),myeer(:,3),'+');
plot([0,2.5],[0,2.5],'k');