function [out,size_dset] = cal_clientd_param(expe, chosen)

%this operation operates on one dimension only!

if nargin<2|length(chosen)==0,
  chosen = 1;
end;

if nargin<3|length(method)==0,
  method = 'orig';
end;

method = lower(method);

%get the model label
model_ID = unique(expe.label{1,1});

%calculate global param
param1 = VR_analysis(expe.dset{1,1}(chosen,:),expe.dset{1,2}(chosen,:));
param2 = VR_analysis(expe.dset{2,1}(chosen,:),expe.dset{2,2}(chosen,:));
out.all.mu(1,:) = [param1.mu_I,param1.mu_C];
out.all.sigma(1,:) = [param1.sigma_I,param1.sigma_C];
out.all.mu(2,:) = [param2.mu_I,param2.mu_C];
out.all.sigma(2,:) = [param2.sigma_I,param2.sigma_C];

%go through each id
%for d=1:2,for k=1:2,
%    out.dset{d,k} = [];%zeros(size(expe.dset{d,k}));
%  end;
%end;

%perform client-dependent normalisation
for id=1:size(model_ID,1),

  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      tmp{d,k} = expe.dset{d,k}(index{d,k},chosen);
      size_dset{d,k}(id) = length(tmp{d,k});
    end;
  end;

  for d=1:2,
    paramj{d} = VR_analysis(tmp{d,1}, tmp{d,2});
 
    out.mu{d,1}(id) = paramj{d}.mu_I;
    out.mu{d,2}(id) = paramj{d}.mu_C;
    out.sigma{d,1}(id) = paramj{d}.sigma_I;
    out.sigma{d,2}(id) = paramj{d}.sigma_C;
  end;
end;

