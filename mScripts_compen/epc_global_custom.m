function [res,pNI,pNC] = epc_global_custom(myout, method, expe_list)

NC = 400;
NI = 111800;

out.fa = [];
out.fr = [];
count = 0;
for i=expe_list,
  count = count + 1;
  out.fa = [out.fa; myout{i}.res{method}.eva.far_apri * NI];
  out.fr = [out.fr; myout{i}.res{method}.eva.frr_apri * NC];
end;
out.NC = count * NC;
out.NI = count * NI;

res.eva.far_apri = sum(out.fa) / out.NI;
res.eva.frr_apri = sum(out.fr) / out.NC;
res.eva.hter_apri = (res.eva.far_apri+res.eva.frr_apri)/2;

pNC = out.NC;
pNI = out.NI;