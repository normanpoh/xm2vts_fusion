%run main_compen_01_gauss.m to load the data

n_samples = 11;
epc_range = [0.1 0.9];

%leg = {'gmm','US-qda','mean(gmm,US-qda)','LR(gmm,US-qda)','no-norm','z-norm','f-norm'};

systype = [1 1 2 2 2  1 1 1 1 0  2 2 2];
syslabel={'(F,DCTs,GMM)', '(F,DCTb,GMM)', '(S,LFCC,GMM)', '(S,PAC,GMM)', '(S,SSC,GMM)',...
	  '(F,DCTs,MLP)','(F,DCTs,iMLP)','(F,DCTb,MLP)','(F,DCTb,iMLP)', '(F,DCTb,GMM)',...
    '(S,LFCC,GMM)', '(S,PAC,GMM)', '(S,SSC,GMM)'};
  
speech = {[3 4 5], [2 3 4]};
for p=1:2,
  for r_ = 1:size(speech{p},2),
    r=speech{p}(r_);
    
    for d=1:2, for k=1:2,
        com{1}.dset{d,k} =data{p}.dset{d,k}(:,r);
      end;
    end;
    
    com{2} = fusion_gmm(data{p}, r);
    com{3} = fusion_gauss_client(data{p}, r, [], 0.5,'ca(mu)-qda');
    com{4} = fusion_clientd_check(data{p}, r, 'f-norm-simple');
    com{5} = fusion_clientd_check(data{p}, r, 'z-norm');
    
    %save the data!
    for m=1:5,
      for d=1:2, for k=1:2,
          ndata{p}{m}.dset{d,k}(:,r_) = com{m}.dset{d,k};
        end;
      end;
    end;
    %for m=1,
    %  [tmp.epc.dev, tmp.epc.eva, epc_cost]  = epc(com{m}.dset{1,1}, com{m}.dset{1,2}, com{m}.dset{2,1}, com{m}.dset{2,2}, n_samples,epc_range);
    %  com{m}.epc = tmp.epc;
    %end;
    
    %a posteriori perf on the eva set
    %for m=1:5,
    %  myout{t}.res{m} = com{m}.epc;
    %  [wer_min, tmp_, tmp_, FAR, FRR] = wer(com{m}.dset{2,1},com{m}.dset{2,2});
    %  myout{t}.eer(m) = wer_min;
    %  [deg,myout{t}.polar(m,:)] = DET2polar(FAR,FRR,[],[]);
    %end;

    fprintf(1,'\n');
  end;
end;

%ndata is the main output
%build expe_config
for i=1:3,
  expe_config.seq{i} = nchoosek(1:3,i);
end;

t=0;
for p =1:2,
  for s=1:length(expe_config.seq),
    for r=1:size(expe_config.seq{s},1),
      t=t+1;
      chosen = expe_config.seq{s}(r,:);
      
      for m=1:5,
        [com{m}, epc_cost] = fusion_lr(ndata{p}{m}, chosen);
      end;
      m=6; %simple average rule
      com{m} = fusion_wsum_nonorm(ndata{p}{2}, chosen, [1:length(chosen)]/length(chosen));
      
      for m=1:6,
        myout{t}.res{m} = com{m}.epc;
        [wer_min, tmp_, tmp_, FAR, FRR] = wer(com{m}.dset{2,1},com{m}.dset{2,2});
        myout{t}.eer(m) = wer_min;
        [deg,myout{t}.polar(m,:)] = DET2polar(FAR,FRR,[],[]);
      end;
      fprintf(1,'.');
    end;
  end;
end;

save main_compen_05_norm_fusion.mat myout

leg = {'baseline(LR)', 'baseline-norm(LR)', 'MS-LLR(LR)', 'F-norm(LR)', 'Z-norm(LR)', 'baseline-norm(sum)'};

%calculating EER
eer_=[];
for t=1:length(myout),
  eer_=[eer_;myout{t}.eer];
end;
eer_= eer_*100;
eer_change = (eer_ ./ repmat(eer_(:,1),1,5) - 1)*100;

set(gca,'fontsize',16);
boxplot(eer_change)
set(gca, 'xticklabel', {leg{:}});
xlabel('');
ylabel('a posteriori EER (%)');
fname = sprintf('Pictures/main_compen05_norm_fusion_rel_EER.eps');
print('-depsc2',fname);

%calculating HTER instead
eer_=[];
for t=1:length(myout),
  clear tmp;
  for i=1:5,
    tmp(i) = [myout{t}.res{i}.eva.hter_apri(6)];
  end;
  eer_=[eer_;tmp];
end;
eer_= eer_*100;

for i=1:14,
  fprintf(1, '%1.2f & %1.2f & %1.2f & %1.2f & %1.2f \\\\\n',eer_(i,1),eer_(i,2),eer_(i,3),eer_(i,4),eer_(i,5));
end;
eer_change = (eer_ ./ repmat(eer_(:,1),1,5) - 1)*100;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(gca,'fontsize',16);
boxplot(eer_change)
set(gca, 'xticklabel', {leg{:}});
xlabel('');
ylabel('a posteriori EER (%)');
fname = sprintf('Pictures/main_compen05_norm_fusion_rel_EER.eps');
print('-depsc2',fname);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
signs={'b-', 'b--', 'r-', 'r--', 'k-', 'k--'};
signs_={'bd', 'bx', 'rs', 'r*', 'k^', 'kv'};

%leg_={};
%for i=1:5,
%  leg_ = {leg_{:} leg{i} [leg{i} '--thrd']};
%end;

%print the first seven experiments
selected_list=[1 3 4 5 6];
for t=1:7,
  figure(t); clf; hold on; set(gca,'fontsize', 14);
  for i=selected_list,
    plot(ppndf(myout{t}.res{i}.eva.far_apri(11)),ppndf(myout{t}.res{i}.eva.frr_apri(11)),signs_{i},'markersize',14);
  end;
  for i=selected_list,
    [FAR,FRR] = polar2DET(myout{t}.polar(i,:));
    plot(FAR,FRR,signs{i});
  end;
  Make_DET;
  legend({leg{selected_list}},'fontsize',12);
end;

i=0;
for t=1:3,
  for r=1:size(expe_config.seq{t},1),
    i=i+1;
    tit{i} = sprintf('Exp %d: %s',i,num2str(expe_config.seq{t}(r,:)));
  end;
end;
for t=1:7,
  figure(t);  title(tit{t});
  fname = sprintf('Pictures/main_compen05_norm_fusion_exp%d.eps',t);
  print('-depsc2',fname);
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


eer_= eer_*100;

my_elist{1} =  [1 2 3  8 9 10 ];
my_elist{2} =  [4 5 6  11 12 13];
my_elist{3} =  [7 14];

clear eer_ eer_change;
for i=1:3,
  eer_{i}=[];
  for t=1:length(my_elist{i}),
    eer_{i}=[eer_{i};myout{my_elist{i}(t)}.eer];
  end;
  eer_{i}= eer_{i}*100;
  eer_change{i} = (eer_{i} ./ repmat(eer_{i}(:,1),1,5) - 1)*100;
end;
for i=1:3,
  subplot(2,3,i);
  boxplot(eer_change{i}(:,3:5));
  axis([.5 3.5 -75 10]);
  set(gca, 'xticklabel', {leg{3:5}});
  xlabel('');  ylabel('');
  title([ '(' char(96+i) ')']);
end;
subplot(2,3,1); ylabel('a posteriori EER (%)');
fname = sprintf('Pictures/main_compen05_norm_fusion_rel_EER_details.eps');
print('-depsc2',fname);

%syslabel_= {'(S,LFCC,GMM)', '(S,PAC,GMM)', '(S,SSC,GMM)'};
 
t=0;
for p =1:2,
  for s=1:length(expe_config.seq),
    for r=1:size(expe_config.seq{s},1),
      t=t+1;
      chosen = expe_config.seq{s}(r,:);
      fprintf(1,'%d & %d & %s & %1.2f & %1.2f & %1.2f & %1.2f & %1.2f \\\\\n', t,p,num2str(chosen), eer_(t,1), eer_(t,2), eer_(t,3), eer_(t,4), eer_(t,5));
    end;
  end;
end;

signs = {'bo-', 'bs-', 'rd--','r+--', 'gx--'};
leg = {'baseline', 'baseline-norm', 'MS-LLR', 'F-norm', 'Z-norm'};

chosen_m = [1 3 4 5];
selected = 1:3:91;

t=0;
for p =1:2,
  for s=1:length(expe_config.seq),
    for r=1:size(expe_config.seq{s},1),
      t=t+1;

      chosen = expe_config.seq{s}(r,:);
      cla; hold on; set(gca,'fontsize', 16);
      for m= chosen_m,
        [FAR,FRR] = polar2DET(myout{t}.polar(m,:));
        plot(FAR(selected),FRR(selected),signs{m},'markersize',10);
      end;
      Make_DET;
      legend({leg{chosen_m}});
      tit = sprintf('p%d: (%s)',p, num2str(chosen));
      title(tit);
      pause;
      axis square;
    end;
    
  end;
end;

%
my_elist{1} =  [1 2 3  8 9 10 ];
my_elist{2} =  [4 5 6  11 12 13];
my_elist{3} =  [7 14];
my_elist{4} =  [1:14];

mylist=1:5;
for i=1:4,
  for m=mylist,
    [mycfg{i}.res{m},pNI,pNC] = epc_global_custom(myout, m, my_elist{i});
  end;
end;

signs = {'bx-','rd--','b*-','k+-','ks--','r^-','kh--','kv-','k^--','ks-','o--'};
lwidth = [ 1 1 2 2 2];

for i=1:4,
  figure(1);
  mylist=[1 3 4 5];
  plot_all_epc(epc_cost(:,1),leg,signs, mycfg{i}, mylist,lwidth, 14,1,0);
  fname = sprintf('Pictures/main_compen05_norm_fusion_%d_DET_all_.eps',i);
  pause;
  print('-depsc2',fname);
  figure(2);
  plot_all_epc(epc_cost(:,1),leg,signs, mycfg{i}, mylist,lwidth, 14,1,1);
  fname = sprintf('Pictures/main_compen05_norm_fusion_%d_EPC_all_.eps',i);
  pause;
  print('-depsc2',fname);
end;
