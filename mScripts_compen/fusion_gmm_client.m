function [com, epc_cost] = fusion_gmm_client(expe, chosen, n_gmm,reliance)
%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
        chosen = [1:2];
end;

if (nargin < 4 | length(reliance)==0),
  reliance = 0; %use common client mu
end;

%go through each id
for d=1:2,for k=1:2,
    com.dset{d,k} = zeros(size(expe.dset{d,k},1),1);
  end;
end;
for d=1:2,for k=1:2,
    expe.dset{d,k} = expe.dset{d,k}(:,chosen);
  end;
end;

%get global param
if (nargin < 3|length(n_gmm)==0),
  n_gmm=[2 6];
end;
param = VR_analysis(expe.dset{1,1},expe.dset{1,2});

%get the model label
model_ID = unique(expe.label{1,1});

local = get_user_bayesS(expe, 1:length(chosen), 1);

%perform client-dependent processing
fprintf(1, 'Processed ID\n');

%estimate the UI-client distribution
data = [expe.dset{1,2}];
labels = ones(size(data,1),1);
FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
bayesS_ui_gen = gmmb_create(data, labels, 'FJ', FJ_params{:});

for id=1:size(model_ID,1),
  %fprintf(1,'%d ',id);
  bayesS_us_gen = local{id}.bayesS;

  for d=1:2,
    for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      tmp{id}.dset{d,k} = expe.dset{d,k}(index{d,k},1:length(chosen));
    end;
  end;
  
  %estimate the US-impostor distribution
  data = [tmp{id}.dset{1,1}];
  labels = ones(size(data,1),1);
  FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
  bayesS_us_imp = gmmb_create(data, labels, 'FJ', FJ_params{:});
  
  %calculate likelihoods
  
  for d=1:2,for k=1:2,
      %US impostor
      c=1;tmp1 =  gmmb_pdf(tmp{id}.dset{d,k}, bayesS_us_imp(c).mu, bayesS_us_imp(c).sigma, bayesS_us_imp(c).weight );
      %adapted US client
      c=2;tmp2 = reliance * gmmb_pdf(tmp{id}.dset{d,k}, bayesS_us_gen(c).mu, bayesS_us_gen(c).sigma, bayesS_us_gen(c).weight ) ...
	+ (1-reliance) * gmmb_pdf(tmp{id}.dset{d,k}, bayesS_ui_gen(1).mu, bayesS_ui_gen(1).sigma, bayesS_us_gen(1).weight );
      com.dset{d,k}(index{d,k},1) = log(tmp2+realmin)-log(tmp1+realmin);
    end;
  end;
  
  fprintf(1, '.%d', id);
end;

com.reliance = reliance;
%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

   
