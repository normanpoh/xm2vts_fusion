clear myout com;

leg = {'gmm','US-qda','mean(gmm,US-qda)','LR(gmm,US-qda)','no-norm','z-norm','f-norm'};
syslabel={'GMM,F','GMM,F','GMM,S','GMM,S','GMM,S',...
	  'MLP,F','MLPi,F','MLP,F','MLPi,F','GMM,F',...
	  'GMM,S','GMM,S','GMM,S'}; 
systype = [1 1 2 2 2  1 1 1 1 1   2 2 2];

t=0;
for p=1:2,
  for r = 1:size(data{p}.dset{d,k},2),
    t=t+1;
    %data{p}.dset{d,k}(:,r);
    [com{1}, epc_cost] = fusion_gmm(data{p}, r);
    [com{2}] = fusion_gauss_client(data{p}, r, [], 0.5,'ca(mu)-qda');
    %compensated
    for d=1:2, for k=1:2,
        tmp.dset{d,k}= [com{1}.dset{d,k} com{2}.dset{d,k}];
      end;
    end;
    tmp.label = data{p}.label;
    
    if (1==0),
      figure(1);set(gca,'fontsize',16);
      draw_empiric(tmp.dset{1,1},tmp.dset{1,2},0.1);
      xlabel('User Independent');
      ylabel('User Dependent');
      fname = sprintf('Pictures/main_compen_norm_task_%02d.eps',t);
      print('-depsc2',fname);
    
      myout{t}.param = VR_analysis(tmp.dset{2,1},tmp.dset{2,2});
      for d=1:2, for k=1:2,
          mytmp = corrcoef(tmp.dset{d,k});
          myout{t}.corr(d,k) = mytmp(1,2);
        end;
      end;
    end;
    
    [com{3}] = fusion_wsum_brute_nonorm(tmp, [1 2],[0.5 0.5]);
    [com{4}] = fusion_lr(tmp, [1 2]);
    %[com{5}] = fusion_svm(tmp, [1 2]);
    %[com{5}] = fusion_gmm_client(expe, [1:2], [2 5], 0.5);
    for i=1:4,   
      myout{t}.res{i} = com{i}.epc;
    end;

  end;
end;


get_fnorm_scores;
get_znorm_scores;

n_samples = 11;
epc_range = [0.1 0.9];

t=0;
for p=1:2,
  for r = 1:size(data{p}.dset{d,k},2),
    t=t+1;
    for d=1:2, for k=1:2,
        tmpo.dset{d,k} =data{p}.dset{d,k}(:,r);
        tmpz.dset{d,k} =zdata{p}.dset{d,k}(:,r);
        tmpf.dset{d,k} =fdata{p}.dset{d,k}(:,r);
      end;
    end;
    [tmpo.epc.dev, tmpo.epc.eva, epc_cost]  = epc(tmpo.dset{1,1}, tmpo.dset{1,2}, tmpo.dset{2,1}, tmpo.dset{2,2}, n_samples,epc_range);
    [tmpz.epc.dev, tmpz.epc.eva, epc_cost]  = epc(tmpz.dset{1,1}, tmpz.dset{1,2}, tmpz.dset{2,1}, tmpz.dset{2,2}, n_samples,epc_range);
    [tmpf.epc.dev, tmpf.epc.eva, epc_cost]  = epc(tmpf.dset{1,1}, tmpf.dset{1,2}, tmpf.dset{2,1}, tmpf.dset{2,2}, n_samples,epc_range);
    myout{t}.res{5} = tmpo.epc;
    myout{t}.res{6} = tmpz.epc;
    myout{t}.res{7} = tmpf.epc;
  end;
end;
clear tmp*

save main_compen_02_norm myout epc_cost
load main_compen_02_norm myout epc_cost

signs = {'bx-','bd--','b*-','r+-','rs--','r^-','kh--','kv-','k^--','ks-','o--'};
lwidth = [ 1 1 2 2 2 2 2];

my_elist{1} =  [1 2 7 9 10];
my_elist{2} = find(systype==2);

{syslabel{my_elist{1}}}
{syslabel{my_elist{2}}}

mylist=1:7;
for i=1:2,
  for m=mylist,
    [mycfg{i}.res{m},pNI,pNC] = epc_global_custom(myout, m, my_elist{i});
  end;
end;

for i=1:2,
  figure(1);
  mylist=[1:7];mylist=[2,5:7];
  plot_all_epc(epc_cost(:,1),leg,signs, mycfg{i}, mylist,lwidth, 14,1,0);
  fname = sprintf('Pictures/main_compen_norm%d_DET_all_.eps',i);
  pause;
  print('-depsc2',fname);
  figure(2);
  plot_all_epc(epc_cost(:,1),leg,signs, mycfg{i}, mylist,lwidth, 14,1,1);
  fname = sprintf('Pictures/main_compen_norm%d_EPC_all_.eps',i);
  pause;
  print('-depsc2',fname);
end;

clear tmp mytmp;
for i=1:2,
  mytmp{i} = [];
  for k=1:2

    for d=1:2,
      tmp{i}=[];
      for t=my_elist{i},
	tmp{i}=[tmp{i} myout{t}.corr(d,k)];
      end;
    end;
    mytmp{i} = [mytmp{i} tmp{i}'];
  
  end;
end;


tit={'face','speech'};
for i=1:2,
  subplot(2,1,i);set(gca,'fontsize',16)
  boxplot(mytmp{i},'orientation','horizontal')
  axis([0.5 1 .5 2.5]);
  a={'Impostor','Client'};
  set(gca,'yticklabel',a);
  ylabel('');
  title(['(',char(96+i),') ' tit{i}]);
  xlabel('');
end;
xlabel('Correlation');
fname = sprintf('Pictures/main_compen_corr_norm.eps');
print('-depsc2',fname);

save main_compen_02_norm.mat myout epc_cost



for t=1:13,
  subplot(4,4,t);
  plot(ppndf(myout{t}.res{1}.dev.far), ppndf(myout{t}.res{1}.dev.frr))
  hold on;
  plot(ppndf(myout{t}.res{7}.dev.far), ppndf(myout{t}.res{7}.dev.frr),'r--')
end;