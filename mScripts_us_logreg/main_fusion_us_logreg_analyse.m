load main_fusion_us_logreg.mat
%%
tmp = eer_ ./ repmat(eer_(:,1),1,11)-1;

leg_{1} = 'orig';
leg_{2} = 'z-norm (lr)';
leg_{3} = 'z-norm, gen (lr)';
leg_{4} = 'z-norm [y,p] (lr)';
leg_{5} = 'z-norm [y,y*p] (lr)';
leg_{6} = 'z-norm [y,p,y*p] (lr)';
leg_{7} = 'z-norm';
leg_{8} = 'f-norm';
leg_{9} = 'f-norm v1 (lr)';
leg_{10} = 'f-norm v2 (lr)';
leg_{11} = 'f-norm-lr';

%%
selected = [1 2 7 3 4 5 8 9 10 11];

selected = [1 7 4 ]; %z-norm
selected = [1 8 9 10 11 ]; %f-norm

set(gca,'fontsize',16);
cla; hold on;
boxplot(tmp(:,selected) * 100,'orientation','horizontal')
plot([0;0],[length(selected) + .5;0],'g-');
set(gca,'yticklabel', {leg_{selected}});
xlabel('relative change of EER (%)');
ylabel('');

signs = {'kv-','bx-','ro-','gx-','b*-','rs-','gd-','bv-','g^-','b<-'};

%% plot individual EPCs
for t=1:13,
  cla; hold on;
  for m=1:length(selected),
    plot(epc_cost(:,1), 100*out{t,selected(m)}.epc.eva.hter_apri,signs{m},'markersize',10);
  end;
  legend({leg_{selected}});
  xlabel('\beta');
  ylabel('HTER (%)');
  title(syslabel{t});
  pause;
end;

face_list = setdiff([1 2 6:10],[7,9]);
face = zeros(1,13);
face(face_list)=1;
%% pooled EPC
for m=1:11,
  epc{1}{m} = epc_global_norm(out, m, find(face));
  epc{2}{m} = epc_global_norm(out, m, find(~face));
  epc{3}{m} = epc_global_norm(out, m, 1:13);
end;
leg__ ={'face','speech','all'};

%% pooled DET
for t=1:3,
  figure(t);
  clf; hold on; set(gca,'fontsize',14);
  for m=1:length(selected),
    plot(ppndf(epc{t}{selected(m)}.eva.far_apri),ppndf(epc{t}{selected(m)}.eva.frr_apri),signs{m},'markersize',10);
  end;
  Make_DET();
  axis tight; axis square;
  legend({leg_{selected}});
  title(leg__{t});
  %fname = sprintf('Pictures/main_fusion_us_logreg_%s_det.eps',leg__{t});
  %pause;
  %print('-depsc2',fname);
end;

%% plot all EPCs
for t=1:3,
  figure(t);
  clf; hold on; set(gca,'fontsize',14);
  for m=1:length(selected),
    plot(epc_cost(:,1), 100*epc{t}{selected(m)}.eva.hter_apri,signs{m},'markersize',10);
  end;
  legend({leg_{selected}},'location','north');
  xlabel('\beta');
  ylabel('HTER (%)');
  title(leg__{t});
  fname = sprintf('Pictures/main_fusion_us_logreg_%s_epc.eps',leg__{t});
  print('-depsc2',fname);
end;

%% box plot

for t=1:13;
  for m=1:11
  hter_{1}(t,m) = out{t,m}.epc.eva.hter_apri(1);
  hter_{2}(t,m) = out{t,m}.epc.eva.hter_apri(6);
  hter_{3}(t,m) = out{t,m}.epc.eva.hter_apri(11);
  end;
end;
%%
index{1} = find(face);
index{2} = find(~face);
index{3} = 1:13;

beta_ = {'0.1','0.5','0.9'};
for t=1:3, %beta = .1, .5 or .9
  for i=1:3, %face, speech or all
    figure(t);
    set(gca,'fontsize',16);
    cla; hold on;
    tmp = hter_{t} ./ repmat(hter_{t}(:,1),1,11)-1;
    boxplot(tmp(index{i},selected) * 100,'orientation','horizontal')
    plot([0;0],[length(selected) + .5;0],'g-');
    set(gca,'yticklabel', {leg_{selected}});
    xlabel('relative change of HTER (%)');
    ylabel('');
    fname = sprintf('Pictures/main_fusion_us_logreg_%s_beta%d.eps',leg__{i},t);
    title([leg__{i} ', \beta=' beta_{t}]);
    %pause;
    print('-depsc2',fname);
  end;
fend;


%% demo
p=1;
b=3;
t=3;
syslabel{t}

%choose this one 
ndat = arrange_expe_norm(data{p}, b, param{p,b}, 'z-norm-gen_norm-lr');
com = fusion_lr(ndat, [1:size(ndat.dset{1,1},2)]);
%or this one
% ndat = arrange_expe_norm(fdata{p}, b, paramf{p,b}, 'fz-norm-lr'); %[y^F, 1/sigma^FI, y^F/sigma^FI]
% com{9} = fusion_lr(ndat, [1 2 ],1);
% xlabel('y^F');
% ylabel('1/\sigma^{FI}');
% com{9}.bl

%% plot
clear prob prob_err b_ratio
t=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2),
    t=t+1;

    for d=1:2,
      for id=1:200,
        mu_C = param{p,b}.mu{d,2}(id);
        mu_I = param{p,b}.mu{d,1}(id);
        sigma_I = param{p,b}.sigma{1,1}(id);
        min_ = min([data{p}.dset{2,1}(:,b);data{p}.dset{2,2}(:,b)]);
        max_ = max([data{p}.dset{2,1}(:,b);data{p}.dset{2,2}(:,b)]);
        range = linspace(min_,max_,100)';
        col1 =  range ./ sigma_I;
        col2 = ones(size(range)) * (- mu_I ./ sigma_I);
        col3 = ones(size(range)) * mu_C ./ sigma_I;
        tmp =[col1 col2 col3];
        prob{t}(:,id) = glmval(com.bl, tmp,'logit');
        err_{d}(:,id) = min(prob{t}(:,id), 1-prob{t}(:,id));
      end;
      prob_err{t}(:,d) = mean(err_{d}) * 100;
      b_ratio{t}(:,d) = 1 ./paramf{p,b}.sigma{d,1};
    end;
  end;
end;

%% plot error
p=1;b=3;t=3;
min_ = min([data{p}.dset{2,1}(:,b);data{p}.dset{2,2}(:,b)]);
max_ = max([data{p}.dset{2,1}(:,b);data{p}.dset{2,2}(:,b)]);
range = linspace(min_,max_,100)';

d=2;err_{d} = min(prob{t}, 1-prob{t});
%% 
cla; set(gca,'fontsize',16);
plot(range, prob{t}(:,1:20));
xlabel('scores');
ylabel('posterior probability');
fname = sprintf('Pictures/main_fusion_us_logreg_demo_prob%d.eps',t);
print('-depsc2',fname);
%%
cla;
plot(range, err_{d}(:,1:20));
xlabel('scores');
ylabel('probability of error');
fname = sprintf('Pictures/main_fusion_us_logreg_demo_err%d.eps',t);
print('-depsc2',fname);

%% demo
clf; hold on;
set(gca,'fontsize',16);
id=140;
plot(range, prob{t}(:,id));
for d=1:2,
  mu_C = param{p,b}.mu{d,2}(id);
  mu_I = param{p,b}.mu{d,1}(id);
  sigma_I = param{p,b}.sigma{d,1}(id);
  sigma_C = param{p,b}.sigma{d,2}(id);
  tmp = mvnpdf(range, mu_I, sigma_I ^ 2);
  plot(range, tmp,'r-','linewidth',d);
  tmp = mvnpdf(range, mu_C, sigma_C ^ 2);
  plot(range, tmp,'g-','linewidth',d);
end;
legend('logistic function','impostor (dev)','client(dev)','impostor (eva)','client(eva)','location','northwest');
xlabel('scores');
ylabel('likelihood, posterior');
fname = sprintf('Pictures/main_fusion_us_logreg_demo_density_logistic_id%d_%d.eps',id,t);
print('-depsc2',fname);

%%
for t=1:13,
  figure(1); subplot(3,5,t)
  plot(prob_err{t}(:,1), prob_err{t}(:,2),'+');
  tmp = corrcoef(prob_err{t});
  corr_(t,1) = tmp(1,2);

  figure(2); subplot(3,5,t)
  plot(b_ratio{t}(:,1), b_ratio{t}(:,2),'+');
  tmp = corrcoef(b_ratio{t});
  corr_(t,2) = tmp(1,2);

  figure(3); subplot(3,5,t)
  plot(prob_err{t}(:,1), b_ratio{t}(:,1),'+');
  tmp = corrcoef([prob_err{t}(:,1) b_ratio{t}(:,1)]);
  corr_(t,3) = tmp(1,2);
  
  figure(4); subplot(3,5,t)
  plot(prob_err{t}(:,2), b_ratio{t}(:,2),'+');
  tmp = corrcoef([prob_err{t}(:,2) b_ratio{t}(:,2)]);
  corr_(t,4) = tmp(1,2);

  for f=1:4,
    figure(f);
    title(sprintf('%s (%1.2f)',syslabel{t},corr_(t,f)));
    axis off; axis tight;
  end;
end;
%%
for f=1:4,
  figure(f);
  fname = sprintf('Pictures/main_fusion_us_logreg_predictability_%d.eps',f);
  print('-depsc2',fname);
end;
%%
cla; set(gca,'fontsize',16);
boxplot(corr_(:,[1 2]));
ylabel('correlation');
txt_ ={'Prob Error','B-ratio'};
set(gca,'xticklabel',txt_);
xlabel('');
fname = sprintf('Pictures/main_fusion_us_logreg_predictability_boxplot.eps');
print('-depsc2',fname);
