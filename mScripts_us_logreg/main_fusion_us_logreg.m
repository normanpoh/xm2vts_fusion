%% begin
addpath ../mScripts_zoo
addpath H:\Norman\learning\myPrograms\Matlab\VR-EER
%for fusion_lr
load_scores;
get_fnorm_scores;

%% cal. us param
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2),
    [param{p,b},size_dset{p}] = cal_clientd_param(data{p},b);
    [paramf{p,b},size_dset{p}] = cal_clientd_param(fdata{p},b);
    %[paramz{p,b},size_dset{p}] = cal_clientd_param(zdata{p},b);
  end;
end;
%%
leg_{1} = 'orig';
leg_{2} = 'z-norm (lr)';
leg_{3} = 'z-norm, gen (lr)';
leg_{4} = 'z-norm [y,p] (lr)';
leg_{5} = 'z-norm [y,y*p] (lr)';
leg_{6} = 'z-norm [y,p,y*p] (lr)';
leg_{7} = 'z-norm';
leg_{8} = 'f-norm';
leg_{9} = 'f-norm v1 (lr)';
leg_{10} = 'f-norm v2 (lr)';
leg_{11} = 'f-norm-lr';

%% run expe
expe_cfg=[];
t=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2),
    t=t+1;
    expe_cfg(t,:)=[p,b];
    mytxt{t} = sprintf('P%d:%d %s',p,b,syslabel{t});
    
    ndat = arrange_expe_norm(data{p}, b, param{p,b}, 'orig');
    com{1} = fusion_lr(ndat, [1:size(ndat.dset{1,1},2)]);
    
    ndat = arrange_expe_norm(data{p}, b, param{p,b}, 'z-norm-lr');
    com{2} = fusion_lr(ndat, [1:size(ndat.dset{1,1},2)]);
    
    ndat = arrange_expe_norm(data{p}, b, param{p,b}, 'z-norm-gen_norm-lr');
    com{3} = fusion_lr(ndat, [1:size(ndat.dset{1,1},2)]);
    
    ndat = arrange_expe_norm(data{p}, b, param{p,b}, 'z-norm-[y,p]-lr');
    com{4} = fusion_lr(ndat, [1:size(ndat.dset{1,1},2)]);
    
    ndat = arrange_expe_norm(data{p}, b, param{p,b}, 'z-norm-[y,y*p]-lr');
    com{5} = fusion_lr(ndat, [1:size(ndat.dset{1,1},2)]);
    
    ndat = arrange_expe_norm(data{p}, b, param{p,b}, 'z-norm-[y,p,y*p]-lr');
    com{6} = fusion_lr(ndat, [1:size(ndat.dset{1,1},2)]);
    
    com{7} = fusion_clientd_check(data{p}, b, 'z-norm');
    
    com{8} = fusion_clientd_check(data{p}, b, 'f-norm-simple');
    
    ndat = arrange_expe_norm(fdata{p}, b, paramf{p,b}, 'fz-norm-lr');
    %[y^F, 1/sigma^FI, y^F/sigma^FI]
    com{9} = fusion_lr(ndat, [1 2 ]);
    com{10} = fusion_lr(ndat, [1 2 3]);
    
    ndat = arrange_expe_norm(data{p}, b, param{p,b}, 'f-norm-lr');
    com{11} = fusion_lr(ndat, [1:size(ndat.dset{1,1},2)]);
    
    %plotting the z-norm score using fusion_lr
    %zcom.bl = [0 1 1]'; zcom.link = 'identity';
    %figure;
    %zcom = fusion_lr(ndat, [1 2], 1,zcom);
    %eer_(1) = wer(zcom.dset{2,1},zcom.dset{2,2});
    %just call the z-norm
    %[tmp, epc_cost] = fusion_clientd_check(data{p}, b, 'z-norm');
    %eer_(1) = wer(tmp.dset{2,1},tmp.dset{2,2},[],2,[],1);
    %eer_(2) = wer(com.dset{2,1},com.dset{2,2},[],2,[],2);
    
    for m=1:11,
      out{t,m}.epc = com{m}.epc;
      %eer_(t,m) = wer(com{m}.dset{2,1},com{m}.dset{2,2},[],2,[],m);
      eer_(t,m) = wer(com{m}.dset{2,1},com{m}.dset{2,2},[],2,[],m);
      verif__ = verif(com{m}.dset{2,1},com{m}.dset{2,2});
      verif_{1}(t,m) = verif__(1);verif_{2}(t,m) = verif__(2);verif_{3}(t,m) = verif__(3);
      
    end;
    legend(leg_);
    
    %plot subset
    figure(1);clf;
    selected=[1 7 2:6]; color_=[14 2 5 6 9 10 11];
    for m=1:numel(selected),
      wer(com{selected(m)}.dset{2,1},com{selected(m)}.dset{2,2},[],2,[],color_(m));
    end;
    legend({leg_{selected}});
    
    figure(2);clf;
    selected=[1 8:11]; color_=[14 5:8];
    for m=1:numel(selected),
      wer(com{selected(m)}.dset{2,1},com{selected(m)}.dset{2,2},[],2,[],color_(m));
    end;
    legend({leg_{selected}});
    
    fprintf(1,'.');
  end;
end;
save main_fusion_us_logreg.mat out eer_ verif_

