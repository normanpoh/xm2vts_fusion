cd /home/learning/norman/xm2vts_fusion/mScripts

dat.dset{1} = normrnd(-1, 1, 100000,2);
dat.dset{2} = normrnd(1, 1, 100000,2);
out.dset{1} = min(dat.dset{1},[], 2);
out.dset{2} = min(dat.dset{2},[], 2);
wer_(1,1) = wer(out.dset{1},out.dset{2});
out.dset{1} = max(dat.dset{1},[], 2);
out.dset{2} = max(dat.dset{2},[], 2);
wer_(1,2) = wer(out.dset{1},out.dset{2});
out.dset{1} = mean(dat.dset{1},2);
out.dset{2} = mean(dat.dset{2},2);
wer_(1,3) = wer(out.dset{1},out.dset{2});
wer_(1,4) = wer(dat.dset{1}(:,1),out.dset{2}(:,1));

wer(dat.dset{1}(:,1),out.dset{2}(:,1), [0.5 0.5],1);
param = vr_analysis(dat.dset{1},dat.dset{2});
f_eer(f_ratio(param,1))


dat.dset{1} = normrnd(-1, 1, 100000,2);
dat.dset{2} = normrnd(1, 2, 100000,2);
out.dset{1} = min(dat.dset{1},[], 2);
out.dset{2} = min(dat.dset{2},[], 2);
wer_(2,1) = wer(out.dset{1},out.dset{2});
out.dset{1} = max(dat.dset{1},[], 2);
out.dset{2} = max(dat.dset{2},[], 2);
wer_(2,2) = wer(out.dset{1},out.dset{2});
out.dset{1} = mean(dat.dset{1},2);
out.dset{2} = mean(dat.dset{2},2);
wer_(2,3) = wer(out.dset{1},out.dset{2});

dat.dset{1} = normrnd(-1, 1, 100000,2);
dat.dset{2} = normrnd(1, 0.5, 100000,2);
out.dset{1} = min(dat.dset{1},[], 2);
out.dset{2} = min(dat.dset{2},[], 2);
wer_(3,1) = wer(out.dset{1},out.dset{2});
out.dset{1} = max(dat.dset{1},[], 2);
out.dset{2} = max(dat.dset{2},[], 2);
wer_(3,2) = wer(out.dset{1},out.dset{2});
out.dset{1} = mean(dat.dset{1},2);
out.dset{2} = mean(dat.dset{2},2);
wer_(3,3) = wer(out.dset{1},out.dset{2});

wer_
%wer_ =
%
%   0.1124    0.1123
%    0.2793    0.1499
%    0.1493    0.2784