%pool all the alpha together


for p=1:2,
	for b=1:size(dat.P{p}.labels,2)
		txt = sprintf('Experiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);

		%load score files
		pooled{p,b}.eva.far_apri = baseline{p,b}.epc.eva.far_apri * NI;
		pooled{p,b}.eva.frr_apri = baseline{p,b}.epc.eva.frr_apri * NC;
		pooled{p,b}.eva.hter_apri = (pooled{p,b}.eva.far_apri + pooled{p,b}.eva.frr_apri)/2;
		
	end;
end;

for i=1:2,
	out.fa{i} = [];
	out.fr{i} = [];
end;

count = 0;
p=1;s=1;r=1;
for s=1:3, %over 3 different configurations
	row = 0;
	rp_index = 0;
	for p=1:2, %over two protocols
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			b_expert = expe.P{p}.seq{s}(r,:);
			count = count + 1;
			for i=1:2,
				out.fa{i} = [out.fa{i}; baseline{p,b_expert(i)}.epc.eva.far_apri * NI;
				out.fr{i} = [out.fr{i}; baseline{p,b_expert(i)}.epc.eva.frr_apri * NC;
			end;
		end;
	end;
end;
out.NC = count * NC;
out.NI = count * NI;

for i=1:2,
far_apri = sum(out.fa) / out.NI;
frr_apri = sum(out.fr) / out.NC;
hter_apri = (far_apri+frr_apri)/2;


hter_apri;baseline{p,b_expert(2)}.epc.eva.hter_apri];

			tmp.min = min(tmp.baseline);
			tmp.mean = mean(tmp.baseline);

			for i=1:4,
				out{i}{p,s,r}.beta_mean = tmp.mean ./ com{i}{p,s,r}.epc.eva.hter_apri;
				out{i}{p,s,r}.beta_min = tmp.min ./ com{i}{p,s,r}.epc.eva.hter_apri;

			end;
			for i=1:8,

				cout{i}{p,s,r}.beta_mean = tmp.mean ./ ccom{i}{p,s,r}.epc.eva.hter_apri;
				cout{i}{p,s,r}.beta_min = tmp.min ./ ccom{i}{p,s,r}.epc.eva.hter_apri;
			end;

			%group by configuration:

			rp_index = rp_index + 1;
			for i=1:n_samples,
				row = row + 1;

				for j =1:4
					res_pooled{s}.mean(row,j) = out{j}{p,s,r}.beta_mean(i);
					res_pooled{s}.min(row,j) = out{j}{p,s,r}.beta_min(i);
					res{s,i}.mean(rp_index,j) = out{j}{p,s,r}.beta_mean(i);
					res{s,i}.min(rp_index,j) = out{j}{p,s,r}.beta_min(i);
				end;
				for j =1:8
					cres_pooled{s}.mean(row,j) = cout{j}{p,s,r}.beta_mean(i);
					cres_pooled{s}.min(row,j) = cout{j}{p,s,r}.beta_min(i);
					cres{s,i}.mean(rp_index,j) = cout{j}{p,s,r}.beta_mean(i);
					cres{s,i}.min(rp_index,j) = cout{j}{p,s,r}.beta_min(i);
				end;


			end;
		end;
	end;
end;
