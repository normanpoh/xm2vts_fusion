function plot_all_epc(epc_cost,leg,signs, cfg, list,lwidth, fontsize,legon,plotepc)

if nargin < 7 | length(fontsize)==0,
  fontsize = 20;
end;
if nargin < 8,
  legon = 1;
end;
 if nargin < 9,
  plotepc = 1;
end;
%figure(1);

clf; hold on;
set(gca,'Fontsize', fontsize);
if plotepc
  for i=list,
    plot(epc_cost(:,1), cfg.res{i}.eva.hter_apri*100,signs{i}, 'MarkerSize', 14,'linewidth',lwidth(i));
  end;
  ylabel('HTER(%)');xlabel('\alpha');
  %set(gca,'Fontsize', 10);
  set(gca, 'Xtick', [0.1:0.1:0.9]);
  axis_tmp = axis;
  axis_tmp([1 2]) = [0.1 0.9];
  axis(axis_tmp);
  grid on
else
  for i=list,
    plot(ppndf(cfg.res{i}.eva.far_apri),ppndf(cfg.res{i}.eva.frr_apri),signs{i}, 'MarkerSize', 14,'linewidth',lwidth(i));
  end; 
  Make_DET;
  axis tight; axis square;
end;
if legon,
  legend(leg{list});
end;
