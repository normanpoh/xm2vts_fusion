cd /home/learning/norman/xm2vts_fusion/mScripts
load main_fusion_allinfo.mat com epc_cost
initialise;

for p=1:2,
  [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
  [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

n_samples = 11;
epc_range = [0.1 0.9];

for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);

      %load score files
      for d=1:2,for k=1:2,
	  nexpe.dset{d,k} = data{p}.dset{d,k}(:,b);
	end;
      end;
      wer_(1) = wer(nexpe.dset{1,1}(:,1),nexpe.dset{1,2}(:,1));
      wer_(2) = wer(nexpe.dset{1,1}(:,2),nexpe.dset{1,2}(:,2));
      if  wer_(1) >  wer_(2),
	better = 2;
      else
	better = 1;
      end;
      [com.epc.dev, com.epc.eva, epc_cost]  = epc(nexpe.dset{1,1}(:,better), nexpe.dset{1,2}(:,better), nexpe.dset{2,1}(:,better), nexpe.dset{2,2}(:,better), n_samples,epc_range);
      bcom{p,s,r} = com;
    end;
  end;
end;

save main_fusion_bline bcom;