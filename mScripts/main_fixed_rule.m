cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];


initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
	                end;end;

            		nexpe{2} = convert2margin_scores(nexpe{1});

			%apply F-Norm
			param_global = VR_analysis(nexpe{1}.dset{1,1},nexpe{1}.dset{1,2});
			[nexpe{3}.dset{1,1}, nexpe{3}.dset{1,2}] = VR_Fnorm(nexpe{1}.dset{1,1}, nexpe{1}.dset{1,2}, param_global, param_global, 1:2, 0);
			[nexpe{3}.dset{2,1}, nexpe{3}.dset{2,2}] = VR_Fnorm(nexpe{1}.dset{2,1}, nexpe{1}.dset{2,2}, param_global, param_global, 1:2, 0);


			%Apply Z normalisation on training and test sets, in a ID-dependnent manner
			model.mean_I = mean(nexpe{1}.dset{1,1});
			model.std_I = std(nexpe{1}.dset{1,1});
			[nexpe{4}.dset{1,1}, nexpe{4}.dset{1,2}] = normalise_scores(nexpe{1}.dset{1,1}, nexpe{1}.dset{1,2}, model.mean_I, model.std_I);
			[nexpe{4}.dset{2,1}, nexpe{4}.dset{2,2}] = normalise_scores(nexpe{1}.dset{2,1}, nexpe{1}.dset{2,2}, model.mean_I, model.std_I);

		        for d=1:2,for k=1:2,
	
                		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
				index = 0;
		                for ds = 1:4,
					index = (ds-1)*5;
	        		        %[com{index+1}{p,s,r}.dset{d,k}] = min(nexpe{ds}.dset{d,k}')';
        	        		%[com{index+2}{p,s,r}.dset{d,k}] = max(nexpe{ds}.dset{d,k}')';
	        	        	%[com{index+3}{p,s,r}.dset{d,k}] = mean(nexpe{ds}.dset{d,k}')';

					%choose one of two based on confidence
	        		        %tmp = abs(nexpe{ds}.dset{d,k}');
        	        		%[v,ind]=max(tmp);
		        	        %ind1=find(ind==1);
        		        	%clear tmp_com;
	        	        	%tmp_com(ind1) = nexpe{ds}.dset{d,k}(ind1,1)';
		        	        %ind2=find(ind==2);
        		        	%tmp_com(ind2) = nexpe{ds}.dset{d,k}(ind2,2)';
	                		%com{index+4}{p,s,r}.dset{d,k} = tmp_com';
			
					
					if (d==1 && k==1), %enter once only!
						%com{index+5}{p,s,r} = fusion_wsum_brute_nonorm(nexpe{ds}, [1 2]);
						com{index+5}{p,s,r} =fusion_wsum_brute_raw(nexpe{ds},[1 2] );

					end;
                
				end;
		                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        	    	end;end;

 	    		%for i=1:size(com,2),
                	%	[com{i}{p,s,r}.epc.dev, com{i}{p,s,r}.epc.eva, epc_cost]  = epc(com{i}{p,s,r}.dset{1,1}, com{i}{p,s,r}.dset{1,2}, com{i}{p,s,r}.dset{2,1}, com{i}{p,s,r}.dset{2,2}, n_samples,epc_range);
            		%end;

	    		%free the memory
	    		for i=1:size(com,2),
				com{i}{p,s,r}.dset={};
	    		end;
        	end;
    	end;
end;            

save main_fixed_rule.mat com epc_cost
load main_fixed_rule.mat com epc_cost

%overall performance
for i=1:size(com,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(com{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(com{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(com{i},[2 3]); %intramodal
end;

signs = {'kx-',  'kd-','ko-','k*-', 'ks-'};
signs = {signs{:}, 'rx--',  'rd--','ro--','r*--', 'rs--'};
signs = {signs{:}, 'bx-.',  'bd-.','bo-.','b*-.', 'bs-.'};
signs = {signs{:}, 'gx-',  'gd-','go-','g*-', 'gs-'};

leg = {'orig,min', 'orig,max', 'orig,mean', 'orig,conf', 'orig,wsum'};
leg = {leg{:}, 'margin,min', 'margin,max', 'margin,mean', 'margin,conf', 'margin,wsum'};
leg = {leg{:}, 'Z,min', 'Z,max', 'Z,mean', 'Z,conf', 'Z,wsum'};
leg = {leg{:}, 'F,min', 'F,max', 'F,mean', 'F,conf', 'F,wsum'};

list = 1:size(com,2);

c=1;
plot_all_epc(epc_cost,leg,signs, out.cfg{c}, list);

%sort by fusion classifier
list = [1 6 11];% 16];
list = [1 6 11 16];
c=1;
for i=1:5,
	plot_all_epc(epc_cost,leg,signs, out.cfg{c}, list);
	grid on;
	axis([0.1 0.9 1 5]);
	set(gca, 'xtick', 0.1:0.1:0.9);
	set(gca, 'ytick', 1:0.5:5);

	%print('-depsc2', ['../Pictures/fixed_rule_by_fusion' num2str(i) '_' num2str(c) '.eps']);
	%pause;

	plot_all_roc(epc_cost,leg,signs, out.cfg{c}, list);
	print('-depsc2', ['../Pictures/fixed_rule_by_fusion' num2str(i) '_' num2str(c) '_roc.eps']);
	pause;
	
	list = list +1; pause;
end;

%sig. test:

%sort by normalisation (with diff. classifiers)
c=2;
c=3;
c=1;
list = [1:3 5];
for i=1:4,
	plot_all_epc(epc_cost,leg,signs, out.cfg{c}, list);
	grid on;
	axis([0.1 0.9 0.5 5]);
	set(gca, 'xtick', 0.1:0.1:0.9);
	set(gca, 'ytick', 0.5:0.5:5);

	%pause;
	%print('-depsc2', ['../Pictures/fixed_rule_' num2str(i) '_' num2str(c) '.eps']);

	plot_all_roc(epc_cost,leg,signs, out.cfg{c}, list); pause;
	print('-depsc2', ['../Pictures/fixed_rule_' num2str(i) '_' num2str(c) '_roc.eps']);
	list = list + 5;
end;
