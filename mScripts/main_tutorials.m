initialise;

%load scores
p=1;s=1;r=5;
[dev.wolves, dev.sheep, dev.label_wolves, dev.label_sheep] = load_raw_scores_labels(datafiles{1}.dev, expe.P{p}.seq{s}(r,:));
[eva.wolves, eva.sheep, eva.label_wolves, eva.label_sheep] = load_raw_scores_labels(datafiles{1}.eva, expe.P{p}.seq{s}(r,:));

%plot the training and test scatter diagrams
param = VR_analysis(dev.wolves, dev.sheep);
figure(1);
percentage = 0.1;
VR_draw(dev.wolves, dev.sheep, percentage, param)

figure(2);
VR_draw(eva.wolves, eva.sheep, percentage, param)
xlabel(dat.P{p}.labels(expe.P{p}.seq{s}(r,1)));
ylabel(dat.P{p}.labels(expe.P{p}.seq{s}(r,2)));
legend('client scores', 'impostor scores', 'client mean', 'impostor mean', 'client Gaussian fit', 'impostor Gaussian fit');
%print( '-depsc2', '../Pictures/scatter_plot_2_7.eps');

%plotting density, FAR&FRR, WER and ROC curves
figure(3);
[wer_min, thrd_min, x] = wer(dev.wolves(:,1), dev.sheep(:,1), [0.5, 0.5], 1);
%print( '-depsc2', '../Pictures/analysis_2_7.eps');

%another way of calculating a priori hter
[epc_res{1}.dev epc_res{1}.eva xx.cost]  = epc(dev.wolves(:,1), dev.sheep(:,1), eva.wolves(:,1), eva.sheep(:,1), 3,[0,1]);
display('The operating cost points');
xx.cost
display('The HTER');
epc_res{1}.eva.hter_apri

%test the epc curve
n_samples = 11;
[epc_res{1}.dev epc_res{1}.eva xx.cost]  = epc(dev.wolves(:,1), dev.sheep(:,1), eva.wolves(:,1), eva.sheep(:,1), n_samples,[0,1]);
[epc_res{2}.dev epc_res{2}.eva xx.cost]  = epc(dev.wolves(:,2), dev.sheep(:,2), eva.wolves(:,2), eva.sheep(:,2), n_samples,[0,1]);
NI = size(eva.wolves,1);
NC = size(eva.sheep,1);

%test the significance:
clear tmpA tmpB tmpC;
for i=1:n_samples,
  tmpA =[epc_res{1}.eva.hter_apri(i), epc_res{2}.eva.hter_apri(i)];
  tmpB =[epc_res{1}.eva.far_apri(i),  epc_res{2}.eva.far_apri(i)];
  tmpC =[epc_res{1}.eva.frr_apri(i),  epc_res{2}.eva.frr_apri(i)];
  [tata,tata,tata,xx.ci_percentage(i)] = hter_significant_test(tmpA,tmpB,tmpC, NI, NC, 0.05,1);
end;

figure(4);
subplot(2,1,1); hold off;
plot(xx.cost(:,1),epc_res{1}.eva.hter_apri * 100, 'b-'); hold on;
plot(xx.cost(:,1),epc_res{2}.eva.hter_apri * 100, 'r--'); hold on;
legend(dat.P{p}.labels(expe.P{p}.seq{s}(r,:)));
ylabel('HTER (%)');
xlabel('\alpha');
title('(a) EPC curves');
%
subplot(2,1,2); hold off;
plot(xx.cost(:,1),xx.ci_percentage);hold on;
plot(xx.cost(:,1),repmat(90,size(xx.ci_percentage)),'b:');
plot(xx.cost(:,1),repmat(50,size(xx.ci_percentage)),'b--');
plot(xx.cost(:,1),repmat(10,size(xx.ci_percentage)),'b-.');
ylabel('Confidence (%)');
xlabel('\alpha');
legend( '% confidence', '90% confidence', '50% confidence','10% confidence');
title('(b) Two-sided HTER significant');
%print( '-depsc2', '../Pictures/epc_sig_2_7.eps');