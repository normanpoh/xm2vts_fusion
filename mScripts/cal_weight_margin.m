function [weight] = cal_weight_margin(expe, chosen, n_weights, sensitivity)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2 | length(chosen)==0),
        chosen = [1:2];
end;

if (nargin < 3 | length(n_weights)==0),
	n_weights = 11;
end;

if (nargin < 4 | length(sensitivity)==0),
    %0.1 of (mu_C - mu_I)
    sensitivity = 0.1; 
end;

w = linspace(0,1,n_weights)';
w = [w 1-w];

%optimise the weights
fprintf(1, 'Iterate thru the weights');
for i=1:size(w,1),
	fprintf(1, '.');
	com.dset{1,1} = expe.dset{1,1} * w(i,:)';
	com.dset{1,2} = expe.dset{1,2} * w(i,:)';
	[out.wer(i), out.thrd(i), out.x{i}, out.FAR{i}, out.FRR{i}] = wer(com.dset{1,1} ,com.dset{1,2});%, [], 6, [],i);
    mu_C(i) = mean(com.dset{1,2});
    mu_I(i) = mean(com.dset{1,1});
end;
fprintf(1, '\n');
%recalculate the mu;
%mu_C = mean(mu_C);
%mu_I = mean(mu_I);
%define the C value as in SVM
%C = sensitivity*(mu_C - mu_I);

for i=1:size(w,1),
    %hold off; plot(out.x{i},out.FAR{i},'r'); hold on;
    %plot(out.x{i},out.FRR{i},'b'); hold on;
    [tmp, index_I] = min(abs(out.FAR{i} - sensitivity));
    %plot(out.x{i}(index_I), out.FAR{i}(index_I),'ro');
    [tmp, index_C] = min(abs(out.FRR{i} - sensitivity));
    %plot(out.x{i}(index_C), out.FRR{i}(index_C),'bo');

    %[tmp, index] = min(out.thrd(i)+C - out.x{i});
    %myout.FRR =out.FRR{index};
    %[tmp, index] = min(out.thrd(i)-C - out.x{i});
    %myout.FAR =out.FAR{index};
    C(i) = out.x{i}(index_C) - out.x{i}(index_I);
    %out.FAR{i}(index_I) + out.FRR{i}(index_C);
end;
[tmp,index2] = min(out.wer);
[tmp,index1] = max(C);
%hold off; subplot(2,1,1);plot( w(:,1),out.wer,'b'); 
%hold on; subplot(2,1,2);plot( w(:,1),C,'r');
weight = [w(index1,:); w(index2,:)]; 
