for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('\nExperiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    
    for d=1:2, for k=1:2,
        expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    expe.label = data{p}.label;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %for MLP output
    if ismlp{p}(b),
      for d=1:2,for k=1:2,
          nexpe{1}.dset{d,k} = expe.dset{d,k};
          ex=1;
          nexpe{1}.dset{d,k}(:,ex) = tanh_inv(expe.dset{d,k}(:,ex));
        end;
      end;
      
      %filter the bad ones
      nexpe{2} = nexpe{1};
      for k=1:2,
        d=1;ex=1;
        selected = find(nexpe{2}.dset{d,k}(:,ex)>30);
        nexpe{2}.dset{d,k}(selected)=[];
        selected = find(nexpe{2}.dset{d,k}(:,ex)<-30);
        nexpe{2}.dset{d,k}(selected)=[];
      end;
      
      param = VR_analysis(nexpe{2}.dset{1,1},nexpe{2}.dset{1,2});      
      
      %now replace bad values with a reasonable one
      nexpe{3} = nexpe{1};
      
      for d=1:2, for k=1:2,
          ex=1;
          selected = find(nexpe{3}.dset{d,k}(:,ex)>30);
          nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_C(ex),size(selected,1),1);
          selected = find(nexpe{3}.dset{d,k}(:,ex)<-30);
          nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_I(ex),size(selected,1),1);
        end;
      end;
    
      myexpe = nexpe{3};  
      myexpe.label = data{p}.label;
    end;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [H,P,ksstat{p}(b,1)] =kstest(expe.dset{1,1});
    if ismlp{p}(b),      
      [H,P,ksstat{p}(b,2)] =kstest(myexpe.dset{1,1});    
    else
      ksstat{p}(b,2)=-1;
    end;
  end;
end;

dat = [ksstat{1}(:,1);ksstat{2}(:,1)];
[f,x] = ksdensity(dat);
plot(x,f);
plot(dat,0,'ro');

[tmp, index] = sort(dat);
nindex = zeros(13,2);
nindex(:,2) = index;

selected = find(index<=8);
nindex(selected,1) = 1;

selected = find(index>8);
nindex(selected,1) = 2;
nindex(selected,2) = nindex(selected,2)-8;
nindex = [nindex,tmp];

cla; hold on;
for t =1:length(nindex),
  p=nindex(t,1);
  b=nindex(t,2);
  myleg{t}= dat.P{p}.labels{b};
  mytmp(t,:) = (baseline{6}{p,b}.epc.eva.hter_apri - baseline{1}{p,b}.epc.eva.hter_apri)./baseline{1}{p,b}.epc.eva.hter_apri;
  plot(epc_cost, mytmp(t,:)*100,signs{t});
  %pause;
end;
legend(myleg{:});

for t =1:length(nindex),
  fprintf(1, '%d & %s \t & %1.3f \\\\\n', nindex(t,1), myleg{t},nindex(t,3));
end;

%mycfg=[1 2; 3 4; 5 6; 7 8; 10 11; 12 13];
mycfg{1} = [1:2];
mycfg{2} = [3:4];
mycfg{3} = [5:6];
mycfg{4} = [7:9];
mycfg{5} = [10:13];

clear mycfg;
mycfg{1} = [1:6];
mycfg{2} = [7:9];
mycfg{3} = [10:13];

clear mycfg;
mycfg{1} = [1:2];
mycfg{2} = [5:6];
mycfg{3} = [7:9];
mycfg{4} = [10:13];

for t=1:4,
  mean(nindex(mycfg{t},3))
end;

syslist =[6 3];

for sys=syslist,
  figure(sys);
  cla; hold on; set(gca,'fontsize',16);
  for t =1:length(mycfg),
    %p1=nindex(mycfg(t,1),1);
    %b1=nindex(mycfg(t,1),2);
    %p2=nindex(mycfg(t,2),1);
    %b2=nindex(mycfg(t,2),2);
    %mytmp_=[p1 b1; p2 b2];
    mytmp_ = nindex(mycfg{t},1:2);
    [tmp,pNI(1),pNC(1)] = epc_global_baseline_custom(baseline{sys}, mytmp_, NI, NC);
    [tmp2,pNI(1),pNC(1)] = epc_global_baseline_custom(baseline{1}, mytmp_, NI, NC);
    mytmp(t,:) = (tmp.eva.hter_apri - tmp2.eva.hter_apri)./tmp2.eva.hter_apri;
    plot(epc_cost, mytmp(t,:)*100,signs{t},'markersize',10);
    %pause;
  end;
  plot(epc_cost, repmat(0,length(epc_cost)),'k--');
  %legend(num2str([1:length(mycfg)]'))
  %h = legend('SSC,GMM','PAC,GMM','LFCC,GMM', 'DCT,GMM','Face,MLP');
  h = legend('Group 1','Group 2', 'Group 3','Group 4');
  set(h, 'fontsize',16);
  axis([0.1 0.9, -50 20]); grid on;
  xlabel('\alpha');
  ylabel('relative change of HTER (%)');
end;
tname = 'FNorm_relchange_groups';
for sys=syslist,
  figure(sys);
  fname = sprintf('../Pictures/%s_%d.eps',tname,sys);    
  print('-depsc2', fname);
end;


%DET
lwidth=[0 0 1 0 0 2];
figure(1); cla; hold on;  
set(gca,'fontsize',16);
for sys=syslist,
  for t =1:length(mycfg),
    mytmp_ = nindex(mycfg{t},1:2);
    [tmp,pNI(1),pNC(1)] = epc_global_baseline_custom(baseline{sys}, mytmp_, NI, NC);
    plot(epc_cost, tmp.eva.hter_apri*100,signs{t},'markersize',10,'linewidth',lwidth(sys));
    %plot(ppndf(tmp.eva.far_apri),ppndf(tmp.eva.frr_apri),signs{t},'markersize',10,'linewidth',lwidth(sys));
    %pause;
  end;
  %plot(epc_cost, repmat(0,length(epc_cost)),'k--');
  h = legend('Group 1','Group 2', 'Group 3','Group 4');
  set(h, 'fontsize',16);
  %axis([0.1 0.9, -50 20]); 
  grid on;
  xlabel('\alpha');
  ylabel('HTER (%)');
end;
%Make_DET;

fname = sprintf('../Pictures/FNorm_groups_det.eps');
fname = sprintf('../Pictures/FNorm_groups_epc.eps');
print('-depsc2', fname);

plotdet=1;
%syslist=[1 2 3 6];
syslist=[1 2 3 4 6];
for t =1:length(mycfg)
  subplot(2,2,t); cla; hold on;  
  set(gca,'fontsize',12);
  for sys=syslist,
    mytmp_ = nindex(mycfg{t},1:2);
    [tmp,pNI(1),pNC(1)] = epc_global_baseline_custom(baseline{sys}, mytmp_, NI, NC);
    if (plotdet),
      plot(ppndf(tmp.eva.far_apri),ppndf(tmp.eva.frr_apri),signs{sys},'markersize',10);
      Make_DET;
    else
      plot(epc_cost, tmp.eva.hter_apri*100,signs{sys},'markersize',10);
    end;
  end;
  %h = legend('Group 1','Group 2', 'Group 3','Group 4');
  %set(h, 'fontsize',16);
  %axis([0.1 0.9, -50 20]); 
  if (~plotdet),
    grid on;
    xlabel('\alpha');
    ylabel('HTER (%)');
    axis tight;
    title(num2str(t));
    %axis_tmp =axis;axis_tmp(3)=0;axis(axis_tmp);
  end;
end;
legend(leg{syslist});
fname = sprintf('../Pictures/FNorm_groups2_epc.eps');
fname = sprintf('../Pictures/FNorm_groups2_det.eps');

fname = sprintf('../Pictures/FNorm_groups3_epc.eps');
fname = sprintf('../Pictures/FNorm_groups3_det.eps');
print('-depsc2', fname);



tmp = quantil(mytmp', [2.5, 50 97.5]);
e = tmp([1,3],:) - repmat(tmp(2,:),2,1);
errorbar(nindex(:,3),tmp(2,:)'*100, e(1,:)'*100, e(2,:)'*100)



for cost=1:length(epc_cost),
  subplot(3,4,cost);
  plot(nindex(:,3),mytmp(:,cost),'+');
  tmp = corrcoef(nindex(:,3),mytmp(:,cost));
  coef(cost) = tmp(1,2);
end;




m=[2 3];
for d=1:2,
  figure(d);
  wer(baseline{m(1)}{p,b}.dset{d,1},baseline{m(1)}{p,b}.dset{d,2},[],2)
  wer(baseline{m(2)}{p,b}.dset{d,1},baseline{m(2)}{p,b}.dset{d,2},[],2,[],1)
  legend(leg{m});
end; 

m=[1 2];
for d=1:2,
  figure(d+2);
  wer(baseline{m(1)}{p,b}.dset{d,1},baseline{m(1)}{p,b}.dset{d,2},[],2)
  wer(baseline{m(2)}{p,b}.dset{d,1},baseline{m(2)}{p,b}.dset{d,2},[],2,[],1)
  legend(leg{m});
end; 
   
list=1:5;
for i=list,
  if i==1,hold off;end;
  plot(epc_cost(:,1), baseline{i}{p,b}.epc.eva.hter_apri*100,signs{i},'markersize',10);
  if i==1,hold on;end;
end;
legend(leg{list});
drawnow;

for i=1:size(beta,1),
  plot(epc_cost(:,1), myout{i}.epc.eva.hter_apri,'b'); hold on;
end;

  for d=1:2,for k=1:2,
      cat.dset{d,k} = [expe.dset{d,k} out.dset{d,k}];
    end;
  end;
  draw_empiric( cat.dset{1,1},cat.dset{1,2});
  
  figure(1);
  hold off;
  plot(epc_cost(:,1), out.epc.eva.hter_apri,'b'); hold on;
  plot(epc_cost(:,1), Z.epc.eva.hter_apri,'g'); 
  plot(epc_cost(:,1), orig.epc.eva.hter_apri,'r');
  legend('F-norm','Z-norm','orig');
  
  
  plot(out.epc.eva.far_apri,out.epc.eva.frr_apri,'b');
  wer(expe.dset{2,1},expe.dset{2,2},[],1,[],3);
  res(i) = wer(out.dset{1,1},out.dset{1,2},[],1);
   wer(out.dset{1,1},out.dset{1,2},[],1)
   wer(Z.dset{1,1},Z.dset{1,2},[],1,[],1);
   wer(Z.dset{2,1},Z.dset{2,2},[],1,[],1);

   wer(Z.dset{d,1},Z.dset{d,2},[],1,[],1);


fprintf(1, '\n');
%plot(beta, res);

%find arg max_i res(i)
[v,i]= max(res);
beta_opt = beta(i);