%compare empirical and theoretical diff
initialise;
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%use the normalised version
load main_baseline_addon_FNorm_rerun baseline
for p=1:2,
  bline{p}.label = data{p}.label;
  for b=1:size(dat.P{p}.labels,2)
    bdat{1}{p,b}.dset = baseline{3}{p,b}.dset; %Z-Norm
    %bdat{2}{p,b}.dset = baseline{6}{p,b}.dset;  %F-Norm
  end;
end;
clear baseline;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% construct the styles for ploting purpose
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
style = {'-','--'};%,':'};
color = {'b', 'g', 'r', 'k'};%, 'm', 'y', 'k'};
i=0;
for c=1:length(color),
  for j=1:2, %repeat twice
    for s =1:length(style),
      i=i+1;
      signs{i}=sprintf('%s%s',color{c},style{s});
      lwidth(i) = j;
    end;
  end;
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

model_ID = unique(data{p}.label{1,1});

p=1;b=6;

%demonstrate the performance changes as a function of users
id_list = [10:10:200];

for d=1:2, for k=1:2,
    nexpe.dset{d,k} = data{p}.dset{d,k}(:,b);
  end;
end;
nexpe.label = data{p}.label;

for i=1:length(id_list),
  chosen_ID = 1:id_list(i);
  rexpe = rndchoose_client_list(nexpe, [], chosen_ID, model_ID);
  [wer_min, thrd_min, x] = wer(rexpe.dset{1,1}, rexpe.dset{1,2}, [], 2,[],i);
end;
legend(num2str(id_list'));
print('-depsc2', '../Pictures/main_eer_sensitivity_demo01_client_size.eps');


fprint('-depsc2', '../Pictures/main_eer_sensitivity_demo02_client_compose.eps');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p=1;b=2;
%demonstrate the instability of DET due to the composition of users

for d=1:2, for k=1:2,
    nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
  end;
end;
nexpe{1}.label = data{p}.label;

%try the normalized version
nexpe{2}.dset = bdat{1}{p,b}.dset; %use Z-Norm
nexpe{2}.label = bline{p}.label;
id_list= {[1:50], [51:100],[101:150],[151:200]};
for t=1:2,
  figure(t);
  for i=1:length(id_list),
    chosen_ID = id_list{i};
    rexpe = rndchoose_client_list(nexpe{t}, [], chosen_ID, model_ID);
    [wer_min, thrd_min, x] = wer(rexpe.dset{1,1}, rexpe.dset{1,2}, [], 2,[],i);
  end;
  [wer_min, thrd_min, x] = wer(nexpe{t}.dset{1,1}, nexpe{t}.dset{1,2}, [], 2,[],5);
  legend('set 1 (50)','set 2 (50)','set 3 (50)','set 4 (50)','all (200)');
end;

print('-depsc2', '../Pictures/main_eer_sensitivity_demo02_client_compose.eps');
%print('-depsc2', '../Pictures/main_eer_sensitivity_demo02_client_compose_speech.eps');
%print('-depsc2', '../Pictures/main_eer_sensitivity_demo02_client_compose_speech_norm.eps');

%demonstrate that US-GMM with some prior knowledge can forecast the
%performance can acutally model score very well.


%demonstrate that US-GMM with some prior knowledge can forecast the performance 
%with larger number of users
clear nexpe;
p=1; b=1;
for d=1:2, for k=1:2,
    nexpe.dset{d,k} = data{p}.dset{d,k}(:,b);
  end;
end;
nexpe.label = data{p}.label;

[bayesS] = cal_clientd_stat(rexpe);
[value, thrd, tmp, err, xx, FARn, FRRn] = estimate_EER_gmm(nexpe.dset{1,1}, nexpe.dset{1,2},[],1);

figure(1);
[wer_min, thrd_min, x] = wer(nexpe.dset{1,1}, nexpe.dset{1,2}, [], 2,[],1);
hold on;
%if i==1, hold off; end;
plot(ppndf(FARn), ppndf(FRRn), signs{2},'linewidth',2);
Make_DET;
%if i==1, hold on; end;
legend('Empirical','US-GMM');
figure(1);
print('-depsc2', '../Pictures/main_eer_sensitivity_demo04_gmm01.eps');
figure(101);
h = legend('FAR,scores','FRR,scores', 'FAR,US-GMM','FRR,US-GMM');
set(h, 'fontsize', 16);
print('-depsc2', '../Pictures/main_eer_sensitivity_demo04_gmm02.eps');

for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('\nExperiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    
    for d=1:2, for k=1:2,
        nexpe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    nexpe.label = data{p}.label;



    end;


  end;
end;

