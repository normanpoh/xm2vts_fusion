load main_predict.mat com
ncom = {com{1:5}};

%orig,gmm from main_norm_effect.m
load norm_expe.mat com epc_cost
ncom{6} = com{4};

load main_predict_advanced.mat com
ncom{7} = com{1};
ncom{8} = com{2};
ncom{9} = com{3};
ncom{10} = com{4};
ncom{11} = com{5};
ncom{12} = com{6};
ncom{13} = com{7};
clear com;

load main_single_best.mat com
ncom{14} = com{1};

%overall performance
for i=1:size(ncom,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(ncom{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(ncom{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(ncom{i},[2 3]); %intramodal
end;

signs = {'kx-',  'kd-','ko-','k*-', 'k^-', 'ks-', 'g-','gs-','gx-','gd-','g*-','go-','gd-','b'};
leg = {'fisher', 'gauss-bf', 'bf', 'svm','gmm-bf'}; 
leg = { leg{:}, 'gmm', 'gmm-eer', 'gmm-100', 'gmm-gradient-cv', 'gmm-discrim-cv-robust'}; 
leg = { leg{:}, 'gmm-discrim-cv-fast', 'gmm-discrim-100-robust', 'gmm-svm-100','single-best'};

list = 1:size(ncom,2);
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);

list = [1 2 3 5 6];
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
print('-depsc2', '../Pictures/linear_com_some2.eps');

c=1; b=[4 8];
hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

c=1; b=[8 12];
hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

c=1; b=[4 13 ];
hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

