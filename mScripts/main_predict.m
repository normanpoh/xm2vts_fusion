%load configurations
%cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%Start experiment
p=1;s=1;r=1;
t=0;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			[nexpe{1}.dset{1,1}, nexpe{1}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev, b);
			[nexpe{1}.dset{2,1}, nexpe{1}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva, b);

			%zero-mean unit variance normalisation
			%nexpe{3} = zmuv_norm(nexpe{1});

			%for MLP output
			%for d=1:2,for k=1:2,
			%	nexpe{2}.dset{d,k} = nexpe{1}.dset{d,k};
			%	for ex=1:2,
			%		if (mlp{p,s,r}(ex)),
			%			fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
			%			nexpe{2}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex));
		        %		end;
            		%	end;
            		%end;end;

			t=t+1;

			n=1;

			%figure(3);hold off;set(gca,'Fontsize',14);
			%[weight,tt(t,1)] = find_weight(nexpe{n},'fisher');
			%[com{1}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{n}, [1 2], weight);
			%[weight,tt(t,2)] = find_weight(nexpe{n},'brute_gauss',1);
			%hold on;
			%[com{2}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{n}, [1 2], weight);
			%[weight,tt(t,3)] = find_weight(nexpe{n},'brute',1);
			%[com{3}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{n}, [1 2], weight);
			%[weight,tt(t,4)] = find_weight(nexpe{n},'svm');
            		%[com{4}{p,s,r}, epc_cost] = fusion_svm(nexpe{n}, [1 2],weight);            
			[weight,tt(t,5)] = find_weight(nexpe{n},'gmm',1);
			[com{5}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{n}, [1 2], weight);
			%legend('brute gauss', 'brute', 'brute-gmm');
            		title(expe_labels.P{p}.seq{s}.row{r});ylabel('EER');ylabel('weight');
			fname = sprintf('../Pictures/brute_orig_%d_%d_%d_%d.eps', p,s,r,n);print('-depsc2', fname);
			
			%n=2;
			%figure(3);hold off;set(gca,'Fontsize',14);
			%[weight,tt(t,1+5)] = find_weight(nexpe{n},'fisher');
			%[com{1+5}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{n}, [1 2], weight);
			%[weight,tt(t,2+5)] = find_weight(nexpe{n},'brute_gauss',1);
			%hold on;
			%[com{2+5}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{n}, [1 2], weight);
			%[weight,tt(t,3+5)] = find_weight(nexpe{n},'brute',1);
			%[com{3+5}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{n}, [1 2], weight);
			%[weight,tt(t,4+5)] = find_weight(nexpe{n},'%svm');
            		%[com{4+5}{p,s,r}, epc_cost] = fusion_svm(nexpe{n}, [1 2],weight);            
			%[weight,tt(t,5+5)] = find_weight(nexpe{n},'gmm',1);
			%[com{5+5}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{n}, [1 2], weight);
			%legend('brute gauss', 'brute', 'brute-gmm');
            		%title(expe_labels.P{p}.seq{s}.row{r});ylabel('EER');ylabel('weight');
            		%fname = sprintf('../Pictures/brute_orig_%d_%d_%d_%d.eps', p,s,r,n);print('-depsc2', fname);
            
			%for n=1:2;
			%figure(n);
			%[xtest, xtesta1, xtesta2, na, nb] = make_test(nexpe{n}, 100);
			%[ypred] = xtest *  com{4+(n-1)*4}{p,s,r}.weight';
			%[tmp_, thrd] = wer(com{4+(n-1)*4}{p,s,r}.dset{1,1},com{4+(n-1)*4}{p,s,r}.dset{1,2});
			%ypred= ypred - thrd;
			%ypredmat=reshape(ypred,na,nb);
			%hold off;
			%colormap(gray(256));
			%contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
			%hold on;
			%draw_empiric(nexpe{n}.dset{2,1},nexpe{n}.dset{2,2});
			%[cs,h]=contour(xtesta1,xtesta2,ypredmat,[-1 0 1],'k');
			%clabel(cs,h);
			%fname = sprintf('../Pictures/orig_inv_%d_%d_%d_%d.eps', p,s,r,n);print('-depsc2', fname);

            %figure(n);
			%[xtest, xtesta1, xtesta2, na, nb] = make_test(nexpe{n}, 100);
			%[ypred] = fusion_svm(xtest, [1 2], com{6+(n-1)*6}{p,s,r}.net, 1);
			%[tmp_, thrd] = wer(com{6+(n-1)*6}{p,s,r}.dset{1,1},com{6+(n-1)*6}{p,s,r}.dset{1,2});
			%%ypred= ypred - thrd;
			%ypredmat=reshape(ypred,na,nb);
			%hold off;
			%colormap(gray(256));
			%contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
			%hold on;
			%draw_empiric(nexpe{n}.dset{2,1},nexpe{n}.dset{2,2});
			%[cs,h]=contour(xtesta1,xtesta2,ypredmat,[-1 0 1],'k');
			%clabel(cs,h);
			%fname = sprintf('../Pictures/orig_inv_%d_%d_%d_%d.eps', p,s,r,n);print('-depsc2', fname);

            %end;

			%free the memory
			for i=1:size(com,2),
				com{i}{p,s,r}.dset={};
			end;

		end;
	end;
end;

save main_predict.mat com epc_cost tt


%overall performance
for i=1:size(com,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(com{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(com{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(com{i},[2 3]); %intramodal
end;

signs = {'kx-',  'kd-','ko-','k*-', 'k^-'};
signs = {signs{:}, 'rx--',  'rd--','ro--','r*--', 'r^-'};

leg = {'orig,fisher', 'orig,gauss-bf', 'orig,bf', 'orig,svm','orig,gmm-bf'};%, 'orig,mean'};
leg = {leg{:}, 'inv,fisher', 'inv,gauss-bf', 'inv,bf', 'inv,svm','inv,gmm-bf'};%, 'inv,mean'};


list = 1:size(com,2);

leg = {'fisher', 'gauss-bf', 'bf', 'svm','gmm-bf'};%, 'mean'};
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
print('-depsc2', '../Pictures/linear_com_some.eps');

p=1;s=1;r=1;


figure(3);
b = [3 5];
epc_compare_print(com{b(1)}, com{b(2)}, epc_cost, '_', {leg{b}});
c=1; hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)})


c=1;
b = [3 5];
hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});


%make 5 figures
find_weight(nexpe{n},'gmm',1,1,1);
find_weight(nexpe{n},'gmm',1,1,20);

figure(1);
legend('Empirical,Client', 'Empirical,Impostor', '1 Gaussian, Client', '1 Gaussian, Impostor', '10 Gaussians, Client', '10 Gaussians, Impostor');

for i=1:5
	figure(i);
	print('-depsc2', ['../Pictures/demo_FAR_FRR_' num2str(i) '.eps']);
end;

p=1;s=1;r=11;n=1
b = expe.P{p}.seq{s}(r,:);
%load score files
[nexpe{1}.dset{1,1}, nexpe{1}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev, b);
[nexpe{1}.dset{2,1}, nexpe{1}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva, b);

hold off;
find_weight(nexpe{n},'brute_gauss',1);
hold on;
find_weight(nexpe{n},'brute',1);
find_weight(nexpe{n},'gmm',1,0,2);
find_weight(nexpe{n},'gmm',1,0,5);
find_weight(nexpe{n},'gmm',1,0,10);
find_weight(nexpe{n},'gmm',1,0,30);
legend('Single gaussian', 'brute-force', 'GMM(2)', 'GMM(5)', 'GMM(10)', 'GMM(30)');
xlabel('weight of expert 1');
ylabel('EER');
print('-depsc2', '../Pictures/demo_weight.eps');


