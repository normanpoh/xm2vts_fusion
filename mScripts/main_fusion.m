%load configurations
initialise;

%Start experiment
%p=1;s=1;r=5;
for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      %print remarks
      txt = sprintf('Experiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      %load score files
      [dev.wolves, dev.sheep, dev.label_wolves, dev.label_sheep] = load_raw_scores_labels(datafiles{p}.dev, expe.P{p}.seq{s}(r,:));
      [eva.wolves, eva.sheep, eva.label_wolves, eva.label_sheep] = load_raw_scores_labels(datafiles{p}.eva, expe.P{p}.seq{s}(r,:));
      
      %model_ID = unique(dev.label_sheep);
      [model,com] = fusion_method(dev, eva);
      
      [com.epc_dfwsum.dev, com.epc_dfwsum.eva, epc_cost]  = epc(tmp.out{1,1}, tmp.out{1,2}, tmp.out{2,1}, tmp.out{2,2}, n_samples,[0,1]);
      expe_res.P{p}.seq{s}.hter_apri(r) = com.hter_apri; 
    end;
  end;
end;

%display HTER results
for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      txt = sprintf('Experiment %d (%s) has HTER: %2.3f\n',r,[expe_labels.P{p}.seq{s}.row{r}], expe_res.P{p}.seq{s}.hter_apri(r) *100); 
      fprintf(1,txt);			
		end;
  end;
end;


