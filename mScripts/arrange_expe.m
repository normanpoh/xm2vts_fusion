function nexpe = arrange_expe(expe, a, chosen_y, chosen_q, qtable)
%a = {1,2,3} is the type of arrangement
%arrangement 1: y
%arrangement 2: q, y
%arrangement 3: y.q, q
%arrangement 4: y.q, q, y
% if the input argument does not contain qtable,
% we take that expe.quality contains the real values instead of index to
% qtable
if nargin<5,
  qtable=[];
end;
switch a
  
  case 1
    for d=1:2, for k=1:2,
        nexpe.dset{d,k} = expe.dset{d,k}(:,chosen_y);
        if length(qtable)==0,
          nexpe.quality{d,k} = expe.quality{d,k}(:,chosen_q);
        else
          nexpe.quality{d,k} = qtable(expe.quality{d,k},chosen_q);
        end;
      end;
    end;

  case 2
    for d=1:2, for k=1:2,
        if length(qtable)==0,
          nexpe.dset{d,k} = [expe.quality{d,k}(:,chosen_q) expe.dset{d,k}(:,chosen_y)];
        else
          nexpe.dset{d,k} = [qtable(expe.quality{d,k},chosen_q) expe.dset{d,k}(:,chosen_y)];
        end;
      end;
    end;

  case 3

    for d=1:2, for k=1:2,
        tmp = [];
        for q_=1:length(chosen_q),
          for y_=1:length(chosen_y),
            if length(qtable)==0,
              tmp = [tmp expe.quality{d,k}(:,chosen_q(q_)) .* expe.dset{d,k}(:,chosen_y(y_))];
            else
              tmp = [tmp qtable(expe.quality{d,k},chosen_q(q_)) .* expe.dset{d,k}(:,chosen_y(y_))];
            end;
          end;
        end;
        nexpe.dset{d,k} = [tmp expe.dset{d,k}(:,chosen_y)];
      end;
    end;

  case 4

    for d=1:2, for k=1:2,
        tmp = [];
        for q_=1:length(chosen_q),
          for y_=1:length(chosen_y),
            if length(qtable)==0,
              tmp = [tmp expe.quality{d,k}(:,chosen_q(q_)) .* expe.dset{d,k}(:,chosen_y(y_))];
            else
              tmp = [tmp qtable(expe.quality{d,k},chosen_q(q_)) .* expe.dset{d,k}(:,chosen_y(y_))];
            end;
          end;
        end;

        if length(qtable)==0,
          nexpe.dset{d,k} = [tmp expe.quality{d,k}(:,chosen_q) expe.dset{d,k}(:,chosen_y)];
        else
          nexpe.dset{d,k} = [tmp qtable(expe.quality{d,k},chosen_q) expe.dset{d,k}(:,chosen_y)];
        end;
        
      end;
    end;
    
end;
nexpe.session = expe.session;
nexpe.label = expe.label;