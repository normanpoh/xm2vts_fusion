function [com] = fusion_wsum(expe, chosen, weight)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
        chosen = [1:2];
end;

%get the fixed weight
if (nargin < 3),
	param = VR_analysis(expe.dset{1,1}, expe.dset{1,2});
	com.weight = cal_weight_fisher(chosen, param);
	%com.weight = cal_weight_brute(chosen, param, 101);
else
	com.weight = weight;
end;

%fuse the scores:
for d=1:2,
    for k=1:2,
	com.dset{d,k} = expe.dset{d,k}(:,chosen) * com.weight';
    end;
end;

