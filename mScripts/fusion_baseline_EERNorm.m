function [com, epc_cost,out] = fusion_baseline_EERNorm(expe)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

%zero-mean unit variance normalisation
model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
model.std = std([expe.dset{1,1}; expe.dset{1,2}]);
[expe.dset{1,1}, expe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
[expe.dset{2,1}, expe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);

%get the model label
model_ID = unique(expe.label{1,1});

%go through each id
for d=1:2,for k=1:2,
	com.dset{d,k} = [];
end;end;

%perform client-dependent normalisation
for id=1:size(model_ID,1),

	%get the data set associated to the ID
	for d=1:2,for k=1:2,
	    index{d,k} = find(expe.label{d,k} == model_ID(id));
	    tmp.dset{d,k} = expe.dset{d,k}(index{d,k},:);
	end;end;
    
	%Apply EER-normalisation on training and test sets, in a ID-dependnent manner
	param = VR_analysis(tmp.dset{1,1}, tmp.dset{1,2});
	opt_thrd = (param.mu_I * param.sigma_C + param.mu_C * param.sigma_I) / (param.sigma_I + param.sigma_C);
	
	%compute the fused scores
	for d=1:2,for k=1:2,
  	    com.dset{d,k}(index{d,k},:) = tmp.dset{d,k} - opt_thrd;
	end;end;
	out.opt_thrd(id) = opt_thrd;
end;

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);
