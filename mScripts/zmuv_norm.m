function [nexpe, model] = zmuv_norm(expe)
%[nexpe model] = zmuv_norm(expe)
% performs zero-mean unit-variance normalisation

model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
model.std = std([expe.dset{1,1}; expe.dset{1,2}]);
[nexpe.dset{1,1}, nexpe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
[nexpe.dset{2,1}, nexpe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);
