function export_data(expe,p,s,r)

filename = sprintf('%d_%02d_%02d',p,s,r);
fname{1} = sprintf('../export/%s_dev.scores', filename);
fname{2} = sprintf('../export/%s_eva.scores', filename);


out{1} = [expe.dset{1,1} zeros( size(expe.dset{1,1},1), 1)
	  expe.dset{1,2} ones( size(expe.dset{1,2},1), 1)];
out{2} = [expe.dset{2,1} zeros( size(expe.dset{2,1},1), 1)
	  expe.dset{2,2} ones( size(expe.dset{2,2},1), 1)];

for i=1:2,
	pm_dlmwrite(fname{i}, out{i}, ' ', 1);
end;