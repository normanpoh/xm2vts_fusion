%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;

for p=1:2, %over two protocols
	[data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
	[data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
			end;end;


			%zero-mean unit variance normalisation
			%nexpe{2} = zmuv_norm(nexpe{1});

			%use margin scores	
			%nexpe{3} = convert2margin_scores(nexpe{1});


			%for MLP output
			for d=1:2,for k=1:2,
				nexpe{2}.dset{d,k} = nexpe{1}.dset{d,k};
				for ex=1:2,
					if (mlp{p,s,r}(ex)),
						fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
						nexpe{2}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex));
					end;
				end;
				%nexpe{3}.dset{d,k} = nexpe{2}.dset{d,k};

				%filter extreme values on the dev set only!
				if (d==1),
					for ex=1:2,
						index = find (abs(nexpe{2}.dset{d,k}(:,ex)) > 30);
						nexpe{2}.dset{d,k}(index,:) = [];
					end;
				end;
			end;end;

			chosen = [1 2];
			[com{1}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{1}, chosen);

			%plot here
			%for i=[1 2];
			%	figure(i); hold off; set(gca, 'Fontsize', 20); 
			%	draw_empiric(nexpe{i}.dset{1,1},nexpe{i}.dset{1,2});
			%	xlabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,1)});
			%	ylabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,2)});

			%	figure(i+2); hold off; set(gca, 'Fontsize', 20); 
			%	draw_empiric(nexpe{i}.dset{2,1},nexpe{i}.dset{2,2});
			%	xlabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,1)});
			%	ylabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,2)});
			%end;
			%pause;

		end;
	end;
end;