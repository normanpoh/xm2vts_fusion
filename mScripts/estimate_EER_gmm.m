
function [value, thrd, bayesS, err, xx, FARn, FRRn] = estimate_EER_gmm(wolves, sheep, alpha, dodisplay, bayesS)
% function [value, thrd, bayesS] = estimate_EER_gmm(impostor, client, alpha, dodisplay, bayesS)
% impostor and client scores must be each of 1 column vector
% set them to empty vectors if not used (e.g., [], [])
% alpha is by default [0.5 0.5] (equal cost of FAR and FRR)
% dodisplay = 1 will display conditional cdf
% dodisplay = 1 will display conditional cdf and conditional pdf
% bayesS is the usual bayesS structure (see gmmb_bayes toolbox)


N = 1000; %number of samples
if (nargin < 3|length(alpha)==0),
  alpha = [0.5 0.5];
end;
if (nargin < 4),
  dodisplay = 0;
end;
if (length(wolves)~=0 & length(sheep)~=0),
  data = [wolves;sheep];
  labels = [ones(size(wolves));2*ones(size(sheep))];
end;
if (nargin < 5),
  FJ_params = { 'Cmax', 4, 'Cmin', 2, 'thr', 1e-2, 'animate', 0, 'verbose', 0};
  bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});
end;
%to test
%result = gmmb_classify(bayesS, xx, 'values');

K = size(bayesS,2);
P = [bayesS.apriories];
if (length(wolves)==0|length(sheep)==0),
  %get some data from bayesS
  for k=1:2,
    var{k} = [];
    tata = [bayesS(k).sigma(1,1,:)];
    tata = reshape(tata, length(tata),1);
    var{k} = tata';
  end;
  %data =[bayesS(1).mu - max(var{1}), bayesS(2).mu + max(var{2})];
  data =[bayesS(1).mu, bayesS(2).mu];
else
  data = [wolves;sheep];
end;

xx = linspace(min(data),max(data), N)';
%pxomega_cdf = zeros(N,K);
pxomega_pdf = zeros(N,K);
pxomega = zeros(N,K);
for k = 1:K
  pxomega_pdf(:,k) = gmmb_pdf(xx, bayesS(k).mu, bayesS(k).sigma, bayesS(k).weight );
  %pxomega_cdf(:,k) = gmmb_cdf(xx, bayesS(k).mu, bayesS(k).sigma, bayesS(k).weight );
end

%sample the cummulatif density function
%for i=1:size(pxomega_pdf,1),
%  spdf(i,:) = sum(pxomega_pdf(1:i,:),1);
%end;
spdf = cumsum(pxomega_pdf);
spdf(:,1) = spdf(:,1) / sum(pxomega_pdf(:,1));
spdf(:,2) = spdf(:,2) / sum(pxomega_pdf(:,2));
spdf(:,1) = 1 - spdf(:,1);

cost = spdf(:,1) * alpha(1) + spdf(:,2) * alpha(2);
[value(1), index]=min(cost);

thrd(1) = xx(index);
%value = mean(spdf(index,:));

if (dodisplay == 1),

  figure(101);hold off;
  if (length(wolves)==0|length(sheep)==0),
  else
    [value(2), thrd(2), x, FAR, FRR]= wer(wolves, sheep, alpha*2, 5,[] ,1);
    value(2) = value(2)/2;
  end;
  %plot(xx, 1-pxomega_cdf(:,1),'r--')
  %plot(xx, pxomega_cdf(:,2),'b--')

  hold on;
  plot(xx, spdf(:,1),'r:')
  plot(xx, spdf(:,2),'b:')
  %plot(xx, 1-pxomega_cdf(:,1),'r--')
  %plot(xx, pxomega_cdf(:,2),'b--')

  %plot(xx, cost, 'g');
end;
if (dodisplay > 1),

  figure(102);hold off;
  if (length(wolves)==0|length(sheep)==0),
  else
    [a_, b_]= wer(wolves, sheep,alpha, 4);
  end;
  hold on;
  plot(xx, pxomega_pdf(:,1),'r--')
  plot(xx, pxomega_pdf(:,2),'b--')

end;

if (nargout >= 4),
  if (length(wolves)~=0 & length(sheep)~=0),
    FARn = interp1(x,FAR,xx, 'linear');
    FRRn = interp1(x,FRR,xx, 'linear');
    err(:,1) = abs(spdf(:,1) - FARn);
    err(:,2) = abs(spdf(:,2) - FRRn);
  end;
end;