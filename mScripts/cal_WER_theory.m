function [min_WER] = cal_WER_theory(nbayesS, cost, n_thrds)

if (nargin<2 | length(cost)==0),
	cost = [0.5 0.5];
end;

if (nargin<3 | length(n_thrds)==0),
	n_thrds = 101;
end;

if (n_thrds >= 1),
	dat = [nbayesS(1).mu nbayesS(2).mu];

	K=2; %two class
	xx = linspace(min(dat),max(dat), n_thrds)';
	pxomega_pdf = zeros(n_thrds,K);
	for k = 1:K
		pxomega_pdf(:,k) = gmmb_pdf(xx, nbayesS(k).mu, nbayesS(k).sigma, nbayesS(k).weight);
	       	%pxomega_cdf(:,k) = gmmb_cdf(xx, nbayesS(k).mu, nbayesS(k).sigma, nbayesS(k).weight);    
	end;

	for i=1:size(pxomega_pdf,1),
		spdf(i,:) = sum(pxomega_pdf(1:i,:),1);
	end;
	spdf(:,1) = spdf(:,1) / sum(pxomega_pdf(:,1));
	spdf(:,2) = spdf(:,2) / sum(pxomega_pdf(:,2));
	spdf(:,1) = 1- spdf(:,1);

	%figure;
	%plot(xx,spdf(:,1), 'r'); hold on;
	%plot(xx,spdf(:,2), 'b');
	%drawnow

	cost = spdf(:,1) * cost(1) + spdf(:,2) *cost(2);
	[min_WER, index]=min(cost);
else

	%my second method: faster but less robust
	%find the gaussian closest to each other
	%c=0;
	for c1 =1:size(nbayesS(1).weight,1),
		for c2 =1:size(nbayesS(2).weight,1),
			%c=c+1;
			%out.dist(c) = abs(nbayesS(1).mu(:,c1) - nbayesS(2).mu(:,c2));
			%out.c1(c)=c1;
			%out.c2(c)=c2;
			dist(c1,c2) = abs(nbayesS(1).mu(:,c1) - nbayesS(2).mu(:,c2));
		end;
	end;

	[Y, row] = min(dist);
	[Y, col] = min(Y, [], 2);
	gauss1 = row(col);
	gauss2 = col;

	%[Y,I]=sort(out.dist);
	%gauss1 = out.c1(I(1));
	%gauss2 = out.c2(I(1));

	%dist(gauss1,gauss2) is the smallest pairing
	%or abs(nbayesS(1).mu(:,gauss1) - nbayesS(2).mu(:,gauss2))

	upper = nbayesS(2).weight(gauss2) *  nbayesS(2).mu(gauss2) - nbayesS(1).weight(gauss1) *  nbayesS(1).mu(gauss1) ;
	lower = nbayesS(2).weight(gauss2) *  nbayesS(2).sigma(:,:,gauss2) + nbayesS(1).weight(gauss1) *  nbayesS(1).sigma(gauss1) ;
	min_WER = f_eer( upper /lower);
end;
