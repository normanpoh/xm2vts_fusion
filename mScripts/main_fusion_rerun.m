cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;
load main_baseline_addon_FNorm_rerun baseline
%delete unused scores
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    %clear memory
    list = [1:13];
    for i=list,
      baseline{i}{p,b}.dset ={};
    end;
  end;
end;

%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      %print remarks
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);

      list=[3 6 12]; %Z, F-fix, F-fix-inv
      for i=1:length(list),
	for d=1:2, for k=1:2,
	    nexpe{i}.dset{d,k} = [baseline{list(i)}{p,b(1)}.dset{d,k} baseline{list(i)}{p,b(2)}.dset{d,k}];
	  end;
	end;
      end;

      for i=1:length(list),
	[mean_com{i}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{i}, [1 2], [ 1/2 *ones(1,2)]);
      	[gmm_com{i}{p,s,r}, epc_cost] =  fusion_gmm(nexpe{i}, [1 2], [2 6],[],0);
	[svm_com{i}{p,s,r}, epc_cost] =  fusion_svm(nexpe{i}, [1 2]);
      end;
      
      for i=1:length(list),
	mean_com{i}{p,s,r}.dset={};
	gmm_com{i}{p,s,r}.dset={};
	svm_com{i}{p,s,r}.dset={};
      end;
    end;
  end;
end;

save main_fusion_rerun mean_com gmm_com svm_com

load main_fusion_rerun mean_com gmm_com svm_com epc_cost
for i=1:length(list),
  ncom{i} = mean_com{i};
  ncom{i+3} = svm_com{i};
  ncom{i+6} = gmm_com{i};
end;

load norm_expe.mat com
ncom{10} = com{8};

load main_fusion_allinfo.mat com
ncom{11} = com{8}; %orig,svm-lin
ncom{12} = com{1}; %orig,gmm

clear com;

%overall performance
for i=1:size(ncom,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(ncom{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(ncom{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(ncom{i},[2 3]); %intramodal
end;


expert = {'mean', 'svm', 'gmm'};
norm_m = {'us-Z', 'us-F','us-F-inv','orig'};
c=0;
for j=1:length(expert),
  for i=1:length(norm_m),
    c=c+1;
    leg{c} = [norm_m{i} ',' expert{j}];
end;end;
signs = {'bo-', 'bs--', 'bx-.', 'bd:'};
signs = {signs{:}, 'ro-', 'rs--', 'rx-.', 'rd:'};
signs = {signs{:}, 'go-', 'gs--', 'gx-.', 'gd:'};	 


c=2
b = [b b+4 b+8];

b = [1 2 4];
b=b+4;

figure(1);subplot(1,1,1);set(gca, 'Fontsize', 14);
plot_all_epc(epc_cost,leg,signs, out.cfg{c}, b);

plot_all_roc(epc_cost,leg,signs, out.cfg{c}, b);
