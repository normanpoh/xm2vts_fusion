initialise;
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;


for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('\nExperiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);

    for d=1:2, for k=1:2,
        expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    expe.label = data{p}.label;

    myexpe = expe;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %for MLP output
    if ismlp{p}(b),
      for d=1:2,for k=1:2,
          nexpe{1}.dset{d,k} = expe.dset{d,k};
          ex=1;
          nexpe{1}.dset{d,k}(:,ex) = tanh_inv(expe.dset{d,k}(:,ex));
        end;
      end;
      
      %filter the bad ones
      nexpe{2} = nexpe{1};
      for k=1:2,
        d=1;ex=1;
        selected = find(nexpe{2}.dset{d,k}(:,ex)>30);
        nexpe{2}.dset{d,k}(selected)=[];
        selected = find(nexpe{2}.dset{d,k}(:,ex)<-30);
        nexpe{2}.dset{d,k}(selected)=[];
      end;
      
      param = VR_analysis(nexpe{2}.dset{1,1},nexpe{2}.dset{1,2});      
      
      %now replace bad values with a reasonable one
      nexpe{3} = nexpe{1};
      
      for d=1:2, for k=1:2,
          ex=1;
          selected = find(nexpe{3}.dset{d,k}(:,ex)>30);
          nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_C(ex),size(selected,1),1);
          selected = find(nexpe{3}.dset{d,k}(:,ex)<-30);
          nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_I(ex),size(selected,1),1);
        end;
      end;
    
      myexpe = nexpe{3};  
      myexpe.label = data{p}.label;
    end;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    [nbayesS{p,b}, paramj, param] = cal_clientd_stat(myexpe);
  end;
end;

%derive second-order statistics
tata=[];t=0;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('\nExperiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    t=t+1;
    for d=1:2, %training, test sets
      for k=1:2,
        tata = [nbayesS{p,b}{d}(k).sigma(1,1,:)];
        tata = reshape(tata, length(tata),1);
        out.sigma{d}(k,:) = tata;
        out.mu{d}(k,:) = nbayesS{p,b}{d}(k).mu;
      end;

      %calculate second-level statistics
      tmp=[out.mu{d}' out.sigma{d}'];
      labels = zeros(size(tmp,1),1);
      n_gmm = [1 10];
      FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
      hbayesS{d}{p,b} = gmmb_create(tmp, labels + 1, 'FJ', FJ_params{:});
      tata(t,d) = length(hbayesS{d}{p,b}.weight);
      
      n_gmm = [1];
      FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
      sbayesS{d}{p,b} = gmmb_create(tmp, labels + 1, 'FJ', FJ_params{:});
      
      %find the transformation function using mu�train by stacking them
      %together
      datX = [out.mu{1}(1,:),out.mu{1}(2,:)];
      datY = [out.mu{2}(1,:),out.mu{2}(2,:)];
      transferf{p,b} = polyfit(datX,datY,1);
      datX = [out.sigma{1}(1,:),out.mu{1}(2,:)];

      %y = polyval(coef_mu,xx);
      %plot(xx,y,signs_curve{3});     grid on;
      %xlabel('\mu^k_j|train');    ylabel('\mu^k_j|test');
      
      %warning: don't know how to model sigma on test given sigma on
      %training
      %[f,x]=ksdensity(out.sigma{2}');
    end;
  end;
end;

save main_eer_sensitivity_gmm nbayesS hbayesS sbayesS transferf
