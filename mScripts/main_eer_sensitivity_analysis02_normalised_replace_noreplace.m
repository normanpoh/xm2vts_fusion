load main_eer_sensitivity_normalised out
t_out{1} = out;
load main_eer_sensitivity_normalised_replace out
t_out{2} = out;

initialise;
list{1} = 5:5:45;
list{2} = 5:5:145;
dd = 1;
clear out;
%calculate statistics
for j=1:2, %Z-normalised bootstrap without replacement vs with replacement
  for ds=1:2,
    for p=1:2,
      for b=1:size(dat.P{p}.labels,2)
        myout{ds,j}{p,b} = [];
        %for length(list{ds}) to ds1
        for i=1:length(list{1}), %sample the no.-of-user interval
          tmp = quantil(t_out{j}{ds}{dd}{p}{i}, [2.5 50 97.5])';
          myout{ds,j}{p,b} = [myout{ds,j}{p,b};tmp(b,:)];
        end;
      end;
    end;
  end;
end;

%compare Z-normalised bootstrap without replacement vs with replacement
for ds=1:2,
  figure(ds);
  t=0;
  for p=1:2,
    for b=1:size(dat.P{p}.labels,2),
      t=t+1;
      %figure(p);
      subplot(4,4,t);
      hold off;
      e = myout{ds,1}{p,b}(:,[1,3]) - repmat(myout{ds,1}{p,b}(:,2),1,2);
      errorbar(list{1},myout{ds,1}{p,b}(:,2)*100, e(:,1)*100, e(:,2)*100,'r--','linewidth',2);
      hold on;
      e2 = myout{ds,2}{p,b}(:,[1,3]) - repmat(myout{ds,2}{p,b}(:,2),1,2);
      errorbar(list{1}+1,myout{ds,2}{p,b}(:,2)*100, e2(:,1)*100, e2(:,2)*100)
      hter_abs_range(t,ds,1) = sum(e(:,2)-e(:,1));
      hter_abs_range(t,ds,2) = sum(e2(:,2)-e2(:,1));
      if (t==13),
        xlabel('No. of clients');
        ylabel('HTER(%)');
        legend('Z-Norm, no replacement','Z-Norm, replacement');
      end;
      %txt = sprintf('Exp. %d Protocol %d %s\n',b,p, dat.P{p}.labels{b});
      txt  = sprintf('[%d] %s',t,dat.P{p}.labels{b});
      title(txt);
      axis([0 50 0 20]);
      grid on;
    end;
  end;
end;
for j=1:2,
  figure(ds);
  fname = sprintf('../Pictures/main_eer_sensitivity_emp%02d.eps',j+8);
  print('-depsc2', fname);
end;


%summarised bar plot
tit= {'50 users','150 users'};
figure(3);
for i=1:2,
  subplot(2,1,i);set(gca, 'fontsize', 18);
  bar([hter_abs_range(:,i,1),hter_abs_range(:,i,2)]);
  if i==1,
    set(gca,'xticklabel', cell(13));
    legend('w replace','w/o replace','location','NorthWest');
  end;
  title(['[' num2str(i) '] ' tit{i}]);
end;
xlabel('Experiments');
ylabel('HTER (%)');

fname = sprintf('../Pictures/main_eer_sensitivity_emp%02d.eps',11);
print('-depsc2', fname);
