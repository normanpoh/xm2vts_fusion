initialise;
load main_eer_sensitivity_gmm_bstrp  out;

dat = dlmread('../Data/multi_modal/PI/dev.label', ' ');
model_ID = unique(dat(:,2));
clear dat;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% experimental param
n_user_list = [25 50 100 200 300 600 1000];
n_samples = 500;

signs ={'r--','go-', 'bx-.'};
leg = {'sample', 'big','small'};
p=1;b=2;
for p=1:2,
  for b=2:size(dat.P{p}.labels,2)
    txt = sprintf('\nExperiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);

    for t=1:3, myout{t}{p,b}=[];end;
    for n = 1:length(n_user_list), %sample the no.-of-user interval
      tmp = quantil(out{p,b}{n}.HTER, [2.5 50 97.5])';
      for t=1:3,
        myout{t}{p,b} = [myout{t}{p,b}; tmp(t,:)];
      end;
    end;%user size
    
    %plot them
    for t=1:3,
      e = myout{t}{p,b}(:,[1,3]) - repmat(myout{t}{p,b}(:,2),1,2);
      if t==1, hold off; end;
      errorbar(n_user_list+(t-1),myout{t}{p,b}(:,2)*100, e(:,1)*100, e(:,2)*100,signs{t});
      if t==1, hold on; end;
    end;
    legend(leg);
    
  end; 
end;