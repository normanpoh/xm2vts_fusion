cd /home/learning/norman/xm2vts_fusion/mScripts

load norm_expe_addon_gauss_client.mat com gauss_com mix_com epc_cost

ncom{1} = com{1};
%qdacom
ncom{2} = gauss_com{1};
ncom{3} = mix_com{1};
%lda
ncom{4} = gauss_com{2};
ncom{5} = mix_com{2};

for i=1:size(ncom,2),
  [out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(ncom{i}); %overall
  [out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(ncom{i},1); %multimodal
  [out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(ncom{i},[2 3]); %intramodal
end;

signs = {'g*-','bd-','bs--','rx-.','ro:'};
leg = {'global(gmm)', 'local(qda)', 'mix(qda)','local(lda)', 'mix(lda)'};

list = [1 2 3 4 5]; i=1;
plot_all_epc(epc_cost,leg,signs, out.cfg{i}, list);
print('-depsc2', 'cross_gauss.eps');
plot_all_roc(epc_cost,leg,signs, out.cfg{i}, list);
print('-depsc2', 'gauss_roc.eps');



%figure(2);
%for i=1:3,
%  plot_all_eoc(epc_cost,leg,signs, out.cfg{i}, list);
%  pause;
%end;

out.config = [];
for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    %txt = sprintf('%s for protocol %d\n', remark{s}, p);
    %fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      out.config =[out.config; p s r gauss_com{1}{p,s,r}.reliance];
    end;
  end;
end;
      