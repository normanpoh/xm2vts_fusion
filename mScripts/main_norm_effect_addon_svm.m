%load configurations
%cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;
%load norm_expe.mat com epc_cost
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;
%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
			end;end;

			%zero-mean unit variance normalisation
			nexpe{2} = zmuv_norm(nexpe{1});

			%use margin scores	
			nexpe{3} = convert2margin_scores(nexpe{1});


			%for MLP output
			for d=1:2,for k=1:2,
				nexpe{4}.dset{d,k} = nexpe{1}.dset{d,k};
				for ex=1:2,
					if (mlp{p,s,r}(ex)),
						fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
						nexpe{4}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex));
					end;
				end;
			end;end;

			%second-level fusion
			for d=1:2,for k=1:2,
				new_expe.dset{d,k} = [nexpe{1}.dset{d,k} nexpe{2}.dset{d,k} nexpe{3}.dset{d,k} nexpe{4}.dset{d,k} ];%nexpe{5}.dset{d,k} trans.dset{d,k}];
			end;end;
			new_expe.label = data{p}.label;

			chosen = [1:2];
	            	[com{1}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen);

            		chosen = [3:4];
            		[com{2}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen);

            		chosen = [5:6];
            		[com{3}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen);
            
			chosen = [7:8];
            		[com{4}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen);

			%free the memory
			for i=1:size(com,2),
				com{i}{p,s,r}.dset={};
                		com{i}{p,s,r}.net={};
			end;

		end;
	end;
end;

save norm_expe_addon_svm.mat com epc_cost


load norm_expe_addon_svm.mat com epc_cost

leg = {'orig', 'zmun', 'margin', 'inv'};
signs = {'k', 'r', 'g','b'};

%overall performance
for i=1:size(com,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(com{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(com{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(com{i},[2 3]); %intramodal
end;

list = 1:size(com,2);

plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
