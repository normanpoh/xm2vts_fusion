initialise;

%this will analyse the data
expe.labels = {'(FH,MLP)', '(DCTb,GMM)', '(LFCC,GMM)', '(PAC,GMM)', '(SSC,GMM)'};

chosen{1} = [1 3 6 7 8];
chosen{2} = [1:5];
p=1; %change p=2 to obtain scores for PII
    [dev.wolves, dev.sheep, dev.label.wolves, dev.label.sheep] = load_raw_scores_labels(datafiles{p}.dev,chosen{p});
    [eva.wolves, eva.sheep, eva.label.wolves, eva.label.sheep] = load_raw_scores_labels(datafiles{p}.eva,chosen{p});
    ID = unique(dev.label.sheep); %same as ID = unique(dev.label_wolves);

    %find for each client, how many client accesses it has
    for j =1:size(ID,1),
       dev.count_c(p,j) = size(find(dev.label.sheep==ID(j)),1);
       dev.count_i(p,j) = size(find(dev.label.wolves==ID(j)),1);   
       eva.count_c(p,j) = size(find(eva.label.sheep==ID(j)),1);
       eva.count_i(p,j) = size(find(eva.label.wolves==ID(j)),1);   
    end;
    display('The number of genuine accesses per client on dev set');
    dev.count_c(p,1)
    display('The number of genuine accesses per client on eva set');
    eva.count_c(p,1)
    display('The number of impostor accesses per client on dev set');
    dev.count_i(p,1)
    display('The number of impostor accesses per client on eva set');
    eva.count_i(p,1)

    for j=1:size(ID,1),
        tmp = [dev.sheep(find(dev.label.sheep==ID(j)),:); eva.sheep(find(eva.label.sheep==ID(j)),:)];
        dout{p,j}.sheep = tmp;
        tmp = [dev.wolves(find(dev.label.wolves==ID(j)),:); eva.wolves(find(eva.label.wolves==ID(j)),:)];
        dout{p,j}.wolves = tmp;
    end;

end;
   
data_out.label.wolves = [];
data_out.label.sheep = [];
data_out.wolves = [];
data_out.sheep = [];

for j=1:size(ID,1),
    tmp_data = dout{1,j}.wolves;%[dout{1,j}.wolves; dout{2,j}.wolves];
    data_out.wolves = [data_out.wolves; tmp_data];
    data_out.label.wolves = [data_out.label.wolves; ones(size(tmp_data,1),1) * ID(j)];
    
    tmp_data = dout{1,j}.sheep;%[dout{1,j}.sheep; dout{2,j}.sheep];
    data_out.sheep = [data_out.sheep; tmp_data];
    data_out.label.sheep = [data_out.label.sheep; ones(size(tmp_data,1),1) * ID(j)];    

end;

%On a per client basis, the number of impostor access and client accesses are, after joining
% the dev and eva set together
sum(data_out.label.wolves == ID(1))
sum(data_out.label.sheep == ID(1))

%apply the VR-EER analysis
param = VR_analysis(data_out.wolves, data_out.sheep);
f_eer(f_ratio(param));

combined = [1 3; 1 4; 1 5; 2 3; 2 4; 2 5];
for k=1:size(combined, 1),
    param = VR_analysis(data_out.wolves(:,combined(k,:)), data_out.sheep(:,combined(k,:)));
    %draw dev and eva data
    subplot(2,3,k);
    percentage = 0.1;
    VR_draw(data_out.wolves(:,combined(k,:)), data_out.sheep(:,combined(k,:)), percentage, param)   
end;

