cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

initialise;

for p=1:2,
  [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
  [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

sens = 0.02;
n_weights = 101;
%Start experiment
p=1;s=1;r=4;
for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);
      
      %load score files
      for d=1:2,
	for k=1:2,
	  nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
	end;end;
	
	
	w = cal_weight_margin(nexpe{1},[],n_weights,sens);
	%large margin criterion
	[com{1}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{1}, [1:2],w(1,:));
	com{1}{p,s,r}.weight = w(1,:);
	%EER criterion
	[com{2}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{1}, [1:2],w(2,:));
	com{2}{p,s,r}.weight = w(2,:);

	
	%free the memory
	for i=1:size(com,2),
	  com{i}{p,s,r}.dset={};
	end;

    end;
  end;
end;

save margin_eer com epc_cost;


%overall performance
for i=1:size(com,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(com{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(com{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(com{i},[2 3]); %intramodal
end;
signs={'b','r--'};
leg = {'large margin', 'EER'};
list = 1:size(com,2);
c=1;
plot_all_epc(epc_cost,leg,signs, out.cfg{c}, list);

c=1; b=[2 1];
hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});


	%by cross-validation to find the C values
	ratio = [0.5 0.5];
	clear tmp;            	
	tmp{1} = splitdata(nexpe{1}.dset{1,1}, ratio);
	tmp{2} = splitdata(nexpe{1}.dset{1,2}, ratio);
	tdata.dset{1,1} = tmp{1}{1};
	tdata.dset{1,2} = tmp{2}{1};
	tdata.dset{2,1} = tmp{1}{2};
	tdata.dset{2,2} = tmp{2}{2};
	
	clear tmp out;
	for c=1:size(sens_cx, 2),
	  w = cal_weight_margin(tdata, [], n_weights, sens_cx(c));
	  for d=1:2,for k=1:2,
	      tcom.dset{d,k} = tdata.dset{d,k} * w';
	    end;end;
	    [tmp, thrd] = wer(tcom.dset{1,1},tcom.dset{1,2});
	    out.hter(c) = hter_apriori(tcom.dset{2,1},tdata.dset{2,2}, thrd);
	end;
	[tmp, index] = min(out.hter);
	sens_cx(index)
