function [com, epc_cost] = fusion_method(dev, eva)

n_samples = 11;

%training and testing
[model, com.dev] = train_method(dev.wolves, dev.sheep);
com.eva = test_method(eva.wolves, eva.sheep, model);

%calculate apriori HTER
[tmp, com.thrd] = wer(com.dev.wolves, com.dev.sheep);
[com.hter_apri, com.far_apri, com.frr_apri] = hter_apriori(com.eva.wolves, com.eva.sheep, com.thrd);

fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dev.wolves, com.dev.sheep, com.eva.wolves, com.eva.sheep,n_samples,[0,1]);

%scatter plot
%figure(1);
%percentage = 0.1; %proportion of data to plot
%param = VR_analysis(dev.wolves, dev.sheep);
%VR_draw(dev.wolves, dev.sheep, percentage, param)
%figure(2);
%VR_draw(eva.wolves, eva.sheep, percentage, param)

%EPC curve
%n_samples=10;
%figure(3);
%[epc_dev, epc_eva, cost] = epc(com.dev.wolves, com.dev.sheep, com.eva.wolves, com.eva.sheep,n_samples);
%result.x = cost(:,1);
%plot(cost(:,1),epc_eva.hter_apri);