function [nexpe, model] = sum2one_norm(expe)
%[nexpe model] = zmuv_norm(expe)

for d=1:2,for k=1:2,
	norm_term = repmat(sum(expe.dset{d,k},2), [1 size(expe.dset{d,k},2)] );
	norm_term = norm_term + realmin;
	nexpe.dset{d,k} = expe.dset{d,k} ./ norm_term;
end;end;
