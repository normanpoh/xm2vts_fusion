%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;
%load main_norm_effect2.mat com epc_cost

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, label{p}{1,1}, label{p}{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, label{p}{2,1}, label{p}{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%load gmm: the global information
load norm_expe_addon_gmm_cv.mat com epc_cost

%Start experiment for local information
p=1;s=1;r=1;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
			end;end;

			%zero-mean unit variance normalisation
			nexpe{2} = zmuv_norm(nexpe{1});

			%use margin scores	
			nexpe{3} = convert2margin_scores(nexpe{1});

			%for MLP output
			for d=1:2,for k=1:2,
				nexpe{4}.dset{d,k} = nexpe{1}.dset{d,k};
				for ex=1:2,
					if (mlp{p,s,r}(ex)),
						fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
						nexpe{4}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex));
					end;
				end;
			end;end;

			%second-level fusion
			for d=1:2,for k=1:2,
				new_expe.dset{d,k} = [nexpe{1}.dset{d,k} nexpe{2}.dset{d,k} nexpe{3}.dset{d,k} nexpe{4}.dset{d,k} ];%nexpe{5}.dset{d,k} trans.dset{d,k}];
        new_expe.label{d,k} = label{p}{d,k};
      end;end;

			chosen = [1:2];
			figure(1);
			[com{1}{p,s,r}] = fusion_gauss_client(new_expe, chosen, 1);
			fname = sprintf('../Pictures/gmm_cv_1_%d_%d_%d.eps', p,s,r); print('-depsc2', fname);

			chosen = [3:4];
			[com{2}{p,s,r}] = fusion_gmm_cv(new_expe, chosen,[], 1);
			fname = sprintf('../Pictures/gmm_cv_2_%d_%d_%d.eps', p,s,r); print('-depsc2', fname);

			chosen = [5:6];
			[com{3}{p,s,r}] = fusion_gmm_cv(new_expe, chosen,[], 1);
			fname = sprintf('../Pictures/gmm_cv_3_%d_%d_%d.eps', p,s,r); print('-depsc2', fname);

			chosen = [7:8];
			[com{4}{p,s,r}] = fusion_gmm_cv(new_expe, chosen,[], 1);
			fname = sprintf('../Pictures/gmm_cv_4_%d_%d_%d.eps', p,s,r); print('-depsc2', fname);

			%free the memory
			for i=1:size(com,2),
				com{i}{p,s,r}.dset={};
			end;

		end;
	end;
end;
