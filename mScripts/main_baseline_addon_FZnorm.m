initialise;

%load score files
load baseline baseline;

%for p=1:2,
%  [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
%  [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
%end;

for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('Experiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);

    %load score files
    expe.dset = baseline{3}{p,b}.dset;
    
    [baseline{5}{p,b}, epc_cost] = fusion_baseline_Fratio(expe);
    
  end;
end;

%regenreate
for i=1:size(baseline,2),
	[res_bes{i}, pNI,pNC] = epc_global_baseline(baseline{i});
end;


hter_significant_plot(res_bes{2}, res_bes{5}, pNI,pNC, epc_cost);
subplot(2,1,1); legend('F norm','FZ-Norm');

hter_significant_plot(res_bes{3}, res_bes{5}, pNI,pNC, epc_cost);
subplot(2,1,1); legend('Z norm','FZ-Norm');
    

    
    
