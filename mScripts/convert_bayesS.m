function newbayesS = convert_bayesS(oldbayesS)
for k=1:2,
  newbayesS(k).mu = oldbayesS{k}.mu;
  newbayesS(k).sigma = oldbayesS{k}.sigma;
  newbayesS(k).weight = oldbayesS{k}.weight;
  newbayesS(k).apriories = oldbayesS{k}.apriories;  
end;
