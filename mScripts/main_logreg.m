cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

load  norm_expe_addon_gmm_cv.mat com epc_cost
com_gmm_cv = com;
clear com;

load main_predict_advanced com epc_cost
initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

chosen = [1 2];
for k=1:2,
  raw{k}  = data{p}.dset{1,k}(:,chosen);
end;
k=1;
labels = [zeros(length(raw{1}),1); ones(length(raw{2}),1)];
N = ones(length(labels),1);
dat = [raw{1}; raw{2}];

[bl,dl,sl] = glmfit(dat,[labels N],'binomial');
[bl,dl,sl] = glmfit(dat,[labels],'normal');
[bl,dl,sl] = glmfit(dat,[labels N],'binomial','probit');
[bl,dl,sl] = glmfit([dat dat .^ 2],[labels N],'binomial');
[bl,dl,sl] = glmfit([dat dat .^ 2 dat(:,1) .* dat(:,2)],[labels N],'binomial');

%Radial basis network
%class DEP Gaussian kernel
FJ_params = { 'Cmax', 8, 'Cmin', 1, 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',1};
bayesS = gmmb_create(dat, labels + 1, 'FJ', FJ_params{:});
clear out;
for k=1:2,
  tmp = [];
  for c = 1:length(bayesS),
    for gmm_c = 1:length(bayesS(c).weight),
      out = cmvnpdf(raw{k}, bayesS(c).mu(:,gmm_c)', bayesS(c).sigma(:,:,gmm_c));
      if (c==1),
	tmp = [tmp -log(out+realmin)];
      else
	tmp = [tmp log(out+realmin)];
      end;
    end;
  end;
  output_scores{k} = tmp;
end;
ndat = [output_scores{1}; output_scores{2}];
[bl,dl,sl] = glmfit(ndat,[labels N],'binomial');

%class INDEP Gaussian kernel: not very useful
FJ_params = { 'Cmax', 8, 'Cmin', 1, 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',1};
bayesS = gmmb_create(dat, ones(length(labels),1), 'FJ', FJ_params{:});

clear out;

for k=1:2,
  tmp = [];
  for c = 1:length(bayesS),
    for gmm_c = 1:length(bayesS(c).weight),
      out = cmvnpdf(raw{k}, bayesS(c).mu(:,gmm_c)', bayesS(c).sigma(:,:,gmm_c));
      if (c==1),
	tmp = [tmp -log(out+realmin)];
      else
	tmp = [tmp log(out+realmin)];
      end;
    end;
  end;
  output_scores{k} = tmp;
end;
ndat = [output_scores{1}; output_scores{2}];
[bl,dl,sl] = glmfit(ndat,[labels N],'binomial');

%3d scatter point plotting
yfit{1} = glmval(bl,raw{1},'logit');
yfit{2} = glmval(bl,raw{2},'logit');
hold off;
plot3(raw{1}(:,1), raw{1}(:,2), yfit{1},'ro'); hold on;
plot3(raw{2}(:,1), raw{2}(:,2), yfit{2},'b+');
xlabel('Expert 1');
ylabel('Expert 2');
zlabel('Prob');


%surface fitting
[xtesta1,xtesta2]=meshgrid( ...
    linspace(min(dat(:,1)), max(dat(:,1)), 100), ...
    linspace(min(dat(:,2)), max(dat(:,2)), 100) );
[na,nb]=size(xtesta1);
xtest1=reshape(xtesta1,1,na*nb);
xtest2=reshape(xtesta2,1,na*nb);
xtest=[xtest1;xtest2]';

ypred = glmval(bl,xtest,'logit');
ypred = glmval(bl,xtest,'identity');
ypred = glmval(bl,xtest,'probit');
ypred = glmval(bl,[xtest xtest.^2 ],'logit');
ypred = glmval(bl,[xtest  xtest.^2  xtest(:,1) .* xtest(:,2) ],'logit');

%class dependent Gaussian kernel
tmp = [];
for c = 1:length(bayesS),
  for gmm_c = 1:length(bayesS(c).weight),
    out = cmvnpdf(xtest, bayesS(c).mu(:,gmm_c)', bayesS(c).sigma(:,:,gmm_c));
    if (c==1),
      tmp = [tmp -log(out+realmin)];
    else
      tmp = [tmp log(out+realmin)];
    end;
  end;
end;
ypred = glmval(bl,tmp,'logit');

%class INDEP Gaussian kernel

ypredmat=reshape(ypred,na,nb);
figure;hold off
contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
hold on
[cs,h]=contour(xtesta1,xtesta2,ypredmat,[0 0.5 1],'k');
clabel(cs,h);
plot(raw{1}(:,1), raw{1}(:,2), 'ro');
plot(raw{2}(:,1), raw{2}(:,2), 'b+');
colorbar;


%binary division
for k=1:2,
  raw{k}  = data{p}.dset{1,k}(:,chosen);
  [dataq{k}, queue{k}, bayesS(k)]=  reduce_data(raw{k});
end;

draw_empiric(dataq{1}, dataq{2});

draw_empiric(raw{1}, raw{2});

clear nbayesS;
dim = 2;
for k=1:2,
  t=0;
  for i=1:size(bayesS(k).weight,1),
    if (rank(bayesS(k).sigma(:,:,i)) < dim)
      length(queue{k}{i})
      %i
      %bayesS(k).sigma(1,2,i) = 0;
      %bayesS(k).sigma(2,1,i) = 0;
    else
      t=t+1;
      nbayesS(k).mu(:,t) = bayesS(k).mu(:,i);
      nbayesS(k).sigma(:,:,t) = bayesS(k).sigma(:,:,i);
      nbayesS(k).weight(t,:) = bayesS(k).weight(i);
    end;
  end;
end;

bayesS = nbayesS;

draw_theory_bayesS(nbayesS)