%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts

%cd c:/cygwin/home/Norman/learning/xm2vts_fusion/mScripts/
load norm_expe_addon_gauss_client.mat com gauss_com mix_com epc_cost
initialise;

%do need to run the following code
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%convert the wrongly saved gmm
for p=1, %over two protocols

  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      com{1}{p,s,r}.bayesS = convert_bayesS(com{1}{p,s,r}.bayesS);
    end;
  end;
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%load main_norm_effect2.mat com epc_cost
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, label{p}{1,1}, label{p}{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, label{p}{2,1}, label{p}{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      
      %print remarks
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);
      
      %load score files
      for d=1:2,for k=1:2,
	  nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
	end;
      end;
	
      %zero-mean unit variance normalisation
      nexpe{2} = zmuv_norm(nexpe{1});
      
      %use margin scores	
      nexpe{3} = convert2margin_scores(nexpe{1});
      
      %for MLP output
      for d=1:2,for k=1:2,
	  nexpe{4}.dset{d,k} = nexpe{1}.dset{d,k};
	  for ex=1:2,
	    if (mlp{p,s,r}(ex)),
              fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
              nexpe{4}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex));
            end;
          end;
        end;
      end;
      %draw_empiric(nexpe{4}.dset{1,1},nexpe{4}.dset{1,2});
      
      %prepare filtered data
      nexpe{5} = nexpe{4};
      for d=1:2,
        for k=1:2,
          for ex=1:2,
            if (mlp{p,s,r}(ex)),
              selected = find(nexpe{5}.dset{d,k}(:,ex)>30);
              nexpe{5}.dset{d,k}(selected,:)=[];
              selected = find(nexpe{5}.dset{d,k}(:,ex)<-30);
              nexpe{5}.dset{d,k}(selected,:)=[];
            end;
          end;
        end;
      end;
      %draw_empiric(nexpe{5}.dset{1,1},nexpe{5}.dset{1,2});
      param = VR_analysis(nexpe{5}.dset{1,1},nexpe{5}.dset{1,2});
      
      %now replace bad values with a reasonable one
      nexpe{6} = nexpe{4};
      for d=1:2,
        for k=1:2,
          for ex=1:2,
            if (mlp{p,s,r}(ex)),
              selected = find(nexpe{6}.dset{d,k}(:,ex)>30);
              nexpe{6}.dset{d,k}(selected,ex)= repmat(param.mu_C(ex),size(selected,1),1);
              selected = find(nexpe{6}.dset{d,k}(:,ex)<-30);
              nexpe{6}.dset{d,k}(selected,ex)= repmat(param.mu_I(ex),size(selected,1),1);
            end;
          end;
        end;
      end;
      %draw_empiric(nexpe{6}.dset{1,1},nexpe{6}.dset{1,2});
      %param2 = VR_analysis(nexpe{5}.dset{1,1},nexpe{5}.dset{1,2});
      
      %second-level fusion
      for d=1:2,for k=1:2,
          new_expe.dset{d,k} = [nexpe{1}.dset{d,k} nexpe{2}.dset{d,k} nexpe{3}.dset{d,k} nexpe{4}.dset{d,k} nexpe{6}.dset{d,k}];
          new_expe.label{d,k} = label{p}{d,k};  
        end;
      end;

      chosen = [1:2];
      [com{1}{p,s,r}] = fusion_gmm(new_expe, chosen, [], com{1}{p,s,r}.bayesS,0);
      
      %cross validation for fusion_gauss_client
      chosen = [9 10];
      rel = linspace(0,1,11);
      for r0 = 1:length(rel),
        %[gcom{r0}] = fusion_gauss_client(new_expe, chosen, param, rel(r0), 'ca(mu)-qda', 0);
	[gcom{r0}] = fusion_gauss_client(new_expe, chosen, param, rel(r0), 'ca(mu)-lda', 0);
      end;
      
      %test the predictability
      out.dev=[]; out.eva = [];
      for r0 = 1:length(rel),
	out.dev = [out.dev gcom{r0}.epc.dev.hter(6)];
	out.eva = [out.eva gcom{r0}.epc.eva.hter_apri(6)];
      end;
      %show the predictability between dev and eva set
      %meaning that there is no overfitting
      %plot(out.dev, out.eva,'+');
      %show the optimisation  performance w.r.t the reliance param
      %plot(rel,out.dev);
      
      
      [tmp, index] = min(out.dev); 
      %gauss_com{1}{p,s,r} = gcom{index};
      gauss_com{2}{p,s,r} = gcom{index};
      
      for d=1:2,for k=1:2,
          %texpe.dset{d,k} = [com{1}{p,s,r}.dset{d,k}, gauss_com{1}{p,s,r}.dset{d,k}];
	  texpe.dset{d,k} = [com{1}{p,s,r}.dset{d,k}, gauss_com{2}{p,s,r}.dset{d,k}];
	end;
      end;
      %mix_com{1}{p,s,r} = fusion_svm(texpe);
      mix_com{2}{p,s,r} = fusion_svm(texpe);
      
      %show the possible gain (or loss due to the combination of
      %both global and local into
      m=2;
      hold off;
      plot(epc_cost(:,1), com{1}{p,s,r}.epc.eva.hter_apri*100,'b+-'); hold on;
      plot(epc_cost(:,1), gauss_com{m}{p,s,r}.epc.eva.hter_apri*100,'ro-');
      plot(epc_cost(:,1), mix_com{m}{p,s,r}.epc.eva.hter_apri*100,'gs-');
      legend('global', 'local', 'mix');
      
      com{1}{p,s,r}.dset ={};
      gauss_com{m}{p,s,r}.dset ={};
      mix_com{m}{p,s,r}.dset ={};
    end;
  end;
end;

save norm_expe_addon_gauss_client.mat com gauss_com mix_com epc_cost

tata = []
for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      tata = [tata; p,s, r, gauss_com{2}{p,s,r}.reliance];
    end;
  end;
end;
%figure;hist(tata(:,4),8);



      %draw_empiric(texpe.dset{1,1},texpe.dset{1,2});
      %figure;draw_empiric(texpe.dset{2,1},texpe.dset{2,2});
      [weight] = find_weight(texpe, 'brute');
      [mix_com{2}{p,s,r}] = fusion_wsum_nonorm(texpe, chosen, weight)

      
      [local_com{1}{p,s,r}] = fusion_gauss_client(new_expe, chosen, param);

      %perform xvalid for the client
      out = create_xvalid_client(new_expe);
      gmm_cv_list = linspace(0,2,10);
      for g=1:size(gmm_cv_list,2),
        for i=1:length(out),
          fprintf(1, '%d.', gmm_cv_list(g));
          %training on dset=1
          data = out{i}.train;
          labels = ones(size(out{i}.train,1),1);
          
          FJ_params = { 'Cmax', gmm_cv_list(g), 'Cmin', gmm_cv_list(g), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
          bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});
          
          %only a single class here!
          c=1;llh_list = gmmb_pdf(out{i}.test, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
          llh(g,i) = mean(log(llh_list+realmin));
        end;
      end;


      [local_com{1}{p,s,r}] = fusion_gauss_client_parzen(new_expe, chosen);
      for d=1:2,for k=1:2,
          texpe.dset{d,k} = [com{1}{p,s,r}.dset{d,k}, local_com{1}{p,s,r}.dset{d,k}];
        end;
      end;
      mix_com{1}{p,s,r} = fusion_svm(texpe);

			chosen = [7:8];
      bayesS = convert_bayesS(com{4}{p,s,r}.bayesS);
			[com{4}{p,s,r}] = fusion_gmm(new_expe, chosen, [], bayesS,0);
      [local_com{2}{p,s,r}] = fusion_gauss_client_parzen(new_expe, chosen);
      for d=1:2,for k=1:2,
          texpe.dset{d,k} = [com{4}{p,s,r}.dset{d,k}, local_com{2}{p,s,r}.dset{d,k}];
        end;
      end;
      mix_com{2}{p,s,r} = fusion_svm(texpe);
      mix_com{2}{p,s,r} = fusion_(texpe);
      %draw_empiric(texpe.dset{1,1},texpe.dset{1,2});
      %combine both
      %com{9}{p,s,r} = fusion_gmm(texpe, [], [2 6], [],1)
      %draw_empiric(new_expe.dset{1,1}(:,chosen), new_expe.dset{1,2}(:,chosen));
      
      chosen = [3:4];
      [com{6}{p,s,r}] = fusion_gauss_client(new_expe, chosen,[], 1);
      
      chosen = [5:6];
      [com{7}{p,s,r}] = fusion_gauss_client(new_expe, chosen,[], 1);
      fname = sprintf('../Pictures/gmm_cv_3_%d_%d_%d.eps', p,s,r); print('-depsc2', fname);

      chosen = [7:8];
      [com{8}{p,s,r}] = fusion_gauss_client(new_expe, chosen, 1);
      draw_empiric(new_expe.dset{1,1}(:,7:8), new_expe.dset{1,2}(:,7:8));
      fname = sprintf('../Pictures/gmm_cv_4_%d_%d_%d.eps', p,s,r); print('-depsc2', fname);
      
      %free the memory
      for i=1:size(com,2),
	com{i}{p,s,r}.dset={};
      end;
      
%		end;
%	end;
%end;

save norm_expe_addon_gmm_cv.mat com epc_cost

%overall performance
for i=1:size(com,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(com{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(com{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(com{i},[2 3]); %intramodal
end;

signs = {'kx-',  'kd-','ko-','k*-'};
leg = {'orig,gmm-cv', ,zmun,gmm-cv', 'margin,gmm-cv', inv,gmm-cv'};

%answer the following questions:
%1. How normalisation can affect classifier performance?
c=1;
%c=2;
%c=3;

list = 1:size(com,2);

%list = [1 2 4 5 6 8 9 10 12 13 14 16 17];

plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);

%sort by model performs
list = [1 8 15 22]; 
i=0;
while i <=6,
	plot_all_epc(epc_cost,leg,signs, out.cfg{1}, [list 29 30]);
	pause;
	list = list + 1; i=i+1;
	print('-depsc2', ['../Pictures/norm_by_model', num2str(i),'.eps']);
end;

%sort by normalisation methods
list = [1:2 4:7];
i=0;
while i <=3,
	plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
	pause;	
	i=i+1;	list = list + 7;
	print('-depsc2', ['../Pictures/norm_by_norm', num2str(i),'.eps']);
end;

%choose the best of each method: gmm
figure(2);
list = [4 11 18 25 29 30];
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);

%plot with significant test
b = [20,27];
b=[16 23];
b=[16 30];
b=[4 25];
b=[29 30];
hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

print('-depsc2', '../Pictures/orig_margin_only_mean_pooled.eps');

%to prope further, what is 
%2. how many components are there?
for p=1:2, %over two protocols
	out.gmm_c{p}=[];
	for s=1:3, %over 3 different configurations
	for r=1:size(expe.P{p}.seq{s},1),
		out.gmm_c{p} = [out.gmm_c{p} size(com{10}{p,s,r}.bayesS(1).weight,1)];
	end;end;
end;


%plot of scores
[nexpe{1}.dset{1,1}, nexpe{1}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
[nexpe{1}.dset{2,1}, nexpe{1}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);

p=1;s=1;r=10;
for i=1:4,
	figure(i); set(gca, 'Fontsize', 14);
	draw_empiric(nexpe{i}.dset{1,1},nexpe{i}.dset{1,2});
	xlabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,1)});
	ylabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,2)});
	pause;
	fname = sprintf('../Pictures/norm_scatter_%d.eps', i);
	print('-depsc2', fname);
end;

tmp1=nexpe{1}.dset{1,1}(:,1);
tmp2=nexpe{1}.dset{1,2}(:,1);
[tran.x{i}, tran.margin{i}] = cal_margin_scores(tmp1,tmp2,[0.5 0.5],1);
print('-depsc2', '../Pictures/demo_margin_scores.eps');


%margin scatter plot
i=4;
figure(i); hold off; set(gca, 'Fontsize', 14); 
draw_empiric(nexpe{i}.dset{1,1},nexpe{i}.dset{1,2});
xlabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,1)});
ylabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,2)});
