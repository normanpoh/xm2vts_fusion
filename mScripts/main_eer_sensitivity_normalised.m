%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;
%load main_norm_effect_addon_wsum_user.mat com epc_cost

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%load normalised scores by Z-Norm and F-Norm
load main_baseline_addon_FNorm_rerun baseline
for p=1:2,
  bline{p}.label = data{p}.label;
  for b=1:size(dat.P{p}.labels,2)
    bdat{1}{p,b}.dset = baseline{3}{p,b}.dset; %Z-Norm
    bdat{2}{p,b}.dset = baseline{6}{p,b}.dset;  %F-Norm
  end;
end;
clear data baseline;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
list{1} = 5:5:45;
list{2} = 5:5:145;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%reuse expe_id created in main_eer_sensitivity.m
load main_eer_sensitivity_draws new_expe_id rnd_list
%load main_eer_sensitivity out

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN expe here
%ds=1; %50 client config

n_super_draws = 1;
n_draws = 100;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;
for ds=1:2, %150 client config
  fprintf(1,'For dset %d\n', ds);
  for p=1:2,
    fprintf(1,'Protocol %d\n', p);
    for dd=1:n_super_draws,
      fprintf(1,'�');

      for d=1:n_draws,
        fprintf(1,'.%d',d);

        for b=1:size(dat.P{p}.labels,2)
          expe.dset = bdat{1}{p,b}.dset; %use Z-Norm
          expe.label = bline{p}.label;
          for i=1:length(list{1}), %force to be the list ds1
            %for i=1:length(list{ds}), %sample the no.-of-user interval
            rexpe = rndchoose_client_list(expe, [], rnd_list{ds}{d}{i}, new_expe_id{ds}{dd}{p}.selected(d,:));

            [tmp_,thrd] = wer(rexpe.dset{1,1}(:,1), rexpe.dset{1,2}(:,1));
            [com.hter_apri, com.far_apri, com.frr_apri] = hter_apriori(rexpe.dset{2,1}(:,1), rexpe.dset{2,2}(:,1),thrd);
            out{ds}{dd}{p}{i}(d,b) = com.hter_apri;
          end;
        end;

      end;
    end;
  end;
end;
time_taken = toc;
save main_eer_sensitivity_normalised out time_taken
%calculate statistics
for ds=1:2,
  for p=1:2,
    for b=1:size(dat.P{p}.labels,2)
      myout{ds}{p,b} = [];
      %for length(list{ds}) to ds1
      for i=1:length(list{1}), %sample the no.-of-user interval
        tmp = quantil(out{ds}{dd}{p}{i}, [2.5 50 97.5])';
        myout{ds}{p,b} = [myout{ds}{p,b};tmp(b,:)];
      end;
    end;
  end;
end;

clear tmp;
p=1;b=1;
t=0;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2), 
    t=t+1;
    %figure(p);
    subplot(4,4,t);
    hold off;
    e = myout{2}{p,b}(:,[1,3]) - repmat(myout{2}{p,b}(:,2),1,2);
    errorbar(list{1},myout{2}{p,b}(:,2)*100, e(:,1)*100, e(:,2)*100,'r--','linewidth',2);
    hold on;
    e = myout{1}{p,b}(:,[1,3]) - repmat(myout{1}{p,b}(:,2),1,2);
    errorbar(list{1},myout{1}{p,b}(:,2)*100, e(:,1)*100, e(:,2)*100)
    %bar(list{ds},(e(:,2)-e(:,1))*10)
    if (t==13),
      xlabel('No. of clients');
      ylabel('HTER(%)');
    end;
    %txt = sprintf('Exp. %d Protocol %d %s\n',b,p, dat.P{p}.labels{b});
    txt  = sprintf('[%d] %s',t,dat.P{p}.labels{b});
    title(txt);
    axis([0 50 0 20]);
    grid on;
  end;
end;
print('-depsc2', '../Pictures/main_eer_sensitivity_emp02_norm.eps');


