p=1;s=1;r=1;

%change the following line of fusion_rbf.m:
FJ_params = { 'Cmax', 5, 'Cmin', 2, 'thr', 1e-2, 'animate', 1, 'verbose', 1, 'covtype',0};

%orig
chosen = [1:2];
[com{3}{p,s,r}, com{4}{p,s,r}, llh] = fusion_rbf(new_expe, chosen, 'nonorm');

%inversion
chosen = [7:8];
[com{3}{p,s,r}, com{4}{p,s,r}, llh] = fusion_rbf(new_expe, chosen, 'nonorm');

chosen = [5:6];
[com{3}{p,s,r}, com{4}{p,s,r}, llh] = fusion_rbf(new_expe, chosen, 'nonorm');

for i=1:6,
	figure(i);
	print('-depsc2', ['../Pictures/norm_gmm', num2str(i),'.eps']);
end;