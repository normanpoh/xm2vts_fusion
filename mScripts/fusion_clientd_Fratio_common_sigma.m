function [out] = fusion_clientd_Fratio_common_sigma(expe)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

%zero-mean unit variance normalisation
model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
model.std = std([expe.dset{1,1}; expe.dset{1,2}]);
[expe.dset{1,1}, expe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
[expe.dset{2,1}, expe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);

%get the model label
model_ID = unique(expe.label{1,1});

%go through each id
for d=1:2,for k=1:2,
	out.dset{d,k} = [];
end;end;

%calculate client-independent sigma.
[tmp.dev, tmp.eva, global_param] = VR_Fnorm(expe.dset{1,1}, expe.dset{1,2});
%see VR_Fnorman and Fratio_norm

%perform client-dependent normalisation
for id=1:size(model_ID,1),

	%get the data set associated to the ID
	for d=1:2,for k=1:2,
	    tmp.dset{d,k} = expe.dset{d,k}(find(expe.label{d,k} == model_ID(id)),:);
	end;end;
    
	%Apply F-ratio normalisation on training and test sets, in an ID-dependnent manner
	[tmp.dev, tmp.eva, clientd{id}.param] = VR_Fnorm(tmp.dset{1,1}, tmp.dset{1,2});

	%change only the sigma_all; mu_C and mu_I remain the same
	clientd{id}.param{2}.sigma_all = global_param{2}.sigma_all;

	[clientd{id}.dset{1,1}, clientd{id}.dset{1,2}, clientd{id}.param] = VR_Fnorm(tmp.dset{1,1}, tmp.dset{1,2}, clientd{id}.param);
    	[clientd{id}.dset{2,1}, clientd{id}.dset{2,2}] = VR_Fnorm(tmp.dset{2,1}, tmp.dset{2,2}, clientd{id}.param);

	%compute the fused scores
	for d=1:2,for k=1:2,
	    out.dset{d,k} = [out.dset{d,k}; clientd{id}.dset{d,k}];
	end;end;

end;