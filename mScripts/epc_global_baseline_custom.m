function [res,pNI,pNC] = epc_global_baseline_custom(res_com, pb_list, NI, NC)

out.fa = [];
out.fr = [];
count = 0;
for i=1:size(pb_list,1),
  count = count + 1;
  out.fa = [out.fa; res_com{pb_list(i,1),pb_list(i,2)}.epc.eva.far_apri * NI];
  out.fr = [out.fr; res_com{pb_list(i,1),pb_list(i,2)}.epc.eva.frr_apri * NC];
end;
out.NC = count * NC;
out.NI = count * NI;

res.eva.far_apri = sum(out.fa) / out.NI;
res.eva.frr_apri = sum(out.fr) / out.NC;
res.eva.hter_apri = (res.eva.far_apri+res.eva.frr_apri)/2;

pNC = out.NC;
pNI = out.NI;