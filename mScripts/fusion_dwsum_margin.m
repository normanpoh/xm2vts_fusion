function [com, raw, tran, epc_cost] = fusion_dwsum_margin(expe, chosen, nonorm)
% tran is the transition function from score to margin
% raw is the transformed score with margin M(y_i) * y_i for each i
% com is the fused score with margin \sum_i M(y_i) * y_i

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if(nargin < 2),
	chosen = [1 2];
end;

%zero-mean unit variance normalisation
if (nargin < 3),
	model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
	model.std = std([expe.dset{1,1}; expe.dset{1,2}]);
	[expe.dset{1,1}, expe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
	[expe.dset{2,1}, expe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);
end;

%calculate the margin on the dev set
for i=1:size(chosen,2),
	[tran.x{i}, tran.margin{i}] = cal_margin(expe.dset{1,1}(:,i), expe.dset{1,2}(:,i)); %expert i
end;

%transform the margin
for d=1:2,for k=1:2,
	for i=1:size(chosen,2),
 	  tran.dset{d,k}(:,i) = interp1(tran.x{i},tran.margin{i}, expe.dset{d,k}(:,i), 'nearest'); %expert i
	end;
end;end

%get the dynamic weight and apply them to the scores
for d=1:2,
for k=1:2,
	%for 1 dimension, don't normalise!
	if (size(chosen,2) == 1),
	  tmp.weights = tran.dset{d,k};
	else
  	  tmp.weights = tran.dset{d,k} ./ repmat(sum(tran.dset{d,k},2),1,size(chosen,2));
	end;
	raw.dset{d,k} = expe.dset{d,k}(:,chosen) .* tmp.weights;
	com.dset{d,k}= sum(raw.dset{d,k},2);
end;end;

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);
