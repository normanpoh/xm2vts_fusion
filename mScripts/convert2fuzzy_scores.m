function [com] = convert2fuzzy_scores(expe)
% com is the fuzzy score with I_1 C_1 I_2 C_2 and so on

n_samples_fusion = size(expe.dset{1,1},2);
%calculate the margin on the dev set
for i=1:n_samples_fusion,
	tmp1=expe.dset{1,1}(:,i);
	tmp2=expe.dset{1,2}(:,i);
	[tran.x{i}, tran.margin_I{i}, tran.margin_C{i}] = cal_fuzzy_scores(tmp1,tmp2);

	%hold off;
	%plot(tran.x{i}, tran.margin_I{i}); hold on;
	%plot(tran.x{i}, tran.margin_C{i}, 'r');
	%pause;
end;

%transform the margin
for d=1:2,for k=1:2,
	com.dset{d,k} = zeros(size(expe.dset{d,k},1), n_samples_fusion * 2);
	for i=1:n_samples_fusion,
 	  com.dset{d,k}(:,i) = interp1(tran.x{i},tran.margin_I{i}, expe.dset{d,k}(:,i), 'nearest'); %expert i
 	  com.dset{d,k}(:,n_samples_fusion+i) = interp1(tran.x{i},tran.margin_C{i}, expe.dset{d,k}(:,i), 'nearest'); %expert i
	end;
end;end

