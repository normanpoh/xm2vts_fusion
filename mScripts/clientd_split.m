function [nexpe] = clientd_split(expe, chosen)

if nargin < 2,
  chosen = 1;
end;
%go through each id
for d=1:2,for k=1:2,
    nexpe.dset{d,k} = [];
    nexpe.label{d,k} = [];
  end;
end;

model_ID = unique(expe.label{1,1});

%perform client-dependent normalisation
for id=1:size(model_ID,1),
  
  %get the data set associated to the ID
  for d=1:1,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      tmp{d,k} = expe.dset{d,k}(index{d,k},chosen);
      out = splitdata(tmp{d,k}, [1 1]);
      nexpe.dset{1,k} = [nexpe.dset{1,k}; out{1}];
      nexpe.dset{2,k} = [nexpe.dset{2,k}; out{2}];
      nexpe.label{1,k} = [nexpe.label{1,k}; ones(size(out{1},1),1)*model_ID(id)];
      nexpe.label{2,k} = [nexpe.label{2,k}; ones(size(out{2},1),1)*model_ID(id)];  
    end;
  end;
end;
  