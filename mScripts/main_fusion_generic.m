%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;
load baseline;
load generic;

%Start experiment
p=1;s=1;r=4;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			[nexpe{1}.dset{1,1}, nexpe{1}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev, b);
			[nexpe{1}.dset{2,1}, nexpe{1}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva, b);

			%perform F-ratio normalisation
			for d=1:2,for k=1:2,
				nexpe{2}.dset{d,k} = [baseline{2}{p,b(1)}.dset{d,k} , baseline{2}{p,b(2)}.dset{d,k} ];
			end;end;

			%Perform Z-Norm
			for d=1:2,for k=1:2,
				nexpe{3}.dset{d,k} = [baseline{3}{p,b(1)}.dset{d,k} , baseline{3}{p,b(2)}.dset{d,k} ];
			end;end;

			%use margin scores
			[com{1}{p,s,r}, nexpe{4}, trans, epc_cost] = fusion_dwsum_margin(nexpe{1});

			%use sorted margin
			%nexpe{5} = fusion_sorted(nexpe{4});

			%second-level fusion
			for d=1:2,for k=1:2,
				new_expe.dset{d,k} = [nexpe{1}.dset{d,k} nexpe{2}.dset{d,k} nexpe{3}.dset{d,k} nexpe{4}.dset{d,k} ];%nexpe{5}.dset{d,k} trans.dset{d,k}];
			end;end;


			%[com{2}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [1:8],[ 1/8 *ones(1,8)]);
			%[com{3}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [1:8]);
			%[com{4}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [1:4], [ 1/4 *ones(1,4)]);
			%[com{5}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [1:4]);	
			%[com{6}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [1 2 4 6], [ 1/4 *ones(1,4)]);
			%[com{7}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [1 2 4 6]);	
			[com{8}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [1 2], [ 1/2 *ones(1,2)]);
			%[com{9}{p,s,r}, epc_cost] = fusion_wsum_brute_nonorm(new_expe, [1 2]);	
			%[com{10}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [3 4], [ 1/2 *ones(1,2)]);
			%[com{11}{p,s,r}, epc_cost] = fusion_wsum_brute_nonorm(new_expe, [3 4]);	
			%[com{12}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [5 6], [ 1/2 *ones(1,2)]);
			%[com{13}{p,s,r}, epc_cost] = fusion_wsum_brute_nonorm(new_expe, [5 6]);	
			%[com{14}{p,s,r}, epc_cost] = fusion_wsum_brute_nonorm(new_expe, [7 8]);	

			%[com{15}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [1:4], [ 1/4 *ones(1,4)]);
			%[com{16}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [1:4]);	
			%[com{17}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [1 2 4 6], [ 1/4 *ones(1,4)]);
			%[com{18}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [1 2 4 6]);	
			%[com{19}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [1:8],[ 1/8 *ones(1,8)]);
			%[com{20}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [1:8]);
			%[com{21}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [1 2 7 8],[ 1/4 *ones(1,4)]);
			%[com{22}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [1 2 7 8]);
			%[com{23}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [1 2 7 8],[ 1/4 *ones(1,4)]);
			%[com{24}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [1 2 7 8]);

			%[com{25}{p,s,r}, nexpe{5}, trans, epc_cost] = fusion_dwsum_margin(new_expe, [3 4],1); %nonorm

			%[com{26}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [1 2], [ 1/2 *ones(1,2)]);
			%[com{27}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [3 4], [ 1/2 *ones(1,2)]);
			%[com{28}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [5 6], [ 1/2 *ones(1,2)]);
			[com{29}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [9 10], weight_apriori);
			[com{30}{p,s,r}, epc_cost] = fusion_wsum(new_expe, [9 10]);
			[com{31}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [9 10], weight_apriori);
			[com{32}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [9 10]);
			[com{32}{p,s,r}, epc_cost] = fusion_wsum_nonorm(new_expe, [9 10]);

			%free the memory
			for i=1:size(com,2),
				%com{i}{p,s,r}.dset={};
			end;

		end;
	end;
end;


save generic com epc_cost

load generic com epc_cost

for i=1:size(com,2),
	[res{i},pNI,pNC] = epc_global(com{i});
	[cfg.res{i},pNI,pNC] = epc_global(com{i});
end;


subplot(1,1,1);
hold off;
signs = {'kx-', 'go-','go--', 'ro-','ro--','bo-','bo--'};
signs = {signs{:}, 'gx-','gx--', 'rx-','rx--','bx-','bx--'};
signs = {signs{:},'kx--',  'rd-','rd--','bd-','bd--'};
signs = {signs{:},'gd-',  'gd--', 'ko-', 'ko--', 'kd-', 'kd--'};
signs = {signs{:},'yx-','gs-', 'rs-', 'bs-'};
signs = {signs{:},'b*-', 'b*--', 'm'};

leg = {'margin[only],mean', 'all,mean','all,wsum', 'F-Norm+orig,mean','F-Norm+orig,wsum' 'Z-Norm+orig,mean','Z-Norm+orig,wsum'};
leg = {leg{:}, 'orig,mean', 'orig,wsum','F-Norm,mean', 'F-Norm,wsum','Z-Norm,mean', 'Z-Norm,wsum'};
leg = {leg{:}, 'margin[only],wsum',  'F-Norm,mean(norm)','F-Norm,wsum(norm)', 'Z-Norm,mean(norm)','Z-Norm,wsum(norm)'};
leg = {leg{:}, 'all,mean(norm)','all,wsum(norm)', 'margin,mean', 'margin,wsum', 'margin,mean(norm)', 'margin,wsum(norm)'};
leg = {leg{:}, 'F-Norm[only],margin','orig,mean(norm)', 'F-Norm,mean(norm)', 'Z-Norm,mean(norm)'};
leg = {leg{:}, 'margin-sorted,apriori','margin-sorted,wsum','ctr:margin[only]+orig,wsum'};

subplot('position',[0.04 0.05 0.7 0.93])
for i=1:size(com,2),
  if (i==1), hold off; end;
    plot(epc_cost(:,1), res{i}.eva.hter_apri*100,signs{i}); 
  if (i==1), hold on; end;
end;
legend(leg);
ylabel('HTER(%)');xlabel('\alpha');

axis_tmp =axis;
axis_tmp([3,4])=[1, 3.5];axis(axis_tmp);

print('-depsc2', '../Pictures/generic_pooled4.eps');


%plot with margin
b = [8,1];
hter_significant_plot(res{b(1)}, res{b(2)}, pNI,pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});
print('-depsc2', '../Pictures/orig_margin_only_mean_pooled.eps');

b = [9,14];
hter_significant_plot(res{b(1)}, res{b(2)}, pNI,pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});
print('-depsc2', '../Pictures/orig_margin_only_wsum_pooled.eps');

hter_significant_plot(com{8}{p,s,r}.epc, com{21}{p,s,r}.epc, NI,NC, epc_cost);
subplot(2,1,1);legend(leg{8}, leg{21});

b = [1, 21];
hter_significant_plot(res{b(1)}, res{b(2)}, pNI,pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});
print('-depsc2', '../Pictures/orig_margin_only_wsum_pooled.eps');

b = [1 14 8 9 21 22];
figure(1);subplot(1,1,1);set(gca, 'Fontsize', 14);
plot_all_epc(epc_cost,leg,signs, cfg, b);

b = [1 8  21];% 31];
signs{8} = 'd--';
signs{21} = 'ro-.';
myleg={'margin', 'orig','margin+orig'};
cfg.res = res;
plot_all_epc(epc_cost(:,1),leg, signs, cfg, b)
grid on;axis tight;
legend(myleg);
print('-depsc2', '../Pictures/orig+margin_mean_pooled.eps');

figure(2);
plot_all_roc(epc_cost(:,1),leg, signs, cfg, b)
legend(myleg);
print('-depsc2', '../Pictures/orig+margin_mean_pooled_roc.eps');

b=[8 21];
leg_text = {leg{b}};
epc_compare_print(com{b(1)}, com{b(2)}, epc_cost, '',leg_text)

hter_significant_plot(com{b(1)}{p,s,r}.epc, com{b(2)}{p,s,r}.epc, NI,NC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

leg_text = {leg{b}};
epc_compare_print(com{b(1)}, com{b(2)}, epc_cost, 'FZ-NormOnly', leg_text)

figure(2);subplot(1,1,1);
b = [14 9 22 ];
subplot(1,1,1);
for i=1:size(b,2),
  if (i==1), hold off; end;
    plot(epc_cost(:,1), res{b(i)}.eva.hter_apri*100,signs{b(i)}); 
  if (i==1), hold on; end;
end;
legend(leg{b});ylabel('HTER(%)');xlabel('\alpha');
%legend('margin', 'orig', 'margin+orig');

count = 0;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			count = count+1;
			wer_apost(count,1) = wer(com{8}{p,s,r}.dset{1,1}, com{8}{p,s,r}.dset{1,2},[0.5 0.5],4,0) ;
			wer_apost(count,2) = wer(com{21}{p,s,r}.dset{1,1}, com{21}{p,s,r}.dset{1,2},[0.5 0.5],4,0,1);
			pause;
			%wer_apost(count,1) = wer(com{8}{p,s,r}.dset{1,1}, com{8}{p,s,r}.dset{1,2},[0.5 0.5]);
			%wer_apost(count,2) = wer(com{21}{p,s,r}.dset{1,1}, com{21}{p,s,r}.dset{1,2},[0.5 0.5]);
		end;
	end;
end;

%plot of F-Norm[only] vs Z-Norm[only]
b = [12 10];
leg_text = {leg{b}};
epc_compare_print(com{b(1)}, com{b(2)}, epc_cost, 'FZ-NormOnly', leg_text)

hter_significant_plot(res{b(1)}, res{b(2)}, pNI,pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)})


%b = [8 12 10 ];
%b = [26 12 10 ];
%b = [8 26 12 17 10 15];
%b = [8 12 10 ];
b = [8 26 12 28 10 27];
signs2={'k-.','k-.','k--','k--', 'k-','k-' };
lw=[1 2 1 2 1 2];
subplot(1,1,1); set(gca, 'FontSize', 14);
%bsigns = {'b:', 'b--', 'b-'}
%bleg = {'mean', 'Z-Norm,mean', 'F-Norm,mean'};
for i=1:size(b,2),
  if (i==1), hold off; end;
    %plot(epc_cost(:,1), res{b(i)}.eva.hter_apri*100,signs{b(i)}); 
    plot(epc_cost(:,1), res{b(i)}.eva.hter_apri*100,signs2{i},'LineWidth',lw(i))
  if (i==1), hold on; end;
end;
legend(leg{b});ylabel('HTER(%)');xlabel('\alpha');
print('-depsc2', '../Pictures/orig_ZFNorm_mean_nonorm_pooled2.eps');

i=25;
[wer_min, thrd_min, x] = wer(com{i}{p,s,r}.dset{1,1}, com{i}{p,s,r}.dset{1,2} , [0.5, 0.5], 1);



[wer_min, thrd_min, x] = wer((:,1), dev.sheep(:,1), [0.5, 0.5], 1);
[tmp, out] = fusion_clientd_Fratio2(expe, 1, beta);


figure(1);
percentage = 1;
param1 = VR_analysis(clientd.dset{1,1},clientd.dset{1,2});
VR_draw(clientd.dset{1,1},clientd.dset{1,2}, percentage,param1);
f_ratio(param1)

figure(2);
param2 = VR_analysis(expe.dset{1,1},expe.dset{1,2});
VR_draw(expe.dset{1,1},expe.dset{1,2}, percentage,param2);
f_ratio(param2)

%save the processed variables
%save fdwsum com_dfwsum com_mean com_wsum epc_cost




hter_significant_plot(com{2}{p,s,r}.epc, com{4}{p,s,r}.epc, NI, NC, epc_cost);
hter_significant_plot(com{2}{p,s,r}.epc, ccom{2}{p,s,r}.epc, NI, NC, epc_cost);
hter_significant_plot(com{3}{p,s,r}.epc, ccom{2}{p,s,r}.epc, NI, NC, epc_cost);

%[wer_min, thrd_min, x] = wer(clientd.dset{1,1(:,1)}, clientd.dset{1,2},[0.5, 0.5], 1);

for i=1:4,
        figure(i);
        [wer_min, thrd_min, x] = wer(com{i,p,s,r}.dset{1,1}, com{i,p,s,r}.dset{1,2},[0.5, 0.5], 1);
end;

