initialise;
%Start experiment

%load data for protocol p, sequence s, row r
p=1;s=1;r=1;
[dev.wolves, dev.sheep, dev.label_wolves, dev.label_sheep] = load_raw_scores_labels(datafiles{1}.dev, expe.P{p}.seq{s}(r,:));
[eva.wolves, eva.sheep, eva.label_wolves, eva.label_sheep] = load_raw_scores_labels(datafiles{1}.eva, expe.P{p}.seq{s}(r,:));
model_ID = unique(dev.label_sheep);

%Why transforming scores into Gaussian distribution?
% VR-EER analysis requires Gaussian assumption
% Some classifiers have problems handling non-Gaussian distributed data
% GMM for instance: more components needed than necessary
% Weighted-sum classifier DOES depend on the Gaussian assumption;
% For instance, its weight derived from Fisher-ratio or least sqaure error
% MAKES use of Gaussian assumption!
%
% Decision boundary-based classifiers (modeling decision boundary) may
% not be affected, however.
%
% one way to avoid this is to use other distribution; another way is to convert the
% data such that the data become Gaussian distributed. We adopt the later here.
% Requires a transformation to do so
% 2 solutions: Histogram equalisation; or sigmoid/tanh function
% Yet another way is to train the classifier to have linear output

%findings:
%After Gaussianisation, only half the data are Gaussian confirmed; the rest
%are extrme values and mostly correct data; this allows us to concentrate
%on ambiguious data
%
% Try to apply this transformation on the test data to show that it generalises well
%
%experiments:
% wsum on untransformed data and on transformed data
%conclusion:
% if wsum is to be used, this transformation should be considered

%Gaussianisation of data, if necessary
tmp.train.data =  [dev.wolves;dev.sheep];
tmp.train.class = [zeros(size(dev.wolves,1),1);ones(size(dev.sheep,1),1)] + 1;
tmp.test.data =  [eva.wolves;eva.sheep];
tmp.test.class = [zeros(size(eva.wolves,1),1);ones(size(eva.sheep,1),1)] + 1;
%FJ_params = { 'Cmax', 6, 'thr', 1e-3, 'animate', 1 };
%Estimate the distribution without animation
FJ_params = { 'Cmax', 10, 'thr', 1e-3, 'animate', 0 };
bayesS = gmmb_create(tmp.train.data(:,1), tmp.train.class(:,1), 'FJ', FJ_params{:});
xx = linspace(-2,2, 100)';
%bayesS(1).apriories=0.5;
%bayesS(2).apriories=0.5;
result = gmmb_classify(bayesS, xx, 'values');

%Estimate the cumulative density function: output to 
N = size(xx,1);
K = size(bayesS,2);
P = [bayesS.apriories];

pxomega_cdf = zeros(N,K);
pxomega = zeros(N,K);
for k = 1:K
	pxomega(:,k) = gmmb_pdf(xx, bayesS(k).mu, bayesS(k).sigma, bayesS(k).weight );
	pxomega_cdf(:,k) = gmmb_cdf(xx, bayesS(k).mu, bayesS(k).sigma, bayesS(k).weight );
end
%tulo = pxomega.*repmat(P,N,1);
%PomegaKx = tulo ./ repmat(sum(tulo,2),1,K);

figure(1);
hold off;
plot(xx, result(:,1), 'b--');
hold on;
plot(xx, result(:,2), 'r--');
%plot(xx, PomegaKx(:,1),  'b-');
%plot(xx, PomegaKx(:,2),  'r-');


hold off;
plot(xx, pxomega_cdf(:,1),  'b--'); hold on;
plot(xx, pxomega_cdf(:,2),'r--')
plot(xx, pxomega(:,1)/8,  'b-')
plot(xx, pxomega(:,2)/8,'r-')
legend('impostor pdf', 'client pdf', 'impostor cdf', 'client cdf');

figure(2);
wer(tmp.train.data(find(tmp.train.class ==1),1),tmp.train.data(find(tmp.train.class ==2),1),[1 1], 5);
hold on;
plot(xx, 1-pxomega_cdf(:,1),'r--')
plot(xx, pxomega_cdf(:,2),'b--')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%first create cross-validation data on the dev set
%lets call it train and test sets
%[tmp.kf_train.data, tmp.kf_train.class, tmp.kf_test.data, tmp.kf_test.class] = subset(tmp.train.data, tmp.train.class, round(size(tmp.train.data,1)/2 ));
%kmax = 2; nr_of_cand = 100;
%[W,M,R,Tlogl] = gmmbvl_em(tmp.train.data(:,1),tmp.train.class(:,1), kmax, nr_of_cand, 0, 1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%plot the transformation function
figure(2);hold off;
plot(xx, sum(pxomega, 2)/2);

%Apply the transformation
D=2; %2 dimensions
N=size(tmp.train.data,1);
tmp_ = zeros(N,K,D);
tmp.train.dataT=zeros(N,D);
for d=1:D,
    for k = 1:K,
        tmp_(:,k,d) = gmmb_cdf(tmp.train.data(:,d), bayesS(k).mu, bayesS(k).sigma, bayesS(k).weight );
    end;
    tmp.train.dataT(:,d) = sum(tmp_(:,:,d),2)/2;
end;

figure(3);
hold off;
plot(tmp.train.dataT(find(tmp.train.class ==1),1),tmp.train.dataT(find(tmp.train.class ==1),2), '+');
hold on;
plot(tmp.train.dataT(find(tmp.train.class ==2),1),tmp.train.dataT(find(tmp.train.class ==2),2), 'ro');

%hist(tmp.train.dataT(find(tmp.train.class ==1),1),100)

figure(4);
plot(tmp.train.data(find(tmp.train.class ==1),1),tmp.train.data(find(tmp.train.class ==1),2), '+');
hold on;
plot(tmp.train.data(find(tmp.train.class ==2),1),tmp.train.data(find(tmp.train.class ==2),2), 'ro');

figure(5);
hold off;
plot(tmp.train.dataT(find(tmp.train.class ==1),1),tmp.train.data(find(tmp.train.class ==1),2), '+');
hold on;
plot(tmp.train.dataT(find(tmp.train.class ==2),1),tmp.train.data(find(tmp.train.class ==2),2), 'ro');


%Try inverse of tanh as a transformation
figure(7);
x=linspace(-5,5,100);
plot( tanh(x),x)

%Try inverse of sigmoid function
alpha = 1;

figure(9);
x=linspace(-1,1,100)';
y=sigmoid_inv(x,alpha);
plot(x,y);


%transform the data; anyting below -15 and above 15 can be ignored literally for modelling purpose
tmp.train.dataS = sigmoid_inv(tmp.train.data(:,1),alpha);

figure(8);
hold off;
plot(tmp.train.dataS(find(tmp.train.class ==1),1),tmp.train.data(find(tmp.train.class ==1),2), '+');
hold on;
plot(tmp.train.dataS(find(tmp.train.class ==2),1),tmp.train.data(find(tmp.train.class ==2),2), 'ro');

figure(1);
wer(tmp.train.dataS(find(tmp.train.class ==1),1),tmp.train.dataS(find(tmp.train.class ==2),1),[0.5 0.5], 1);
figure(2);
wer(tmp.train.data(find(tmp.train.class ==1),1),tmp.train.data(find(tmp.train.class ==2),1),[0.5 0.5], 1,45,1);

%show the histogram
figure(10);
hist(tmp.train.dataS(find(tmp.train.class ==1),1),100);
figure(12);
hist(tmp.train.dataS(find(tmp.train.class ==2),1),100);

%now filter away the data
clear tmp_;
tmp_a = find( tmp.train.dataS > -15);
tmp_b = find( tmp.train.dataS < 15);
tmp.train.dataSF(:,1) = tmp.train.dataS(intersect(tmp_a, tmp_b));
tmp.train.dataSF(:,2) = tmp.train.data(intersect(tmp_a, tmp_b),2);
tmp.train.class_SF = tmp.train.class(intersect(tmp_a, tmp_b));

lillietest(tmp.train.dataSF(find(tmp.train.class_SF==1),1))
wer(tmp.train.dataSF(find(tmp.train.class_SF ==1),1),tmp.train.dataSF(find(tmp.train.class_SF ==2),1),[0.5 0.5], 1,45,1);

%show the historgram of filtered data
figure(10);
hist(tmp.train.dataSF(find(tmp.train.class_SF ==1),1),100);
figure(12);
hist(tmp.train.dataSF(find(tmp.train.class_SF ==2),1),100);

%Should do a Gaussianity test here
%repeat to change the alpha value such that a Gaussian distribution is obtained.


figure(9);
hold off;
plot(tmp.train.dataSF(find(tmp.train.class_SF ==1),1),tmp.train.dataSF(find(tmp.train.class_SF ==1),2), '+');
hold on;
plot(tmp.train.dataSF(find(tmp.train.class_SF ==2),1),tmp.train.dataSF(find(tmp.train.class_SF ==2),2), 'ro');



%Apply F-norm on training and test sets
[expe.dset{p,s,r}.dev.wolves, expe.dset{p,s,r}.dev.sheep, expe.dset{p,s,r}.dev.param] = VR_Fnorm(dev.wolves, dev.sheep);
[expe.dset{p,s,r}.eva.wolves, expe.dset{p,s,r}.eva.sheep] = VR_Fnorm(eva.wolves, eva.sheep, expe.dset{p,s,r}.dev.param);
param = VR_analysis(expe.dset{p,s,r}.dev.wolves, expe.dset{p,s,r}.dev.sheep);

%draw dev and eva data
figure(1);
percentage = 0.1;
VR_draw(expe.dset{p,s,r}.dev.wolves, expe.dset{p,s,r}.dev.sheep, percentage, param)
figure(2);
VR_draw(expe.dset{p,s,r}.eva.wolves, expe.dset{p,s,r}.eva.sheep, percentage, param)








