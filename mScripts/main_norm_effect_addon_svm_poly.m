%load configurations
%cd /home/learning/norman/xm2vts_fusion/mScripts

%clear all
%main_norm_effect_addon_svm_poly

initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%Start experiment
p=1;s=1;r=1;

%cv = [1:10];
cv = [1:3];
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
            	        end;end;

			%zero-mean unit variance normalisation
			nexpe{2} = zmuv_norm(nexpe{1});

			%use margin scores	
			nexpe{3} = convert2margin_scores(nexpe{1});

			%for MLP output
			for d=1:2,for k=1:2,
				nexpe{4}.dset{d,k} = nexpe{1}.dset{d,k};
				for ex=1:2,
					if (mlp{p,s,r}(ex)),
						fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
						nexpe{4}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex));
                                        end;
                                end;
                        end;end;

			%second-level fusion
			for d=1:2,for k=1:2,
				new_expe.dset{d,k} = [nexpe{1}.dset{d,k} nexpe{2}.dset{d,k} nexpe{3}.dset{d,k} nexpe{4}.dset{d,k} ];
                        end;end;

        
                        %by cross-validation to find the C values
                        ratio = [0.5 0.5];

                        tmp{1} = splitdata(new_expe.dset{1,1}, ratio);
                        tmp{2} = splitdata(new_expe.dset{1,2}, ratio);
                        tdata.dset{1,1} = tmp{1}{1};
                        tdata.dset{1,2} = tmp{2}{1};
                        tdata.dset{2,1} = tmp{1}{2};
                        tdata.dset{2,2} = tmp{2}{2};                

			chosen_list = [1 2; 3 4; 5 6; 7 8];
                        for i=1:4,
                                chosen = chosen_list(i,:);
                                for c=1:length(cv),
                                        fprintf(1,'c is %d\n', c);
                                        fprintf(1,txt);
                                        [tmp_com{c}, epc_cost] = fusion_svm(tdata, chosen, [], [], [], 'poly', cv(c));
                                        %c_value = 1;
                                        %[tmp_com{c}, epc_cost] = fusion_svm(tdata, chosen, [], [], c_value, 'rbf', cv(c));
                                end;        
                                clear hter_apri tot_hter_apri                        
                                for c=1:length(cv),
                                        tot_hter_apri(c) = sum(tmp_com{c}.epc.eva.hter_apri);
                                        hter_apri(c) = tmp_com{c}.epc.eva.hter_apri(6);
                                end;
                        
                                [tmp_ , index]= min(tot_hter_apri);
                                kparam = cv(index(1));
                                [com{i}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen, [], [], [], 'poly', kparam);
                                com{i}{p,s,r}.polyd = kparam;
                                fprintf(1,txt);
                        end;
			%free the memory
			for i=1:size(com,2),
				com{i}{p,s,r}.dset={};
                                com{i}{p,s,r}.net={};
			end;

		end;
	end;
end;

save norm_expe_addon_svm_poly.mat com epc_cost
%save norm_expe_addon_svm_rbf.mat com epc_cost

polyd = [];
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
                        t_=[];
                        for i=1:size(com,2),
                                t_ = [t_ com{i}{p,s,r}.polyd];
                        end;
                        polyd = [polyd; t_];
                end;
        end;
end;
                        
%overall performance
for i=1:size(com,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(com{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(com{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(com{i},[2 3]); %intramodal
end;

signs = {'kx-',  'kd-','ko-','k*-'};
leg = {'orig,svm-poly','z,svm-poly','margin,svm-poly','inv,svm-poly'};

list = 1:size(com,2);
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);

%c=1;
%b=[1 3];
%hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
%subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});
