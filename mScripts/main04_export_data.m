cd learning/xm2vts_fusion/mScripts/
initialise;

for p=2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}, data{p}.imp_id{1}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}, data{p}.imp_id{2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%export nist format
%claim id, true id, scores
d=1;
chosen = [2 4];
tmp=[data{p}.label{d,1}, data{p}.imp_id{d} data{p}.dset{d,1}(:,chosen)];
for i=1:length(tmp),
  fprintf(1, '%d %d %f %f\n', tmp(i,1),tmp(i,2), tmp(i,3), tmp(i,4));
end;
  
data{p}.label{d,2}, data{p}.label{1,2} data{p}.dset{d,2}(:,chosen)

for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      b = expe.P{p}.seq{s}(r,:);
      
      %load score files
      for d=1:2,for k=1:2,
	  expe.dset{d,k} = data{p}.dset{d,k}(:,b);
	end;end;	

	export_data(expe, p,s,r);
    end;
  end;
end;
    
    
[test.dset{1,1}, test.dset{1,2}] = load_raw_scores('../export/1_01_01_dev.scores');


