cd /home/learning/norman/xm2vts_fusion/mScripts

%corr and variance ratio
corr = linspace(0,1,11);
var_ratio = linspace(1,4,10);

mu_I = [0 0];
mu_C = [1 1];
var{1} = 0.1 * ones(1,size(var_ratio,2)); %
var{2} = var{1} .* var_ratio;

n_dat_samples = 10000;
%sample scores from Gaussian

%epc
n_samples = 11; epc_range = [0.1 0.9];
n_com = 7;

tot = 0;
for j=1:size(var_ratio,2),
        for c=1:size(corr,2),
                tot = tot+1;
                fprintf(1, 'Running %d of %d experiments\n', tot, size(var_ratio,2)*size(corr,2));
                cov_mat(1,1) = var{1}(j);
                cov_mat(2,2) = var{2}(j);
                cov_mat(1,2) = sqrt(var{1}(j)) * sqrt(var{2}(j)) * corr(c);
                cov_mat(2,1) = cov_mat(1,2);
                expe{1} = mvnrnd(mu_I, cov_mat, n_dat_samples);
                expe{2} = mvnrnd(mu_C, cov_mat, n_dat_samples);
                %%figure(1); hold off; wer(expe{1}(:,1),expe{2}(:,1), [0.5 0.5], 1)
                %%figure(2); hold off; wer(expe{1}(:,2),expe{2}(:,2), [0.5 0.5], 1)
                %figure(3); subplot(size(var_ratio,2), size(corr,2), tot);
                %draw_empiric(expe{1},expe{2}, 0.1); axis([-5 5 -5 5]);
		%set(gca, 'xtick', [-5 0 5]);
		%set(gca, 'ytick', [-5 0 5]);
	        %set(gca, 'xticklabel', {'', '', ''});
	        %set(gca, 'yticklabel', {'', '', ''});

                nexpe.dset{1,1} = expe{1}; nexpe.dset{1,2} = expe{2};
                nexpe.dset{2,1} = expe{1}; nexpe.dset{2,2} = expe{2};

		%for product normalisation into [0, 1]
		model.max = max([nexpe.dset{1,1}; nexpe.dset{1,2}]);
		model.min = min([nexpe.dset{1,1}; nexpe.dset{1,2}]);
		[pexpe.dset{1,1}, pexpe.dset{1,2}] = normalise_scores(nexpe.dset{1,1}, nexpe.dset{1,2}, model.min, model.max - model.min);
		[pexpe.dset{2,1}, pexpe.dset{2,2}] = normalise_scores(nexpe.dset{2,1}, nexpe.dset{2,2}, model.min, model.max - model.min);

                %perform fusion
                %[nsorted] = fusion_sorted(nexpe);
                %com{1} = fusion_wsum_nonorm(nexpe, [1 2], [0.5 0.5]);
                %com{2} = fusion_wsum_brute_nonorm(nexpe, [1 2]);
                for d=1:2,for k=1:2,
                        %com{3}.dset{d,k} = nsorted.dset{d,k}(:,1); %min
                        %com{4}.dset{d,k} = nsorted.dset{d,k}(:,2); %max
                        %com{5}.dset{d,k} = nexpe.dset{d,k}(:,1); %the best
                        %com{6}.dset{d,k} = nexpe.dset{d,k}(:,2); %the worse
                        com{7}.dset{d,k} = pexpe.dset{d,k}(:,2); %prod
                end;end;
                %for i=3:n_com,
		for i=n_com,
                        fprintf(1,'\nCalculating EPC');
                        [com{i}.epc.dev, com{i}.epc.eva, epc_cost]  = epc(com{i}.dset{1,1}, com{i}.dset{1,2}, com{i}.dset{2,1}, com{i}.dset{2,2}, n_samples,epc_range);
                end;
                for i=1:n_com,
                        com{i}.dset = {};
                end;
		%for i=1:n_com,
		for i=n_com,
			ncom{j,c}{i} = com{i};                
		end;
                %.epc.dev.hter(5)
        end;
end;

figure(3);
for c=1:size(corr,2),
	tot = c;
	subplot(size(var_ratio,2), size(corr,2), tot);
	txt=sprintf('%1.2f', corr(c));
	title(txt);
end;
for j=1:size(var_ratio,2),
	tot = (j-1) * size(corr,2)+1;
	subplot(size(var_ratio,2), size(corr,2), tot);
	txt=sprintf('%1.2f', var_ratio(j));
	xlabel('');	
	ylabel(txt);
end;

save expe_simulation ncom
load expe_simulation ncom

clear out;
count = 0;
for j=1:size(var_ratio,2),
        for c=1:size(corr,2),
                count = count + 1;
                for i=[1:n_com],
                        out{i}(count,1) = var_ratio(j);
                        out{i}(count,2) = corr(c);
			%gain_min
                        out{i}(count,3) = ncom{j,c}{5}.epc.dev.hter(5) / ncom{j,c}{i}.epc.dev.hter(5);
			%gain_mean

                        out{i}(count,4) = ave_HTER /ncom{j,c}{i}.epc.dev.hter(5);
			for t=1:11,
	                        out{i}(count,4+t) = ncom{j,c}{5}.epc.dev.hter(t) / ncom{j,c}{i}.epc.dev.hter(t);
			end;
                end;
                
        end;
end;

figure(3);
print('-depsc2', '../Pictures/simulation.eps');


%plot all

signs = {'+', 'ro','gx', 'k*', 'bo', 'bo', 'ms'};
leg = {'mean', 'wsum', 'min', 'max', 'best', 'worse', 'prod'};

column = 3;
column = 5;
figure(2); hold off;

list = [1 2 3 4 7];
[X,Y] = meshgrid(var_ratio,corr);
for i=list,
        if i==list(1), hold off; end;
                Z = griddata(out{i}(:,1),out{i}(:,2),out{i}(:,column),X,Y,'cubic');
                hidden on; hold on;
                surf(X,Y,Z) %interpolated
                plot3(out{i}(:,1), out{i}(:,2), out{i}(:,column), signs{i})
end;
grid on
xlabel('variance ratio');
ylabel('corrrelation');
zlabel('HTER');
legend({leg{list}});
%AZ = 41; EL = 26;
AZ = 126; EL = 28;
view(AZ, EL);
print('-depsc2', '../Pictures/simulation_all.eps');



%plot 2by2
combi = nchoosek([1:4 7],2);
[X,Y] = meshgrid(var_ratio,corr);
AZ= 27; EL = 32;
for i=1:10,
	figure(i);
	set(gca,'Fontsize', 14);
        %subplot(2,3,i);
        hold off;
        Z = griddata(out{combi(i,1)}(:,1),out{combi(i,1)}(:,2),out{combi(i,1)}(:,column),X,Y,'cubic');
        hold on;
        hidden on;
        surf(X,Y,Z) %interpolated
        plot3(out{combi(i,1)}(:,1), out{combi(i,1)}(:,2), out{combi(i,1)}(:,column), signs{combi(i,1)});

        Z = griddata(out{combi(i,2)}(:,1),out{combi(i,2)}(:,2),out{combi(i,2)}(:,column),X,Y,'cubic');
        hidden on;
        surf(X,Y,Z) %interpolated
        plot3(out{combi(i,2)}(:,1), out{combi(i,2)}(:,2), out{combi(i,2)}(:,column), signs{combi(i,2)});
        h = legend({leg{combi(i,:)}});
	set(h, 'Fontsize',10);
        view(AZ,EL);
        grid on;
        %if i==1, 
                xlabel('var. ratio');
                ylabel('corr.');
		if column == 3,
	                zlabel('\beta_{min}');         
		else
	                zlabel('\beta_{mean}');         
                	%zlabel('HTER');         
		end;	
        %end;
	set(gca,'xtick', [1:4]);
	set(gca,'ytick', [0:0.2:1]);
	set(gca,'ztick', [0:5]);
end;
for i=1:10,
	figure(i);
	print('-depsc2', ['../Pictures/simulation_2by2_' num2str(i) '.eps']);
end;

%boxplot
figure(1);
list = [7 3 4 1 2];
set(gca, 'Fontsize',16);
boxplot([out{7}(:,3) out{3}(:,3) out{4}(:,3) out{1}(:,3) out{2}(:,3) ],1,'o',[],1.0);
set(gca, 'xticklabel', {leg{list}});
ylabel('\beta_{min}');
axis([0.5 5.5 0 2.5])
grid on;
print('-depsc2', ['../Pictures/simulation_boxplot.eps']);

%individual plot
column = 3;
column = 4+1;
column = 4+3;
epc_cost = linspace(0.1,0.9,11);

list = [1 2 3 4 7];
[X,Y] = meshgrid(var_ratio,corr);
AZ= 27; EL = 32;
%cmap = colormap;
for i=list,

	figure(i);set(gca, 'Fontsize',16);
        hold off;
	Z = griddata(out{i}(:,1),out{i}(:,2),out{i}(:,column),X,Y,'cubic');
        hidden on; hold on;

	%[C,h] = contour(X,Y,Z) %interpolated
	%clabel(C,h);
	surf(X,Y,Z) %interpolated
	plot3(out{i}(:,1), out{i}(:,2), out{i}(:,column), 'ro')

	Z = griddata(out{i}(:,1),out{i}(:,2), ones( size(out{i}(:,2),1),1) ,X,Y,'cubic');
        hidden on;
	surf(X,Y,Z) %interpolated
	colormap(gray(256));
	colorbar;
	xlabel('var ratio');
	ylabel('corrr');
	zlabel('\beta_{min}');
	title(leg{i});
	grid on;
        view(AZ,EL);
	axis([1 4 0 1 0 5]);
end;

for i=list,
	figure(i);
	print('-depsc2', ['../Pictures/simulation_beta_min_gray_' num2str(i) '_' num2str(column) '.eps']);
end;
	
legend({leg{list}});
%AZ = 41; EL = 26;
AZ = 126; EL = 28;



%analyse the weights of wsum
myout=[];
count = 0;
for j=1:size(var_ratio,2),
        for c=1:size(corr,2),
                count = count + 1;
		myout(j,c) = ncom{j,c}{2}.weight(2);
	end;
end;

clear txt;
for j=1:size(var_ratio,2),
	txt{j}=sprintf('%1.2f',var_ratio(j));
end;
clear txt2;
for c=1:size(corr,2),
	txt2{c}=sprintf('%1.1f',corr(c));
end;

pm_view(myout);set(gca,'Fontsize', 14);
set(gca,'xtick',[1:11]);
set(gca,'ytick', 1:10);
set(gca,'yticklabel', txt);
set(gca,'xticklabel', txt2);
xlabel('Correlation');
ylabel('Variance ratio');
colorbar
print('-depsc2', '../Pictures/simulation_reliance_of_2.eps');

%analyse 
myout=[];
count = 0;
for j=1:size(var_ratio,2),
        for c=1:size(corr,2),
                count = count + 1;
		myout(count,1) = ncom{j,c}{5}.epc.dev.hter(5);
		myout(count,2) = ncom{j,c}{6}.epc.dev.hter(5);
	end;
end;

set(gca,'Fontsize', 16);
for i=list,
	boxplot(out{i}(:,5:15));
	title(leg{i});
	axis([0.5 11.5 0.2 3]);
	set(gca, 'xticklabel', epc_cost);
	xlabel('\alpha');
	ylabel('\beta_{min}');
	grid on;
	pause;
	print('-depsc2', ['../Pictures/simulation_beta_min_boxplot_' num2str(i) '.eps']);
end;