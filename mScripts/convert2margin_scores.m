function [com] = convert2margin_scores(expe)
% com is the fuzzy score with s_1, s_2, and so on

n_samples_fusion = size(expe.dset{1,1},2);
%calculate the margin on the dev set

for i=1:n_samples_fusion,
	tmp1=expe.dset{1,1}(:,i);
	tmp2=expe.dset{1,2}(:,i);
	[tran.x{i}, tran.margin{i}] = cal_margin_scores(tmp1,tmp2);
end;

%transform the margin
for d=1:2,for k=1:2,
	for i=1:n_samples_fusion,
 	  com.dset{d,k}(:,i) = interp1(tran.x{i},tran.margin{i}, expe.dset{d,k}(:,i), 'nearest'); %expert i

	end;
end;end

