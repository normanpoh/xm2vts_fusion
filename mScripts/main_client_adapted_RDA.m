cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

initialise;
%load norm_expe.mat com epc_cost
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;
%Start experiment
p=1;s=1;r=1;

for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      %print remarks
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);
      
      %load score files
      for d=1:2,for k=1:2,
	  expe.dset{d,k} = data{p}.dset{d,k}(:,b);
	end;
      end;
      expe.label = data{p}.label;
      
      chosen = [1 2];
      [com{1}{p,s,r}, epc_cost] = fusion_RDA_client(expe, chosen);
      %fname = sprintf('../Pictures/user_weight_%d_%d_%d.eps', p,s,r);
      %print('-depsc2', fname);
      
      %free the memory
      for i=1:size(com,2),
	com{i}{p,s,r}.dset={};
      end;
    end;
  end;
end;

