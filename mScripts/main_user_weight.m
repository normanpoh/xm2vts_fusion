cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

initialise;
%load norm_expe.mat com epc_cost
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;
%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			for d=1:2,for k=1:2,
				expe.dset{d,k} = data{p}.dset{d,k}(:,b);
	                end;end;
			expe.label = data{p}.label;

			chosen = [1 2];
			[com{1}{p,s,r}, epc_cost] = fusion_wsum_client(expe, chosen);
			%fname = sprintf('../Pictures/user_weight_%d_%d_%d.eps', p,s,r);
			%print('-depsc2', fname);

		    	%free the memory
		    	for i=1:size(com,2),
				com{i}{p,s,r}.dset={};
		    	end;
		end;
        end;
    end;
end;            

save main_user_weight.mat com epc_cost

%overall performance
for i=1:size(com,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(com{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(com{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(com{i},[2 3]); %intramodal
end;

signs = {'kx-',  'kd-','ko-','k*-'};
signs = {signs{:}, 'rx--',  'rd--','ro--','r*--'};
leg = {'orig,min', 'orig,max', 'orig,mean', 'orig,conf'};
leg = {leg{:}, 'margin,min', 'margin,max', 'margin,mean', 'margin,conf'};
list = 1:size(com,2);

c=1;
plot_all_epc(epc_cost,leg,signs, out.cfg{c}, list);

list = [1 2 3];
plot_all_epc(epc_cost,leg,signs, out.cfg{c}, list);


%debug
p=1;s=1;r=1;i=1;

out.fa = [];
out.fr = [];

for id=1:size(model_ID,1),
	
	%for d=1:2,for k=1:2,
    	%	index{d,k} = find(expe.label{d,k} == model_ID(id));
    	%	tmp{d,k} = expe.dset{d,k}(index{d,k},chosen);
	%end;end;
	%d=2;
	%wer(com{i}{p,s,r}.dset{d,1}(index{d,1},:), com{i}{p,s,r}.dset{d,2}(index{d,2},:), [0.5 0.5],1)
	[epc_.dev, epc_.eva, epc_cost]  = epc( ...
		com{i}{p,s,r}.dset{1,1}(index{1,1},:),...
		com{i}{p,s,r}.dset{1,2}(index{1,2},:),...
		com{i}{p,s,r}.dset{2,1}(index{2,1},:),...
		com{i}{p,s,r}.dset{2,2}(index{2,2},:),n_samples,epc_range);

	%on the eva set
	Ni = size(com{i}{p,s,r}.dset{2,1}(index{2,1},:),1);
	Nc = size(com{i}{p,s,r}.dset{2,1}(index{2,2},:),1);
	out.fa = [out.fa; epc_.eva.far_apri * Ni];
	out.fr = [out.fr; epc_.eva.frr_apri * Nc];
	fprintf(1, '%d',id);
end;
 	res.eva.far_apri = sum(out.fa) / NI;
	res.eva.frr_apri = sum(out.fr) / NC;
	res.eva.hter_apri = (res.eva.far_apri+res.eva.frr_apri)/2;


wer(com{i}{p,s,r}.dset{d,1}, com{i}{p,s,r}.dset{d,2}, [0.5 0.5],1)
	

	pause;
end;