cd /home/learning/norman/xm2vts_fusion/mScripts

load norm_expe.mat com epc_cost
com1 = com;

load norm_expe_addon_svm.mat com epc_cost
com2 = com;

load norm_expe_addon_gmm_cv.mat com 
com3 = com;

load norm_expe_addon_wsum_user.mat com 
com4 = com;

load norm_expe_addon_svm_poly.mat com 
com5 = com;

load norm_expe_addon_wsum_bf.mat com 
com6 = com;

n_expe = 12;
for j=1:4,
	for i=1:n_expe;
		ind = (j-1)*n_expe + i;		
		if (i==8),
			ncom{ind} = com2{j};
	        elseif (i==9),
			ncom{ind} = com3{j};
		elseif (i==10),
			ncom{ind} = com4{j}
		elseif (i==11),
			ncom{ind} = com5{j}
		elseif (i==12),
			ncom{ind} = com6{j}
		else
			%i=1:7
			ind_orig = (j-1)*7 + i;
			ncom{ind} = com1{ind_orig};
		end;
	end;
end;
tot=4*n_expe;
ncom{tot+1} = com1{29};
%ncom{tot+2} =com1{30};

clear com com3 com1 com2 com4 com5 com6;

%overall performance
for i=1:size(ncom,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(ncom{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(ncom{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(ncom{i},[2 3]); %intramodal
end;

expert = {'mean', 'wsum-fisher', 'rbf-fisher', 'gmm-fj','rbf-wDT', 'rbf-DT', 'DT', 'svm-linear', 'gmm-cv', 'wsum-user', 'svm-poly', 'wsum-bf'};
se = {'o','s','x','^','v','d','*','+','p','h','<','.'};
norm_m = {'orig', 'zmun', 'margin', 'inv'};
sn = {'k', 'r', 'g','b'};
sn2= {'-','--', '-.', ':'};

c=0;
for i=1:size(norm_m,2),
	for j=1:size(expert,2),
		c=c+1;
		leg{c} = [norm_m{i} ',' expert{j}];
		signs{c} = [sn{i}  se{j} sn2{i}]
end;end;
leg{tot+1} = 'poss,DT'; 	signs{tot+1} = 'bx-';
%leg{tot+2} = 'poss1,DT'; 	signs{tot+2} = 'bx--';

%answer the following questions:
%1. How normalisation can affect classifier performance?
c=1;
%c=2;
%c=3;

list = 1:size(ncom,2);
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);



%sort by model performs
list = [0:3]*n_expe+1; 
i=0;
while i <n_expe,
	%plot_all_epc(epc_cost,leg,signs, out.cfg{1}, [list tot+1 ]);
	plot_all_epc(epc_cost,leg,signs, out.cfg{1}, [list]);
	axis([0.1 0.9 1 3]);set(gca, 'YTick', [1:0.2:3]);grid on;set(gca, 'XTick', [0.1:0.1:0.9]);
	pause;
	list = list + 1; i=i+1;
	%print('-depsc2', ['../Pictures/norm_by_model', num2str(i),'.eps']);
end;

%sort by normalisation methods
list = [1:2 7:n_expe];
i=0;
while i <=3,
	plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
	axis([0.1 0.9 0.8 3]);set(gca, 'YTick', [0.8:0.2:3]);grid on;set(gca, 'XTick', [0.1:0.1:0.9]);
	h=legend(leg{list});
	set(h, 'Fontsize',12);
	pause;	
	i=i+1;	list = list + n_expe;
	%print('-depsc2', ['../Pictures/norm_by_norm', num2str(i),'.eps']);
end;

%choose the best of each method: gmm
figure(2);
list = [4 8 9];
list = [list list+9 list+18 list+27];
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);


c=1;
%compare between gmm-fj and gmm-cv
b=[4 9];
for i=1:4,
        hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
        subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});
        pause;
        b=b+9;
end;


%calculate the sensitivity


%sort by model performs
sens1 = [];
hter1 = [];
list = [0:3]*n_expe+1; 
i=0;
while i <n_expe,
	%plot_all_epc(epc_cost,leg,signs, out.cfg{1}, [list tot+1 ]);
	plot_all_epc(epc_cost,leg,signs, out.cfg{1}, [list]);
	axis([0.1 0.9 1 3]);set(gca, 'YTick', [1:0.2:3]);grid on;set(gca, 'XTick', [0.1:0.1:0.9]);

	tmp = [];
	for j=1:length(list),
		 tmp = [tmp; out.cfg{1}.res{list(j)}.eva.hter_apri];
	end;

	sensitivity(i+1) = mean(abs(min(tmp) - max(tmp)));
	sensitivity2(i+1) = mean(log(std(tmp)));
	sens1 = [sens1, log(abs(min(tmp) - max(tmp)))'];
	hter1 = [hter1,  reshape(tmp, prod(size(tmp)),1)];

	%pause;
	list = list + 1; i=i+1;
	%print('-depsc2', ['../Pictures/norm_by_model', num2str(i),'.eps']);
end;

set(gca, 'Fontsize', 14);
list = [1 2 10 8 9 7];
boxplot(sens1(:,list));
set(gca, 'XTickLabel', {expert{list}}, 'Fontsize', 12);
ylabel('HTER Sensitivity');
xlabel('');grid on;
%print('-depsc2', '../Pictures/norm_hter_sensitivity.eps');

set(gca, 'Fontsize', 14);
list = [1 2 10 8 9 7];
boxplot(hter1(:,list)*100);
set(gca, 'XTickLabel', {expert{list}}, 'Fontsize', 12);
ylabel('HTER (%)');
xlabel('');grid on;
%print('-depsc2', '../Pictures/norm_hter_perf.eps');


subplot(2,1,1);
%list = [1 2 10 8 9 7 10 11];
%list = [8 9 1 10 7 2];
list = [11 8 9 4 1 10 12 7 2 ];
set(gca, 'Fontsize', 14);
boxplot(hter1(:,list)*100,1,'o',[],1.0)
set(gca, 'XTickLabel', {expert{list}}, 'Fontsize', 10);
ylabel('HTER (%)');
xlabel('');grid on;

subplot(2,1,2);
set(gca, 'Fontsize', 14);
boxplot(sens1(:,list),1,'o',[],1.0)
set(gca, 'XTickLabel', {expert{list}}, 'Fontsize', 10);
ylabel('HTER Sensitivity');
xlabel('');grid on;


print('-depsc2', '../Pictures/norm_hter_sens_perf.eps');
