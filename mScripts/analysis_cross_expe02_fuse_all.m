cd /home/learning/norman/xm2vts_fusion/mScripts

load main_fusion_allinfo.mat com epc_cost
M=7;
list =[ 1 3 4 6 2]; %gmm
list =[ list, list + M];%svm
for i=1:10,
	ncom{i} =com{list(i)};
end;

leg = {'orig,gmm', 'F,gmm', 'margin,gmm', 'orig-F-margin,gmm', 'Z,gmm'};
leg = {leg{:},'orig,svm', 'F,svm', 'margin,svm', 'orig-F-margin,svm', 'Z,svm'};

clear com

load generic com epc_cost
leg = {leg{:}, 'orig,mean', 'F,mean', 'margin,mean'};
ncom{11} = com{8};
ncom{12} = com{10};
ncom{13} = com{1};

load norm_expe_addon_gauss_client.mat com gauss_com mix_com epc_cost

ncom{14} = com{1};
ncom{15} = gauss_com{1};
ncom{16} = mix_com{1};
ncom{17} = gauss_com{2};
ncom{18} = mix_com{2};
leg = {leg{:},'local','global(qda)','mix(qda)','global(lda)','mix(lda)'};


%overall performance
for i=1:size(ncom,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(ncom{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(ncom{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(ncom{i},[2 3]); %intramodal
end;

signs = {'kx-',  'kd-','ko-', 'ks-', 'g-'};
signs = {signs{:}, 'kx--',  'kd--','ko--', 'ks--', 'g--'};
signs = {signs{:}, 'rx:',  'rd:','ro:'};

signs = {'kx-',  'kx-','kx-', 'ks-', 'g-'};
signs = {signs{:}, 'bd--',  'bd--','bd--', 'bs--', 'g--'};
signs = {signs{:}, 'ro:',  'ro:','ro:'};

signs = {signs{:}, 'gs-',  'gs:','gs--'};
list = 1:size(ncom,2);
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);

figure(1);
list = [1 6 11];
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
axis([0.1 0.9 0.8 2.4]);set(gca, 'YTick', [0.8:0.2:2.4]);grid on;set(gca, 'XTick', [0.1:0.1:0.9]);
print('-depsc2', '../Pictures/cross_expe02_orig.eps');

plot_all_roc(epc_cost,leg,signs, out.cfg{1}, list);
print('-depsc2', '../Pictures/cross_expe02_orig_roc.eps');

figure(2);
list = list + 1;
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
axis([0.1 0.9 0.8 2.4]);set(gca, 'YTick', [0.8:0.2:2.4]);grid on;set(gca, 'XTick', [0.1:0.1:0.9]);
plot(sublist.res{i}.eva.far_apri*100, sublist.res{i}.eva.frr_apri*100,'rd-','Markersize',14);
print('-depsc2', '../Pictures/cross_expe02_F.eps');

plot_all_roc(epc_cost,leg,signs, out.cfg{1}, list);
%i=2;plot(sublist.res{i}.eva.far_apri*100, sublist.res{i}.eva.frr_apri*100,'go-','Markersize',14);
print('-depsc2', '../Pictures/cross_expe02_F_roc.eps');


figure(3);
list = list + 1;
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
axis([0.1 0.9 0.8 2.4]);set(gca, 'YTick', [0.8:0.2:2.4]);grid on;set(gca, 'XTick', [0.1:0.1:0.9]);
print('-depsc2', '../Pictures/cross_expe02_margin.eps');

plot_all_roc(epc_cost,leg,signs, out.cfg{1}, list);
print('-depsc2', '../Pictures/cross_expe02_margin_roc.eps');

figure(4);
%list = [6 11+1 6+2 4 9];
list = [6 11+1 6+2 9];
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
axis([0.1 0.9 0.8 2.4]);set(gca, 'YTick', [0.8:0.2:2.4]);grid on;set(gca, 'XTick', [0.1:0.1:0.9]);
print('-depsc2', '../Pictures/cross_expe02_all.eps');

plot_all_roc(epc_cost,leg,signs, out.cfg{1}, list);
print('-depsc2', '../Pictures/cross_expe02_all_roc.eps');

%sig. test
figure(5);
c=1;
b=[12 9];hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

figure(5);
c=1;
b=[6 9];hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

%get the best
list = [1 6 11];
tmp=[];
for i=1:3,
  tmp = [tmp; out.cfg{1}.res{list(i)}.eva.hter_apri];
end;
[sublist.res{1}.eva.hter_apri, I] = min(tmp);
for i=1:length(I),
  sublist.res{1}.eva.far_apri(i) = out.cfg{1}.res{list(I(i))}.eva.far_apri(i);
  sublist.res{1}.eva.frr_apri(i) = out.cfg{1}.res{list(I(i))}.eva.frr_apri(i);
end;

list = list + 1;
tmp=[];
for i=1:3,
	tmp = [tmp; out.cfg{1}.res{list(i)}.eva.hter_apri];
end;
sublist.res{2}.eva.hter_apri=min(tmp);
for i=1:length(I),
  sublist.res{2}.eva.far_apri(i) = out.cfg{1}.res{list(I(i))}.eva.far_apri(i);
  sublist.res{2}.eva.frr_apri(i) = out.cfg{1}.res{list(I(i))}.eva.frr_apri(i);
end;

list = list + 1;
tmp=[];
for i=1:3,
	tmp = [tmp; out.cfg{1}.res{list(i)}.eva.hter_apri];
end;
sublist.res{3}.eva.hter_apri=min(tmp);
for i=1:length(I),
  sublist.res{3}.eva.far_apri(i) = out.cfg{1}.res{list(I(i))}.eva.far_apri(i);
  sublist.res{3}.eva.frr_apri(i) = out.cfg{1}.res{list(I(i))}.eva.frr_apri(i);
end;

figure(5);hold off;
set(gca,'Fontsize', 18);
plot(epc_cost(:,1), sublist.res{1}.eva.hter_apri*100,signs{1},'Markersize',14); hold on;
plot(epc_cost(:,1), sublist.res{2}.eva.hter_apri*100,signs{2},'Markersize',14); 
plot(epc_cost(:,1), sublist.res{3}.eva.hter_apri*100,signs{3},'Markersize',14); 
%plot(epc_cost(:,1), out.cfg{1}.res{4}.eva.hter_apri*100,signs{4}); 
plot(epc_cost(:,1), out.cfg{1}.res{9}.eva.hter_apri*100,signs{9},'Markersize',14); 
%list = [6 11+1 6+2 4 9];
axis([0.1 0.9 0.8 2.4]);set(gca, 'YTick', [0.8:0.2:2.4]);grid on;
set(gca, 'XTick', [0.1:0.1:0.9]);
legend('best orig', 'best F', 'best margin', leg{9});
print('-depsc2', '../Pictures/cross_expe02_all.eps');

%roc plot
set(gca,'Fontsize', 18);
hold off;
i=1;plot(sublist.res{i}.eva.far_apri*100, sublist.res{i}.eva.frr_apri*100,'bo:','Markersize',14);
hold on;
i=2;plot(sublist.res{i}.eva.far_apri*100, sublist.res{i}.eva.frr_apri*100,'rd-','Markersize',14);
i=3;plot(sublist.res{i}.eva.far_apri*100, sublist.res{i}.eva.frr_apri*100,'k*:','Markersize',14);
plot(out.cfg{1}.res{9}.eva.far_apri*100, out.cfg{1}.res{9}.eva.frr_apri*100,signs{9},'Markersize',14); 
axis tight; grid on;
ylabel('FRR(%)');xlabel('FAR(%)');
legend('best orig', 'best F', 'best margin', leg{9});
print('-depsc2', '../Pictures/cross_expe02_all.eps');


c=1;
b=[12 9];hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});


list = [1 6 2 7 3 8 4];
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
print('-depsc2', '../Pictures/best_com.eps');

b=[1 6];hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

b=[2 7];hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

b=[3 8];hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

%for i=[1 2 3 6 7 8
	
out.cfg{1}.res{i}.eva.hter_apri
