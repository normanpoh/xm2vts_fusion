function [weight, myout, cfg, myleg, mysign] = fusion_clientd_Fnorm_tune(expe, chosen, beta_list, toplot)
if nargin < 2,
  chosen = 1;
end;
if nargin < 3 | length(beta_list)==0,
  beta_list = linspace(0,1,11);
end;
if nargin < 4,
  toplot = 0;
end;

[xvalid] = clientd_split(expe);

for i=1:length(beta_list),
  fprintf(1,'|');
  [myout{i},epc_cost] = fusion_clientd_check(xvalid, 1, 'F-norm',beta_list(i));
end;
    
%plot tuned results
if toplot,
  mysigns={'kd-', 'b.-','bo-','bx-','r.-','ro-','rx-','g.-','go-','gx-','k.-','ko-','kx-'};
  for i=1:11, myleg{i}=num2str(beta_list(i));cfg.res{i}.eva = myout{i}.epc.eva; end;
  plot_all_epc(epc_cost(:,1),myleg,mysigns, cfg, 1:11);
end;  

%pick the optimal one
w=abs( linspace(-0.8,0.8,11));     w=1-w;    w = w / sum(w);
tmp_ = [];     for i=1:11,      tmp_ = [tmp_;myout{i}.epc.eva.hter_apri];     end;
[tmp_, index] = min(tmp_ * w');
weight = beta_list(index);