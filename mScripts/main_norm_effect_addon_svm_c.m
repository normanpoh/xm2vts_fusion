%load configurations
%cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;
load norm_expe_addon_svm_c.mat com epc_cost
%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
            	end;end;

			%zero-mean unit variance normalisation
			%nexpe{2} = zmuv_norm(nexpe{1});

			%use margin scores	
			%nexpe{3} = convert2margin_scores(nexpe{1});

			%for MLP output
			%for d=1:2,for k=1:2,
			%	nexpe{4}.dset{d,k} = nexpe{1}.dset{d,k};
			%	for ex=1:2,
			%		if (mlp{p,s,r}(ex)),
			%			fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
			%			nexpe{4}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex));
            %		end;
            %	end;
            %end;end;

			%second-level fusion
			for d=1:2,for k=1:2,
				new_expe.dset{d,k} = [nexpe{1}.dset{d,k}];% nexpe{2}.dset{d,k} nexpe{3}.dset{d,k} nexpe{4}.dset{d,k} ];%nexpe{5}.dset{d,k} trans.dset{d,k}];
            end;end;

			chosen = [1:2];
            Ni = size(nexpe{1}.dset{1,1},1); Nc = size(nexpe{1}.dset{1,2},1);

            %c_value = [Nc Ni] ./ (Nc+Ni) * 10;
            %[com{1}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen, [], [], c_value);

            %c_value = [Ni Nc] ./ (Nc+Ni) * 10;
            %[com{2}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen, [], [], c_value);

            %c_value = 10;
            %[com{3}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen, [], [], c_value);

            %by cross-validation to find the C values
            ratio = [0.5 0.5];
            cv = [100 1; 10 1; 1 1; 1 10 ; 1 100];
            cv = [cv; cv * 10];
            tmp{1} = splitdata(nexpe{1}.dset{1,1}, ratio);
            tmp{2} = splitdata(nexpe{1}.dset{1,2}, ratio);
            tdata.dset{1,1} = tmp{1}{1};
            tdata.dset{1,2} = tmp{2}{1};
            tdata.dset{2,1} = tmp{1}{2};
            tdata.dset{2,2} = tmp{2}{2};                

            for c=1:size(cv, 1),
                [tmp_com{c}, epc_cost] = fusion_svm(tdata, chosen, [], [], cv(c,:));
            end;
            for c=1:size(cv, 1),
                tot_hter_apri(c) = sum(tmp_com{c}.epc.eva.hter_apri);
                hter_apri(c) = tmp_com{c}.epc.eva.hter_apri(6);
            end;
            [tmp_ , index]= min(tot_hter_apri);
            c_value = cv(index(1),:);
            [com{4}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen, [], [], c_value);
            com{4}{p,s,r}.c_value = c_value;
            %chosen = [3:4];
            %[com{2}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen);

            %chosen = [5:6];
            %[com{3}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen);
            
			%chosen = [7:8];
            %[com{4}{p,s,r}, epc_cost] = fusion_svm(new_expe, chosen);

			%free the memory
			for i=1:size(com,2),
				com{i}{p,s,r}.dset={};
                com{i}{p,s,r}.net={};
			end;

		end;
	end;
end;

save norm_expe_addon_svm_c.mat com epc_cost

%overall performance
for i=1:size(com,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(com{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(com{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(com{i},[2 3]); %intramodal
end;

signs = {'kx-',  'kd-','ko-','k*-'};
leg = {'svm CI prior','svm imbalanced prior','svm common c','svm cross-valid'};

list = 1:size(com,2);
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);

c=1;
b=[1 3];
hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});
