%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%Start experiment
p=1;s=1;r=1;

for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
            	        end;end;

			chosen_list = [1 2; 3 4];
                        for i=1:2,
                                chosen = chosen_list(i,:);
				c=0;
				for d=1:2,for k=1:2,
					c=c+1;
					tmp_ = corrcoef(new_expe.dset{d,k}(:,chosen));
					tmp{i}(c) = tmp_(1,2);
				end;end;
				corr{s,i} = [corr{s,i}; tmp{i}];
                        end;
		end;
	end;
end;

hold off;
[f,x] = ksdensity([corr{1,1}(:,1); corr{1,1}(:,3)]);
plot(x,f,'b');
[f,x] = ksdensity([corr{2,1}(:,1); corr{2,1}(:,3)]);
hold on;
plot(x,f,'r');

set(gca, 'Fontsize', 16);
hold off;
[f,x] = ksdensity([corr{1,1}(:,2); corr{1,1}(:,4)]);
plot(x,f,'r--');
[f,x] = ksdensity([corr{2,1}(:,2); corr{2,1}(:,4)]);
hold on;
plot(x,f,'b');
xlabel('Class-Dependent Correlations');
ylabel('Likelihood');
legend('Multimodal fusion','Intramodal fusion')
grid on;
print('-depsc2', '../Pictures/xm2vts_summary.eps');

set(gca, 'Fontsize', 14);
hold off;
[f,x] = ksdensity(corr{1,1}(:,2));
plot(x,f,'r--');
hold on;
[f,x] = ksdensity(corr{1,1}(:,4));
plot(x,f,'r-.');
[f,x] = ksdensity(corr{2,1}(:,2));
plot(x,f,'b-');
[f,x] = ksdensity(corr{2,1}(:,4));
plot(x,f,'b:');
xlabel('Class-Dependent Correlations');
ylabel('Frequency');
legend('Impostor','Client')


min([corr{1,1}, corr{1,2}; corr{2,1}, corr{2,2}; corr{3,1}, corr{3,2}])
max([corr{1,1}, corr{1,2}; corr{2,1}, corr{2,2}; corr{3,1}, corr{3,2}])