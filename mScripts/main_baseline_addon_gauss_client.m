cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

load baseline baseline res_bes epc_cost
initialise;
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

p=1;b=6; %demo
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('Experiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    
    for d=1:2, for k=1:2,
	expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    expe.label = data{p}.label;
    
    %for MLP output
    if ismlp{p}(b),
      for d=1:2,for k=1:2,
	  nexpe{1}.dset{d,k} = expe.dset{d,k};
	  ex=1;
	  nexpe{1}.dset{d,k}(:,ex) = tanh_inv(expe.dset{d,k}(:,ex));
	end;
      end;
      
      %filter the bad ones
      nexpe{2} = nexpe{1};
      for k=1:2,
	d=1;ex=1;
	selected = find(nexpe{2}.dset{d,k}(:,ex)>30);
	nexpe{2}.dset{d,k}(selected)=[];
	selected = find(nexpe{2}.dset{d,k}(:,ex)<-30);
      	nexpe{2}.dset{d,k}(selected)=[];
      end;

      param = VR_analysis(nexpe{2}.dset{1,1},nexpe{2}.dset{1,2});      

      %now replace bad values with a reasonable one
      nexpe{3} = nexpe{1};
      
      for d=1:2, for k=1:2,
	  ex=1;
	  selected = find(nexpe{3}.dset{d,k}(:,ex)>30);
	  nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_C(ex),size(selected,1),1);
	  selected = find(nexpe{3}.dset{d,k}(:,ex)<-30);
	  nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_I(ex),size(selected,1),1);
	end;
      end;
    else
      nexpe{3} = expe;
    end;
    
    %client-dependent normalisation
    [baseline{1}{p,b}, epc_cost] = fusion_baseline(expe);
    %[baseline{2}{p,b}, epc_cost] = fusion_baseline_Fratio(expe);
    %[baseline{3}{p,b}, epc_cost] = fusion_baseline_ZNorm(expe);
    %[baseline{4}{p,b}, epc_cost] = fusion_baseline_EERNorm(expe);
    
    
    %Z-norm
    for d=1:2,for k=1:2,
	%texpe.dset{d,k} = [expe.dset{d,k}, baseline{2}{p,b}.dset{d,k}, ...
	texpe.dset{d,k} = [nexpe{3}.dset{d,k}, baseline{2}{p,b}.dset{d,k}, ...
			   baseline{3}{p,b}.dset{d,k}];
      end;
    end;
    %draw_empiric(texpe.dset{1,1},texpe.dset{1,2});
    %draw_empiric(texpe.dset{2,1},texpe.dset{2,2});

    %Z-norm
    chosen=[1 3];
    mix_bline{1}{p,b} = fusion_wsum_brute_nonorm(texpe, chosen); %mix_bline{1}{p,b} = fusion_svm(texpe, chosen); 
    while  mean(mix_bline{1}{p,b}.epc.dev.hter)>0.5,
      mix_bline{1}{p,b} = fusion_wsum_brute_nonorm(texpe, chosen);%fusion_svm(texpe, chosen);
    end;

    mix_bline{2}{p,b} = fusion_wsum_brute_nonorm(texpe, chosen,[0.5 0.5]);

    %F-norm
    chosen=[1 2];
    mix_bline{3}{p,b} = fusion_wsum_brute_nonorm(texpe, chosen);%fusion_svm(texpe, chosen);
    %mix_bline{3}{p,b} = fusion_svm(texpe, chosen); 
    while  mean(mix_bline{1}{p,b}.epc.dev.hter)>0.5,
      %mix_bline{3}{p,b} = fusion_svm(texpe, chosen); 
      mix_bline{3}{p,b} = fusion_wsum_brute_nonorm(texpe, chosen);%fusion_svm(texpe, chosen);
    end;

    mix_bline{4}{p,b} = fusion_wsum_brute_nonorm(texpe, chosen,[0.5 0.5]);
    
    hold off;
    plot(epc_cost(:,1), baseline{1}{p,b}.epc.eva.hter_apri*100,'bx-');
    hold on;
    plot(epc_cost(:,1), baseline{2}{p,b}.epc.eva.hter_apri*100,'b*-');
    plot(epc_cost(:,1), baseline{3}{p,b}.epc.eva.hter_apri*100,'bo--');
    plot(epc_cost(:,1), mix_bline{1}{p,b}.epc.eva.hter_apri*100,'g*:');
    plot(epc_cost(:,1), mix_bline{2}{p,b}.epc.eva.hter_apri*100,'go:');
    plot(epc_cost(:,1), mix_bline{3}{p,b}.epc.eva.hter_apri*100,'r*-.');
    plot(epc_cost(:,1), mix_bline{4}{p,b}.epc.eva.hter_apri*100,'ro-.');
    legend('no norm', 'Z-norm', 'F-Norm', 'mix-Z,svm','mix-Z,mean','mix-F,svm','mix-F,mean'); 
    drawnow;
    
    %draw_empiric(texpe.dset{1,1},texpe.dset{1,2});
  end;
end;

save mix_bline_wnorm baseline mix_bline epc_cost
load mix_bline_wnorm baseline mix_bline epc_cost
clear res_bes;
for i=1:size(baseline,2),
  [res_bes{i}, pNI,pNC] = epc_global_baseline(baseline{i});
end;
for i=[1:length(mix_bline)],
  j=3+i;
  [res_bes{j}, pNI,pNC] = epc_global_baseline(mix_bline{i});
end;
mycfg.res =res_bes;

leg ={'no norm', 'F-norm', 'Z-Norm', 'mix-Z,svm','mix-Z,mean','mix-F,svm','mix-F,mean'};
signs = {'bx-','b*-','bo--','g*:','go:','r*-.','ro-.'};
list =1:7;
list =[1 3 4 5];
list =[1 2 6 7];
plot_all_epc(epc_cost(:,1),leg,signs, mycfg, list);
plot_all_roc(epc_cost(:,1),leg,signs, mycfg, list);

%plot filtered list
pb_list=[];
t=0;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    t=t+1;
    txt = sprintf('Experiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    pb_list=[pb_list; p b];
  end;
end;

type =[dat.P{1}.type dat.P{2}.type];
[[1:t]' type' pb_list ]
%face:
face = find(type==1);
speech = find(type==0);
faceNoFH = setdiff(face,1);

clear cfg;
for j=1:size(baseline,2),
  [cfg{1}.res{j}, pNI,pNC] = epc_global_baseline_custom(baseline{j},pb_list(face,:),NI,NC);
  [cfg{2}.res{j}, pNI,pNC] = epc_global_baseline_custom(baseline{j},pb_list(faceNoFH,:),NI,NC);
  [cfg{3}.res{j}, pNI,pNC] = epc_global_baseline_custom(baseline{j},pb_list(speech,:),NI,NC);
  [cfg{4}.res{j}, pNI,pNC] = epc_global_baseline_custom(baseline{j},pb_list,NI,NC);
end;
for j=[1:length(mix_bline)],
  i=3+j;
  [cfg{1}.res{i}, cfg{1}.pNI, cfg{1}.pNC] = epc_global_baseline_custom(mix_bline{j},pb_list(face,:),NI,NC);
  [cfg{2}.res{i}, cfg{2}.pNI, cfg{2}.pNC] = epc_global_baseline_custom(mix_bline{j},pb_list(faceNoFH,:),NI,NC);
  [cfg{3}.res{i}, cfg{3}.pNI, cfg{3}.pNC] = epc_global_baseline_custom(mix_bline{j},pb_list(speech,:),NI,NC);
  [cfg{4}.res{i}, cfg{4}.pNI, cfg{4}.pNC] = epc_global_baseline_custom(mix_bline{j},pb_list,NI,NC);
end;

list =1:7;
list =[1 3 4 5]; %Z-norm
list =[1 2 6 7]; %F-norm
list =[1 2 3 4 5];

cfgname = {'face','faceNoFH','speech','all'};
for i=[1 3],
  figure(i);
  plot_all_epc(epc_cost(:,1),leg,signs, cfg{i}, list);pause;
  %print('-depsc2', ['../Pictures/main_baseline_addon_gauss_client_epc_cfg_',cfgname{i},'.eps']);
end;
for i=[1 3],%1:4,
  %figure(i);
  subplot(2,2,i);
  list =[1 3 4 5]; %Z-norm
  plot_all_epc(epc_cost(:,1),leg,signs, cfg{i}, list,10);
  subplot(2,2,i+1);
  list =[1 2 6 7]; %F-norm
  plot_all_epc(epc_cost(:,1),leg,signs, cfg{i}, list,10);
end;

list =[1 3 4 5]; %Z-norm
for i=[1 3],
  figure(i);
  plot_all_epc(epc_cost(:,1),leg,signs, cfg{i}, list);
  pause;
  print('-depsc2', ['../Pictures/main_baseline_addon_gauss_client_Z_epc_',num2str(i),'.eps']);
  %plot_all_roc(epc_cost(:,1),leg,signs, cfg{i}, list);
  plot_all_roc(epc_cost(:,1),leg,signs, cfg{i}, list,[],[],1);
  pause;
  %print('-depsc2', ['../Pictures/main_baseline_addon_gauss_client_Z_roc_',num2str(i),'.eps']);
  print('-depsc2', ['../Pictures/main_baseline_addon_gauss_client_Z_det_',num2str(i),'.eps']);
end;

list =[1 2 6 7]; %F-norm
for i=[1 3],
  figure(i);  
  plot_all_epc(epc_cost(:,1),leg,signs, cfg{i}, list);
  pause;
  print('-depsc2', ['../Pictures/main_baseline_addon_gauss_client_F_epc_',num2str(i),'.eps']);
  plot_all_roc(epc_cost(:,1),leg,signs, cfg{i}, list);
  pause;
  print('-depsc2', ['../Pictures/main_baseline_addon_gauss_client_F_roc_',num2str(i),'.eps']);
end;

plot_all_roc(epc_cost(:,1),leg,signs, cfg, list);

b = [3 5];
c=3;
hter_significant_plot(cfg{c}.res{b(1)}, cfg{c}.res{b(2)}, pNI,pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

b = [1 4];
c=1;
hter_significant_plot(cfg{c}.res{b(1)}, cfg{c}.res{b(2)}, pNI,pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

print('-depsc2', '../Pictures/Z_F_norm_pooled.eps');
