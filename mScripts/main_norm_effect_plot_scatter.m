for p=1:2, %over two protocols
	[data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
	[data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
			end;end;


			%zero-mean unit variance normalisation
			nexpe{2} = zmuv_norm(nexpe{1});

			%use margin scores	
			nexpe{3} = convert2margin_scores(nexpe{1});


			%for MLP output
			for d=1:2,for k=1:2,
				nexpe{4}.dset{d,k} = nexpe{1}.dset{d,k};
				for ex=1:2,
					if (mlp{p,s,r}(ex)),
						fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
						%nexpe{4}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex), 1/0.8,0.4);
						nexpe{4}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex));
						%filter extreme values:
					end;
				end;
				for ex=1:2,
					index = find (abs(nexpe{4}.dset{d,k}(:,ex)) > 30);
					nexpe{4}.dset{d,k}(index,:) = [];
					nexpe{1}.dset{d,k}(index,:) = [];
				end;
			end;end;

		

			%plot here
			for i=[1 4];
				figure(i); hold off; set(gca, 'Fontsize', 12); 
				draw_empiric(nexpe{i}.dset{1,1},nexpe{i}.dset{1,2});
				xlabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,1)});
				ylabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,2)});
	
				param = VR_analysis(nexpe{i}.dset{1,1},nexpe{i}.dset{1,2});
				draw_theory([1 2], param, [0.5 0.5]);
				%legend('Client', 'Impostor', 'Client Gaussian', 'Imposter Gaussian', 'Client Mean', 'Impostor Gaussian Mean');

				c=0;
				for d=1:2,for k=1:2,
					c=c+1;
					out{i}(:,:,c) = corrcoef(nexpe{i}.dset{d,k});
				end;end;
				corr_out=mean(out{i},3)
				txt = sprintf('corr = %1.3f', corr_out(1,2));
				title(txt);
				fname = sprintf('../Pictures/norm_scatter_inv_%d_%d_%d_%02d.eps', i, p,s,r);
				
				print('-depsc2', fname);
			end;
			pause;
		end;
	end;
end;