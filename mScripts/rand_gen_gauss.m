function rand_gen_gauss(mu1,cov1,N)

d = length(mu1);
z = randn(N, d);                    %  N x d, 
meanz = mean(z)';                   %  d x 1
covz = cov(z);                      %  1 x d
meanz2 = [meanz meanz];             %  d x d
mu11 = mu1'; 
mu1 = [mu11 mu11];                  %  d x d

[row, col] = size(z');
[row1, col1] = size(meanz2);

while col1 ~= col
    meanz2 = [meanz2 meanz];
    mu1 = [mu1 mu11];
    col1 = col1 + 1;
end

[zvects, zeigs] = eig(covz);        % eigenvectors = d x d , eigenvalues = d x d, (lambdas=diagonals)
[x1vects, x1eig] = eig(cov1);

yp = (zvects * zeigs ^ (-.5))'*z'- meanz2;            % A Whitening Transform for zero means correction
x1 = (((x1vects * x1eig^(-.5))')^-1) * yp + mu1;      % d x N, the inverse Whitening to center the dist. to means 

x1p = x1';
meanx1 = mean(x1p);                         % d x d
covx1 = cov(x1p);

figure(1)
hold on
plot(x1(1,:),x1(2,:),'.r')
title('2 dimensional gaussian samples'),
