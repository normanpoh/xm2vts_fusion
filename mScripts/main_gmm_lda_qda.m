cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

load main_gmm_lda_qda com
p=1;s=1;r=1;

for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      %print remarks
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);
      
      %load score files
      for d=1:2,for k=1:2,
	  nexpe.dset{d,k} = data{p}.dset{d,k}(:,b);
	end;
      end;
      %Quadratic discriminant analysis
      %[com{1}{p,s,r}, epc_cost ] = fusion_gmm(nexpe, [1 2], 1, [],0);
      %draw_empiric(nexpe.dset{1,1}, nexpe.dset{1,2});

      %Linear discriminant analysis
      %bayesS(1).sigma = (com{1}{p,s,r}.bayesS(1).sigma + com{1}{p,s,r}.bayesS(2).sigma)/2;
      %bayesS(2).sigma = bayesS(1).sigma;
      %bayesS(1).mu = com{1}{p,s,r}.bayesS(1).mu;
      %bayesS(2).mu = com{1}{p,s,r}.bayesS(2).mu;
      %bayesS(1).apriories = com{1}{p,s,r}.bayesS(1).apriories;      
      %bayesS(2).apriories = com{1}{p,s,r}.bayesS(2).apriories;
      %bayesS(1).weight = com{1}{p,s,r}.bayesS(1).weight;
      %bayesS(2).weight = com{1}{p,s,r}.bayesS(2).weight;
      %[com{2}{p,s,r}, epc_cost ] = fusion_gmm(nexpe, [1 2], 1,bayesS,0);
    
      com{3}{p,s,r} = fusion_wsum_brute_raw(nexpe,[1 2]);

      com{1}{p,s,r}.dset = {};
      com{2}{p,s,r}.dset = {};      
      com{3}{p,s,r}.dset = {};      
    end;
  end;
end;

save main_gmm_lda_qda_inv com

