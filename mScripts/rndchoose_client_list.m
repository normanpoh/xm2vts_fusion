function [com] = rndchoose_client_list(expe, chosen, client_list, selected_id)

if (nargin < 2|length(chosen)==0),
  chosen = 1:size(expe.dset{1,1},2);%use all available expert scores
end;

%if (nargin < 3 & length(n_clients)==0),
%  n_clients = 50;
%end;

if (nargin < 4),
  selected_id = [];
end;

%get the model label
model_ID = unique(expe.label{1,1});

if length(selected_id) == 0,
  selected_id = randperm(size(model_ID,1));
  selected_id = model_ID(selected_id);
end;

%go through each id
for d=1:2,for k=1:2,
    out{d,k} = [];%zeros(,length(chosen));
    label{d,k} = [];%zeros(,length(chosen));
  end;
end;

%perform client-dependent processing
%fprintf(1, 'Processed ID:\n');
for id=client_list,
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      selected = find(expe.label{d,k} == selected_id(id));
      if (size(selected,1)==0),
        error([ 'size is zero for id ', num2str(id)]);
      end;
      tmp{d,k} = expe.dset{d,k}(selected,chosen);
    end;
  end;

  for d=1:2,for k=1:2,
      out{d,k} = [out{d,k};  tmp{d,k}];
      label{d,k} = [label{d,k}; selected_id(id) * ones(size(tmp{d,k},1),1)];
    end;
  end;

  %fprintf(1, '.%d', id);
end;
com.dset = out;
com.label = label;
