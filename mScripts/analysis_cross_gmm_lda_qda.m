cd /home/learning/norman/xm2vts_fusion/mScripts

load  main_gmm_lda_qda com
ncom = com;

load  main_gmm_lda_qda_inv com
ncom{3} = com{1};
ncom{4} = com{2};

load norm_expe_addon_gmm_cv.mat com
ncom{5} = com{1};
ncom{6} = com{4};

load main_fixed_rule.mat com epc_cost
ncom{7} = com{5};

load  main_gmm_lda_qda_inv com
ncom{8} = com{3}; % inv, wsum

load  main_gmm_lda_qda_inv_reg com
ncom{9} = com{1}; % inv,lda-qda-regularised client only
ncom{10} = com{2}; % inv,lda-qda-regularised, client and impostor
ncom{11} = com{3}; % inv,gmm 2 components
ncom{12} = com{4}; % inv,RDA

load  main_gmm_lda_qda_inv com
ncom{13} = com{5}; % inv-recentred, QDA
ncom{14} = com{6}; % inv-recentred, LDA

%overall performance
for i=1:size(ncom,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(ncom{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(ncom{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(ncom{i},[2 3]); %intramodal
end;

signs = {'bs-','bd-','rs--','rd--','go-','go--', 'k*-', 'k*--','md--','md-','gs-','ms--','ys-','yd--'};
leg = {'orig,QDA', 'orig,LDA', 'inv,QDA', 'inv,LDA', 'orig,GMM', 'inv,GMM','orig,wsum','inv,wsum','inv,reg','inv,reg2','orig,GMM(2)','inv,RDA'};
leg = {leg{:}, 'inv-recentred,QDA','inv-recentred,lDA'};
%list = [1 3 2 4 5 6]
list = [1 2 4 5 6 7 8 10 12 13 14]
figure(1);
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
print('-depsc2', '../Pictures/gmm_lda_qda_reg.eps');
plot_all_roc(epc_cost,leg,signs, out.cfg{1}, list);
print('-depsc2', '../Pictures/gmm_lda_qda_reg_roc.eps');

list = [1 2 5 7];
figure(1);
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
print('-depsc2', '../Pictures/gmm_lda_qda.eps');
figure(2);
plot_all_roc(epc_cost,leg,signs, out.cfg{1}, list);
print('-depsc2', '../Pictures/gmm_lda_qda_roc.eps');

figure(2);
c=1;
b=[10 12];
hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});
