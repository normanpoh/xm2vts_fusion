function [com, epc_cost] = fusion_gauss_client(expe, chosen, param, reliance, method, toplot)
%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
        chosen = [1:2];
end;

if (nargin < 4 | length(reliance)==0),
  reliance = 0; %use common client mu
end;

if (nargin < 5),
  method = 'ca(mu)-qda';
end;

if (nargin < 6),
  toplot = 0;
end;

%go through each id
for d=1:2,for k=1:2,
	com.dset{d,k} = zeros(size(expe.dset{d,k},1),1);
end;end;
for d=1:2,for k=1:2,
	expe.dset{d,k} = expe.dset{d,k}(:,chosen);
end;end;

%get global param
if (nargin < 3|length(param)==0),
  param = VR_analysis(expe.dset{1,1},expe.dset{1,2});
end;

%get the model label
model_ID = unique(expe.label{1,1});

%get client independent cov
count = 0;
commonID = [];
for id=1:size(model_ID,1),
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      if (size(index{d,k},1)==0),
        warning([ 'size is zero for id ', num2str(id)]);
      end;
      tmp{id}.dset{d,k} = expe.dset{d,k}(index{d,k},:);
    end;
  end;
end;

for id=1:size(model_ID,1),
  d=1;
  for k=1:2,
    if (size(tmp{id}.dset{d,k},1)==0),
      error('Cannot do anything, sorry!');
    elseif (size(tmp{id}.dset{d,k},1)==1),
      %with one example, we cannot calculate cov
      %but we can calculate the mean at least!
      local{id}.bayesS(k).sigma = eye(2);
      %mu is the example itself!
      local{id}.bayesS(k).mu = tmp{id}.dset{d,k}';
      local{id}.status(k) = 1;
    elseif (size(tmp{id}.dset{d,k},1)==2),
      %with two examples, we cannot calculate cov but we can calculate
      %the mean at least AND variance!
      local{id}.bayesS(k).sigma = cov(tmp{id}.dset{1,k});
      %reset the covariance elements
      local{id}.bayesS(k).sigma(1,2) = 0;
      local{id}.bayesS(k).sigma(2,1) = 0;
      local{id}.bayesS(k).mu = mean(tmp{id}.dset{1,k})';
      local{id}.status(k) = 2;
    else
      %with 3 or more examples, we can estimate cov
      local{id}.bayesS(k).sigma = cov(tmp{id}.dset{1,k});
      local{id}.bayesS(k).mu = mean(tmp{id}.dset{1,k})';
      local{id}.status(k) = 3;
    end;
    local{id}.bayesS(k).weight = 1;
  end;
end;

%visualise
if (toplot),
  for id=1:size(model_ID,1),
    %if (local{id}.status(2) == 3),
    %if (size(tmp{id}.dset{1,2},1)>2),
    if rank(local{id}.bayesS(2).sigma)>=2,
      id
      draw_empiric(tmp{id}.dset{1,1}, tmp{id}.dset{1,2});
      draw_theory_bayesS(local{id}.bayesS);
    end;
  end;
  
  pause;
end;

okay_id=[];
for id=1:size(model_ID,1),
  d=1;
  for k=1:2,
    if (size(tmp{id}.dset{d,k},1)==0),
      %no info available at all
      if (k==1),
	local{id}.bayesS(k).sigma = param.cov_I;
	local{id}.bayesS(k).mu = param.mu_I';
      else %client
	local{id}.bayesS(k).sigma = param.cov_C;
	local{id}.bayesS(k).mu = param.mu_C';
      end;
    
    elseif (size(tmp{id}.dset{d,k},1)==1),
      %with one example, we cannot calculate cov
      %but we can calculate the mean at least!
      if (k==1),
	local{id}.bayesS(k).sigma = param.cov_I;
	local{id}.bayesS(k).mu = param.mu_I';
      end;
      %mu is the example itself!
      local{id}.bayesS(k).mu = tmp{id}.dset{1,k}';
    
    elseif (size(tmp{id}.dset{d,k},1)==2),
      %with two examples, we cannot calculate cov but we can calculate
      %the mean at least AND variance!
      local{id}.bayesS(k).sigma = cov(tmp{id}.dset{1,k});
      %reset the covariance elements
      local{id}.bayesS(k).sigma(1,2) = 0;
      local{id}.bayesS(k).sigma(2,1) = 0;
    
      local{id}.bayesS(k).mu = mean(tmp{id}.dset{1,k})';
    else
      okay_id=[okay_id id];
      %no change, i.e.,:
      %with 3 or more examples, we can estimate cov
      %local{id}.bayesS(k).sigma = cov(tmp{id}.dset{1,k});
      %local{id}.bayesS(k).mu = mean(tmp{id}.dset{1,k})';
    end;
    local{id}.bayesS(k).weight = 1;
  end;
end;
%common.bayesS(1).sigma ./ CD.bayesS(1).sigma
%common.bayesS(2).sigma

%perform client-dependent processing
fprintf(1, 'Processed ID:\n');
% gamma_mu = 1;
% gamma_qda = 1;
% for id=1:size(model_ID,1),
% 
%   %Quadratic discriminant analysis
%   bayesS(1).mu = local{id}.bayesS(1).mu;
%   bayesS(1).sigma = local{id}.bayesS(1).sigma;
%   bayesS(2).mu = gamma_mu * local{id}.bayesS(2).mu + (1-gamma_mu)*CD.bayesS(2).mu;
%   bayesS(2).sigma = gamma_qda * local{id}.bayesS(2).sigma + (1-gamma_qda)*CD.bayesS(2).sigma;
% 
%   %linear discriminant analysis
%   bayesS(1).sigma = (bayesS(1).sigma + bayesS(2).sigma)/2;
%   bayesS(2).sigma = bayesS(2).sigma;
%   
%   %irrelevant 
%   bayesS(1).apriories = 1;
%   bayesS(2).apriories = 1;
%   bayesS(1).weight = 1;
%   bayesS(2).weight = 1;
% 
%   [tcmp{id}, epc_cost ] = fusion_gmm(tmp{id}, [1 2], 1,bayesS,0);
%   %draw_empiric(tmp{id}.dset{2,1},tmp{id}.dset{2,2});
%   for d=1:2,for k=1:2,
%       %combine the fused scores
%       index{d,k} = find(expe.label{d,k} == model_ID(id));
%       com.dset{d,k}(index{d,k},1) = tcmp{id}.dset{d,k};
%     end;
%   end;
%   com.CD_bayesS{id} = bayesS;
%  
%  fprintf(1, '.%d', id);
%end;
%com.dset = out;


for id=1:size(model_ID,1),

  bayesS = local{id}.bayesS;
  switch lower(method)
  case{'cd-qda'}
    %client-dependent quadratic discriminant analysis
    %do nothing
  case{'ca(mu)-qda'}
    %client-adapted MU quadratic discriminant analysis
    bayesS(2).mu = reliance * bayesS(2).mu + (1-reliance) * param.mu_C';
    bayesS(2).sigma = param.cov_C;
  case{'cd-lda'}
    %client-dependent linear discriminant analysi
    bayesS(1).sigma = (bayesS(1).sigma + bayesS(2).sigma)/2;
    bayesS(2).sigma = bayesS(2).sigma;
  case{'ca(mu)-lda'}
    %client-independent linear discriminant analysis
    bayesS(2).mu = reliance * bayesS(2).mu + (1-reliance) * param.mu_C';
    bayesS(2).sigma = param.cov_C;
    bayesS(1).sigma = (bayesS(1).sigma + bayesS(2).sigma)/2;
    bayesS(2).sigma = bayesS(2).sigma;
  otherwise
    error('Option does not exist');
  end;
  [tcmp{id}, epc_cost ] = fusion_gmm(tmp{id}, [1 2], 1,bayesS,0);
  %draw_empiric(tmp{id}.dset{2,1},tmp{id}.dset{2,2});
  for d=1:2,for k=1:2,
      %combine the fused scores
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      com.dset{d,k}(index{d,k},1) = tcmp{id}.dset{d,k};
    end;
  end;
  com.CD_bayesS{id} = local{id}.bayesS;
  fprintf(1, '.%d', id);
end;

com.reliance = reliance;
%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

   
