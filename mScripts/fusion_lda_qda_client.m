function [com, epc_cost] = fusion_lda_qda_client(expe, chosen, toplot)
%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
        chosen = [1:2];
end;

if (nargin < 3),
        toplot = 0;
end;

%go through each id
for d=1:2,for k=1:2,
	com.dset{d,k} = zeros(size(expe.dset{d,k},1),1);
end;end;

%get the model label
model_ID = unique(expe.label{1,1});

%get client independent cov
count = 0;
commonID = [];
for id=1:size(model_ID,1),
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      if (size(index{d,k},1)==0),
        warning([ 'size is zero for id ', num2str(id)]);
      end;
      tmp{id}.dset{d,k} = expe.dset{d,k}(index{d,k},chosen);
    end;
  end;
end;

%collect global info:
param = VR_analysis(expe.dset{1,1},expe.dset{1,2});

common.bayesS(1).mu = param.mu_I';
common.bayesS(2).mu = param.mu_C';
common.bayesS(1).sigma = param.cov_I;
common.bayesS(2).sigma = param.cov_C;

for id=1:size(model_ID,1),
  d=1;
  for k=1:2,
    if (size(tmp{id}.dset{d,k},1)==0),
      %no info available at all
      local{id}.bayesS(k).sigma = zeros(2);
      local{id}.bayesS(k).mu = [0 0]';
      local{id}.status(k) = 0;
    elseif (size(tmp{id}.dset{d,k},1)==1),
      %with one example, we cannot calculate cov
      %but we can calculate the mean at least!
      local{id}.bayesS(k).sigma = zeros(2);
      %mu is the example itself!
      local{id}.bayesS(k).mu = tmp{id}.dset{d,k}';
      local{id}.status(k) = 1;
    elseif (size(tmp{id}.dset{d,k},1)==2),
      %with two examples, we cannot calculate cov but we can calculate
      %the mean at least AND variance!
      local{id}.bayesS(k).sigma = cov(tmp{id}.dset{1,k});
      %reset the covariance elements
      local{id}.bayesS(k).sigma(1,2) = 0;
      local{id}.bayesS(k).sigma(2,1) = 0;
      local{id}.bayesS(k).mu = mean(tmp{id}.dset{1,k})';
      local{id}.status(k) = 2;
    else
      %with 3 or more examples, we can estimate cov
      local{id}.bayesS(k).sigma = cov(tmp{id}.dset{1,k});
      local{id}.bayesS(k).mu = mean(tmp{id}.dset{1,k})';
      local{id}.status(k) = 3;
    end;
    local{id}.bayesS(k).weight = 1;
  end;
end;

%visualise
for id=1:size(model_ID,1),
  if (local{id}.status(2) == 3),
    for k=1:2,
      common.bayesS(k).sigma = local{id}.bayesS(k).sigma;
    end;
    %if (size(tmp{id}.dset{1,2},1)>2),
    draw_theory_bayesS(local{id}.bayesS);
  end;
end;
draw_empiric(expe.dset{1,1}, expe.dset{1,2});

%counter
stats.c0 = 0;
stats.c = [0 0 0];
stats.m = 0;
for id=1:size(model_ID,1),
  %analyse the impostor
  stats.sigma_I(:,:,id) = local{id}.bayesS(1).sigma;
  stats.mu_I(:,id) = local{id}.bayesS(1).mu;
  %analyse the client
  if(local{id}.status(2) == 0)
    stats.c0 = stats.c0 + 1;
  end;
  if(local{id}.status(2) == 1)
    stats.c(1) = stats.c(1) + 1;
  end;
  %process the sigma
  if(local{id}.status(2) == 2)
    stats.c(2) = stats.c(2) + 1;
    stats.sigma_C{2}(:,:,stats.c(2)) = local{id}.bayesS(2).sigma;  
  end;
  if(local{id}.status(2) == 3)
    stats.c(3) = stats.c(3) + 1;
    stats.sigma_C{3}(:,:,stats.c(3)) = local{id}.bayesS(2).sigma;  
  end;
  %process the mean
  if(local{id}.status(2) >= 1)
    stats.m = stats.m + 1;
    stats.mu_C(:,stats.m) = local{id}.bayesS(2).mu;    
  end;
end;
%CD impostor info
CD.bayesS(1).sigma = mean(stats.sigma_I,3);
CD.bayesS(1).mu = mean(stats.mu_I,2);

gamma_rho = 0;
%CD client info
CD.bayesS(2).mu = mean(stats.mu_C,2);
if stats.c(3) == size(model_ID,1),
  %all sigma_C are in case 3
  CD.bayesS(2).sigma = mean(stats.sigma_C{3},3);
elseif stats.c(2) == size(model_ID,1),
  %all sigma_C are in case 2
  %no cov info at at all!
  CD.bayesS(2).sigma = mean(stats.sigma_C{2},3);
  %need to borrow rho from impostor distribution
  rho_I = CD.bayesS(1).sigma(1,2) / (sqrt(CD.bayesS(1).sigma(1,1)) * sqrt(CD.bayesS(1).sigma(2,2)));
  CD.bayesS(2).sigma(1,2) = (rho_I + gamma_rho) * sqrt(CD.bayesS(2).sigma(1,1))*sqrt(CD.bayesS(2).sigma(2,2));
  %copy to the other side
  CD.bayesS(2).sigma(2,1) = CD.bayesS(2).sigma(1,2);
else
  %some sigma_C are in case 2; some in case 3
  CD.bayesS(2).sigma = mean(stats.sigma_C{2},3) + mean(stats.sigma_C{3},3);
  CD.bayesS(2).sigma(1,1) = CD.bayesS(2).sigma(1,1)/2;
  CD.bayesS(2).sigma(2,2) = CD.bayesS(2).sigma(2,2)/2;
end;

for id=1:size(model_ID,1),
  d=1;
  for k=1:2,
    if (size(tmp{id}.dset{d,k},1)==0),
      %no info available at all
      local{id}.bayesS(k).sigma = common.bayesS(k).sigma;
      local{id}.bayesS(k).mu = common.bayesS(k).mu;
    elseif (size(tmp{id}.dset{d,k},1)==1),
      %with one example, we cannot calculate cov
      %but we can calculate the mean at least!
      local{id}.bayesS(k).sigma = common.bayesS(k).sigma;
      %mu is the example itself!
      %local{id}.bayesS(k).mu = tmp{id}.dset{1,k}';
    elseif (size(tmp{id}.dset{d,k},1)==2),
      %with two examples, we cannot calculate cov but we can calculate
      %the mean at least AND variance!
      local{id}.bayesS(k).sigma = cov(tmp{id}.dset{1,k});
      %reset the covariance elements
      local{id}.bayesS(k).sigma(1,2) = common.bayesS(k).sigma(1,2);
      local{id}.bayesS(k).sigma(2,1) = common.bayesS(k).sigma(2,1);
    %else
      %no change, i.e.,:
      %with 3 or more examples, we can estimate cov
      %local{id}.bayesS(k).sigma = cov(tmp{id}.dset{1,k});
      %local{id}.bayesS(k).mu = mean(tmp{id}.dset{1,k})';
    end;
    local{id}.bayesS(k).weight = 1;
  end;
end;
%common.bayesS(1).sigma ./ CD.bayesS(1).sigma
%common.bayesS(2).sigma

%perform client-dependent processing
fprintf(1, 'Processed ID:\n');
gamma_mu = 1;
gamma_qda = 1;
for id=1:size(model_ID,1),

  %Quadratic discriminant analysis
  bayesS(1).mu = local{id}.bayesS(1).mu;
  bayesS(1).sigma = local{id}.bayesS(1).sigma;
  bayesS(2).mu = gamma_mu * local{id}.bayesS(2).mu + (1-gamma_mu)*CD.bayesS(2).mu;
  bayesS(2).sigma = gamma_qda * local{id}.bayesS(2).sigma + (1-gamma_qda)*CD.bayesS(2).sigma;

  %linear discriminant analysis
  bayesS(1).sigma = (bayesS(1).sigma + bayesS(2).sigma)/2;
  bayesS(2).sigma = bayesS(2).sigma;
  
  %irrelevant 
  bayesS(1).apriories = 1;
  bayesS(2).apriories = 1;
  bayesS(1).weight = 1;
  bayesS(2).weight = 1;

  [tcmp{id}, epc_cost ] = fusion_gmm(tmp{id}, [1 2], 1,bayesS,0);
  %draw_empiric(tmp{id}.dset{2,1},tmp{id}.dset{2,2});
  for d=1:2,for k=1:2,
      %combine the fused scores
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      com.dset{d,k}(index{d,k},1) = tcmp{id}.dset{d,k};
    end;
  end;
  com.CD_bayesS{id} = bayesS;
  
  fprintf(1, '.%d', id);
end;
com.dset = out;

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

   
