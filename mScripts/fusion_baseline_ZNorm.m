function [com, epc_cost] = fusion_baseline_ZNorm(expe)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

%zero-mean unit variance normalisation
%model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
%model.std = std([expe.dset{1,1}; expe.dset{1,2}]);
%[expe.dset{1,1}, expe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
%[expe.dset{2,1}, expe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);

%get the model label
model_ID = unique(expe.label{1,1});

%go through each id
for d=1:2,for k=1:2,
	com.dset{d,k} = [];
end;end;

%perform client-dependent normalisation
for id=1:size(model_ID,1),

	%get the data set associated to the ID
	for d=1:2,for k=1:2,
	    index{d,k} = find(expe.label{d,k} == model_ID(id));
	    tmp.dset{d,k} = expe.dset{d,k}(index{d,k},:);
	end;end;
    
	%Apply Z normalisation on training and test sets, in a ID-dependnent manner
	com.mean_I(id) = mean(tmp.dset{1,1});
	com.std_I(id) = std(tmp.dset{1,1});

	[clientd{id}.dset{1,1}, clientd{id}.dset{1,2}] = normalise_scores(tmp.dset{1,1}, tmp.dset{1,2}, com.mean_I(id), com.std_I(id));
	[clientd{id}.dset{2,1}, clientd{id}.dset{2,2}] = normalise_scores(tmp.dset{2,1}, tmp.dset{2,2}, com.mean_I(id), com.std_I(id));

	%compute the fused scores
	for d=1:2,for k=1:2,
  	    com.dset{d,k}(index{d,k},:) = clientd{id}.dset{d,k};
	end;end;

end;

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);
