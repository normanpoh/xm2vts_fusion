function [com_dist, epc_cost] = fusion_DT_nonorm(expe, chosen, weight)
% [com_dist, epc_cost] = fusion_DT_nonorm(expe, chosen, weight)
% this function can perform weighted Decision Template

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

c= size(expe.dset{1,1},2);
if (nargin < 2),
	%use all experts
	chosen = [1:c];	
end;

if (nargin < 3),
	weight = ones(1, c) / c; %equal weights
end

%P(I|f1) ... P(I|fn) P(C|f1) ... P(C|fn)

for d=1:2,
    for k=1:2,
	expe.dset{d,k} = expe.dset{d,k}(:,chosen);
end;end;

%training on dev set
for k=1:2,
	template{k} = mean(expe.dset{1,k});
end;

%fuse the scores:
for d=1:2,
    for k=1:2,
	%distance of impostor - that of client	
	com_dist.dset{d,k} = norm_dist(template{1}, expe.dset{d,k}, weight) - norm_dist(template{2}, expe.dset{d,k}, weight) ;
	%	
    end;
end;

%epc curve
fprintf(1,'\nCalculating EPC');
[com_dist.epc.dev, com_dist.epc.eva, epc_cost]  = epc(com_dist.dset{1,1}, com_dist.dset{1,2}, com_dist.dset{2,1}, com_dist.dset{2,2}, n_samples,epc_range);

function dist = norm_dist(template, samples, weight)

[n,c] = size(samples);
%equal weight
%dist = 1 - sum((repmat(template,n,1 )- samples) .^ 2 ,2) / c;
%weighte sum of square
dist = sum((repmat(template,n,1 )- samples) .^ 2 ./ repmat(weight,n,1) ,2);
