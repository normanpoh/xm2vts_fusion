function epc_compare_print(epc1, epc2, epc_cost, eps_prefix, legendtext, print)

if (nargin < 6),
  print = 0;
end;
%load the config
initialise;

t=0;
for s=1:3, %over 3 different configurations
  for p=1:2, %over two protocols
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      t=t+1;
      txt = sprintf('[%d] Fusion of (%d %d %d) %s',t,p,s,r,[expe_labels.P{p}.seq{s}.row{r}]);
      hter_significant_plot(epc1{p,s,r}.epc, epc2{p,s,r}.epc, NI, NC, epc_cost);
      subplot(2,1,1);
      %legend(dat.P{p}.labels{expe.P{p}.seq{s}(r,1)}, dat.P{p}.labels{expe.P{p}.seq{s}(r,2)});
      legend(legendtext{1}, legendtext{2});
      title(txt);
      if (print),
	print('-depsc2', sprintf('../Pictures/%s_%d_%d_%d_epc.eps',epc_prefix,p,s,r));
      else
	pause; %pause for viewing
      end;
    end;
  end;
end;
