function ent = cal_KL_dep(expe, bayesS_dep, bayesS_indep,draw)
%cal_KL_dep(expe, bayesS_dep, bayesS_indep)
% expe is used only to find the suitable scales

if (nargin<4)
  draw=0;
end;

data = [expe.dset{1,1}; expe.dset{1,2}];
[xtesta1,xtesta2]=meshgrid( ...
    linspace(min(data(:,1)), max(data(:,1)), 100), ...
    linspace(min(data(:,2)), max(data(:,2)), 100) );
[na,nb]=size(xtesta1);
xtest1=reshape(xtesta1,1,na*nb);
xtest2=reshape(xtesta2,1,na*nb);
xtest=[xtest1;xtest2]';

%independent
for e=1:2,
  c=1;tmp1{e} = gmmb_pdf(xtest(:,e), bayesS_indep{e}(c).mu, bayesS_indep{e}(c).sigma, bayesS_indep{e}(c).weight );
  c=2;tmp2{e} = gmmb_pdf(xtest(:,e), bayesS_indep{e}(c).mu, bayesS_indep{e}(c).sigma, bayesS_indep{e}(c).weight );
end;
prob_indep{1} = tmp1{1} .* tmp1{2};
prob_indep{2} = tmp2{1} .* tmp2{2};
%ypred_indep = log(tmp2{1}+realmin) + log(tmp2{2}+realmin) ...
%	-(log(tmp1{1}+realmin) +log(tmp1{2}+realmin)) ;

%dependent
c=1;temp1 = gmmb_pdf(xtest, bayesS_dep(c).mu, bayesS_dep(c).sigma, bayesS_dep(c).weight );
c=2;temp2 = gmmb_pdf(xtest, bayesS_dep(c).mu, bayesS_dep(c).sigma, bayesS_dep(c).weight );
%ypred_dep = log(temp2+realmin)-log(temp1+realmin);
prob_dep{1} = temp1;
prob_dep{2} = temp2;

%normalise:
for k=1:2,
  prob_indep{k} = prob_indep{k} / sum(prob_indep{k});
  prob_dep{k} = prob_dep{k} / sum(prob_dep{k});
end;

%plot to check
if (draw),
  subplot(2,2,1);k=1;
  ypredmat=reshape(prob_dep{k},na,nb);
  hold off;
  contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
  title('Impostor,dep');
  
  subplot(2,2,2);k=2;
  ypredmat=reshape(prob_dep{k},na,nb);
  hold off;
  contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
  title('Client,dep');
  
  subplot(2,2,3);k=1;
  ypredmat=reshape(prob_indep{k},na,nb);
  hold off;
  contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
  title('Impostor,indep');
  
  subplot(2,2,4);k=2;
  ypredmat=reshape(prob_indep{k},na,nb);
  hold off;
  contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
  title('Client,indep');
end;

%calculate relative entropy
for k=1:2,
  ent(k) = sum(prob_indep{k} .* log(prob_indep{k}) - prob_indep{k} .* log(prob_dep{k}));
end;
