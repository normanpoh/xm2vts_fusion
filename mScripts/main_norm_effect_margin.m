%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;
%load norm_expe.mat com epc_cost

[all.dset{1,1}, all.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
[all.dset{2,1}, all.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);

for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = all.dset{d,k}(:,b);
			end;end;

			%use margin scores	
			nexpe{3} = convert2margin_scores(nexpe{1});
			i=3;
			figure(i); set(gca, 'Fontsize', 14);
			hold off;
			draw_empiric(nexpe{i}.dset{1,1},nexpe{i}.dset{1,2},0.5);
			xlabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,1)});
			ylabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,2)});
			range = -0.5:0.1:0.5;
			set(gca, 'XTick', range);
			set(gca, 'YTick', range);
			plot(zeros(size(range)), range,'k');
			plot(range, zeros(size(range)), 'k');
			%pause;
			fname = sprintf('../Pictures/norm_scatter_all_%d_%d_%d.eps', p,s,r);
			print('-depsc2', fname);
		end;
	end;
end;


