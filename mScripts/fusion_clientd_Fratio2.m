function [beta_opt] = fusion_clientd_Fratio2(expe, chosen, beta)
%function [out,beta_opt] = fusion_clientd_Fratio2(expe, chosen, beta)

%this operation operates on one dimension only!

%get the model label
model_ID = unique(expe.label{1,1});

%calculate global param
param_global = VR_analysis(expe.dset{1,1},expe.dset{1,2});


%only on the clients
for i=1:size(beta,1),
  fprintf(1, '.');
  
  %go through each id
  for d=1:1,for k=1:2,
      out.dset{d,k} = [];
    end;
  end;
  
  %perform client-dependent normalisation
  for id=1:size(model_ID,1),
    
    %get the data set associated to the ID
    for d=1:1,for k=1:2,
	index{d,k} = find(expe.label{d,k} == model_ID(id));
	tmp{d,k} = expe.dset{d,k}(index{d,k},chosen);
      end;end;
      
      %Apply F-ratio normalisation on training and test sets, in a ID-dependnent manner
      param_client = VR_analysis(tmp{1,1}, tmp{1,2});
      [clientd{id}.dset{1,1}, clientd{id}.dset{1,2}] = VR_Fnorm(tmp{1,1}, tmp{1,2}, param_client, param_global, 1, beta(i));
      %[clientd{id}.dset{2,1}, clientd{id}.dset{2,2] = VR_Fnorm(tmp{2,1}, tmp{2,2}, param_client, param_global, 1, beta(i));
      
      %compute the fused scores
      for d=1:1,for k=1:2,
	  out.dset{d,k}(index{d,k},:) = clientd{id}.dset{d,k};
	end;
      end;
  end;
  
  %calculate the dispersion
  param = VR_analysis(out.dset{1,1},out.dset{1,2});
  res(i) = f_ratio(param);
  
end;
fprintf(1, '\n');
%plot(beta, res);

%find arg max_i res(i)
[v,i]= max(res);
beta_opt = beta(i);