function [com, epc_cost] = fusion_mean(expe)

n_samples = 11;
epc_range = [0.1 0.9];

%training and testing
[model, com.dev] = train_method(expe.dset{1,1}, expe.dset{1,2});
com.eva = test_method(expe.dset{2,1}, expe.dset{2,2}, model);

%calculate apriori HTER
[tmp, com.thrd] = wer(com.dev.wolves, com.dev.sheep);
[com.hter_apri, com.far_apri, com.frr_apri] = hter_apriori(com.eva.wolves, com.eva.sheep, com.thrd);

fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dev.wolves, com.dev.sheep, com.eva.wolves, com.eva.sheep,n_samples,epc_range);

%scatter plot
%figure(1);
%percentage = 0.1; %proportion of data to plot
%param = VR_analysis(dev.wolves, dev.sheep);
%VR_draw(dev.wolves, dev.sheep, percentage, param)
%figure(2);
%VR_draw(eva.wolves, eva.sheep, percentage, param)

%EPC curve
%n_samples=10;
%figure(3);
%[epc_dev, epc_eva, cost] = epc(com.dev.wolves, com.dev.sheep, com.eva.wolves, com.eva.sheep,n_samples);
%result.x = cost(:,1);
%plot(cost(:,1),epc_eva.hter_apri);