cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];


initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

load main_predict_advanced com epc_cost
%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
	                end;end;
			
			n=1;
			wer_(1)= wer(nexpe{1}.dset{1,1}(:,1),nexpe{1}.dset{1,2}(:,1));
			wer_(2)= wer(nexpe{1}.dset{1,1}(:,2),nexpe{1}.dset{1,2}(:,2));
			if (wer_(1) > wer_(2)),
				better=2;
			else
				better=1;
			end;
			for d=1:2,for k=1:2,
				tcom.dset{d,k} = nexpe{1}.dset{d,k}(:,better);
			end;end;

			%epc curve
			fprintf(1,'\nCalculating EPC');
			[tcom.epc.dev, tcom.epc.eva, epc_cost]  = epc(tcom.dset{1,1}, tcom.dset{1,2}, tcom.dset{2,1}, tcom.dset{2,2}, n_samples,epc_range);

			com{1}{p,s,r} = tcom;

			for i=1:size(com,2),
				com{i}{p,s,r}.dset={};
			end;

		end;
	end;
end;

save main_single_best com epc_cost


