function [model, com] = train_method(wolves, sheep)

%zero-mean unit variance normalisation
model.mean = mean([wolves; sheep]);
model.std = std([wolves; sheep]);

[nwolves, nsheep] = normalise_scores(wolves, sheep, model.mean, model.std);
com.wolves = mean(nwolves, 2);
com.sheep = mean(nsheep, 2);
