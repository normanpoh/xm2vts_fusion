function [com, epc_cost] = fusion_fdwsum(expe)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

%zero-mean unit variance normalisation
model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
model.std = std([expe.dset{1,1}; expe.dset{1,2}]);
[expe.dset{1,1}, expe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
[expe.dset{2,1}, expe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);

%get the margin
[tran.x{1}, tran.margin{1}] = cal_margin(expe.dset{1,1}(:,1), expe.dset{1,2}(:,1)); %expert1
[tran.x{2}, tran.margin{2}] = cal_margin(expe.dset{1,1}(:,2), expe.dset{1,2}(:,2)); %expert2

%transform the margin
for d=1:2,
for k=1:2,
	tran.dset{d,k}(:,1) = interp1(tran.x{1},tran.margin{1}, expe.dset{d,k}(:,1), 'nearest'); %expert1
	tran.dset{d,k}(:,2) = interp1(tran.x{2},tran.margin{2}, expe.dset{d,k}(:,2), 'nearest'); %expert2
end;end

%get the fixed weight
chosen = [1 2];
param = VR_analysis(expe.dset{1,1}, expe.dset{1,2});
tmp.fixed = cal_weight_fisher(chosen, param);

%get the dynamic weight
for d=1:2,
for k=1:2,
	tmp.dynamic{d,k} = tran.dset{d,k} ./ repmat(sum(tran.dset{d,k},2),1,2);
end;end;

%find the beta
clear beta;
beta(:,1) = linspace(0,1,11)';
beta(:,2) = 1- beta(:,1);
d=1;%on dev set!
fprintf(1,'\nOptimising beta param');
for i=1:size(beta,1),
    fprintf(1,'.');
    for k=1:2,
	tmp.weight=[];
	tmp.weight(:,1) = beta(i,1) * tmp.fixed(1) + beta(i,2) * tmp.dynamic{d,k}(:,1);
	tmp.weight(:,2) = beta(i,1) * tmp.fixed(2) + beta(i,2) * tmp.dynamic{d,k}(:,2);
	tmp.weight = tmp.weight  ./ repmat(sum(tmp.weight,2),1,2);
	tmp.out{k} = sum(expe.dset{d,k} .* tmp.weight,2);
    end;
    [tmp.wer(i), tmp.thrd] = wer(tmp.out{1},tmp.out{2});
end;

%figure(1);
%plot(beta(:,1),tmp.wer)
%pause;
[tmp.i] = find( tmp.wer == min(tmp.wer));

com.beta_opt = beta(tmp.i(size(tmp.i,2)),:); %choose the last one

com.wer_beta_tune = tmp.wer; %record the output

%apply this beta to the dev and eva set
for d=1:2,
    for k=1:2,
	tmp.weight=[];
	tmp.weight(:,1) = com.beta_opt(1) * tmp.fixed(1) + com.beta_opt(2) * tmp.dynamic{d,k}(:,1);
	tmp.weight(:,2) = com.beta_opt(1) * tmp.fixed(2) + com.beta_opt(2) * tmp.dynamic{d,k}(:,2);
	tmp.weight = tmp.weight  ./ repmat(sum(tmp.weight,2),1,2);
	tmp.out{d,k} = sum(expe.dset{d,k} .* tmp.weight,2);
    end;
end;

%figure(1);
%[wer_min, thrd_min, x] = wer(tmp.out{1,1}, tmp.out{1,2},[0.5, 0.5], 1);
%figure(2);
%[wer_min, thrd_min, x] = wer(tmp.out{2,1}, tmp.out{2,2},[0.5, 0.5], 1);

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(tmp.out{1,1}, tmp.out{1,2}, tmp.out{2,1}, tmp.out{2,2}, n_samples,epc_range);

com.weight = tmp.fixed; %record the weight

%pause;
%scatter plot
%figure(1);
%percentage = 0.1; %proportion of data to plot
%param = VR_analysis(dev.wolves, dev.sheep);
%VR_draw(dev.wolves, dev.sheep, percentage, param)
%figure(2);
%VR_draw(eva.wolves, eva.sheep, percentage, param)

%EPC curve
%n_samples=10;
%figure(3);
%[epc_dev, epc_eva, cost] = epc(com.dev.wolves, com.dev.sheep, com.eva.wolves, com.eva.sheep,n_samples);
%result.x = cost(:,1);
%plot(cost(:,1),epc_eva.hter_apri);