function [com, epc_cost] = fusion_baseline_Fratio(expe)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

%beta
beta = linspace(0,1,11)';

%zero-mean unit variance normalisation
model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
model.std = std([expe.dset{1,1}; expe.dset{1,2}]);

[expe.dset{1,1}, expe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
[expe.dset{2,1}, expe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);

%optimize beta
fprintf(1, 'Optimize beta');
com.beta_opt = fusion_clientd_Fratio2(expe, 1, beta);

%final output
com.dset = fusion_clientd_Fratio(expe, com.beta_opt);

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

