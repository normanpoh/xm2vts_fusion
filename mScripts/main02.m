%Test to see if model-dependent F-norm can improve fusion performance or not
initialise;

%Protocol 1
% VR via modalities
expe.P{1}.seq{1} = [2 3 6 7 8];
expe.P{2}.seq{1} = [2 3 4 5];


p=1;s=1;r=1;
[dev.wolves, dev.sheep, dev.label_wolves, dev.label_sheep] = load_raw_scores_labels(datafiles{1}.dev, expe.P{p}.seq{s}(r,:));
[eva.wolves, eva.sheep, eva.label_wolves, eva.label_sheep] = load_raw_scores_labels(datafiles{1}.eva, expe.P{p}.seq{s}(r,:));
model_ID = unique(dev.label_sheep);

%go through each id
expe.dset{p,s,r}.dev.wolves_com = [];
expe.dset{p,s,r}.dev.sheep_com = [];
expe.dset{p,s,r}.eva.wolves_com = [];
expe.dset{p,s,r}.eva.sheep_com = [];

for id=1:size(model_ID,1),
    tmp.dev.wolves = dev.wolves(find(dev.label_wolves == model_ID(id)),:);
    tmp.dev.sheep = dev.sheep(find(dev.label_sheep == model_ID(id)),:);
    tmp.eva.wolves = eva.wolves(find(eva.label_wolves == model_ID(id)),:);
    tmp.eva.sheep = eva.sheep(find(eva.label_sheep == model_ID(id)),:);
    
    %Apply F-norm on training and test sets, in a data-dependent manner
    [expe.dset{p,s,r}.dev.wolves{id}, expe.dset{p,s,r}.dev.sheep{id}, expe.dset{p,s,r}.dev.param] = VR_Fnorm(tmp.dev.wolves, tmp.dev.sheep);
    [expe.dset{p,s,r}.eva.wolves{id}, expe.dset{p,s,r}.eva.sheep{id}] = VR_Fnorm(tmp.eva.wolves, tmp.eva.sheep, expe.dset{p,s,r}.dev.param);

    expe.dset{p,s,r}.dev.wolves_com = [expe.dset{p,s,r}.dev.wolves_com; expe.dset{p,s,r}.dev.wolves{id}];
    expe.dset{p,s,r}.dev.sheep_com = [expe.dset{p,s,r}.dev.sheep_com; expe.dset{p,s,r}.dev.sheep{id}];    
    expe.dset{p,s,r}.eva.wolves_com = [expe.dset{p,s,r}.eva.wolves_com; expe.dset{p,s,r}.eva.wolves{id}];
    expe.dset{p,s,r}.eva.sheep_com = [expe.dset{p,s,r}.eva.sheep_com; expe.dset{p,s,r}.eva.sheep{id}];    
    
end;


param_com= VR_analysis(expe.dset{p,s,r}.eva.wolves_com, expe.dset{p,s,r}.eva.sheep_com);
param = VR_analysis(eva.wolves, eva.sheep);
f_eer(f_ratio(param_com))
f_eer(f_ratio(param))
