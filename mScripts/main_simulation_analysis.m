%individual plot
epc_cost = linspace(0.1,0.9,11);
close all;
for alpha =1:11,
	column = 4 + alpha;
	list = [1 2 3 4 7];
	[X,Y] = meshgrid(var_ratio,corr);
	AZ= 27; EL = 32;
	%cmap = colormap;
	for i=list,

		figure(i);set(gca, 'Fontsize',16);
	        hold off;
		Z = griddata(out{i}(:,1),out{i}(:,2),out{i}(:,column),X,Y,'cubic');
	        hidden on; hold on;

		surf(X,Y,Z) %interpolated
		plot3(out{i}(:,1), out{i}(:,2), out{i}(:,column), 'ro')

		Z = griddata(out{i}(:,1),out{i}(:,2), ones( size(out{i}(:,2),1),1) ,X,Y,'cubic');
        	hidden on;
		surf(X,Y,Z) %interpolated
		%colormap(gray(256));
		colorbar;
		xlabel('var ratio');
		ylabel('corrr');
		zlabel('\beta_{min}');
		txt = sprintf('%s : \\alpha = %1.2f',leg{i}, epc_cost(alpha));
		title(txt);
		grid on;
        	view(AZ,EL);
		axis([1 4 0 1 0 5]);
		txt = sprintf('../Pictures/simulation_animation_%d_%02d.eps',i,alpha);
		print('-depsc2', txt);
	end;
	close all;
end;
