close all
clear all
%
%  SVM Classification 2D examples
%
%  15/06/00 AR


cd /home/learning/norman/xm2vts_fusion/mScripts
initialise;

p=1;s=1;r=1;			
b = expe.P{p}.seq{s}(r,:);
%load score files
[nexpe{1}.dset{1,1}, nexpe{1}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev, b);
[nexpe{1}.dset{2,1}, nexpe{1}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva, b);



%-------------Data Set-------------------------------------
%nbapp=200;
%nbtest=0;
%sigma=2;
%[xapp,yapp]=dataset('Checkers',nbapp,nbtest,sigma);
%[xapp,yapp]=dataset('Gaussian',nbapp,nbtest,sigma);
xapp = [nexpe{1}.dset{1,1};nexpe{1}.dset{1,2}];
yapp = [-1*ones(size(nexpe{1}.dset{1,1},1),1);ones(size(nexpe{1}.dset{1,2},1),1)];

%-----------------------------------------------------
%   Learning and Learning Parameters
c = inf;
epsilon = .000001;
kerneloption= 1;
%kernel='gaussian';
kernel='polyhomog';
verbose = 1;
tic
[xsup,w,b,pos]=svmclass(xapp,yapp,c,epsilon,kernel,kerneloption,verbose);
toc
%--------------Testing Generalization performance ---------------
[xtesta1,xtesta2]=meshgrid([-4:0.1:4],[-4:0.1:4]);
[na,nb]=size(xtesta1);
xtest1=reshape(xtesta1,1,na*nb);
xtest2=reshape(xtesta2,1,na*nb);
xtest=[xtest1;xtest2]';
ypred = svmval(xtest,xsup,w,b,kernel,kerneloption);
ypredmat=reshape(ypred,na,nb);
%----------------------Plotting---------------------------------

ind1=find(ypred>0);
indm1=find(ypred<0);

hold off
contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
hold on
[cs,h]=contour(xtesta1,xtesta2,ypredmat,[-1 0 1],'k');
clabel(cs,h);
h1=plot(xapp(yapp==1,1),xapp(yapp==1,2),'+k'); 
set(h1,'LineWidth',2);

h2=plot(xapp(yapp==-1,1),xapp(yapp==-1,2),'ok'); 
set(h2,'LineWidth',2);
h3=plot(xsup(:,1),xsup(:,2),'*k'); 
set(h3,'LineWidth',2);



xlabel('x1');ylabel('x2');
title('Learning Data and Margin ');
%axis([-4 4 -4 4]);
