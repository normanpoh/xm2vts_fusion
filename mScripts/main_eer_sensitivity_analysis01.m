clear myout;
load main_eer_sensitivity out
t_out{1} = out;
load main_eer_sensitivity_normalised out
t_out{2} = out;

initialise;
list{1} = 5:5:45;
list{2} = 5:5:145;
dd = 1;
clear out myout;
%calculate statistics
for j=1:2, %orig and Z-normalised
  for ds=1:2,
    for p=1:2,
      for b=1:size(dat.P{p}.labels,2)
        myout{ds,j}{p,b} = [];
        %for length(list{ds}) to ds1
        for i=1:length(list{1}), %sample the no.-of-user interval
          tmp = quantil(t_out{j}{ds}{dd}{p}{i}, [2.5 50 97.5])';
          myout{ds,j}{p,b} = [myout{ds,j}{p,b};tmp(b,:)];
        end;
      end;
    end;
  end;
end;

%compare orig and normalised scores
for ds=1:2,
  figure(ds);
  t=0;
  for p=1:2,
    for b=1:size(dat.P{p}.labels,2),
      t=t+1;
      %figure(p);
      subplot(4,4,t);
      hold off;
      e = myout{ds,1}{p,b}(:,[1,3]) - repmat(myout{ds,1}{p,b}(:,2),1,2);
      errorbar(list{1},myout{ds,1}{p,b}(:,2)*100, e(:,1)*100, e(:,2)*100,'r--','linewidth',2);
      hold on;
      e = myout{ds,2}{p,b}(:,[1,3]) - repmat(myout{ds,2}{p,b}(:,2),1,2);
      errorbar(list{1}+1,myout{ds,2}{p,b}(:,2)*100, e(:,1)*100, e(:,2)*100)
      %bar(list{ds},(e(:,2)-e(:,1))*10)
      if (t==13),
        xlabel('No. of clients');
        ylabel('HTER(%)');
        legend('orig','Z-Norm');
      end;
      %txt = sprintf('Exp. %d Protocol %d %s\n',b,p, dat.P{p}.labels{b});
      txt  = sprintf('[%d] %s',t,dat.P{p}.labels{b});
      title(txt);
      axis([0 50 0 20]);
      grid on;
    end;
  end;
end;
for ds=1:2,
  figure(ds);
  fname = sprintf('../Pictures/main_eer_sensitivity_emp%02d.eps',ds);
  print('-depsc2', fname);
end;





%bar plot on trend of decreasing HTER as num of users increases
%comparison between original and normalised scores
for ds=1:2,
  figure(ds);
  t=0;
  for p=1:2,
    for b=1:size(dat.P{p}.labels,2),
      t=t+1;
      %figure(p);
      subplot(4,4,t);
      hold off;
      e = myout{ds,1}{p,b}(:,[1,3]) - repmat(myout{ds,1}{p,b}(:,2),1,2);
      e2 = myout{ds,2}{p,b}(:,[1,3]) - repmat(myout{ds,2}{p,b}(:,2),1,2);
      bar(list{1}, [(e(:,2)-e(:,1)),(e2(:,2)-e2(:,1))]*100);
      if (t==13),
        xlabel('No. of clients');
        ylabel('HTER(%)');
        legend('orig','Z-Norm');
      end;
      %txt = sprintf('Exp. %d Protocol %d %s\n',b,p, dat.P{p}.labels{b});
      txt  = sprintf('[%d] %s',t,dat.P{p}.labels{b});
      title(txt);
      grid on;
    end;
  end;
end;

for ds=1:2,
  figure(ds);
  fname = sprintf('../Pictures/main_eer_sensitivity_emp%02d.eps',ds+4);
  print('-depsc2', fname);
end;

%bar plot on trend of decreasing HTER as num of users increases
%comparison between 50-user and 150 user datasets
for i=1:2,
  figure(i);
  t=0;
  for p=1:2,
    for b=1:size(dat.P{p}.labels,2),
      t=t+1;
      %figure(p);
      subplot(4,4,t);
      hold off;
      e = myout{1,i}{p,b}(:,[1,3]) - repmat(myout{1,i}{p,b}(:,2),1,2);
      e2 = myout{2,i}{p,b}(:,[1,3]) - repmat(myout{2,i}{p,b}(:,2),1,2);
      bar(list{1}, [(e(:,2)-e(:,1)),(e2(:,2)-e2(:,1))]*100);
      if (t==13),
        xlabel('No. of clients');
        ylabel('HTER(%)');
        legend('50 users','150 users');
        tmp = cell(length(list{1}),1);
        tmp{length(list{1})}=50;
        tmp{1}=list{1}(1);
        set(gca,'xticklabel', tmp);
      else
        tmp = cell(length(list{1}),1);
        set(gca,'xticklabel', tmp);
      end;
      %txt = sprintf('Exp. %d Protocol %d %s\n',b,p, dat.P{p}.labels{b});
      txt  = sprintf('[%d] %s',t,dat.P{p}.labels{b});
      title(txt);
      axis tight;
      grid on;
    end;
  end;
end;

for i=1:2,
  figure(i);
  fname = sprintf('../Pictures/main_eer_sensitivity_emp%02d.eps',i+6);
  print('-depsc2', fname);
end;

%error bar plot
for ds=1:2,
  figure(ds);
  t=0;
  for p=1:2,
    for b=1:size(dat.P{p}.labels,2),
      t=t+1;
      %figure(p);
      subplot(4,4,t);
      hold off;
      e = myout{ds,1}{p,b}(:,[1,3]) - repmat(myout{ds,1}{p,b}(:,2),1,2);
      e2 = myout{ds,2}{p,b}(:,[1,3]) - repmat(myout{ds,2}{p,b}(:,2),1,2);
      %bar(list{1}, [e(:,2)-e(:,1), e2(:,2)-e2(:,1)]*100);
      bar(list{1}, [(e(:,2)-e(:,1))-(e2(:,2)-e2(:,1))]*100);
      hter_abs_range(t,ds,1) = sum(e(:,2)-e(:,1));
      hter_abs_range(t,ds,2) = sum(e2(:,2)-e2(:,1));
      if (t==13),
        xlabel('No. of clients');
        ylabel('HTER(%)');
        %legend('orig','Z-Norm');
      end;
      %txt = sprintf('Exp. %d Protocol %d %s\n',b,p, dat.P{p}.labels{b});
      txt  = sprintf('[%d] %s',t,dat.P{p}.labels{b});
      title(txt);
      %axis([0 50 0 20]);
      grid on;
    end;
  end;
end;

hter_range = hter_abs_range(:,:,1) - hter_abs_range(:,:,2);

tit={'50 users', '150 users'}
figure(3);
for i=1:2,
  subplot(2,1,i);set(gca, 'fontsize', 18);
  bar([hter_abs_range(:,i,1),hter_abs_range(:,i,2)]);
  if i==1,
    set(gca,'xticklabel', cell(13));
    legend('Original','Normalized','location','NorthWest');
  end;
  title(['[' num2str(i) '] ' tit{i}]);
end;
xlabel('Experiments');
ylabel('HTER (%)');

fname = sprintf('../Pictures/main_eer_sensitivity_emp%02d.eps',3);
print('-depsc2', fname);

tit={'Original', 'Normalized'}
figure(3);
for i=1:2,
  subplot(2,1,i);set(gca, 'fontsize', 18);
  bar(hter_abs_range(:,:,i));
  if i==1,set(gca,'xticklabel', cell(13)),end;
  title(['[' num2str(i) '] ' tit{i}]);
end;

legend('50 users','150 users', 'location','NorthWest');
xlabel('Experiments');
ylabel('HTER (%)'); subplot(2,1,1); ylabel('HTER (%)');
fname = sprintf('../Pictures/main_eer_sensitivity_emp%02d.eps',4);
print('-depsc2', fname);

