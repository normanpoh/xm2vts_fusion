%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;
%load main_norm_effect_addon_wsum_user.mat com epc_cost

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%super draw can go up to 1000
n_super_draws = 10;
n_draws = 1000;
list{1} = 5:5:45;
list{2} = 5:5:145;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%reuse expe_id created in main_scalable_EER.m
load main_scalable_EER

for dd=1:n_super_draws,
  for p=1:2,
    for d=1:n_draws,
      %small set
      model_ID = expe_id{p}.selected(dd,1:50);
      selected_id = randperm(length(model_ID));
      selected_id = model_ID(selected_id);
      new_expe_id{1}{dd}{p}.selected(d,:) = selected_id;
      for i=1:length(list{1}),
        tmp = randperm(50);
        rnd_list{1}{d}{i} = tmp(1:list{1}(i));
      end;
      %big set
      model_ID = expe_id{p}.selected(dd,51:200);
      selected_id = randperm(length(model_ID));
      selected_id = model_ID(selected_id);
      new_expe_id{2}{dd}{p}.selected(d,:) = selected_id;
      for i=1:length(list{2}),
        tmp = randperm(150);
        rnd_list{2}{d}{i} = tmp(1:list{2}(i));
      end;
    end;
  end;
end;
save main_eer_sensitivity_draws new_expe_id rnd_list


load main_eer_sensitivity_draws new_expe_id rnd_list
load main_eer_sensitivity out
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN expe here
%ds=1; %50 client config
ds=2; %150 client config

n_super_draws = 1;
n_draws = 100;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;
for p=1:2,
  %do all at once
  expe.dset = data{p}.dset;
  expe.label = data{p}.label;
  fprintf(1,'Protocol %d\n', p);
  for dd=1:n_super_draws,
      fprintf(1,'�');      
      
      for d=1:n_draws,
        fprintf(1,'.%d',d);
        for i=1:length(list{1}), %force to be the list ds1
          %for i=1:length(list{ds}), %sample the no.-of-user interval
          rexpe = rndchoose_client_list(expe, [], rnd_list{ds}{d}{i}, new_expe_id{ds}{dd}{p}.selected(d,:));

          for b=1:size(dat.P{p}.labels,2)
            [tmp_,thrd] = wer(rexpe.dset{1,1}(:,b), rexpe.dset{1,2}(:,b));
            [com.hter_apri, com.far_apri, com.frr_apri] = hter_apriori(rexpe.dset{2,1}(:,b), rexpe.dset{2,2}(:,b),thrd);
            out{ds}{dd}{p}{i}(d,b) = com.hter_apri;
          end;
        end;

      end;
  end;
end;
time_taken = toc;
save main_eer_sensitivity out time_taken
%calculate statistics
for ds=1:2,
  for p=1:2,
    for b=1:size(dat.P{p}.labels,2)
      myout{ds}{p,b} = [];
      %for length(list{ds}) to ds1
      for i=1:length(list{1}), %sample the no.-of-user interval
        tmp = quantil(out{ds}{dd}{p}{i}, [2.5 50 97.5])';
        myout{ds}{p,b} = [myout{ds}{p,b};tmp(b,:)];
      end;
    end;
  end;
end;

clear tmp;
p=1;b=1;
t=0;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2), 
    t=t+1;
    %figure(p);
    subplot(4,4,t);
    hold off;
    e = myout{2}{p,b}(:,[1,3]) - repmat(myout{2}{p,b}(:,2),1,2);
    errorbar(list{1},myout{2}{p,b}(:,2)*100, e(:,1)*100, e(:,2)*100,'r--','linewidth',2);
    hold on;
    e = myout{1}{p,b}(:,[1,3]) - repmat(myout{1}{p,b}(:,2),1,2);
    errorbar(list{1},myout{1}{p,b}(:,2)*100, e(:,1)*100, e(:,2)*100)
    %bar(list{ds},(e(:,2)-e(:,1))*10)
    if (t==13),
      xlabel('No. of clients');
      ylabel('HTER(%)');
    end;
    %txt = sprintf('Exp. %d Protocol %d %s\n',b,p, dat.P{p}.labels{b});
    txt  = sprintf('[%d] %s',t,dat.P{p}.labels{b});
    title(txt);
    axis([0 50 0 20]);
    grid on;
  end;
end;
print('-depsc2', '../Pictures/main_eer_sensitivity_emp01.eps');


