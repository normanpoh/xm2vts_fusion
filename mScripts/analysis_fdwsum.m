initialise
load baseline
%see main_baseline.m
load fdwsum
%see main_fusion_fdwsum.m
n_samples = 11;

figure(7);
%display HTER results
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			txt = sprintf('Fusion of %s',[expe_labels.P{p}.seq{s}.row{r}]);
			hter_significant_plot(com_wsum{p,s,r}.epc, com_dfwsum{p,s,r}.epc, NI, NC, epc_cost);
			subplot(2,1,1);
			%legend(dat.P{p}.labels{expe.P{p}.seq{s}(r,1)}, dat.P{p}.labels{expe.P{p}.seq{s}(r,2)});
			legend('wsum', 'dfwsum');
			title(txt);
			print('-depsc2', sprintf('../Pictures/%d_%d_%d_epc.eps',p,s,r));
			pause;
		end;
	end;
end;

%p=1;s=1;r=1;
for s=1:3, %over 3 different configurations
	row = 0;
	rp_index = 0;
	for p=1:2, %over two protocols
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%txt = sprintf('Experiment %d (%s) has HTER: %2.3f\n',r,[expe_labels.P{p}.seq{s}.row{r}], expe_res.P{p}.seq{s}.hter_apri(r) *100); 
			%fprintf(1,txt);			
			b_expert = expe.P{p}.seq{s}(r,:);
			out{p,s,r}.baseline = [baseline{p,b_expert(1)}.epc.eva.hter_apri;baseline{p,b_expert(2)}.epc.eva.hter_apri];
			out{p,s,r}.min = min(out{p,s,r}.baseline);
			out{p,s,r}.mean = mean(out{p,s,r}.baseline);
			out{p,s,r}.beta_mean_wsum = out{p,s,r}.mean ./ com_wsum{p,s,r}.epc.eva.hter_apri;
			out{p,s,r}.beta_min_wsum = out{p,s,r}.min ./ com_wsum{p,s,r}.epc.eva.hter_apri;
			out{p,s,r}.beta_mean_dfwsum = out{p,s,r}.mean ./ com_dfwsum{p,s,r}.epc.eva.hter_apri;
			out{p,s,r}.beta_min_dfwsum = out{p,s,r}.min ./ com_dfwsum{p,s,r}.epc.eva.hter_apri;
			out{p,s,r}.beta_mean_mean =  out{p,s,r}.mean ./ com_mean{p,s,r}.epc.eva.hter_apri;
			out{p,s,r}.beta_min_mean =  out{p,s,r}.min ./ com_mean{p,s,r}.epc.eva.hter_apri;
			%group by configuration:

			rp_index = rp_index + 1;
			for i=1:n_samples,
				row = row + 1;
				res{s,1}(row,1) = out{p,s,r}.beta_mean_mean(i);
				res{s,1}(row,2) = out{p,s,r}.beta_mean_wsum(i);
				res{s,1}(row,3) = out{p,s,r}.beta_mean_dfwsum(i);
				res{s,2}(row,1) = out{p,s,r}.beta_min_mean(i);
				res{s,2}(row,2) = out{p,s,r}.beta_min_wsum(i);
				res{s,2}(row,3) = out{p,s,r}.beta_min_dfwsum(i);

				res2{s,i,1}(rp_index,1) = out{p,s,r}.beta_mean_mean(i);
				res2{s,i,1}(rp_index,2) = out{p,s,r}.beta_mean_wsum(i);
				res2{s,i,1}(rp_index,3) = out{p,s,r}.beta_mean_dfwsum(i);
				res2{s,i,2}(rp_index,1) = out{p,s,r}.beta_min_mean(i);
				res2{s,i,2}(rp_index,2) = out{p,s,r}.beta_min_wsum(i);
				res2{s,i,2}(rp_index,3) = out{p,s,r}.beta_min_dfwsum(i);

			end;
		end;
	end;
end;

a={'mean', 'wsum', 'dfwsum'};

limit_y= [10 3 ];
for s=1:2,
	figure(s);
	t=0;
	for i=1:2:n_samples,
		
		%subplot(1,n_samples,i);
		t=t+1;
		subplot(1,6,t);
		boxplot(res2{s,i,1});
		tmp_axis = axis; tmp_axis([3, 4]) = [0 limit_y(s)];axis(tmp_axis);
		xlabel(sprintf('%1.2f',epc_cost(i,1)));
		ylabel('');
	end;
end;

upper_limit_y= [7 2 ];
lower_limit_y= [0 0.6];
for s=1:2,
	figure(2+s);
	t=0;
	for i=1:2:n_samples,
		
		%subplot(1,n_samples,i);
		t=t+1;
		subplot(1,6,t);
		boxplot(res2{s,i,2});
		tmp_axis = axis; tmp_axis([3, 4]) = [lower_limit_y(s) upper_limit_y(s)];axis(tmp_axis);
		xlabel(sprintf('%1.2f',epc_cost(i,1)));
		ylabel('');
	end;
end;

upper_limit_y= [7 2.5 ];
lower_limit_y= [0.5 0.5];
figure(5);
for s=1:2,
	subplot(1,2,s);
	boxplot(res{s,1});
	tmp_axis = axis; tmp_axis([3, 4]) = [lower_limit_y(s) upper_limit_y(s)];axis(tmp_axis);
	set(gca,'XTicklabel',a)
end;

upper_limit_y= [4 2 ];
lower_limit_y= [0.5 0.5];
figure(6);
for s=1:2,
	subplot(1,2,s);
	boxplot(res{s,2});
	tmp_axis = axis; tmp_axis([3, 4]) = [lower_limit_y(s) upper_limit_y(s)];axis(tmp_axis);
	ylabel('\beta_{min}');
	set(gca,'XTicklabel',a)
end;

