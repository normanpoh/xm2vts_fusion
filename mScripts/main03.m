% Test to see the idea of dynamic weighting based on quality will work or not
initialise;

p=1;s=3;r=1;
[dev.wolves, dev.sheep, dev.label_wolves, dev.label_sheep] = load_raw_scores_labels(datafiles{1}.dev, expe.P{p}.seq{s}(r,:));
[eva.wolves, eva.sheep, eva.label_wolves, eva.label_sheep] = load_raw_scores_labels(datafiles{1}.eva, expe.P{p}.seq{s}(r,:));

%Apply F-norm on training and test sets
[expe.dset{p,s,r}.dev.wolves, expe.dset{p,s,r}.dev.sheep, expe.dset{p,s,r}.dev.param] = VR_Fnorm(dev.wolves, dev.sheep);
[expe.dset{p,s,r}.eva.wolves, expe.dset{p,s,r}.eva.sheep] = VR_Fnorm(eva.wolves, eva.sheep, expe.dset{p,s,r}.dev.param);
param = VR_analysis(expe.dset{p,s,r}.dev.wolves, expe.dset{p,s,r}.dev.sheep);

%draw dev and eva data
figure(1);
percentage = 1;
VR_draw(expe.dset{p,s,r}.dev.wolves, expe.dset{p,s,r}.dev.sheep, percentage, param)
figure(2);
VR_draw(expe.dset{p,s,r}.eva.wolves, expe.dset{p,s,r}.eva.sheep, percentage, param)

%first expert is better than the second expert
better_w = find(expe.dset{p,s,r}.dev.wolves(:,1) < expe.dset{p,s,r}.dev.wolves(:,2));
better_s = find(expe.dset{p,s,r}.dev.sheep(:,1) > expe.dset{p,s,r}.dev.sheep(:,2));
worse_w = setdiff([1:size(dev.wolves,1)], better_w)';
worse_s = setdiff([1:size(dev.sheep,1)], better_s)';

%plot the training and test scatter diagrams
figure(1);
percentage = 1;
VR_draw(expe.dset{p,s,r}.dev.wolves, expe.dset{p,s,r}.dev.sheep, percentage, param)
figure(2);
VR_draw(expe.dset{p,s,r}.eva.wolves, expe.dset{p,s,r}.eva.sheep, percentage, param)
%plot the partition when the better criterion is used
figure(3);hold off;
draw_empiric(expe.dset{p,s,r}.dev.wolves(better_w,:), expe.dset{p,s,r}.dev.sheep(better_s,:), percentage, {'b+', 'r.'});
draw_empiric(expe.dset{p,s,r}.dev.wolves(worse_w,:), expe.dset{p,s,r}.dev.sheep(worse_s,:), percentage, {'g+', 'm.'});


expe.dset{p,s,r}.dev.wolves_com = [expe.dset{p,s,r}.dev.wolves(better_w,1); expe.dset{p,s,r}.dev.wolves(worse_w,2)];
expe.dset{p,s,r}.dev.sheep_com = [expe.dset{p,s,r}.dev.sheep(better_s,1); expe.dset{p,s,r}.dev.sheep(worse_s,2)];

param_com= VR_analysis(expe.dset{p,s,r}.dev.wolves_com, expe.dset{p,s,r}.dev.sheep_com);

f_eer(f_ratio(param))
f_eer(f_ratio(param_com))