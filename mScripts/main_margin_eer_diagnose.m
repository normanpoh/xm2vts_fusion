cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

signs = {'b', 'g', 'r', 'c', 'm', 'y', 'k'};
signs=  {signs{:}, 'b--', 'g--', 'r--', 'c--', 'm--', 'y--', 'k--'};    
signs=  {signs{:}, 'b-.', 'g-.', 'r-.', 'c-.', 'm-.', 'y-.', 'k-.'};    
signs=  {signs{:}, 'b:', 'g:', 'r:', 'c:', 'm:', 'y:', 'k:'};    

initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%Start experiment
p=1;s=1;r=4;

w = linspace(0,1,11)';
w = [w 1-w];
for i=1:size(w,1),
	leg{i} = sprintf('%0.2f', w(i,1));
end;

leg = 
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),

			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);

			b = expe.P{p}.seq{s}(r,:);

			%load score files
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
	                end;end;
			%hold off;
			%draw_empiric(nexpe{1}.dset{1,1},nexpe{1}.dset{1,2});
			%param = VR_analysis(nexpe{1}.dset{1,1},nexpe{1}.dset{1,2});
			%draw_theory([1 2], param, [0.5 0.5]);
			%legend('Client', 'Impostor', 'Client Gaussian', 'Imposter Gaussian', 'Client Mean', 'Impostor Gaussian Mean');
			for i=1:size(w,1),
				[com] = fusion_wsum(nexpe{1}, [1:2], w(i,:));
				[out.wer(i), out.thrd(i), out.x{i}, out.FAR{i}, out.FRR{i}] = wer(com.dset{1,1} ,com.dset{1,2}(:,1));%, [], 6, [],i);
			end;

			figure(1);	
			for i=1:size(w,1),
				if i==1, hold off; end;
				%plot(out.x{i} - out.thrd(i), out.wer(i) + abs(out.FRR{i} - out.FAR{i}), signs{i})
				plot(out.x{i} - out.thrd(i), abs(out.FRR{i} - out.FAR{i}), signs{i});
				if i==1, hold on; end;
			end;
			legend(leg);

			figure(2);
			for i=1:size(w,1),
				if i==1, hold off; end;
				plot(out.x{i} - out.thrd(i), (out.FRR{i} + out.FAR{i}) / 2, signs{i})
				tot_HTER(i) = mean((out.FRR{i} + out.FAR{i}) / 2);
				if i==1, hold on; end;
				%drawnow;
			end;
			legend(leg);

			figure(3);
			plot(w(:,1),tot_HTER);
			hold on;
			plot(w(:,1),out.wer, 'r');

		end;
	end;
end;