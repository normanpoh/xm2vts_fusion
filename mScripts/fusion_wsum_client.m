function [com, epc_cost] = fusion_wsum_client(expe, chosen, toplot)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
        chosen = [1:2];
end;

if (nargin < 3),
        toplot = 0;
end;

%go through each id
for d=1:2,for k=1:2,
	out{d,k} = [];%zeros(size(expe.dset{d,k},1),1);
end;end;

%get the model label
model_ID = unique(expe.label{1,1});

out2.fa = [];
out2.fr = [];

%perform client-dependent processing
fprintf(1, 'Processed ID:\n');
for id=1:size(model_ID,1),
	%get the data set associated to the ID
	for d=1:2,for k=1:2,
	    index{d,k} = find(expe.label{d,k} == model_ID(id));
	    if (size(index{d,k},1)==0),
		error([ 'size is zero of id ', num2str(id)]);
	    end;
	    tmp{d,k} = expe.dset{d,k}(index{d,k},chosen);
	end;end;
   
	%calculate weight by brute-force on the data!
	a = [0:0.02:1]';
	w_ = [a 1-a];
	for i=1:size(w_,1),
		w = tmp{1,1} * w_(i,:)';
		s = tmp{1,2} * w_(i,:)';
		wer_(i) = wer(w, s,[1 1]);
	end;

	wer_zero = find(wer_ == 0);
	if (length(wer_zero)> 1),
		%more than one candidates, find the one closest to equal weight
		tmp_ = sum(abs((w_(wer_zero,:) - repmat([0.5 0.5], length(wer_zero),1))'));
		[v,ind_zero] = min( tmp_);
		ind = wer_zero(ind_zero);
	        v=wer_(ind);
	else
		[v, ind]= min(wer_);	
	end;

	if (toplot),
		subplot(20,10,id);
		hold off;plot(a,wer_);hold on;plot(a(ind),wer_(ind),'o');
		if (length(wer_zero)> 1),
			plot(a(wer_zero),tmp_, 'r-');
		end;
	end;
	
	weight = w_(ind,:);

	for d=1:2,for k=1:2,
		%combine the fused scores
		clientd{id}.dset{d,k} = tmp{d,k} * weight';
		out{d,k}(index{d,k},1) = clientd{id}.dset{d,k};
	end;end;

	%figure(1);wer(clientd{id}.dset{1,1},clientd{id}.dset{1,2},[0.5 0.5],1)
	%figure(2);wer(clientd{id}.dset{2,1},clientd{id}.dset{2,2},[0.5 0.5],1)
	%pause;

	com.weight(id,:) = weight;

	fprintf(1, '.%d', id);
end;
com.dset = out;

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);
