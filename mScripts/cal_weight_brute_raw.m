function [weight, alpha, eer] = cal_weight_brute_raw(chosen, wolves,sheep, n_samples)
%
% [weight, alpha, eer] = cal_weight_brute_raw(chosen, param, n_samples)
% This function calculate weights by brute-force directly on the data
% chosen = the chosen column
% param  = the parametric representation of the data given by VR_analysis
% currently solving only 2 dimensions
% to see the weights, do:
% plot(alpha(:,1), eer*100);
% xlabel('\alpha'); ylabel('EER(%)');

if nargin < 4,
	n_samples = 11;
end;
alpha = linspace(0,1,n_samples);
alpha =[alpha' 1-alpha'];

fprintf(1, 'Searching the weights');
for i=1:size(alpha,1),
	fprintf(1, '.');
	eer(i) = wer(wolves * alpha(i,:)', sheep * alpha(i,:)');
end;
[eer_, index] = min(eer);
weight = alpha(index(1),:);
fprintf(1, '\n');
