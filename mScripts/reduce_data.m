function [dataq, queue, bayesS]=  reduce_data(data)

%init the queue
clear queue;
queue{1}.dat = data;
split(1) = 1; 

while(sum(split))
  nqueue = {};
  nsplit=[];
  for i=1:length(queue),
    %decide to split or not
    if (split(i)==0),
      %don't split, so copy
      nqueue = {nqueue{:}, queue{i}};
      nsplit=[nsplit split(i)];
    else
      %check if splitting is necessary
      [centres, mse, labels] = kmeans(queue{i}.dat,2);
      
      dset{1}.dat = queue{i}.dat(find(labels == 1),:);
      dset{2}.dat = queue{i}.dat(find(labels == 2),:);
      
      %find log-likelihood
      %
      %LLbefore = sum(log(realmin + mvnpdf(queue{i}.dat, mean(queue{i}.dat), cov(queue{i}.dat))));
      %cov(dset{1})
      %cov(dset{2})
      %LLafter = sum(log(realmin + mvnpdf(dset{1}.dat, mean(dset{1}.dat), cov(dset{1}.dat))));
      %LLafter = LLafter + sum( log(realmin + mvnpdf(dset{2}.dat, mean(dset{2}.dat), cov(dset{2}.dat))));
      
      if (mse > 0.01)
	%if (LLafter > LLbefore),
	nqueue = {nqueue{:}, dset{1}, dset{2}};
	nsplit = [nsplit 1 1];
      else
	nqueue = {nqueue{:}, queue{i}};
	nsplit = [nsplit 0];
      end;
    end;

  end;
  queue = nqueue;
  split = nsplit;
  %queue{:}
  
end;

dataq=[];
bayesS.mu=[];
for i=1:length(queue),
  if size(queue{i}.dat,1)==1,
    centre = queue{i}.dat;
  else
    centre = mean(queue{i}.dat);
  end;
  dataq = [dataq; centre];
  bayesS.mu = [bayesS.mu centre'];
  bayesS.sigma(:,:,i) = cov(queue{i}.dat);
  bayesS.weight(i) = size(queue{i}.dat,1) / size(data,1);
end;
bayesS.weight = bayesS.weight';

