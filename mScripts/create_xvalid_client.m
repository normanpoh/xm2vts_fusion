function out = create_xvalid_client(expe)

model_ID = unique(expe.label{1,1});

for id=1:size(model_ID,1),
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      if (size(index{d,k},1)==0),
        warning([ 'size is zero for id ', num2str(id)]);
      end;
      tmp{id}.dset{d,k} = expe.dset{d,k}(index{d,k},:);
    end;
  end;
end;

d=1;k=2; %work on client dev set only
for id=1:size(model_ID,1),
  %get the data set associated to the ID
  for i=1:size(tmp{id}.dset{d,k},1),
    train{id,i} = tmp{id}.dset{d,k};
    test{id,i} = train{id,i}(i,:);
    train{id,i}(i,:) = [];
  end;
end;

for i=1:size(tmp{1}.dset{d,k},1),
  out{i}.train=[];
  out{i}.test=[];
  for id=1:size(model_ID,1),
    out{i}.train = [out{i}.train;train{id,i}];
    out{i}.test = [out{i}.test;test{id,i}];  
  end;
end;

