function [com] = rndcombine_client(expe, chosen, n_clients, selected_id)
%[nexpe] = rndcombine_client(expe, chosen, n_clients, selected_id)
% combine two modalities randomly
if (nargin < 2|length(chosen)==0),
  chosen = [1:2];
end;

if (nargin < 3 & length(n_clients)==0),
  n_clients = 50;
end;

if (nargin < 4),
  selected_id = [];
end;

%get the model label
model_ID = unique(expe.label{1,1});

if length(selected_id) == 0,
  selected_id = randperm(size(model_ID,1));
  selected_id = model_ID(selected_id);
else
  %fprintf(1,'Using supplied permutation...');
end;

if n_clients > length(selected_id),
  error(['The number of clients desired should be less than the total' ...
	 'available']);
end;

for d=1:2,for k=1:2,
    out{d,k} = [];%zeros(,length(chosen));
    label{d,k} = [];%zeros(,length(chosen));
  end;
end;

%perform client-dependent processing
%fprintf(1, 'Processed ID:\n');
for id=1:n_clients,
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      selected = find(expe.label{d,k} == selected_id(id));
      selected_orig = find(expe.label{d,k} == model_ID(id));
      if (size(selected,1)==0),
	error([ 'size is zero for id ', num2str(id)]);
      end;
      if (size(selected_orig,1)==0),
	error([ 'size is zero for id ', num2str(id)]);
      end;
      tmp{d,k} = [expe.dset{d,k}(selected_orig,chosen(1)) expe.dset{d,k}(selected,chosen(2))];
    end;
  end;
  
  for d=1:2,for k=1:2,
      out{d,k} = [out{d,k};  tmp{d,k}];
      label{d,k} = [label{d,k}; selected_id(id) * ones(size(tmp{d,k},1),1)];
    end;
  end;
  
  %fprintf(1, '.%d', id);
end;
com.dset = out;
com.label = label;
%fprintf(1, 'done\n');