cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

load main_gmm_lda_qda_inv_reg com

p=1;s=1;r=1;

for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      %print remarks
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);
      
      %load score files
      %for MLP output
      for d=1:2,for k=1:2,
	  nexpe.dset{d,k} = data{p}.dset{d,k}(:,b);
	  oexpe.dset{d,k} = nexpe.dset{d,k};
	  for ex=1:2,
	    if (mlp{p,s,r}(ex)),
	      fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
	      nexpe.dset{d,k}(:,ex) = tanh_inv(nexpe.dset{d,k}(:,ex));%, 1/0.8,0.4);
	      %nexpe{4}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex));i
	    end;
	  end;
	end;
      end;

      texpe = nexpe;
      
      %filter bad ones
      for d=1:2,
	for k=1:2,
	  for ex=1:2,
	    if (mlp{p,s,r}(ex)),
	      selected = find(nexpe.dset{d,k}(:,ex)>30);
	      nexpe.dset{d,k}(selected,:)=[];
	      selected = find(nexpe.dset{d,k}(:,ex)<-30);
	      nexpe.dset{d,k}(selected,:)=[];
	    end;
	  end;
	end;
      end;

      %close all;
      %figure(1);
      
      %regularised lda-qda
      %[com{2}{p,s,r}, epc_cost ] = fusion_lda_qda_reg(nexpe, texpe, [1 2],11,[],0,0);
      %draw_empiric(texpe.dset{2,1},texpe.dset{2,2},0.5);
      
      %fprintf('Lambda is %1.2f\n', com{1}{p,s,r}.lambda);

      %[com{3}{p,s,r}, epc_cost] = fusion_gmm(texpe, [1 2], [1 2]);
      
      %regularised lda-qda with relevance factor
      [com{4}{p,s,r}, epc_cost ] = fusion_RDA(nexpe, texpe, [1 2],10,[],0,0);
      
      %free the memory
      for i=1:size(com,2),
	com{i}{p,s,r}.dset={};
      end;
      
      %figure(1);
      %wer(com{1}{p,s,r}.dset{1,1}, com{1}{p,s,r}.dset{1,2},[],1,[],0);
      %wer(com{1}{p,s,r}.dset{2,1}, com{1}{p,s,r}.dset{2,2},[],1,[],1);
    
    end;
  end;
end;

save main_gmm_lda_qda_inv_reg com

%show how regularised parameters help controling QDA
t=0;
for i=linspace(0,1,5),
  t=t+1;
  [tcom, epc_cost ] = fusion_lda_qda_reg(nexpe, texpe, [1 2],[],i,1,1);
  print('-depsc2', ['../Pictures/lda_qda_reg_demo_' num2str(t) '.eps']);
  draw_empiric(texpe.dset{2,1},texpe.dset{2,2},0.1);
  pause;
end;

      %regularised lda-qda
      [com{1}{p,s,r}, epc_cost ] = fusion_lda_qda_reg(nexpe, [1 2],[],[],1);

      fprintf('Lambda is %1.2f\n', com{1}{p,s,r}.lambda);
      com{1}{p,s,r}.dset = {};

      [com{1}{p,s,r}, epc_cost ] = fusion_lda_qda_reg(nexpe, [1 2],[],1,1);

      draw_empiric(texpe.dset{2,1},texpe.dset{2,2},0.1);

      figure(4);
      wer(com{1}{p,s,r}.dset{1,1}, com{1}{p,s,r}.dset{1,2},[],1,[],0);
      wer(com{1}{p,s,r}.dset{2,1}, com{1}{p,s,r}.dset{2,2},[],1,[],1);

      figure(4);
      wer(tcom2.dset{1,1}, tcom2.dset{1,2},[],1,[],0);
      wer(tcom2.dset{2,1}, tcom2.dset{2,2},[],1,[],1);
    
      [com{1}{p,s,r}, epc_cost ] = fusion_lda_qda_reg(nexpe, [1 2],[],1,1,1);
    
      [com{1}{p,s,r}, epc_cost ] = fusion_lda_qda_reg(nexpe, [1 2],[],1,1,1);

