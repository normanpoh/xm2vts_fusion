initialise
load baseline
%see main_baseline.m
load general 
%see main_fusion_fdwsum.m
n_samples = 11;

figure(1);
%display HTER results

leg_text = {'wsum','margin + wsum'};
epc_compare_print(com{2}, ccom{4}, epc_cost, 'dwsum+wsum', leg_text)

leg_text = {'mean','bf wsum'};
epc_compare_print(com{3}, com{8}, epc_cost, 'dwsum+wsum', leg_text)

leg_text = {'wsum','margin'};
epc_compare_print(com{2}, com{1}, epc_cost, 'dwsum+wsum', leg_text)

leg_text = {'mean','F-Norm+mean'};
epc_compare_print(com{3}, com{8}, epc_cost, 'dwsum+wsum', leg_text)

for i=1:size(com,2),
	[res{i}] = epc_global(com{i});
end;
for i=1:size(ccom,2),
	[cres{i},pNI, pNC] = epc_global(ccom{i});
end;



hter_significant_plot(cres{5}, cres{9}, pNI,pNC, epc_cost);
hter_significant_plot(cres{6}, cres{10}, pNI,pNC, epc_cost);
hter_significant_plot(cres{7}, cres{11}, pNI,pNC, epc_cost);
hter_significant_plot(cres{8}, cres{12}, pNI,pNC, epc_cost);


hter_significant_plot(res{3}, cres{6}, pNI,pNC, epc_cost);

%margin
hter_significant_plot(res{2}, res{1}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('wsum', 'margin');
print('-depsc2', '../Pictures/wsum_vs_margin_pooled.eps');

hter_significant_plot(res{3}, res{1}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('mean', 'margin');
print('-depsc2', '../Pictures/mean_vs_margin_pooled.eps');

%margin+sth else
hter_significant_plot(res{2}, cres{1}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('wsum', 'wsum+margin');
print('-depsc2', '../Pictures/wsum_vs_margin_pooled.eps');

hter_significant_plot(res{3}, cres{1}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('mean', 'mean+margin');
print('-depsc2', '../Pictures/mean_vs_margin_pooled.eps');

%F-ratio
hter_significant_plot(res{2}, res{4}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('wsum', 'Fratio+wsum');
print('-depsc2', '../Pictures/Fratio_wsum_pooled.eps');

hter_significant_plot(res{3}, res{5}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('mean', 'F-Norm+mean');
print('-depsc2', '../Pictures/mean_vs_Fratio_pooled.eps');

hter_significant_plot(res{7}, res{5}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('Z-Norm+mean', 'F-Norm+mean');
print('-depsc2', '../Pictures/ZNorm_vs_Fratio_pooled.eps');

hter_significant_plot(res{3}, res{7}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('mean', 'Z-Norm+mean');
subplot(1,1,1);
hold off;
plot(epc_cost(:,1), res{3}.eva.hter_apri*100,'b:'); hold on;
plot(epc_cost(:,1), res{7}.eva.hter_apri*100, 'b--'); 
plot(epc_cost(:,1), res{5}.eva.hter_apri*100, 'b'); 
legend('Mean', 'Z-Norm+Mean', 'F-Norm+Mean');
ylabel('HTER(%)');
xlabel('\alpha');
print('-depsc2', '../Pictures/ZNorm_vs_Fratio_vs_mean_pooled.eps');

subplot(1,1,1);
hold off;
plot(epc_cost(:,1), res{2}.eva.hter_apri*100,'b:'); hold on;
plot(epc_cost(:,1), res{6}.eva.hter_apri*100, 'b--'); 
plot(epc_cost(:,1), res{4}.eva.hter_apri*100, 'b'); 
legend('Mean', 'Z-Norm+Mean', 'F-Norm+Mean');
ylabel('HTER(%)');
xlabel('\alpha');

%F-ratio+margin+sth else
hter_significant_plot(res{2}, cres{3}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('wsum', 'Fratio+margin+wsum');
print('-depsc2', '../Pictures/wsum_vs_Fratio+wsum_pooled.eps');

hter_significant_plot(res{3}, cres{4}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('mean', 'Fratio+margin+mean');
print('-depsc2', '../Pictures/mean_vs_Fratio+mean_pooled.eps');

%best
hter_significant_plot(res{3}, res{5}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('mean', 'Fratio+mean');
print('-depsc2', '../Pictures/mean_vs_Fratio+mean_pooled.eps');

hter_significant_plot(res{5}, cres{12}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('Fratio+mean', 'Fratio+mean+margin');

%compare between wsum and brute-force weighted sum

hter_significant_plot(res{2}, res{8}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('wsum', 'bf wsum');
print('-depsc2', '../Pictures/wsum_vs_bfwsum_pooled.eps');

hter_significant_plot(res{3}, res{8}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('mean', 'bf wsum');
print('-depsc2', '../Pictures/wsum_vs_bfwsum_pooled.eps');

hter_significant_plot(res{3}, res{8}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('mean', 'bf wsum');


%F-ratio vs Z-Norm
hter_significant_plot(res{6}, res{4}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('Z-Norm with wsum', 'Fratio with wsum');
print('-depsc2', '../Pictures/Fratio_ZNorm_mean_pooled.eps');

hter_significant_plot(res{7}, res{5}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('Z-Norm with mean', 'Fratio with mean');
print('-depsc2', '../Pictures/Fratio_ZNorm_wsum_pooled.eps');


%F-ratio, margin + sth else
hter_significant_plot(res{2}, cres{1}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('wsum', 'Fratio+margin+wsum');
hter_significant_plot(res{3}, cres{1}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('mean', 'Fratio+margin+mean');

hter_significant_plot(cres{2}, cres{1}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('Fratio+margin+wsum', 'all');

hter_significant_plot(cres{3}, cres{1}, pNI,pNC, epc_cost);
subplot(2,1,1);legend('Fratio+margin+mean', 'all');


hter_significant_plot(res{4}, res{5}, pNI,pNC, epc_cost);


%p=1;s=1;r=1;
for s=1:3, %over 3 different configurations
	row = 0;
	rp_index = 0;
	for p=1:2, %over two protocols
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			b_expert = expe.P{p}.seq{s}(r,:);
			tmp.baseline = [baseline{p,b_expert(1)}.epc.eva.hter_apri;baseline{p,b_expert(2)}.epc.eva.hter_apri];
			tmp.min = min(tmp.baseline);
			tmp.mean = mean(tmp.baseline);

			for i=1:4,
				out{i}{p,s,r}.beta_mean = tmp.mean ./ com{i}{p,s,r}.epc.eva.hter_apri;
				out{i}{p,s,r}.beta_min = tmp.min ./ com{i}{p,s,r}.epc.eva.hter_apri;

			end;
			for i=1:8,

				cout{i}{p,s,r}.beta_mean = tmp.mean ./ ccom{i}{p,s,r}.epc.eva.hter_apri;
				cout{i}{p,s,r}.beta_min = tmp.min ./ ccom{i}{p,s,r}.epc.eva.hter_apri;
			end;

			%group by configuration:

			rp_index = rp_index + 1;
			for i=1:n_samples,
				row = row + 1;

				for j =1:4
					res_pooled{s}.mean(row,j) = out{j}{p,s,r}.beta_mean(i);
					res_pooled{s}.min(row,j) = out{j}{p,s,r}.beta_min(i);
					res{s,i}.mean(rp_index,j) = out{j}{p,s,r}.beta_mean(i);
					res{s,i}.min(rp_index,j) = out{j}{p,s,r}.beta_min(i);
				end;
				for j =1:8
					cres_pooled{s}.mean(row,j) = cout{j}{p,s,r}.beta_mean(i);
					cres_pooled{s}.min(row,j) = cout{j}{p,s,r}.beta_min(i);
					cres{s,i}.mean(rp_index,j) = cout{j}{p,s,r}.beta_mean(i);
					cres{s,i}.min(rp_index,j) = cout{j}{p,s,r}.beta_min(i);
				end;


			end;
		end;
	end;
end;

%further processing:
clear cres2;
for s=1:2,
	for i=1:n_samples,
		cres{s,i}.prctile.min = prctile(cres{s,i}.min, [25 50 75]);
		res{s,i}.prctile.min = prctile(res{s,i}.min, [25 50 75]);
		cres{s,i}.mean = mean(cres{s,i}.min);
		res{s,i}.mean = mean(res{s,i}.min);
		cres{s,i}.std = std(cres{s,i}.min);
		res{s,i}.std = std(res{s,i}.min);
	end;

	for j=1:3,
		cres2{s}.prc{j} =[];
		res2{s}.prc{j} =[];
	end;
	cres2{s}.mean =[];
	res2{s}.mean =[];
	cres2{s}.std =[];
	res2{s}.std =[];

	for i=1:n_samples,
		for j=1:3,
			cres2{s}.prc{j} = [cres2{s}.prc{j}; cres{s,i}.prctile.min(j,:)];
			res2{s}.prc{j} = [res2{s}.prc{j}; res{s,i}.prctile.min(j,:)];
		end;
		cres2{s}.mean = [cres2{s}.mean; cres{s,i}.mean];
		res2{s}.mean = [res2{s}.mean; res{s,i}.mean];
		cres2{s}.std= [cres2{s}.std; cres{s,i}.std];
		res2{s}.std = [res2{s}.std; res{s,i}.std];

	end;
end;

signs1 = {'b:','b','b--', 'r:','r','r--'};
signs2 = {'r:','r','r--', 'b:','b','b--'};

%plot the median values
for s=1:2,
	figure(s);subplot(1,1,1);
	for i=1:3,
		if i==1,hold off;end;
		plot( epc_cost(:,1), cres2{s}.prc{i}(:,4),signs1{i}); 
		if i==1,hold on;end;
		plot( epc_cost(:,1), res2{s}.prc{i}(:,2),signs2{i}); 
	end;
	ylabel('\beta_{min}');
	xlabel('\alpha');
end;


%a={'mean', 'wsum', 'dfwsum'};


%box plot
upper_limit_y= [8 6];
lower_limit_y= [0.5 0.5];
for s=1:2,
	figure(s+2);
	t=0;
	for i=1:2:n_samples,
		%subplot(1,n_samples,i);
		t=t+1;
		subplot(1,6,t);
		boxplot(cres{s,i}.min);
		tmp_axis = axis; tmp_axis([3, 4]) = [lower_limit_y(s) upper_limit_y(s)];axis(tmp_axis);
		xlabel(sprintf('%1.2f',epc_cost(i,1)));
		ylabel('');
	end;
end;


%plot the average beta values

%dwsum + sth else
xx = [4 2; 5 3];
leg{1} = {'wsum+margin', 'wsum'};
leg{2} = {'mean+margin', 'mean'};
tit = {'Multimodal', 'Intramodal'};
configf = 'margin';
analysis_general_a;

xx = [8 2];
leg{1} = {'wsum+mean+margin', 'mean'};
tit = {'Multimodal', 'Intramodal'};
configf = 'margin_mean';


%F-ratio + sth else
xx = [6 2; 7 3];
leg{1} = {'wsum+F-rato', 'wsum'};
leg{2} = {'mean+F-ratio', 'mean'};
tit = {'Multimodal', 'Intramodal'};
configf = 'frationorm';
analysis_general_a;

%dwsum + F-ratio + sth else
xx = [2 2; 3 3];
leg{1} = {'wsum+F-rato+margin', 'wsum'};
leg{2} = {'mean+F-ratio+margin', 'mean'};
tit = {'Multimodal', 'Intramodal'};
configf = 'frationorm_margin';
analysis_general_a;
