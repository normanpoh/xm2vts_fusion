function [res,pNI,pNC] = epc_global_baseline(res_com)
%[far_apri, frr_apri, hter_apri] = epc_global(res_com)
%call using:
%[sysA.epc.eva.far_apri, sysA.epc.eva.frr_apri, sysA.epc.eva.hter_apri] = epc_global(comB)
%[sysB.epc.eva.far_apri, sysB.epc.eva.frr_apri, sysB.epc.eva.hter_apri] = epc_global(comA)
%[ci_percentage] = hter_significant_plot(sysA, sysB, n_impostors, n_clients, cost, alpha, test)

initialise;

out.fa = [];
out.fr = [];
count = 0;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    count = count + 1;
    out.fa = [out.fa; res_com{p,b}.epc.eva.far_apri * NI];
    out.fr = [out.fr; res_com{p,b}.epc.eva.frr_apri * NC];
  end;
end;
out.NC = count * NC;
out.NI = count * NI;

res.eva.far_apri = sum(out.fa) / out.NI;
res.eva.frr_apri = sum(out.fr) / out.NC;
res.eva.hter_apri = (res.eva.far_apri+res.eva.frr_apri)/2;

pNC = out.NC;
pNI = out.NI;