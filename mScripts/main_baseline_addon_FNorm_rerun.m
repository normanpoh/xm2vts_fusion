cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

%load baseline baseline res_bes epc_cost
initialise;
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

leg = {'no norm', 'Z-shift', 'Z-norm', 'EER-shift'};
leg ={leg{:},'F-norm','F-norm-fix','F-norm-1'};
leg ={leg{:},'Z-shift,inv', 'Z-norm,inv', 'EER-shift,inv'};
leg ={leg{:},'F-norm,inv','F-norm-fix,inv','F-norm-1,inv'};

signs = {'gx-'};
signs ={signs{:},'b*-','bo-', 'bd-'};
signs ={signs{:},'r*-','k.-','ro--'};
signs ={signs{:},'b*:','bo:', 'bd:'};
signs ={signs{:},'rd:','ko:','ro-.'};

p=1;b=6;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('\nExperiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    
    for d=1:2, for k=1:2,
        expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    expe.label = data{p}.label;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %for MLP output
    if ismlp{p}(b),
      for d=1:2,for k=1:2,
          nexpe{1}.dset{d,k} = expe.dset{d,k};
          ex=1;
          nexpe{1}.dset{d,k}(:,ex) = tanh_inv(expe.dset{d,k}(:,ex));
        end;
      end;
      
      %filter the bad ones
      nexpe{2} = nexpe{1};
      for k=1:2,
        d=1;ex=1;
        selected = find(nexpe{2}.dset{d,k}(:,ex)>30);
        nexpe{2}.dset{d,k}(selected)=[];
        selected = find(nexpe{2}.dset{d,k}(:,ex)<-30);
        nexpe{2}.dset{d,k}(selected)=[];
      end;
      
      param = VR_analysis(nexpe{2}.dset{1,1},nexpe{2}.dset{1,2});      
      
      %now replace bad values with a reasonable one
      nexpe{3} = nexpe{1};
      
      for d=1:2, for k=1:2,
          ex=1;
          selected = find(nexpe{3}.dset{d,k}(:,ex)>30);
          nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_C(ex),size(selected,1),1);
          selected = find(nexpe{3}.dset{d,k}(:,ex)<-30);
          nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_I(ex),size(selected,1),1);
        end;
      end;
    
      myexpe = nexpe{3};  
      myexpe.label = data{p}.label;
    end;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    [baseline{1}{p,b}, epc_cost] = fusion_clientd_check(expe, 1, 'orig');
    [baseline{2}{p,b}, epc_cost] = fusion_clientd_check(expe, 1, 'Z-shift');
    [baseline{3}{p,b}, epc_cost] = fusion_clientd_check(expe, 1, 'Z-norm');
    [baseline{4}{p,b}, epc_cost] = fusion_clientd_check(expe, 1, 'EER-shift');
    %F-norm and its variations
    weight = fusion_clientd_Fnorm_tune(expe, 1, linspace(0,1,11), 0)
    [baseline{5}{p,b}, epc_cost] = fusion_clientd_check(expe, 1, 'F-norm', weight);
    [baseline{6}{p,b}, epc_cost] = fusion_clientd_check(expe, 1, 'F-norm', 0.5);
    [baseline{7}{p,b}, epc_cost] = fusion_clientd_check(expe, 1, 'F-norm', 0.5, 1); %single-sample!

    %for MLP output
    if ismlp{p}(b),
      [baseline{8}{p,b}, epc_cost] = fusion_clientd_check(myexpe, 1, 'Z-shift');
      [baseline{9}{p,b}, epc_cost] = fusion_clientd_check(myexpe, 1, 'Z-norm');
      [baseline{10}{p,b}, epc_cost] = fusion_clientd_check(myexpe, 1, 'EER-shift');

      weight = fusion_clientd_Fnorm_tune(myexpe, 1, linspace(0,1,11), 0)
      [baseline{11}{p,b}, epc_cost] = fusion_clientd_check(myexpe, 1, 'F-norm', weight);
      [baseline{12}{p,b}, epc_cost] = fusion_clientd_check(myexpe, 1, 'F-norm', 0.5);
      [baseline{13}{p,b}, epc_cost] = fusion_clientd_check(myexpe, 1, 'F-norm', 0.5, 1); %single-sample!  
    else
      baseline{8}{p,b} = baseline{2}{p,b};
      baseline{9}{p,b} = baseline{3}{p,b};
      baseline{10}{p,b} = baseline{4}{p,b};
      baseline{11}{p,b} = baseline{5}{p,b};  
      baseline{12}{p,b} = baseline{6}{p,b};  
      baseline{13}{p,b} = baseline{7}{p,b};  
    end;
    
    %clear memory
    list = [];%[1 4 8];1:size(baseline,2)
    for i=1:list,
      baseline{i}{p,b}.dset ={};
    end;

    %check
    list = setdiff([1:13], [4 10]);
    for i=1:13, cfg.res{i}.eva = baseline{i}{p,b}.epc.eva; end;
    if ismlp{p}(b),
      list = [1:13];      plot_all_epc(epc_cost(:,1),leg,signs, cfg, list,10);
    else
      list = [1:7];      plot_all_epc(epc_cost(:,1),leg,signs, cfg, list,10);
    end;

    drawnow;
  end;
end;

save main_baseline_addon_FNorm_rerun baseline
load main_baseline_addon_FNorm_rerun baseline

epc_cost = linspace(0.1,0.9,11);
for i=1:size(baseline,2),
  [res_bes{i}, pNI,pNC] = epc_global_baseline(baseline{i});
end;
cfg.res = res_bes;
b = [1:7];
figure(1);subplot(1,1,1);set(gca, 'Fontsize', 14);
plot_all_epc(epc_cost,leg,signs, cfg, b);
plot_all_roc(epc_cost,leg,signs, cfg, b);

%individual plot
pb_list=[];
t=0;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    t=t+1;
    txt = sprintf('Experiment %d (%s)\n',t,dat.P{p}.labels{b}); fprintf(1,txt);
    pb_list=[pb_list; p b];
    pb_name{t} = dat.P{p}.labels{b};
  end;
end;

isFH = [1 9];
isMLP = [1 4 5 9];
isMLPNotFH = setdiff(isMLP, isFH);
isFace = [1 2 3 4 5 9 10];
isFaceNotMLP = setdiff(isFace,isMLP);
isSpeech = setdiff(1:13, isFace);

[pb_name{isFaceNotMLP}]

for i=1:size(baseline,2),
  [mycfg{1}.res{i},pNI(1),pNC(1)] = epc_global_baseline_custom(baseline{i}, pb_list(isFaceNotMLP,:), NI, NC);
  [mycfg{2}.res{i},pNI(2),pNC(2)] = epc_global_baseline_custom(baseline{i}, pb_list(isMLPNotFH,:), NI, NC);
  [mycfg{3}.res{i},pNI(3),pNC(3)] = epc_global_baseline_custom(baseline{i}, pb_list(isFH,:), NI, NC);
  [mycfg{4}.res{i},pNI(4),pNC(4)] = epc_global_baseline_custom(baseline{i}, pb_list(isFace,:), NI, NC);
  [mycfg{5}.res{i},pNI(5),pNC(5)] = epc_global_baseline_custom(baseline{i}, pb_list(isSpeech,:), NI, NC);
  [mycfg{6}.res{i},pNI(6),pNC(6)] = epc_global_baseline_custom(baseline{i}, pb_list, NI, NC);
end;

b = setdiff([1:13],[4,10]);
b= [1 5 6]; tname='Fnorm-tune-or-not'; list=2:3;

b= [1 6 12]; tname='Fnorm-inv-or-not'; list=2:3;
b= [1 6 12 3 9 ]; tname='Fnorm-inv-or-not-Z';

b= [1 6 7 ]; tname='Fnorm-sample-size'; list=4:5;
b= [1 2 3 4 6 ]; tname='Fnorm-vs-others'; list=4:5;
b= [1 2 3 4 12 ]; tname='Fnorm-inv-vs-others';
b= [1 2 3 4 7 ]; tname='Fnorm1-vs-others';
b= [1 2 3 4 13 ]; tname='Fnorm1-inv-vs-others';

b= [1 3 7 ]; tname='Fnorm1-vs-others-simple'; list=4:5;

b=[1 2 8]; tname='Fnorm-Zshift'; list=2:3;
close all

for i=list,    
  figure(i);subplot(1,2,1);set(gca, 'Fontsize', 1);
  plot_all_epc(epc_cost,leg,signs, mycfg{i}, b, 14,0);
  subplot(1,2,2);set(gca, 'Fontsize', 14);
  plot_all_roc(epc_cost,leg,signs, mycfg{i}, b, 14,1,1);
  %fname = sprintf('../Pictures/%s_%d.eps',tname,i);  
  pause;
  %print('-depsc2', fname);
end;

%to tune or not to tune beta :F-norm, F-norm-fix
c=2;m=[1 6];
figure(7);hter_significant_plot(mycfg{c}.res{m(1)}, mycfg{c}.res{m(2)}, pNI(c), pNC(c), epc_cost(:,1));subplot(2,1,1); legend(leg{m});plot_all_epc(epc_cost,leg,signs, mycfg{c}, m);
%

c=2;m=[1 12];
figure(7);hter_significant_plot(mycfg{c}.res{m(1)}, mycfg{c}.res{m(2)}, pNI(c), pNC(c), epc_cost(:,1));subplot(2,1,1); legend(leg{m});plot_all_epc(epc_cost,leg,signs, mycfg{c}, m);

c=2;m=[1 6];
figure(7);hter_significant_plot(mycfg{c}.res{m(1)}, mycfg{c}.res{m(2)}, pNI(c), pNC(c), epc_cost(:,1));subplot(2,1,1); legend(leg{m});plot_all_epc(epc_cost,leg,signs, mycfg{c}, m);

c=3;m=[1 6];
figure(7);hter_significant_plot(mycfg{c}.res{m(1)}, mycfg{c}.res{m(2)}, pNI(c), pNC(c), epc_cost(:,1));subplot(2,1,1); legend(leg{m});plot_all_epc(epc_cost,leg,signs, mycfg{c}, m);

c=3;m=[1 12];
figure(7);hter_significant_plot(mycfg{c}.res{m(1)}, mycfg{c}.res{m(2)}, pNI(c), pNC(c), epc_cost(:,1));subplot(2,1,1); legend(leg{m});plot_all_epc(epc_cost,leg,signs, mycfg{c}, m);

c=5;m=[3 6];
figure(7);hter_significant_plot(mycfg{c}.res{m(1)}, mycfg{c}.res{m(2)}, pNI(c), pNC(c), epc_cost(:,1));subplot(2,1,1); legend(leg{m});plot_all_epc(epc_cost,leg,signs, mycfg{c}, m);

t_=[];
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    t_=[t_; baseline{5}{p,b}.beta baseline{11}{p,b}.beta];
  end;
end;

[pb_list t_]


list = [6 7 ]; tname='Fnorm-sample-size';
list = [12 13 ]; tname='Fnorm-sample-size-inv';
%plot all possible DET
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('Experiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    
    set(gca,'fontsize',14);
    for i=1:length(list),
      wer(baseline{list(i)}{p,b}.dset{2,1},baseline{list(i)}{p,b}.dset{2,2},[],2,[],i);
    end;
    Make_DET(0.25);
    legend(leg{list});
    tit=sprintf('P %d: %s',p,dat.P{p}.labels{b});
    title(tit);
    drawnow;
    fname = sprintf('../Pictures/%s_DET_%d_%d.eps',tname,p,b);
    print('-depsc2', fname);
  end;
end;

%delete scores
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    %clear memory
    list = [1:13];
    for i=list,
      baseline{i}{p,b}.dset ={};
    end;
  end;
end;

save main_baseline_addon_FNorm_rerun_light baseline
    