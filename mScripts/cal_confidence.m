function conf = cal_confidence(df1_list)
%function conf = cal_confidence(df1_list)
% calculate confidence from a set of scores
% df1_list contains n_rows of df1 scores
% Each column is processed separately
for i=1:size(df1_list,2),
  n_size = length(find(df1_list(:,i)>0));
  conf(i) = n_size/size(df1_list,1);
end;
