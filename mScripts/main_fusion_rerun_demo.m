tit={'orig','Z','Z-inv','F','F-inv',};


for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      %print remarks
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);

      list=[1 3 9 6 12]; %Z, F-fix, F-fix-inv
      for i=1:length(list),
	for d=1:2, for k=1:2,
	    nexpe{i}.dset{d,k} = [baseline{list(i)}{p,b(1)}.dset{d,k} baseline{list(i)}{p,b(2)}.dset{d,k}];
	  end;
	end;
      end;
      
      for i=1:5;
	subplot(2,3,i);
	draw_empiric(nexpe{i}.dset{1,1},nexpe{i}.dset{1,2});
      end;
      pause;
    end;
  end;
end;



