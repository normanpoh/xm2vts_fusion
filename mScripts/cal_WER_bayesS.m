function [opt_weight, alpha, WER, n_backoff] = cal_WER_bayesS(bayesS,expe, dodisplay, criterion)

if (nargin < 3),
	dodisplay = 0;
end;

if (nargin < 4),
	criterion = 1;
end;

alpha = linspace(0,1,101)';
%alpha = linspace(0,1,5)';

weight = [alpha 1-alpha];
%weight
t_=[];
for i=1:size(alpha,1),
	nbayesS = com_bayesS(bayesS,weight(i,:));
	[WER(i), xx, spdf] = cal_min_WER2(nbayesS, [0.5 0.5], expe, criterion);

    if (dodisplay),
	figure(i);
	w = expe.dset{1,1} * weight(i,:)';
	s = expe.dset{1,2} * weight(i,:)';
	hold off;
        [value_, thrd_] =wer(w, s,[1 1], 5);
	%[value_, thrd_] =wer(w, s,[1 1]);
        hold on;
	plot(xx, spdf(:,1),'r:'); 
	plot(xx, spdf(:,2),'b:');
	%plot(xx, spdf(:,1),'r--'); 
	%plot(xx, spdf(:,2),'b--');
	weight(i,:)
	[v_, index_]=min(abs(spdf(:,1)-spdf(:,2)));
	[thrd_, xx(index_), thrd_ - xx(index_)]
	t_ =[t_;value_, WER(i) ];
	pause;
    end;
end;
[v, index]=min(WER);
opt_weight = weight(index,:);

%backoff strategy
%index = find (WER < 0.001);
%n_backoff = size(index,2);
%if (prod(size(index)) > 0),
%	fprintf(1,'Turn to back-off strategy (%d iterations)!\n', n_backoff);
%	c=0;
%	for i=index
%		w = expe.dset{1,1} * weight(i,:)';
%		s = expe.dset{1,2} * weight(i,:)';
%		c=c+1;
%		nWER(c) = wer(w, s,[1 1]);
%	end;
%	[v, ind]=min(nWER);%
%
%	opt_weight = weight(ind,:);
%	alpha = alpha(index,1);
%	WER=nWER;
%	%WER(index) = WER(1:n_backoff);
%
%else
%	[v, ind]= min(WER);
%	opt_weight = weight(ind,:);%
%
%end;

if (dodisplay),
	throw_away = find(WER ==0);
	WER(throw_away)=[];
	alpha(throw_away)=[];

	figure;
	t_(throw_away,:)=[];
	plot(t_(:,1), t_(:,2),'+');
end;




function [min_WER, xx, spdf]  = cal_min_WER(nbayesS, cost, expe, criterion)

	dat = [nbayesS(1).mu nbayesS(2).mu];
	n_thrds = 101;

	K=2; %two class
	xx = linspace(min(dat),max(dat), n_thrds)';
	pxomega_pdf = zeros(n_thrds,K);
	for k = 1:K
		pxomega_pdf(:,k) = gmmb_pdf(xx, nbayesS(k).mu, nbayesS(k).sigma, nbayesS(k).weight);
        	%pxomega_cdf(:,k) = gmmb_cdf(xx, nbayesS(k).mu, nbayesS(k).sigma, nbayesS(k).weight);    
    end
   	for i=1:size(pxomega_pdf,1),
		spdf(i,:) = sum(pxomega_pdf(1:i,:),1);
	end;
	spdf(:,1) = spdf(:,1) / sum(pxomega_pdf(:,1));
	spdf(:,2) = spdf(:,2) / sum(pxomega_pdf(:,2));
    	%1 - spdf(:,1)

    %spdf_(:,1) = log(sum(pxomega_pdf(:,1)) - spdf(:,1) + realmin) - log(sum(pxomega_pdf(:,1)));
    %spdf_(:,2) = log(sum(pxomega_pdf(:,1)) - spdf(:,1) + realmin) - log(sum(pxomega_pdf(:,1)));

    %pxomega_cdf(:,1) = 1 - pxomega_cdf(:,1);
    %pxomega_cdf(:,1) =  pxomega_cdf(:,1) * cost(1);
    %pxomega_cdf(:,2) =  pxomega_cdf(:,2) * cost(2);

    spdf_(:,1) = -(log( max(1 - spdf(:,1), realmin))) - cost(1);
    spdf_(:,2) = -(log( max(spdf(:,2), realmin))) - cost(2);

    %spdf(:,1) = (1- pxomega_pdf(:,1)) * cost(1);
    %spdf(:,2) = pxomega_pdf(:,2) * cost(2);
    %spdf(:,1) = (1- pxomega_cdf(:,1)) * cost(1);
    %spdf(:,2) = pxomega_cdf(:,2) * cost(2);

	%min_WER = abs(spdf_(index,1) - spdf_(index,2));
	to_del = find(spdf_(:,1) > 700);
	spdf_(to_del,:) = [];xx(to_del)=[];
	to_del = find(spdf_(:,2) > 700);
	spdf_(to_del,:) = [];xx(to_del)=[];

    [value_, index]=min(abs(spdf_(:,1) - spdf_(:,2)));
    chosen = find(abs(spdf_(:,1) - spdf_(:,2)) < value_ + 6);

    if (criterion == 1),
        if (size(index,1) == 0)
            min_WER=+inf;
        else
            %min_WER = -log(exp(-spdf_(index(1),1)) + exp(-spdf_(index(1),2)));
	    min_WER = mean(-log(exp(-spdf_(chosen,1)) + exp(-spdf_(chosen,2))));
        end;
    elseif (criterion == 2),
    	%min_WER = exp(-spdf_(index,1));%-log(exp(-spdf_(index,1)) + exp(-spdf_(index,2)));
	%spdf = pxomega_cdf;
	%[value_, index]=min(abs(spdf(:,1) - spdf(:,2)));
	%min_WER = pxomega_cdf(index,1) + pxomega_cdf(index,2);
    elseif (criterion == 3),
	%min_WER = log(exp(-spdf_(index,1)
	%spdf_(index,1) + spdf_(index,2);
    end;


    spdf = spdf_;


function [min_WER, xx, spdf]  = cal_min_WER2(nbayesS, cost, expe, criterion)
	dat = [nbayesS(1).mu nbayesS(2).mu];
	n_thrds = 101;

	K=2; %two class
	xx = linspace(min(dat),max(dat), n_thrds)';
	pxomega_pdf = zeros(n_thrds,K);
	for k = 1:K
		pxomega_pdf(:,k) = gmmb_pdf(xx, nbayesS(k).mu, nbayesS(k).sigma, nbayesS(k).weight);
        	%pxomega_cdf(:,k) = gmmb_cdf(xx, nbayesS(k).mu, nbayesS(k).sigma, nbayesS(k).weight);    
        end

	for i=1:size(pxomega_pdf,1),
		spdf(i,:) = sum(pxomega_pdf(1:i,:),1);
	end;
	spdf(:,1) = spdf(:,1) / sum(pxomega_pdf(:,1));
	spdf(:,2) = spdf(:,2) / sum(pxomega_pdf(:,2));
	spdf(:,1) = 1- spdf(:,1);

	cost = spdf(:,1) * cost(1) + spdf(:,2) *cost(2);
	[min_WER, index]=min(cost);
