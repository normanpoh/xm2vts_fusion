function [com, epc_cost] = fusion_RDA_client(expe, chosen, toplot)
%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
        chosen = [1:2];
end;

if (nargin < 3),
        toplot = 0;
end;

%go through each id
for d=1:2,for k=1:2,
	out{d,k} = [];%zeros(size(expe.dset{d,k},1),1);
end;end;

%get the model label
model_ID = unique(expe.label{1,1});

%perform client-dependent processing
fprintf(1, 'Processed ID:\n');
for id=1:size(model_ID,1),
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      if (size(index{d,k},1)==0),
	error([ 'size is zero of id ', num2str(id)]);
      end;
      tmp.dset{d,k} = expe.dset{d,k}(index{d,k},chosen);
    end;
  end;
  
  %Quadratic discriminant analysis
  [mycom, epc_cost ] = fusion_gmm(tmp, [1 2], 1, [],1);
  draw_empiric(tmp.dset{1,1}, tmp.dset{1,2});
  pause;
  %fname = sprintf('../Pictures/tata_%d_%d_%02d.eps');
  %print('-depsc2', fname);
  
  %Linear discriminant analysis
  bayesS(1).sigma = (mycom.bayesS(1).sigma + mycom.bayesS(2).sigma)/2;
  bayesS(2).sigma = bayesS(1).sigma;
  bayesS(1).mu = mycom.bayesS(1).mu;
  bayesS(2).mu = mycom.bayesS(2).mu;
  bayesS(1).apriories = mycom.bayesS(1).apriories;      
  bayesS(2).apriories = mycom.bayesS(2).apriories;
  bayesS(1).weight = mycom.bayesS(1).weight;
  bayesS(2).weight = mycom.bayesS(2).weight;
  [mycom2, epc_cost ] = fusion_gmm(nexpe, [1 2], 1,bayesS,0);
  
  for d=1:2,for k=1:2,
      %combine the fused scores
      clientd{id}.dset{d,k} = mycom2.dset{d,k};
      out{d,k}(index{d,k},1) = clientd{id}.dset{d,k};
    end;
  end;
  com.CD_bayesS{id} = bayesS;
  
  fprintf(1, '.%d', id);
end;
com.dset = out;

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

   
