%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts
load main_fusion_allinfo.mat com epc_cost
initialise;
load baseline;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%load list:
seq = {[1 2], [3 4], [5 6], [7 8]};
seq{5} = [1 2 5 6 7 8];
seq{6} = [1 2 3 4 7 8];
seq{7} = [1:8];
M = size(seq,2);
%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
	                end;end;

			%perform F-ratio normalisation
			for d=1:2,for k=1:2,
				nexpe{2}.dset{d,k} = [baseline{2}{p,b(1)}.dset{d,k} , baseline{2}{p,b(2)}.dset{d,k} ];
			end;end;

			%Perform Z-Norm
			for d=1:2,for k=1:2,
				nexpe{3}.dset{d,k} = [baseline{3}{p,b(1)}.dset{d,k} , baseline{3}{p,b(2)}.dset{d,k} ];
			end;end;

			%use margin scores
			[tmp_, nexpe{4}, trans, epc_cost] = fusion_dwsum_margin(nexpe{1});

			%second-level fusion
			for d=1:2,for k=1:2,
				new_expe.dset{d,k} = [nexpe{1}.dset{d,k} nexpe{2}.dset{d,k} nexpe{3}.dset{d,k} nexpe{4}.dset{d,k} ];
			end;end;

			%for m=1:M,  [tmp_, com{m}{p,s,r}] = fusion_rbf(new_expe, seq{m}, 'nonorm',20,2); %gmm
                        %end;

	    	        %for m=1:M,  [com{m+M}{p,s,r}, epc_cost] = fusion_svm(new_expe, seq{m});%svm
	                %end;

	            	for m=1:M,  
                                %by cross-validation to find the C values
                                ratio = [0.5 0.5];

                                tmp{1} = splitdata(new_expe.dset{1,1}, ratio);
                                tmp{2} = splitdata(new_expe.dset{1,2}, ratio);
                                tdata.dset{1,1} = tmp{1}{1};
                                tdata.dset{1,2} = tmp{2}{1};
                                tdata.dset{2,1} = tmp{1}{2};
                                tdata.dset{2,2} = tmp{2}{2};                

                                cv = [1:3];
                                for c=1:length(cv),
                                        fprintf(1,'c is %d\n', c);
                                        fprintf(1,txt);
                                        c_value=10;
                                        [tmp_com{c}, epc_cost] = fusion_svm(tdata, seq{m}, [], [], c_value, 'poly', cv(c));
                                end;        
                                clear hter_apri tot_hter_apri                        
                                for c=1:length(cv),
                                        tot_hter_apri(c) = sum(tmp_com{c}.epc.eva.hter_apri);
                                        hter_apri(c) = tmp_com{c}.epc.eva.hter_apri(6);
                                end;
                        
                                [tmp_ , index]= min(tot_hter_apri);
                                kparam = cv(index(1));
                                [com{m+2*M}{p,s,r}, epc_cost] = fusion_svm(new_expe, seq{m}, [], [], [], 'poly', kparam);
                                com{m+2*M}{p,s,r}.polyd = kparam;
                                fprintf(1,txt);
				

                        end;

			%com{m+M}{p,s,r}.net={};
                        com{m+2*M}{p,s,r}.net={};
                        
                        %free the memory
			for i=1:size(com,2),
				com{i}{p,s,r}.dset={};
			end;
                end;
	end;
end;

save main_fusion_allinfo.mat com epc_cost

expert = {'gmm', 'svm-lin', 'svm-poly'};
norm_m = {'orig', 'Z-Norm', 'F-Norm', 'margin', 'orig-Z-margin', 'orig-F-margin', 'orig-Z-F-margin'};
c=0;
for j=1:2,
	for i=1:7,
		c=c+1;
		leg{c} = [norm_m{i} ',' expert{j}];
end;end;
signs = {'ko--', 'bs--', 'bx--', 'k^--','rv--','rd--','r*--'};
signs = {signs{:},'ko-', 'bs-', 'bx-', 'k^-','rv-','rd-','r*-'};

%overall performance
for i=1:size(com,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(com{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(com{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(com{i},[2 3]); %intramodal
end;

figure(1);
list = 1:size(com,2);
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);

figure(2);
M=7;
list =[ 1 3 4 6 ] + M;
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);

list = [1:7]+M;
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);

print('-depsc2', '../Pictures/main_fusion_allinfo.eps');

