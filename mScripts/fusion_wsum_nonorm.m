function [com, epc_cost] = fusion_wsum_nonorm(expe, chosen, weight)

%default config: 
n_samples = 11; epc_range = [0.1 0.9];

if (nargin < 2),
        chosen = [1:2];
end;

%zero-mean unit variance normalisation
%model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
%model.std = std([expe.dset{1,1}; expe.dset{1,2}]);
%[expe.dset{1,1}, expe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
%[expe.dset{2,1}, expe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);

%get the fixed weight
%if (prod(size(weight)) == 0 ),
if ( nargin < 3),
	param = VR_analysis(expe.dset{1,1}, expe.dset{1,2});
	[com.weight, com.Sw] = cal_weight_fisher(chosen, param);
	%com.weight = cal_weight_brute(chosen, param, 101);
else
	com.weight = weight;
end;

%fuse the scores:
for d=1:2,
    for k=1:2,
	com.dset{d,k} = expe.dset{d,k}(:,chosen) * com.weight';
    end;
end;

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

