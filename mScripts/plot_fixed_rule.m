
a=[-1 1 -1 1];
n_samples = 100;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[xtesta1,xtesta2]=meshgrid(linspace(a(1),a(2),n_samples), linspace(a(3),a(4),n_samples));
[na,nb]=size(xtesta1);
xtest1=reshape(xtesta1,1,na*nb);
xtest2=reshape(xtesta2,1,na*nb);
xtest=[xtest1;xtest2]';

%min
[ypred{1}] = min(xtest')';
[ypred{2}] = max(xtest')';
[ypred{3}] = mean(xtest')';

tmp = abs(xtest');
tmp_s = sign(xtest');
for i=1:size(xtest,1),
    [value, index]=max(tmp(:,i));
    ypred{4}(i) = value * tmp_s(index,i);
end;

for i=1:4,
    ypredmat{i}=reshape(ypred{i},na,nb);
    figure(1);subplot(2,2,i);
    hold off;
    colormap(gray(256));
    contourf(xtesta1,xtesta2,ypredmat{i},50);shading flat;
    hold on;
    %draw_empiric(nexpe{n}.dset{2,1},nexpe{n}.dset{2,2});
    [cs,h]=contour(xtesta1,xtesta2,ypredmat{i},[-0.5 0 0.5],'k');
    clabel(cs,h);
    colorbar;
end;
