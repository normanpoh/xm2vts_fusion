%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;
%load main_norm_effect_addon_wsum_user.mat com epc_cost

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);

			%load score files
			for d=1:2,for k=1:2,
				nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
			end;end;

			%zero-mean unit variance normalisation
			nexpe{2} = zmuv_norm(nexpe{1});

			%use margin scores
			nexpe{3} = convert2margin_scores(nexpe{1});

			%for MLP output
			for d=1:2,for k=1:2,
				nexpe{4}.dset{d,k} = nexpe{1}.dset{d,k};
				for ex=1:2,
					if (mlp{p,s,r}(ex)),
						fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
						nexpe{4}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex));
					end;
				end;
			end;end;


			%second-level fusion
			for d=1:2,for k=1:2,
				new_expe.dset{d,k} = [nexpe{1}.dset{d,k} nexpe{2}.dset{d,k} nexpe{3}.dset{d,k} nexpe{4}.dset{d,k} ];
			end;end;
			new_expe.label = data{p}.label;

			chosen = [1:2];
			[com{1}{p,s,r}, epc_cost] = fusion_wsum_client(new_expe, chosen);

			chosen = [3:4];
			[com{2}{p,s,r}, epc_cost] = fusion_wsum_client(new_expe, chosen);

			chosen = [5:6];
			[com{3}{p,s,r}, epc_cost] = fusion_wsum_client(new_expe, chosen);

			chosen = [7:8];
			[com{4}{p,s,r}, epc_cost] = fusion_wsum_client(new_expe, chosen);

			%free the memory
			for i=1:size(com,2),
				com{i}{p,s,r}.dset={};
			end;

		end;
	end;
end;

save norm_expe_addon_wsum_user.mat com epc_cost

%overall performance
for i=1:size(com,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(com{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(com{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(com{i},[2 3]); %intramodal
end;

signs = {'kx-',  'kd-','ko-','k*-'};
signs = {signs{:}, 'rx--',  'rd--','ro--','r*--'};
signs = {signs{:}, 'gx-.',  'gd-.','go-.','g*-.'};
signs = {signs{:}, 'bx:',  'bd:','bo:','b*:'};
signs = {signs{:}, 'cx:',  'cd:','co:','c*:'};

expert = {'mean', 'wsum', 'rbf-fisher', 'gmm','rbf-wDT', 'rbf-DT', 'DT'};
se = {'o','s','x','^','v','d','*'};
norm_m = {'orig', 'zmun', 'margin', 'inv'};
sn = {'k', 'r', 'g','b'};
sn2= {'-','--', '-.', ':'};

c=0;
for i=1:size(norm_m,2),
	for j=1:size(expert,2),
		c=c+1;
		leg{c} = [norm_m{i} ',' expert{j}];
		signs{c} = [sn{i}  se{j} sn2{i}]
end;end;
leg{29} = 'poss,DT'; 	signs{29} = 'bx-';
leg{30} = 'poss1,DT'; 	signs{30} = 'bx--';

%answer the following questions:
%1. How normalisation can affect classifier performance?
c=1;
%c=2;
%c=3;

list = 1:size(com,2);

%list = [1 2 4 5 6 8 9 10 12 13 14 16 17];
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);

%sort by model performs
list = [1 8 15 22]; 
i=0;
while i <=6,
	plot_all_epc(epc_cost,leg,signs, out.cfg{1}, [list 29 30]);
	pause;
	list = list + 1; i=i+1;
	print('-depsc2', ['../Pictures/norm_by_model', num2str(i),'.eps']);
end;

%sort by normalisation methods
list = [1:2 4:7];
i=0;
while i <=3,
	plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
	pause;	
	i=i+1;	list = list + 7;
	print('-depsc2', ['../Pictures/norm_by_norm', num2str(i),'.eps']);
end;

%choose the best of each method: gmm
figure(2);
list = [4 11 18 25 29 30];
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);

%plot with significant test
b = [20,27];
b=[16 23];
b=[16 30];
b=[4 25];
b=[29 30];
hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});

print('-depsc2', '../Pictures/orig_margin_only_mean_pooled.eps');

%to prope further, what is 
%2. how many components are there?
for p=1:2, %over two protocols
	out.gmm_c{p}=[];
	for s=1:3, %over 3 different configurations
	for r=1:size(expe.P{p}.seq{s},1),
		out.gmm_c{p} = [out.gmm_c{p} size(com{10}{p,s,r}.bayesS(1).weight,1)];
	end;end;
end;


%plot of scores
[nexpe{1}.dset{1,1}, nexpe{1}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
[nexpe{1}.dset{2,1}, nexpe{1}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);

p=1;s=1;r=10;
for i=1:4,
	figure(i); set(gca, 'Fontsize', 14);
	draw_empiric(nexpe{i}.dset{1,1},nexpe{i}.dset{1,2});
	xlabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,1)});
	ylabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,2)});
	pause;
	fname = sprintf('../Pictures/norm_scatter_%d.eps', i);
	print('-depsc2', fname);
end;

tmp1=nexpe{1}.dset{1,1}(:,1);
tmp2=nexpe{1}.dset{1,2}(:,1);
[tran.x{i}, tran.margin{i}] = cal_margin_scores(tmp1,tmp2,[0.5 0.5],1);
print('-depsc2', '../Pictures/demo_margin_scores.eps');


%margin scatter plot
i=4;
figure(i); hold off; set(gca, 'Fontsize', 14); 
draw_empiric(nexpe{i}.dset{1,1},nexpe{i}.dset{1,2});
xlabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,1)});
ylabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,2)});
