%load configurations
initialise;
load baseline;
load general;
clear res_bes;
%Start experiment

p=1;s=1;r=4;
for p=1:2, %over two protocols
	for s=1:3, %over 3 different configurations
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
		fprintf(1,txt);
		for r=1:size(expe.P{p}.seq{s},1),
			%print remarks
			txt = sprintf('Experiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
			
			b = expe.P{p}.seq{s}(r,:);
			%load score files
			[expe.dset{1,1}, expe.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev, b);
			[expe.dset{2,1}, expe.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva, b);

			%NI(p,s,r) = size(expe.dset{2,1},1);
			%NC(p,s,r) = size(expe.dset{2,2},1);
			%[com_dfwsum{p,s,r}, epc_cost] = fusion_fdwsum(expe);

			[com{1}{p,s,r}, raw, trans, epc_cost] = fusion_dwsum_margin(expe);
			[com{2}{p,s,r}, epc_cost] = fusion_wsum_brute(expe);
			[com{3}{p,s,r}, epc_cost] = fusion_wsum(expe, [1 2],[0.5 0.5]);

			%perform F-ratio normalisation
			%clientd = fusion_clientd_Fratio(expe);
			for d=1:2,for k=1:2,
				clientd.dset{d,k} = [baseline{2}{p,b(1)}.dset{d,k} , baseline{2}{p,b(2)}.dset{d,k} ];
			end;end;
	
			[com{4}{p,s,r}, epc_cost] = fusion_wsum(clientd);
			[com{5}{p,s,r}, epc_cost] = fusion_wsum(clientd, [1 2],[0.5 0.5]);

			%Perform Z-Norm
			%clientd = fusion_clientd_ZNorm(expe);
			for d=1:2,for k=1:2,
				clientd.dset{d,k} = [baseline{3}{p,b(1)}.dset{d,k} , baseline{3}{p,b(2)}.dset{d,k} ];
			end;end;

			[com{6}{p,s,r}, epc_cost] = fusion_wsum(clientd); 
			[com{7}{p,s,r}, epc_cost] = fusion_wsum(clientd, [1 2],[0.5 0.5]);


			[com{8}{p,s,r}, epc_cost] = fusion_wsum_brute(expe);

			%perform F-ratio normalisation with global sigma
			%clientd = fusion_clientd_Fratio_common_sigma(expe);
			%[com{5}{p,s,r}, epc_cost] = fusion_wsum(clientd);
			
			%second-level fusion
			for d=1:2,for k=1:2,
		        	expe_com.dset{d,k} = [com{1}{p,s,r}.dset{d,k} com{2}{p,s,r}.dset{d,k} com{3}{p,s,r}.dset{d,k} com{4}{p,s,r}.dset{d,k} com{5}{p,s,r}.dset{d,k}];
			end;end


			%[ccom{1}{p,s,r}, epc_cost] = fusion_wsum(expe_com, [1 2]);
			%[ccom{2}{p,s,r}, epc_cost] = fusion_wsum(expe_com, [1 3]);
			%[ccom{3}{p,s,r}, epc_cost] = fusion_wsum(expe_com, [1 4]);
			%[ccom{4}{p,s,r}, epc_cost] = fusion_wsum(expe_com, [1 5]);

			[ccom{5}{p,s,r}, epc_cost] = fusion_wsum(expe_com, [1 2], [0.5 0.5]);
			[ccom{6}{p,s,r}, epc_cost] = fusion_wsum(expe_com, [1 3], [0.5 0.5]);
			[ccom{7}{p,s,r}, epc_cost] = fusion_wsum(expe_com, [1 4], [0.5 0.5]);
			[ccom{8}{p,s,r}, epc_cost] = fusion_wsum(expe_com, [1 5], [0.5 0.5]);

			[ccom{9}{p,s,r}, epc_cost] = fusion_wsum_brute(expe_com, [1 2]);
			[ccom{10}{p,s,r}, epc_cost] = fusion_wsum_brute(expe_com, [1 3]);
			[ccom{11}{p,s,r}, epc_cost] = fusion_wsum_brute(expe_com, [1 4]);
			[ccom{12}{p,s,r}, epc_cost] = fusion_wsum_brute(expe_com, [1 5]);

			%free the memory
			for i=1:size(com,2),
				com{i}{p,s,r}.dset={};
			end;
			for c=1:size(ccom,2),
				ccom{c}{p,s,r}.dset = {};
			end;

		end;
	end;
end;

save general com ccom epc_cost

%3D plot of margin
p=1;s=1;r=4;
%load scores
d=1;k=1;
x = [expe.dset{d,1}(:,1);expe.dset{d,2}(:,1)];
y = [expe.dset{d,1}(:,2);expe.dset{d,2}(:,2)];
z = sum([trans.dset{d,1};trans.dset{d,2}]  ,2 );

plot(x, trans.dset{d,k}(:,1),'+');
hold on;
plot(y, trans.dset{d,k}(:,2),'r+');

hold off;
plot(x, z,'+');
hold on;
plot(y, trans.dset{d,k}(:,2),'r+');

xlin = linspace(min(x),max(x),66);
ylin = linspace(min(y),max(y),66);

[X,Y] = meshgrid(xlin,ylin);

Z = griddata(x,y,z,X,Y,'cubic');

hold off;
mesh(X,Y,Z) %interpolated
axis tight; hold on
xlabel('Expert 1');
ylabel('Expert 2');
zlabel('margin');
colorbar
%[AZ,EL] = view;
%AZ = -7.5000;
%EL = 78;
AZ = 29.5;
EL = 30;
view(AZ,EL);
%plot3(x,y,z,'.','MarkerSize',1) %nonuniform
print('-depsc2', '../Pictures/margin_plot2.eps');



[wer_min, thrd_min, x] = wer(ccom{7}{p,s,r}.dset{1,1}, ccom{7}{p,s,r}.dset{1,2} , [0.5, 0.5], 1);
beta = linspace(0.9,1,11)';

[wer_min, thrd_min, x] = wer((:,1), dev.sheep(:,1), [0.5, 0.5], 1);
[tmp, out] = fusion_clientd_Fratio2(expe, 1, beta);



figure(1);
percentage = 1;
param1 = VR_analysis(clientd.dset{1,1},clientd.dset{1,2});
VR_draw(clientd.dset{1,1},clientd.dset{1,2}, percentage,param1);
f_ratio(param1)

figure(2);
param2 = VR_analysis(expe.dset{1,1},expe.dset{1,2});
VR_draw(expe.dset{1,1},expe.dset{1,2}, percentage,param2);
f_ratio(param2)

%save the processed variables
%save fdwsum com_dfwsum com_mean com_wsum epc_cost




hter_significant_plot(com{2}{p,s,r}.epc, com{4}{p,s,r}.epc, NI, NC, epc_cost);
hter_significant_plot(com{2}{p,s,r}.epc, ccom{2}{p,s,r}.epc, NI, NC, epc_cost);
hter_significant_plot(com{3}{p,s,r}.epc, ccom{2}{p,s,r}.epc, NI, NC, epc_cost);

%[wer_min, thrd_min, x] = wer(clientd.dset{1,1(:,1)}, clientd.dset{1,2},[0.5, 0.5], 1);

for i=1:4,
        figure(i);
        [wer_min, thrd_min, x] = wer(com{i,p,s,r}.dset{1,1}, com{i,p,s,r}.dset{1,2},[0.5, 0.5], 1);
end;

