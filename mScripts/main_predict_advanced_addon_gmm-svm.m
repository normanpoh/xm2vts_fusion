cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

load  norm_expe_addon_gmm_cv.mat com epc_cost
com_gmm_cv = com;
clear com;

load main_predict_advanced com epc_cost
initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

load main_predict_advanced com epc_cost
%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      %print remarks
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);
      
      %load score files
      for d=1:2,for k=1:2,
	  nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
	end;
      end;
      
      n=1;
      
      %reformat the (wrong) bayesS
      bayesS = com_gmm_cv{1}{p,s,r}.bayesS;
      for i=1:2,
	nbayesS(i).mu = bayesS{i}.mu;
	nbayesS(i).sigma = bayesS{i}.sigma;
	nbayesS(i).weight = bayesS{i}.weight;
      end;
      bayesS = nbayesS;
      
      %based on bayesS of gmm with cv
      [weight,tt, bayesS] = find_weight(nexpe{n},'gmm-svm',0,0, 30,bayesS);%,bayesS,0.1,101);
      [com{7}{p,s,r}, epc_cost] = fusion_wsum_nonorm(nexpe{n}, [1 2], weight);
      com{7}{p,s,r}.bayesS = bayesS;

      %clf reset;
      %subplot(2,2,1);%set(gca,'Fontsize',10);
      subplot(1,1,1);hold off;
      draw_theory_bayesS(bayesS);
      xlabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,1)});
      ylabel(dat.P{p}.labels{expe.P{p}.seq{s}(r,2)});
      fname = sprintf('../Pictures/gmm-svm_%d_%d_%02d.eps',p,s,r);      
      print('-depsc2', fname);
      %free the memory
      for i=1:size(com,2),
	com{i}{p,s,r}.dset={};
      end;
      
    end;
  end;
end;

save main_predict_advanced com epc_cost



