function [com, epc_cost] = fusion_gauss_client_parzen(expe, chosen, width, toplot)
%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
        chosen = [1:2];
end;

if (nargin < 3 | length(width)==0),
  width = 0.5;
end;
if (nargin < 4),
        toplot = 0;
end;

%go through each id
for d=1:2,for k=1:2,
	com.dset{d,k} = zeros(size(expe.dset{d,k},1),1);
end;end;
for d=1:2,for k=1:2,
	expe.dset{d,k} = expe.dset{d,k}(:,chosen);
end;end;

%get the model label
model_ID = unique(expe.label{1,1});

for id=1:size(model_ID,1),
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      if (size(index{d,k},1)==0),
        warning([ 'size is zero for id ', num2str(id)]);
      end;
      tmp{id}.dset{d,k} = expe.dset{d,k}(index{d,k},:);
    end;
  end;
end;

for id=1:size(model_ID,1),
  d=1;
  k=1;
  %with 3 or more examples, we can estimate cov
  local{id}.bayesS(k).sigma = cov(tmp{id}.dset{1,k});
  local{id}.bayesS(k).mu = mean(tmp{id}.dset{1,k})';
  local{id}.bayesS(k).weight = 1;

  %parzen window estimate for the client data
  k=2;
  local{id}.bayesS(k).mu = tmp{id}.dset{d,k}';
  for i=1:size(tmp{id}.dset{d,k},1),
    local{id}.bayesS(k).sigma(:,:,i) = width * eye(2);
    local{id}.bayesS(k).weight(i,:) = 1/  size(tmp{id}.dset{d,k},1);
  end;
end;

%visualise
if (toplot),
  for id=1:size(model_ID,1),
    %if (local{id}.status(2) == 3),
    %if (size(tmp{id}.dset{1,2},1)>2),
    %if rank(local{id}.bayesS(2).sigma)>=2,
    id
    draw_theory_bayesS(local{id}.bayesS);
    %end;
  end;
  figure(2);
  draw_empiric(expe.dset{1,1}, expe.dset{1,2});
end;

%perform client-dependent processing
fprintf(1, 'Processed ID:\n');
for id=1:size(model_ID,1),

  [tcmp{id}, epc_cost ] = fusion_gmm(tmp{id}, [1 2], 1,local{id}.bayesS,0);
  %draw_empiric(tmp{id}.dset{2,1},tmp{id}.dset{2,2});
  for d=1:2,for k=1:2,
      %combine the fused scores
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      com.dset{d,k}(index{d,k},1) = tcmp{id}.dset{d,k};
    end;
  end;
  com.CD_bayesS{id} = local{id}.bayesS;
  
  fprintf(1, '.%d', id);
end;

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

   
