function [res,pNI,pNC] = epc_global(res_com, config_list)
%[far_apri, frr_apri, hter_apri] = epc_global(res_com, config_list)
%call using:
%[sysA.epc.eva.far_apri, sysA.epc.eva.frr_apri, sysA.epc.eva.hter_apri] = epc_global(comB)
%[sysB.epc.eva.far_apri, sysB.epc.eva.frr_apri, sysB.epc.eva.hter_apri] = epc_global(comA)
%[ci_percentage] = hter_significant_plot(sysA, sysB, n_impostors, n_clients, cost, alpha, test)

initialise;

if (nargin < 2),
	config_list = [1:3];
end;

out.fa = [];
out.fr = [];
count = 0;

for s=config_list, %over 3 different configurations
  for p=1:2, 
    %over two protocols
    %txt = sprintf('%s for protocol %d\n', remark{s}, p);
    %fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      count = count + 1;
      out.fa = [out.fa; res_com{p,s,r}.epc.eva.far_apri * NI];
      out.fr = [out.fr; res_com{p,s,r}.epc.eva.frr_apri * NC];
    end;
  end;
end;
out.NC = count * NC;
out.NI = count * NI;

res.eva.far_apri = sum(out.fa) / out.NI;
res.eva.frr_apri = sum(out.fr) / out.NC;
res.eva.hter_apri = (res.eva.far_apri+res.eva.frr_apri)/2;

pNC = out.NC;
pNI = out.NI;