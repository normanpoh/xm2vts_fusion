function [com] = test_method(wolves, sheep, model)

[nwolves, nsheep] = normalise_scores(wolves, sheep, model.mean, model.std);
com.wolves = mean(nwolves, 2);
com.sheep = mean(nsheep, 2);
