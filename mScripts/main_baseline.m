cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

load baseline baseline res_bes epc_cost
initialise;
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('Experiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    
    for d=1:2, for k=1:2,
	expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    expe.label = data{p}.label;
    
    %load score files
    %NI(p,b) = size(expe.dset{2,1},1);
    %NC(p,b) = size(expe.dset{2,2},1);
    
    %client-dependent normalisation
    [baseline{1}{p,b}, epc_cost] = fusion_baseline(expe);
    %[baseline{2}{p,b}, epc_cost] = fusion_baseline_Fratio(expe);
    %[baseline{3}{p,b}, epc_cost] = fusion_baseline_ZNorm(expe);
    %[baseline{4}{p,b}, epc_cost] = fusion_baseline_EERNorm(expe);
    
    
    %Z-norm
    for d=1:2,for k=1:2,
	texpe.dset{d,k} = [expe.dset{d,k}, baseline{2}{p,b}.dset{d,k}, ...
			   baseline{3}{p,b}.dset{d,k}];
      end;
    end;
    %draw_empiric(texpe.dset{1,1},texpe.dset{1,2});
    %draw_empiric(texpe.dset{2,1},texpe.dset{2,2});

    %Z-norm
    chosen=[1 3];
    mix_bline{1}{p,b} = fusion_svm(texpe, chosen); 
    while  mean(mix_bline{1}{p,b}.epc.dev.hter)>0.5,
      mix_bline{1}{p,b} = fusion_svm(texpe, chosen); 
    end;

    mix_bline{2}{p,b} = fusion_wsum_brute_nonorm(texpe, chosen,[0.5 0.5]);

    %F-norm
    chosen=[1 2];
    mix_bline{3}{p,b} = fusion_svm(texpe, chosen); 
    while  mean(mix_bline{1}{p,b}.epc.dev.hter)>0.5,
      mix_bline{3}{p,b} = fusion_svm(texpe, chosen); 
    end;

    mix_bline{4}{p,b} = fusion_wsum_brute_nonorm(texpe, chosen,[0.5 0.5]);
    
    hold off;
    plot(epc_cost(:,1), baseline{1}{p,b}.epc.eva.hter_apri*100,'bx-');
    hold on;
    plot(epc_cost(:,1), baseline{2}{p,b}.epc.eva.hter_apri*100,'b*-');
    plot(epc_cost(:,1), baseline{3}{p,b}.epc.eva.hter_apri*100,'bo--');
    plot(epc_cost(:,1), mix_bline{1}{p,b}.epc.eva.hter_apri*100,'g*:');
    plot(epc_cost(:,1), mix_bline{2}{p,b}.epc.eva.hter_apri*100,'go:');
    legend('no norm', 'Z-norm', 'F-Norm', 'mix-Z,svm','mix-F,svm'); 
    drawnow;
  end;
end;


baseline{5} = mix_bline{1};
baseline{6} = mix_bline{2};

for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('Experiment %d (%s) w=(%1.2f,%1.2f)\n',b,dat.P{p}.labels{b},baseline{5}{p,b}.weight(1),baseline{5}{p,b}.weight(2)); fprintf(1,txt);

    %close all;
    
    hold off;
    plot(baseline{1}{p,b}.epc.eva.far_apri*100,baseline{1}{p,b}.epc.eva.frr_apri*100,'bo-');
    hold on;
    plot(baseline{3}{p,b}.epc.eva.far_apri*100,baseline{3}{p,b}.epc.eva.frr_apri*100,'gs-.');
    plot(baseline{5}{p,b}.epc.eva.far_apri*100,baseline{5}{p,b}.epc.eva.frr_apri*100,'rx--');
    legend('no norm', 'Z-norm', 'Z-mixed');
    xlabel('FAR');    ylabel('FRR');
    title(txt);
    fname = sprintf('../Pictures/bline_mixed_roc_%d_%d_roc.eps',p,b);
    print('-depsc2', fname);
    
    hold off;
    plot(epc_cost(:,1), baseline{1}{p,b}.epc.eva.hter_apri*100,'bo-');
    hold on;
    plot(epc_cost(:,1), baseline{3}{p,b}.epc.eva.hter_apri*100,'gs:');
    plot(epc_cost(:,1), baseline{5}{p,b}.epc.eva.hter_apri*100,'rx--');
    xlabel('\alpha');    ylabel('HTER(%)');
    legend('no norm', 'Z-norm', 'Z-mixed');
    fname = sprintf('../Pictures/bline_mixed_epc_%d_%d.eps',p,b);
    print('-depsc2', fname);
  end;
end;

wer(baseline{1}{p,b}.dset{2,1},baseline{1}{p,b}.dset{2,2},[],2,[],1);
    wer(baseline{3}{p,b}.dset{2,1},baseline{3}{p,b}.dset{2,2},[],2,[],2);
    wer(baseline{5}{p,b}.dset{2,1},baseline{5}{p,b}.dset{2,2},[],2,[],3);
    legend('no norm', 'Z-norm', 'Z-mixed');
    title(txt);
    fname = sprintf('../Pictures/bline_mixed_%d_%d.eps',p,b);
    print('-depsc2', fname);


%the following could not work!
%overfitted when adapting the cov
%underfitted  when adapting the mean (using common cov)
%try not to use the client info
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('Experiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    
    for d=1:2,
      for k=1:2,
	expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    nexpe = expe;
      
    if (b==1),
      %prepare filtered data
      for d=1:2,
        for k=1:2,
	  nexpe.dset{d,k} = tanh_inv(nexpe.dset{d,k});
	  selected = find(nexpe.dset{d,k}>30);
	  nexpe.dset{d,k}(selected,:)=[];
	  selected = find(nexpe.dset{d,k}<-30);
	  nexpe.dset{d,k}(selected,:)=[];
        end;
      end;
    end;
    
    param = VR_analysis(nexpe.dset{1,1},nexpe.dset{1,2});
    
    rexpe = nexpe;
    if (b==1),
      for d=1:2,
        for k=1:2,
	  rexpe.dset{d,k} = tanh_inv(expe.dset{d,k});
	  selected = find(rexpe.dset{d,k}>30);
	  rexpe.dset{d,k}(selected,:) = repmat(param.mu_C, length(selected),1);
	  selected = find(rexpe.dset{d,k}<-30);
	  rexpe.dset{d,k}(selected,:)=repmat(param.mu_I, length(selected),1);
      	end;
      end;
    end;
    %wer(nexpe.dset{1,1}, nexpe.dset{1,2},[],1);
    %wer(rexpe.dset{1,1}, rexpe.dset{1,2},[],1);

    [com{p,b}.epc.dev, com{p,b}.epc.eva, epc_cost]  = epc(rexpe.dset{1,1}, rexpe.dset{1,2}, rexpe.dset{2,1}, rexpe.dset{2,2}, n_samples,epc_range);
    
     rel = linspace(0,1,11);
     for r0 = 1:length(rel),
       [tcom{r0}, epc_cost] = fusion_baseline_wLLR(rexpe, param,rel(r0));
     end;

     %test the predictability
      out.dev=[]; out.eva = [];
      for r0 = 1:length(rel),
	out.dev = [out.dev tcom{r0}.epc.dev.hter(6)];
	out.eva = [out.eva tcom{r0}.epc.eva.hter_apri(6)];
      end;

      %show the predictability between dev and eva set
      %meaning that there is no overfitting
      plot(out.dev, out.eva,'+');
      %show the optimisation  performance w.r.t the reliance param
      plot(rel,out.dev);
      plot(rel,out.eva);      
      [tmp, index] = min(out.dev); 
      %gauss_com{1}{p,s,r} = gcom{index};
      baseline2{1}{p,b} = tcom{index};


      %draw_empiric(texpe.dset{1,1},texpe.dset{1,2});
      %mix_com{1}{p,s,r} = fusion_svm(texpe);
      mix_bline{1}{p,b} = fusion_svm(texpe);

      
      mix_bline{2}{p,b} = fusion_svm(texpe);
      
      hold off;
      plot(epc_cost(:,1), baseline2{1}{p,b}.epc.eva.hter_apri*100,'b-');
      hold on;
      plot(epc_cost(:,1), com{p,b}.epc.eva.hter_apri*100,'r--');
      plot(epc_cost(:,1), mix_bline{1}{p,b}.epc.eva.hter_apri*100,'go:');
      
      mix_bline{1}{b,p}.net = {};
      %clear memory
      clear_list = [5];
      for i=1:size(clear_list,2),
	baseline{clear_list(i)}{p,b}.dset = {};
      end;	
  end;
end;

%used global client mu and global client sigma:
%and local impostor mu and sigma
%This should be equivalent to Z-Norm but ...
%did not work! 
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('Experiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    
    for d=1:2,
      for k=1:2,
	expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;

    nexpe = expe;
    if (b==1),
      %prepare filtered data
      for d=1:2,
        for k=1:2,
	  nexpe.dset{d,k} = tanh_inv(nexpe.dset{d,k});
	  selected = find(nexpe.dset{d,k}>30);
	  nexpe.dset{d,k}(selected,:)=[];
	  selected = find(nexpe.dset{d,k}<-30);
	  nexpe.dset{d,k}(selected,:)=[];
        end;
      end;
    end;
    
    param = VR_analysis(nexpe.dset{1,1},nexpe.dset{1,2});
    
    rexpe = expe;
    if (b==1),
      for d=1:2,
        for k=1:2,
	  rexpe.dset{d,k} = tanh_inv(expe.dset{d,k});
	  selected = find(rexpe.dset{d,k}>30);
	  rexpe.dset{d,k}(selected,:) = repmat(param.mu_C, length(selected),1);
	  selected = find(rexpe.dset{d,k}<-30);
	  rexpe.dset{d,k}(selected,:)=repmat(param.mu_I, length(selected),1);
      	end;
      end;

    end;
    %wer(nexpe.dset{1,1}, nexpe.dset{1,2},[],1);
    %wer(rexpe.dset{1,1}, rexpe.dset{1,2},[],1);
    
    expe.label = data{p}.label;
    rexpe.label = data{p}.label;

    [com{p,b}.epc.dev, com{p,b}.epc.eva, epc_cost]  = epc(rexpe.dset{1,1}, rexpe.dset{1,2}, rexpe.dset{2,1}, rexpe.dset{2,2}, n_samples,epc_range);
    
    %renormalise scores
    data = [expe.dset{1,1}; expe.dset{1,2}];
    labels = [ zeros(size(expe.dset{1,1},1),1);ones(size(expe.dset{1,2},1),1)];
    n_gmm = [1 4];
    FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
    bayesS = gmmb_create(data, labels + 1, 'FJ', FJ_params{:});

    for d=1:2,for k=1:2,
	c=1;tmp1 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
	c=2;tmp2 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
	%c=1;tmp1 = gmmb_pdf(expe.dset{d,k}, param.mu_I, param.cov_I, 1);
	%c=2;tmp2 = gmmb_pdf(expe.dset{d,k}, param.mu_C, param.cov_C, 1);
	gcom.dset{d,k} = log(tmp2+realmin)-log(tmp1+realmin);
      end;
    end;

    data = [gcom.dset{1,1}; gcom.dset{1,2}];
    labels = [ zeros(size(expe.dset{1,1},1),1);ones(size(expe.dset{1,2},1),1)];
    n_gmm = [1 4];
    FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
    bayesS = gmmb_create(data, labels + 1, 'FJ', FJ_params{:});
    for d=1:2,for k=1:2,
	c=1;tmp1 = gmmb_pdf(gcom.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
	c=2;tmp2 = gmmb_pdf(gcom.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
	%c=1;tmp1 = gmmb_pdf(expe.dset{d,k}, param.mu_I, param.cov_I, 1);
	%c=2;tmp2 = gmmb_pdf(expe.dset{d,k}, param.mu_C, param.cov_C, 1);
	gcom2.dset{d,k} = log(tmp2+realmin)-log(tmp1+realmin);
      end;
    end;
    wer(expe.dset{1,1}, expe.dset{1,2},[],1,[],1);    
    wer(gcom.dset{1,1}, gcom.dset{1,2},[],1,[],1);    
    wer(gcom2.dset{1,1}, gcom2.dset{1,2},[],1,[],2);    
    
    
    baseline2{1}{p,b} = fusion_baseline_wLLR(rexpe, param,0);
    
    for d=1:2,for k=1:2,
	texpe.dset{d,k} = [rexpe.dset{d,k}, baseline2{1}{p,b}.dset{d,k}];
      end;
    end;
    


      %draw_empiric(texpe.dset{1,1},texpe.dset{1,2});
      %mix_com{1}{p,s,r} = fusion_svm(texpe);
      mix_bline{1}{p,b} = fusion_svm(texpe);

      
      mix_bline{2}{p,b} = fusion_svm(texpe);
      
      hold off;
      plot(epc_cost(:,1), baseline2{1}{p,b}.epc.eva.hter_apri*100,'b-');
      hold on;
      plot(epc_cost(:,1), com{p,b}.epc.eva.hter_apri*100,'r--');
      plot(epc_cost(:,1), mix_bline{1}{p,b}.epc.eva.hter_apri*100,'go:');
      
      mix_bline{1}{b,p}.net = {};
      %clear memory
      clear_list = [5];
      for i=1:size(clear_list,2),
	baseline{clear_list(i)}{p,b}.dset = {};
      end;	
  end;
end;

p=1;b=3;
method = 4;
figure(1);
[wer_min, thrd_min, x] = wer(baseline{method}{p,b}.dset{1,1}, baseline{method}{p,b}.dset{1,2}, [0.5, 0.5], 1);
param = VR_analysis(baseline{method}{p,b}.dset{1,1}, baseline{method}{p,b}.dset{1,2})

figure(2);
[wer_min, thrd_min, x] = wer(expe.dset{1,1}, expe.dset{1,2}, [0.5, 0.5], 1);

param = VR_analysis(baseline{2}{p,b}.dset{1,1}, baseline{2}{p,b}.dset{1,2});
%plot DET curve only
subplot(1,1,1);                                                                                             
[wer_min, thrd_min, x] = wer(expe.dset{2,1}, expe.dset{2,2}, [0.5, 0.5], 2);                                
[wer_min, thrd_min, x] = wer(baseline{2}{p,b}.dset{2,1}, baseline{2}{p,b}.dset{2,2}, [0.5, 0.5], 2,-1000,1);
[wer_min, thrd_min, x] = wer(baseline{3}{p,b}.dset{2,1}, baseline{3}{p,b}.dset{2,2}, [0.5, 0.5], 2,-1000,2);
legend('no norm', 'F-Norm', 'Z-Norm');
print('-deps2c','../Pictures/ssc_FZnorm.eps');


%plotting
[wer_min, thrd_min, x] = wer(baseline{method}{p,b}.dset{1,1}, baseline{method}{p,b}.dset{1,2}, [0.5, 0.5], 2);
print('-deps2c','../Pictures/DET.eps')
[wer_min, thrd_min, x] = wer(baseline{method}{p,b}.dset{1,1}, baseline{method}{p,b}.dset{1,2}, [0.5, 0.5], 3);
print('-deps2c','../Pictures/ROC.eps')
[wer_min, thrd_min, x] = wer(baseline{method}{p,b}.dset{1,1}, baseline{method}{p,b}.dset{1,2}, [0.5, 0.5], 4);
print('-deps2c','../Pictures/density.eps')
[wer_min, thrd_min, x] = wer(baseline{method}{p,b}.dset{1,1}, baseline{method}{p,b}.dset{1,2}, [0.5, 0.5], 5);
print('-deps2c','../Pictures/FAR-FRR.eps')
[wer_min, thrd_min, x] = wer(baseline{method}{p,b}.dset{1,1}, baseline{method}{p,b}.dset{1,2}, [0.5, 0.5], 6);
print('-deps2c','../Pictures/WER.eps')

% 
cal_margin(baseline{2}{p,b}.dset{1,1}, baseline{2}{p,b}.dset{1,2}, [0.5 0.5], 1);
print('-deps2c','../Pictures/margin_example.eps');

%epc_range = [0.1 0.9];
[com.epc.dev, com.epc.eva, epc_cost]  = epc(baseline{3}{p,b}.dset{1,1}, baseline{3}{p,b}.dset{1,2}, baseline{3}{p,b}.dset{2,1}, baseline{3}{p,b}.dset{2,2}, n_samples,epc_range);

for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('Experiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    hter_significant_plot(baseline{3}{p,b}.epc, baseline{2}{p,b}.epc, NI, NC, epc_cost);
    subplot(2,1,1); legend('Z-Norm','F-Norm');
    title(dat.P{p}.labels{b});
    pause;
    fname = sprintf('../Pictures/bline_%d_%d.eps',p,b);
    print('-deps2c',fname);
  end;
end;

%regenreate
for i=1:size(baseline,2),
  [res_bes{i}, pNI,pNC] = epc_global_baseline(baseline{i});
end;

hter_significant_plot(res_bes{1}, res_bes{3}, pNI,pNC, epc_cost);
subplot(2,1,1); legend('no norm','Z-Norm');

hter_significant_plot(res_bes{1}, res_bes{2}, pNI,pNC, epc_cost);
subplot(2,1,1); legend('no norm','Fratio-Norm');

hter_significant_plot(res_bes{3}, res_bes{2}, pNI,pNC, epc_cost);
subplot(2,1,1); legend('Z-Norm','F-Norm');
print('-depsc2', '../Pictures/Z_F_norm_pooled.eps');

%plot EPC of different curves
subplot(1,1,1);hold off;
plot(epc_cost(:,1), res_bes{1}.eva.hter_apri*100, 'b:'); hold on;
plot(epc_cost(:,1), res_bes{3}.eva.hter_apri*100, 'b--');
plot(epc_cost(:,1), res_bes{2}.eva.hter_apri*100, 'b');
%plot(epc_cost(:,1), res_bes{4}.eva.hter_apri*100, 'b-.');
plot(epc_cost(:,1), res_bes{5}.eva.hter_apri*100, 'go-');
plot(epc_cost(:,1), res_bes{6}.eva.hter_apri*100, 'gx-');
%legend('no norm','Z-Norm','F-Norm','EER');
legend('no norm','Z-Norm','F-Norm');
legend('no norm','Z-Norm','F-Norm','Z-combined','F-combined');
xlabel('\alpha');
ylabel('HTER(%)');
print('-depsc2', '../Pictures/norm_no_Z_F_norm_EER_pooled.eps');
print('-depsc2', '../Pictures/norm_no_Z_F_norm_pooled.eps');

save baseline baseline res_bes epc_cost

hold off;
plot(res_bes{1}.eva.far_apri*100, res_bes{1}.eva.frr_apri*100, 'bs:'); hold on;
plot(res_bes{3}.eva.far_apri*100, res_bes{3}.eva.frr_apri*100, 'b*--');
plot(res_bes{2}.eva.far_apri*100, res_bes{2}.eva.frr_apri*100, 'b+');
%plot(epc_cost(:,1), res_bes{4}.eva.hter_apri*100, 'b-.');
plot(res_bes{5}.eva.far_apri*100, res_bes{5}.eva.frr_apri*100, 'go-');
plot(res_bes{6}.eva.far_apri*100, res_bes{6}.eva.frr_apri*100, 'gx-');
%legend('no norm','Z-Norm','F-Norm','EER');
legend('no norm','Z-Norm','F-Norm');
legend('no norm','Z-Norm','F-Norm','Z-combined','F-combined');
