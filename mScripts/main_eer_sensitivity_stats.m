cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

initialise;
%load norm_expe.mat com epc_cost
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%Start experiment
p=1;b=2;

t=0;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('\nExperiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);

    for d=1:2, for k=1:2,
        expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    expe.label = data{p}.label;

    myexpe = expe;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %for MLP output
    if ismlp{p}(b),
      for d=1:2,for k=1:2,
          nexpe{1}.dset{d,k} = expe.dset{d,k};
          ex=1;
          nexpe{1}.dset{d,k}(:,ex) = tanh_inv(expe.dset{d,k}(:,ex));
        end;
      end;
      
      %filter the bad ones
      nexpe{2} = nexpe{1};
      for k=1:2,
        d=1;ex=1;
        selected = find(nexpe{2}.dset{d,k}(:,ex)>30);
        nexpe{2}.dset{d,k}(selected)=[];
        selected = find(nexpe{2}.dset{d,k}(:,ex)<-30);
        nexpe{2}.dset{d,k}(selected)=[];
      end;
      
      param = VR_analysis(nexpe{2}.dset{1,1},nexpe{2}.dset{1,2});      
      
      %now replace bad values with a reasonable one
      nexpe{3} = nexpe{1};
      
      for d=1:2, for k=1:2,
          ex=1;
          selected = find(nexpe{3}.dset{d,k}(:,ex)>30);
          nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_C(ex),size(selected,1),1);
          selected = find(nexpe{3}.dset{d,k}(:,ex)<-30);
          nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_I(ex),size(selected,1),1);
        end;
      end;
    
      myexpe = nexpe{3};  
      myexpe.label = data{p}.label;
    end;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    [bayesS, paramj, param] = cal_clientd_stat(myexpe);

    if(1==0),
      %compare ecdf and user-specific mixture cdf
      %plot the two
      [value, thrd, tmp, err, xx] = estimate_EER_gmm(myexpe.dset{1,1}, myexpe.dset{1,2}, [], 1, bayesS{1});
      %plot the residual
      plot(xx,err*50)
      %just plot theory, without any empirical data
      %[value, thrd, tmp] = estimate_EER_gmm([], [], [], 1, bayesS{1});
      drawnow;
    end;
    
    %now analye the plot
    %C = size(bayesS{d},2); %component size (num of clients in our case)
    for d=1:2, %training, test sets
      for k=1:2,
        tata = [bayesS{d}(k).sigma(1,1,:)];
        tata = reshape(tata, length(tata),1);
        out.sigma{d}(k,:) = tata;
        out.mu{d}(k,:) = bayesS{d}(k).mu;
      end;
    end;
    
    if(1==0),
      %t=t+1; subplot(4,6,t)
      figure(1);
      hold off;
      plot(out.mu{1}(1,:),out.mu{2}(1,:),'r+');
      hold on;
      plot(out.mu{1}(2,:),out.mu{2}(2,:),'bo');
      txt = sprintf('[%d]',round(t/2)); title(txt);
      grid on; axis square

      %t=t+1; subplot(4,6,t)
      figure(2);
      hold off;
      plot(out.sigma{1}(1,:),out.sigma{2}(1,:),'r+');
      hold on;
      plot(out.sigma{1}(2,:),out.sigma{2}(2,:),'bo');
      grid on; axis square
    end;
    
    tmpX = [out.mu{1}(1,:), out.mu{1}(2,:)];
    xx = linspace(min(tmpX),max(tmpX),100);

    figure(3);subplot(1,2,1)
    signs = {'r+','bo'};
    signs_curve = {'r--','b--','g-'};
    for k=1:2,
      datX = [out.mu{1}(k,:)];    datY = [out.mu{2}(k,:)];
      out.coef_mu{k} = polyfit(datX,datY,1);    y = polyval(out.coef_mu{k},xx);
      if k==1, hold off; end;
      plot(xx,y, signs_curve{k});
      if k==1, hold on; end;
      plot(out.mu{1}(k,:),out.mu{2}(k,:),signs{k});
    end;
    datX = [out.mu{1}(1,:),out.mu{1}(2,:)];    datY = [out.mu{2}(1,:),out.mu{2}(2,:)];
    out.coef_mu{3} = polyfit(datX,datY,1);    y = polyval(out.coef_mu{3},xx);
    plot(xx,y,signs_curve{3});     grid on;
    xlabel('\mu^k_j|train');    ylabel('\mu^k_j|test');
    %curve fitting individually for sigma
    %figure(4);
    subplot(1,2,2)
    tmpX = [out.sigma{1}(1,:), out.sigma{1}(2,:)];
    xx = linspace(min(tmpX),max(tmpX),100);
    k=1; %for impostor only
    out.coef_sigma{1} = polyfit(out.sigma{1}(k,:), out.sigma{2}(k,:),1);

    %second method: bias is zero
    out.coef_sigma{2} = [mean(out.sigma{2}(k,:)  ./ out.sigma{1}(k,:)), 0];
    %third non-adapted method: bias is zero
    out.coef_sigma{3} = [out.coef_mu{3}(1), 0];

    hold off;       plot(out.sigma{1}(k,:),out.sigma{2}(k,:),'r+');  grid on; hold on;
    for i=1:3,
      y = polyval(out.coef_sigma{i},xx);
      plot(xx,y,signs_curve{i});
    end;
    xlabel('\sigma^I_j|train');    ylabel('\sigma^I_j|test');
    
    %pause;
    txt = sprintf('../Pictures/main_eer_sensitivity_stats_emp_%d_%02d.eps',p,b);
    print('-depsc2', txt);
    
    tmpX=[out.mu{1}' out.sigma{1}(2,:)'];
    plot3(tmpX(:,1),tmpX(:,2),tmpX(:,3),'+');
    xlabel('\mu^I_j');ylabel('\mu^C_j');zlabel('\sigma^I_j');
    view(2)
    plot((tmpX(:,1),tmpX(:,2),'+');
    
  end;
end;

p=1;b=2;
for d=1:2, for k=1:2,
    expe.dset{d,k} = data{p}.dset{d,k}(:,b);
  end;
end;
expe.label = data{p}.label;
myexpe = expe;
[bayesS, paramj, param] = cal_clientd_stat(myexpe);
%now analye the plot
%C = size(bayesS{d},2); %component size (num of clients in our case)
for d=1:2, %training, test sets
  for k=1:2,
    tata = [bayesS{d}(k).sigma(1,1,:)];
    tata = reshape(tata, length(tata),1);
    out.sigma{d}(k,:) = tata;
    out.mu{d}(k,:) = bayesS{d}(k).mu;
  end;
end;
tmp=[out.mu{1}' out.sigma{1}'];
labels = zeros(size(tmp,1),1);
n_gmm = [1 10];
FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
bayesS = gmmb_create(tmp, labels + 1, 'FJ', FJ_params{:});

%set the number of users to sample
n_user_list = [50:50:1000];
n_samples = 100;
d=1;
for n = 1:length(n_user_list),
  fprintf(1,'.');
  for i=1:n_samples,
    tot = round(bayesS.weight * n_user_list(n));
    tmp = [];
    for c=1:length(bayesS.weight),
      tmp = [tmp; mvnrnd(bayesS.mu(:,c), bayesS.sigma(:,:,c),tot(c))];
    end;
    clear nbayesS
    for k=1:2,
      nbayesS{d}(k).sigma(1,1,:) = abs(tmp(:,2+k));
      nbayesS{d}(k).mu = tmp(:,k)';
      nbayesS{d}(k).weight = ones(1,n_user_list(n))/n_user_list(n);
      nbayesS{d}(k).apriories = 1;
    end;
    [eer(i), thrd] = estimate_EER_gmm([],[], [], 0, nbayesS{d});
  end;
  out.stat(n,:) = quantil(eer', [2.5 50 97.5])';
end;
