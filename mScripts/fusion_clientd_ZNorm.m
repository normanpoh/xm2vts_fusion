function [out] = fusion_clientd_ZNorm(expe)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

%zero-mean unit variance normalisation
model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
model.std = std([expe.dset{1,1}; expe.dset{1,2}]);
[expe.dset{1,1}, expe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
[expe.dset{2,1}, expe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);

%get the model label
model_ID = unique(expe.label{1,1});

%go through each id
for d=1:2,for k=1:2,
	out.dset{d,k} = [];
end;end;

%perform client-dependent normalisation
for id=1:size(model_ID,1),

	%get the data set associated to the ID
	for d=1:2,for k=1:2,
	    index{d,k} = find(expe.label{d,k} == model_ID(id));
	    tmp{d,k} = expe.dset{d,k}(index{d,k},:);
	end;end;
    
	%Apply Z normalisation on training and test sets, in a ID-dependnent manner
	model.mean_I = mean(tmp.dset{1,2});
	model.std_I = std(tmp.dset{1,2});

	[clientd{id}.dset{1,1}, clientd{id}.dset{1,2}] = normalise_scores(tmp{1,1}, tmp{1,2}, model.mean_I, model.std_I);
	[clientd{id}.dset{2,1}, clientd{id}.dset{2,2}] = normalise_scores(tmp{2,1}, tmp{2,2}, model.mean_I, model.std_I);

	%compute the fused scores
	for d=1:2,for k=1:2,
	    %out.dset{d,k} = [out.dset{d,k}; clientd{id}.dset{d,k}];
  	    out.dset{d,k}(index{d,k},:) = clientd{id}.dset{d,k};
	end;end;

end;