function [com, com_gmm, llh] = fusion_rbf(expe, chosen, nonorm, max_gmm, min_gmm)
% use [com, com_gmm] = fusion_rbf(expe, chosen, 'nonorm') for no normalisation

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
        chosen = [1:2];
end;

if (nargin < 3),
	nonorm = 0;
elseif (length(nonorm)==0),
	nonorm = 0;
end;

if (nargin < 4),
	max_gmm = 5;
end;

if (nargin < 5),
	min_gmm = 2;
end;

if (nonorm ~= 0),
	%zero-mean unit variance normalisation
	model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
	model.std = std([expe.dset{1,1}; expe.dset{1,2}]);
	[expe.dset{1,1}, expe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
	[expe.dset{2,1}, expe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);
end;

%make chosen
for d=1:2,for k=1:2,
	expe.dset{d,k} = expe.dset{d,k}(:,chosen);
end;end;

%standard procedure
data = [expe.dset{1,1}; expe.dset{1,2}];
labels = [ ones(size(expe.dset{1,1},1),1);2*ones(size(expe.dset{1,2},1),1)];

%data = [nexpe{1}.dset{1,1}; nexpe{1}.dset{1,2}];
%labels = [ ones(size(nexpe{1}.dset{1,1},1),1);2*ones(size(nexpe{1}.dset{1,2},1),1)];

%FJ_params = { 'Cmax', 100, 'Cmin', 4, 'thr', 1e-2, 'animate', 0, 'verbose', 1,'covtype',1};
FJ_params = { 'Cmax', max_gmm, 'Cmin', min_gmm, 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});

%hold off;
%plot(bayesS(1).mu(:,1), diag(bayesS(1).sigma(:,:,1)),'b+');hold on;
%plot(bayesS(1).mu(:,2), diag(bayesS(1).sigma(:,:,2)),'bo');
%plot(bayesS(2).mu, diag(bayesS(2).sigma),'r+');

%hold off;
%plot(bayesS(1).mu(:,1), diag(bayesS(1).sigma(:,:,1)),'r+');hold on;
%plot(bayesS(2).mu(:,1), diag(bayesS(2).sigma(:,:,1)),'b+');

%calculate likelihoods
for d=1:2,for k=1:2,
	%for com_gmm
	c=1;tmp1 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
	c=2;tmp2 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
	com_gmm.dset{d,k} = log(tmp2+realmin)-log(tmp1+realmin);

	%for com
	tmp = [];
	for c = 1:size(bayesS,2),
	for gmm_c = 1:size(bayesS(c).weight,1),
		out = cmvnpdf(expe.dset{d,k}, bayesS(c).mu(:,gmm_c)', bayesS(c).sigma(:,:,gmm_c));
		if (c==1),
			tmp = [tmp -log(out+realmin)];
		else
			tmp = [tmp log(out+realmin)];
		end;
	end;end;
	llh.dset{d,k} = tmp;
end;end;

llh.weight = [bayesS(1).weight'  bayesS(2).weight' ]; % a column vector of Gaussian weights

%data = [llh.dset{1,1}; llh.dset{1,2}];
%[W, Sw, Sb, vp]= fisher_analysis(data, labels);
%plot the signficant eigen values; beyond rk is not informative
%plot(abs(vp(1:rk)));
%com.fisher_ratio = trace(inv(Sw)*Sb);

param = VR_analysis(llh.dset{1,1}, llh.dset{1,2})

[W, com.Sw] = cal_weight_fisher(1:size(param.mu_all,2), param);
W = W';

%project
%fuse the scores:
for d=1:2,
    for k=1:2,
	com.dset{d,k} = llh.dset{d,k} * W(:,1);
    end;
end;

com.weight = W(:,1);
com.bayesS = bayesS;
com_gmm.bayesS = bayesS;

%[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);
[com_gmm.epc.dev, com_gmm.epc.eva, epc_cost]  = epc(com_gmm.dset{1,1}, com_gmm.dset{1,2}, com_gmm.dset{2,1}, com_gmm.dset{2,2}, n_samples,epc_range);