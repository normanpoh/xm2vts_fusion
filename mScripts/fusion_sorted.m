function [com] = fusion_sorted(expe)

%sort the scores:
for d=1:2,
    for k=1:2,
	%fprintf(1, '%d,%d\n',d,k);
	index = find(expe.dset{d,k}(:,1) < expe.dset{d,k}(:,2));
	com.dset{d,k}(index,:) = expe.dset{d,k}(index,:);
	index = find (expe.dset{d,k}(:,1) >= expe.dset{d,k}(:,2));
	com.dset{d,k}(index,:) = expe.dset{d,k}(index,[2 1]);
	%for i=1:size(expe.dset{d,k},1),
	%	if (expe.dset{d,k}(i,1) < expe.dset{d,k}(i,2)),
	%		com.dset{d,k}(i,1:2) = [expe.dset{d,k}(i,1)  expe.dset{d,k}(i,2)];
	%	else
	%		com.dset{d,k}(i,1:2) = [expe.dset{d,k}(i,2)  expe.dset{d,k}(i,1)];
	%	end;
	%end;
    end;
end;

%fprintf(1, '\n');