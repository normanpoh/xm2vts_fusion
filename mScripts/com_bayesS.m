function nbayesS = com_bayesS(bayesS, weight)
for k=1:2,
	for c=1:size(bayesS(k).weight,1), % no. components

		nbayesS(k).weight = bayesS(k).weight;
	
		%calulate mu
		nbayesS(k).mu(1,c) = [bayesS(k).mu(:,c)]' * weight';

		%calculate sigma
		for i=1:2,for j=1:2,
		     sigma(i,j)= weight(i) * weight(j) * bayesS(k).sigma(i,j,c);
		end;end;

        	nbayesS(k).sigma(1,1,c) = sqrt(sum(sum(sigma)));
		
	end;
end;
