function [com, epc_cost] = fusion_wsum_brute_raw(expe, chosen, weight)

%default config:
n_samples = 11;
n_samples_bf = 41;
epc_range = [0.1 0.9];

if (nargin < 2),
        chosen = [1:2];
end;

%get the fixed weight
if (nargin < 3),
	[com.weight, alpha] = cal_weight_brute_raw(chosen, expe.dset{1,1}, expe.dset{1,2}, n_samples_bf);
else
	com.weight = weight;
end;

%fuse the scores:
for d=1:2,
    for k=1:2,
	com.dset{d,k} = expe.dset{d,k}(:,chosen) * com.weight';
    end;
end;

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

