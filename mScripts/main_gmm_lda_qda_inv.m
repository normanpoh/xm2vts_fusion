cd /home/learning/norman/xm2vts_fusion/mScripts

n_samples = 11;
epc_range = [0.1 0.9];

initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

load main_gmm_lda_qda_inv com

p=1;s=1;r=1;

for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      %print remarks
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);
      
      %load score files
      %for MLP output
      for d=1:2,for k=1:2,
	  nexpe.dset{d,k} = data{p}.dset{d,k}(:,b);
	  for ex=1:2,
	    if (mlp{p,s,r}(ex)),
	      fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
	      nexpe.dset{d,k}(:,ex) = tanh_inv(nexpe.dset{d,k}(:,ex));%, 1/0.8,0.4);
	    end;
	  end;
	end;
      end;
      %draw_empiric(nexpe.dset{1,1},nexpe.dset{1,2})

      nexpe.label = data{p}.label;

      rexpe = nexpe;
      
      %filter the bad ones
      d=1;
      for k=1:2,
	for ex=1:2,
	  if (mlp{p,s,r}(ex)),
	    selected = find(nexpe.dset{d,k}(:,ex)>30);
	    nexpe.dset{d,k}(selected,:)=[];
	    nexpe.label{d,k}(selected,:)=[];
	    selected = find(nexpe.dset{d,k}(:,ex)<-30);
	    nexpe.dset{d,k}(selected,:)=[];
	    nexpe.label{d,k}(selected,:)=[];
	  end;
	end;
      end;

      param = VR_analysis(nexpe.dset{1,1},nexpe.dset{1,2});

      %now replace bad values with a reasonable one
      for d=1:2,
        for k=1:2,
          for ex=1:2,
            if (mlp{p,s,r}(ex)),
              selected = find(rexpe.dset{d,k}(:,ex)>30);
              rexpe.dset{d,k}(selected,ex)= repmat(param.mu_C(ex),size(selected,1),1);
              selected = find(rexpe.dset{d,k}(:,ex)<-30);
              rexpe.dset{d,k}(selected,ex)= repmat(param.mu_I(ex),size(selected,1),1);
            end;
          end;
        end;
      end;
      %draw_empiric(rexpe.dset{1,1},rexpe.dset{1,2});
      
      %close all;
      %figure(1);
      
      %Quadratic discriminant analysis
      %[com{1}{p,s,r}, epc_cost ] = fusion_gmm(nexpe, [1 2], 1, [],1);
      %draw_empiric(nexpe.dset{1,1}, nexpe.dset{1,2});
      %fname = sprintf('../Pictures/tata_%d_%d_%02d.eps');
      %print('-depsc2', fname);
      
      %Linear discriminant analysis
      %bayesS(1).sigma = (com{1}{p,s,r}.bayesS(1).sigma + com{1}{p,s,r}.bayesS(2).sigma)/2;
      %bayesS(2).sigma = bayesS(1).sigma;
      %bayesS(1).mu = com{1}{p,s,r}.bayesS(1).mu;
      %bayesS(2).mu = com{1}{p,s,r}.bayesS(2).mu;
      %bayesS(1).apriories = com{1}{p,s,r}.bayesS(1).apriories;      
      %bayesS(2).apriories = com{1}{p,s,r}.bayesS(2).apriories;
      %bayesS(1).weight = com{1}{p,s,r}.bayesS(1).weight;
      %bayesS(2).weight = com{1}{p,s,r}.bayesS(2).weight;
      %[com{2}{p,s,r}, epc_cost ] = fusion_gmm(nexpe, [1 2], 1,bayesS,0);
   
      %com{3}{p,s,r} = fusion_wsum_brute_raw(nexpe,[1 2]);
      %[com{4}{p,s,r}, epc_cost ] = fusion_lda_qda_client(nexpe, [1 2]);

      %Quadratic discriminant analysis

      [com{5}{p,s,r}, epc_cost ] = fusion_gmm(rexpe, [1 2],1, [],0);
      %draw_empiric(rexpe.dset{2,1}, rexpe.dset{2,2});

      %Linear discriminant analysis
      bayesS(1).sigma = (com{5}{p,s,r}.bayesS(1).sigma + com{5}{p,s,r}.bayesS(2).sigma)/2;
      bayesS(2).sigma = bayesS(1).sigma;
      bayesS(1).mu = com{5}{p,s,r}.bayesS(1).mu;
      bayesS(2).mu = com{5}{p,s,r}.bayesS(2).mu;
      bayesS(1).apriories = com{5}{p,s,r}.bayesS(1).apriories;      
      bayesS(2).apriories = com{5}{p,s,r}.bayesS(2).apriories;
      bayesS(1).weight = com{5}{p,s,r}.bayesS(1).weight;
      bayesS(2).weight = com{5}{p,s,r}.bayesS(2).weight;
      [com{6}{p,s,r}, epc_cost ] = fusion_gmm(rexpe, [1 2], 1,bayesS,0);
      
      %free the memory
      for i=1:size(com,2),com{i}{p,s,r}.dset={};end;
      
    end;
  end;
end;

save main_gmm_lda_qda_inv com

