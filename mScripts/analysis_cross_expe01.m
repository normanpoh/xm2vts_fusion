%comparing results of several classifiers from different sources

%orig,svm
load main_predict.mat com
ncom{1} = com{4};
clear com;


%orig,gmm from main_norm_effect.m
load norm_expe.mat com epc_cost
ncom{2} = com{4}; %orig,gmm
ncom{6} = com{1}; %orig,mean

clear com;
%mean,F-ratio
load generic.mat com
ncom{3} = com{10};
clear com;

load main_fusion_allinfo.mat com
ncom{4} = com{9};  %Z-Norm,svm
ncom{5} = com{13}; %org-F-margin,svm

load norm_expe_addon_wsum_user
ncom{7} = com{1};

load norm_expe_addon_svm_poly.mat com
ncom{8} = com{2};
ncom{9} = com{3};

load main_fusion_bline bcom;
ncom{10} = bcom;

%overall performance
for i=1:size(ncom,2),
	[out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(ncom{i}); %overall
	[out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(ncom{i},1); %multimodal
	[out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(ncom{i},[2 3]); %intramodal
end;

signs = {'kx-',  'kd-','ko-', 'k*-', 'ks-', 'k^-', 'kp-','r<-', 'r>-','gs--'};

leg = {'orig,svm', 'orig,gmm', 'F,mean', 'Z,svm', 'orig-F-margin,svm', 'orig,mean', 'orig,user-wsum', 'zmun,svm-poly','margin,svm-poly','best'};

list = 1:size(ncom,2);
plot_all_epc(epc_cost,leg,signs, out.cfg{1}, list);
h=legend(leg{list});
set(h, 'Fontsize',12);
axis([0.1 0.9 0.8 2.6]);set(gca, 'YTick', [0.8:0.2:2.6]);grid on;set(gca, 'XTick', [0.1:0.1:0.9]);
print('-depsc2', '../Pictures/best_com.eps');

c=1;
b=[4 5];
hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});


list = [6 10];
plot_all_epc(epc_cost,leg,signs, out.cfg{2}, list);
print('-depsc2', '../Pictures/expe01_mean_multimodal.eps');
plot_all_roc(epc_cost,leg,signs, out.cfg{2}, list);
print('-depsc2', '../Pictures/expe01_mean_multimodal_roc.eps');

plot_all_epc(epc_cost,leg,signs, out.cfg{3}, list);
print('-depsc2', '../Pictures/expe01_mean_intramodal.eps');
plot_all_roc(epc_cost,leg,signs, out.cfg{3}, list);
print('-depsc2', '../Pictures/expe01_mean_intramodal_roc.eps');

c=3;
b=[10 6];
hter_significant_plot(out.cfg{c}.res{b(1)}, out.cfg{c}.res{b(2)}, out.cfg{c}.pNI,out.cfg{c}.pNC, epc_cost);
subplot(2,1,1);legend(leg{b(1)}, leg{b(2)});
print('-depsc2', '../Pictures/expe01_mean_intramodal_hter_sig.eps');

i = 6;
set(gca,'Fontsize', 20);
hold off;
plot(epc_cost(:,1), out.cfg{2}.res{i}.eva.hter_apri*100,'o-', 'MarkerSize', 14); 
hold on;
plot(epc_cost(:,1), out.cfg{3}.res{i}.eva.hter_apri*100,'x--', 'MarkerSize', 14); 
%plot_all_epc(epc_cost,leg,signs, out.cfg{2}, list);
%plot_all_epc(epc_cost,leg,signs, out.cfg{3}, list,1);
%axis([0.1 0.9 1 3.5]);
ylabel('HTER(%)');xlabel('\alpha');

print('-depsc2', '../Pictures/expe01_mean_multimodal.eps');

plot_all_epc(epc_cost,leg,signs, out.cfg{3}, list);
print('-depsc2', '../Pictures/expe01_mean_intramodal.eps');