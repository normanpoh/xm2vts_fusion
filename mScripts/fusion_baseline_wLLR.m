function [com, epc_cost] = fusion_baseline_wLLR(expe, param, reliance, ...
						toplot)
if (nargin < 4),
  toplot = 0;
end;
%default config:
n_samples = 11;
epc_range = [0.1 0.9];

%zero-mean unit variance normalisation
%model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
%model.std = std([expe.dset{1,1}; expe.dset{1,2}]);
%[expe.dset{1,1}, expe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
%[expe.dset{2,1}, expe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);

%get the model label
model_ID = unique(expe.label{1,1});

%go through each id
for d=1:2,for k=1:2,
    com.dset{d,k} = zeros(size(expe.dset{d,k},1),1);
  end;
end;

%perform client-dependent normalisation
fprintf(1, 'Processed ID:\n');
for id=1:size(model_ID,1),

  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      tmp.dset{d,k} = expe.dset{d,k}(index{d,k},:);
    end;
  end;
  
  %data = [expe.dset{1,1}; expe.dset{1,2}];
  %labels = [ zeros(size(expe.dset{1,1},1),1);ones(size(expe.dset{1,2},1),1)];
  data = [expe.dset{1,1}];
  labels = [ zeros(size(expe.dset{1,1},1),1)];

  %n_gmm = [1 3];
  %FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
  %bayesS_mix = gmmb_create(data, labels + 1, 'FJ', FJ_params{:});
  bayesS_mix(1).mu = mean(tmp.dset{1,1})';
  bayesS_mix(1).sigma = cov(tmp.dset{1,2});
  bayesS_mix(1).weight = 1;
  bayesS_mix(1).apriories = 1;
  
  bayesS_gauss(2).mu = mean(tmp.dset{1,2})';
  bayesS_gauss(2).sigma = cov(tmp.dset{1,2});
  bayesS_gauss(2).weight = 1;
  bayesS_gauss(2).apriories = 1;

  %QDA
  %impostor distribution is modeled as well as possible
  bayesS(1) = bayesS_mix(1);

  %client distribution fall back to a common cov
  bayesS(2).weight = bayesS_gauss(2).weight;
  bayesS(2).sigma = param.cov_C;
  %bayesS(2).sigma = bayesS_gauss(2).sigma;
  %bayesS(2).sigma = reliance * bayesS_gauss(2).sigma +(1-reliance) * param.cov_C;
  %whose mu is adapted
  bayesS(2).mu = reliance * bayesS_gauss(2).mu + (1-reliance) * param.mu_C;
  %bayesS(2).mu =  bayesS_gauss(2).mu;
  
  %bayesS(1)
  %bayesS(2)
  %compute the fused scores
  for d=1:2,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      c=1;tmp1 = gmmb_pdf(tmp.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
      c=2;tmp2 = gmmb_pdf(tmp.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
      com.dset{d,k}(index{d,k},1) = log(tmp2+realmin)-log(tmp1+realmin);
    end;
  end;

  com.client{id}.bayesS = bayesS;
  if (toplot), 
    figure(1);  wer(tmp.dset{1,1}, tmp.dset{1,2}, [], 4);
    adat = [tmp.dset{1,1};tmp.dset{1,2}];
    x=linspace(min(adat),max(adat),100)';
    c=1; f = gmmb_pdf(x, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
    hold on;
    plot(x,f,'r--');
    c=2; f = gmmb_pdf(x, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
    plot(x,f,'b--'); legend('impostor-emp','client-emp','impostor-approx','client-approx');

    figure(2);
    x=linspace(min(adat),max(adat),100)';
    hold off;
    plot(x, normpdf(x, bayesS(1).mu, sqrt(bayesS(1).sigma)), 'r');
    hold on;
    plot(x, normpdf(x, bayesS(2).mu, sqrt(bayesS(2).sigma)), 'b');
    axis_tmp = axis;
    axis_tmp(3:4) = [0 4];
    axis(axis_tmp);
    pause;
  end;
  fprintf(1, '.%d', id);  
end;

com.reliance = reliance;
%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);
