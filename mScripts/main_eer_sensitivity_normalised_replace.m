%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%super draw can go up to 1000
n_super_draws = 5;
n_draws = 500;
list{1} = 5:5:45;
list{2} = 5:5:145;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%reuse expe_id created in main_scalable_EER.m
load main_scalable_EER

for dd=1:n_super_draws,
  for p=1:2,
    for d=1:n_draws,
      %small set
      model_ID = expe_id{p}.selected(dd,1:50);
      new_expe_id{1}{dd}{p}.selected(d,:) = model_ID;
      for i=1:length(list{1}),
        selected_id = floor(rand(1,length(model_ID))*length(model_ID))+1; %randomly choose the user size but with replacement
        rnd_list{1}{d}{i} = selected_id(1:list{1}(i));
      end;
      %big set
      model_ID = expe_id{p}.selected(dd,51:200);
      new_expe_id{2}{dd}{p}.selected(d,:) = model_ID;
      for i=1:length(list{2}),
        selected_id = floor(rand(1,length(model_ID))*length(model_ID))+1; %randomly choose the user size but with replacement
        rnd_list{2}{d}{i} = selected_id(1:list{2}(i));
      end;
    end;
  end;
end;
save main_eer_sensitivity_draws_replace new_expe_id rnd_list

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

initialise;
%load main_norm_effect_addon_wsum_user.mat com epc_cost

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%load normalised scores by Z-Norm and F-Norm
load main_baseline_addon_FNorm_rerun baseline
for p=1:2,
  bline{p}.label = data{p}.label;
  for b=1:size(dat.P{p}.labels,2)
    bdat{1}{p,b}.dset = baseline{3}{p,b}.dset; %Z-Norm
    %bdat{2}{p,b}.dset = baseline{6}{p,b}.dset;  %F-Norm
  end;
end;
clear data baseline;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
list{1} = 5:5:45;
list{2} = 5:5:145;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%reuse expe_id created in main_eer_sensitivity.m
load main_eer_sensitivity_draws_noreplace new_expe_id rnd_list

%load main_eer_sensitivity out

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN expe here
%ds=1; %50 client config

n_super_draws = 1;
n_draws = 100;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;
for ds=1:2, %150 client config
  fprintf(1,'For dset %d\n', ds);
  for p=1:2,
    fprintf(1,'Protocol %d\n', p);
    for dd=1:n_super_draws,
      fprintf(1,'�');

      for d=1:n_draws,
        fprintf(1,'.%d',d);

        for b=1:size(dat.P{p}.labels,2)
          expe.dset = bdat{1}{p,b}.dset; %use Z-Norm
          expe.label = bline{p}.label;
          for i=1:length(list{1}), %force to be the list ds1
            %for i=1:length(list{ds}), %sample the no.-of-user interval
            rexpe = rndchoose_client_list(expe, [], rnd_list{ds}{d}{i}, new_expe_id{ds}{dd}{p}.selected(d,:));

            [tmp_,thrd] = wer(rexpe.dset{1,1}(:,1), rexpe.dset{1,2}(:,1));
            [com.hter_apri, com.far_apri, com.frr_apri] = hter_apriori(rexpe.dset{2,1}(:,1), rexpe.dset{2,2}(:,1),thrd);
            out{ds}{dd}{p}{i}(d,b) = com.hter_apri;
          end;
        end;

      end;
    end;
  end;
end;
time_taken = toc;
save main_eer_sensitivity_normalised_replace out time_taken

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_super_draws = 1;
n_draws = 100;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;
for ds=1:2, %150 client config
  fprintf(1,'For dset %d\n', ds);
  for p=1:2,
    fprintf(1,'Protocol %d\n', p);

    for b=1:size(dat.P{p}.labels,2)
      expe.dset = bdat{1}{p,b}.dset; %use Z-Norm
      expe.label = bline{p}.label;

      [nbayesS{p,b}, paramj, param] = cal_clientd_stat(expe);
      
      for dd=1:n_super_draws,
        fprintf(1,'�');

        for d=1:n_draws,
          fprintf(1,'.%d',d);

          for i=1:length(list{1}), %force to be the list ds1
            %for i=1:length(list{ds}), %sample the no.-of-user interval

            selected_id = rnd_list{ds}{d}{i};
            for d=1:2,for k=1:2,
                for j=1:length(selected_id),
                  tbayesS{2}{d}(k).sigma(1,1,j) = nbayesS{p,b}{1}(k).sigma(1,1,selected_id(j));
                end;
                tbayesS{2}{d}(k).mu = nbayesS{p,b}{1}(k).mu(selected_id);
                tbayesS{2}{d}(k).weight = ones(1,n_user_list(n))/n_user_list(n);
                tbayesS{2}{d}(k).apriories = 1;
              end;
            end;

            rexpe = rndchoose_client_list(expe, [], , new_expe_id{ds}{dd}{p}.selected(d,:));

            [tmp_,thrd] = wer(rexpe.dset{1,1}(:,1), rexpe.dset{1,2}(:,1));
            [com.hter_apri, com.far_apri, com.frr_apri] = hter_apriori(rexpe.dset{2,1}(:,1), rexpe.dset{2,2}(:,1),thrd);
            out{ds}{dd}{p}{i}(d,b) = com.hter_apri;
          end;
        end;

      end;
    end;
  end;
end;
time_taken = toc;
save main_eer_sensitivity_normalised_replace out time_taken

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%STOP HERE











