function nexpe = draw_samples(expe, chosen)
if nargin<2|length(chosen)==0,
  chosen = 1;
end;

for d=1:2,for k=1:2,
    nexpe.dset{d,k} = [];
    nexpe.label{d,k} = [];
  end;
end;

%for id=1:size(model_ID,1),
for d=1:2,for k=1:2,
    nrows = size(expe.dset{d,k},1);
    selected = randi(nrows, 1, nrows);
    nexpe.dset{d,k} = expe.dset{d,k}(selected,chosen);
    nexpe.label{d,k} = expe.label{d,k}(selected,chosen);
  end;
end;
