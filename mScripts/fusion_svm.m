function [com, epc_cost] = fusion_svm(expe, chosen, net, mode, c_value, kernel, kparam)

%default config: 
n_samples = 11; epc_range = [0.1 0.9];

if (nargin < 2 | length(chosen)==0)
        chosen = [1:2];
end;

if (nargin < 3 | length(net)==0),
    net = [];
end;

if (nargin < 4 | length(mode)==0),
        mode = 0;
end;

if (nargin < 5 | length(c_value)==0),
        c_value = 10;
end;

if (nargin < 6 | length(kernel)==0),
        kernel = 'linear';
        kparam = [];
end;

if (length(net) == 0),
    
    X=[expe.dset{1,1}(:,chosen);expe.dset{1,2}(:,chosen)];
    Y=[-1*ones(size(expe.dset{1,1},1),1);ones(size(expe.dset{1,2},1),1)];

    net = svm(size(X, 2), kernel, kparam, c_value);
    net = svmtrain(net, X, Y);

    com.net = net;
end;

if (mode == 0),
    %fuse the scores:
    for d=1:2,
        for k=1:2,
    	[tmp, com.dset{d,k}] = svmfwd(net, expe.dset{d,k}(:,chosen));
    end;
    end;

    %epc curve
    fprintf(1,'\nCalculating EPC');
    [com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);
else
    [tmp,com] = svmfwd(net, expe(:,chosen));
    epc_cost = 0;
end;
