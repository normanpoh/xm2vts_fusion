initialise;
load main_eer_sensitivity_gmm nbayesS hbayesS sbayesS transferf

dat = dlmread('../Data/multi_modal/PI/dev.label', ' ');
model_ID = unique(dat(:,2));
clear dat;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% experimental param
n_user_list = [25 50 100 200 300 600 1000];

n_user_list = 5:5:45;
n_samples = 500;

p=1;b=2;
for p=1:2,
  for b=2:size(dat.P{p}.labels,2)
    
    txt = sprintf('\nExperiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);

    for n = 1:length(n_user_list),
      fprintf(1,'*');
      for i=1:n_samples, %each bootstrap
        %if (i/10) == round(i/10),        fprintf(1,'.'); end;

        clear tbayesS;
        %METHOD 1:
        %sample from the second level statistics to get back the first
        %level of statistics
        d=1;
        tot = round(hbayesS{d}{p,b}.weight * n_user_list(n));
        if sum(tot)>n_user_list(n),
          chosen=floor(rand * 3)+1;
          tot(chosen)=tot(chosen)-1; %randomly break tie
        end;
        if sum(tot)<n_user_list(n),
          chosen=floor(rand * 3)+1;
          tot(chosen)=tot(chosen)+1; %randomly break tie
        end;
        tmp = [];
        for c=1:length(hbayesS{d}{p,b}.weight),
          tmp = [tmp; mvnrnd(hbayesS{d}{p,b}.mu(:,c), hbayesS{d}{p,b}.sigma(:,:,c),tot(c))];
        end;
        for k=1:2,
          d=1;%sample the training set ONLY
          tbayesS{1}{d}(k).sigma(1,1,:) = abs(tmp(:,2+k));
          tbayesS{1}{d}(k).mu = tmp(:,k)';
          tbayesS{1}{d}(k).weight = ones(1,n_user_list(n))/n_user_list(n);
          tbayesS{1}{d}(k).apriories = 1;

          d=2;%trasnform to the test set
          for j=1:n_user_list(n),
            tbayesS{1}{d}(k).sigma(1,1,j) = transferf{p,b}(1) * tbayesS{1}{1}(k).sigma(1,1,j);
          end;
          mytmp = [tbayesS{1}{1}(k).mu; ones(1,n_user_list(n))];
          tbayesS{1}{d}(k).mu = [mytmp' * transferf{p,b}']';
          tbayesS{1}{d}(k).weight = ones(1,n_user_list(n))/n_user_list(n);
          tbayesS{1}{d}(k).apriories = 1;
        end;
        %check


        %map from train to test

        %METHOD 2:
        %calculate from the big data set with 150 users, bootstrap w
        %replacement
        selected_id = floor(rand(1,n_user_list(n))*150)+1 + 50; %location 51 to 150
        for d=1:2,for k=1:2,
            for j=1:length(selected_id),
              tbayesS{2}{d}(k).sigma(1,1,j) = nbayesS{p,b}{1}(k).sigma(1,1,selected_id(j));
            end;
            tbayesS{2}{d}(k).mu = nbayesS{p,b}{1}(k).mu(selected_id);
            tbayesS{2}{d}(k).weight = ones(1,n_user_list(n))/n_user_list(n);
            tbayesS{2}{d}(k).apriories = 1;
          end;
        end;

        %METHOD 3:
        %calculate from the small data set with 50 users
        selected_id = floor(rand(1,n_user_list(n))*50)+1; %location 1 to 50
        for d=1:2,for k=1:2,
            for j=1:length(selected_id),
              tbayesS{3}{d}(k).sigma(1,1,j) = nbayesS{p,b}{1}(k).sigma(1,1,selected_id(j));
            end;
            tbayesS{3}{d}(k).mu = nbayesS{p,b}{1}(k).mu(selected_id);
            tbayesS{3}{d}(k).weight = ones(1,n_user_list(n))/n_user_list(n);
            tbayesS{3}{d}(k).apriories = 1;
          end;
        end;
        
        %evaluate all three of them
        for t=1:3,
          [out{p,b}{n}.eer_apri(i,t), thrd] = estimate_EER_gmm([],[], [], 0, tbayesS{t}{1});
          [out{p,b}{n}.HTER(i,t), out{p,b}{n}.FAR(i,t), out{p,b}{n}.FRR(i,t)] = estimate_HTER_gmm_apriori(tbayesS{t}{2}, thrd);
        end;

      end;%bootstrap sample
      %tmp = quantil(out{p,b}{n}.HTER, [2.5 50 97.5])';
      
      %out.{p,b}{n}.HTER_quantil = 

    end; %user size
    
    
  end;
end;

save main_eer_sensitivity_gmm_bstrp out;