function [com, epc_cost] = fusion_RDA(filtered_expe, expe, chosen, n_lambda, lambda,toplot,draw)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 3),
  chosen = [1:2];
end;

if (nargin < 4 | length(n_lambda) ==0),
  n_lambda = 8;
end;
lambda_list = 10 .^ [0:n_lambda];

if (nargin < 5|length(lambda)==0),
  lambda = [];
end;

if (nargin < 6),
  toplot = 0;
end;

if (nargin < 7),
  draw = 0;
end;

%make chosen
for d=1:2,for k=1:2,
    expe.dset{d,k} = expe.dset{d,k}(:,chosen);
end;end;

%training on filtered dset=1
data = [filtered_expe.dset{1,1}; filtered_expe.dset{1,2}];
labels = [ zeros(size(filtered_expe.dset{1,1},1),1);ones(size(filtered_expe.dset{1,2},1),1)];

FJ_params = { 'Cmax', 1, 'Cmin', 1, 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
bayesS = gmmb_create(data, labels+1, 'FJ', FJ_params{:});

%draw_empiric(tdata.dset{1,1},tdata.dset{1,2});
sigma_all = (bayesS(1).sigma + bayesS(2).sigma) / 2;

%tuning by cross-validation
if (length(lambda)==0),

  for i=1:length(lambda_list),
    %relevance factor
    NI = length(filtered_expe.dset{1,1});
    NC = length(filtered_expe.dset{1,2});
    rf(1) = NI/(NI+lambda_list(i));
    rf(2) = NC/(NC+lambda_list(i));    
    
    %fprintf(1, 'Relevance factor of Impostors and Clients: (%1.2f %1.2f)\t', rfI,rfC);
    d=1;
    for k=1:2,
      %impostor model
      c=1;
      mysigma = rf(c) * bayesS(c).sigma + (1-rf(c)) * sigma_all;
      tmp1 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, mysigma, bayesS(c).weight );
      %client model
      c=2;
      mysigma = rf(c) * bayesS(c).sigma + (1-rf(c)) * sigma_all;
      tmp2 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, mysigma, bayesS(c).weight );
      out.dset{d,k} = log(tmp2+realmin)-log(tmp1+realmin);
    end;
    wer_(i) = wer(out.dset{d,1},out.dset{d,2});
  
    fprintf(1, '%08d\t (%1.3f,%1.3f) %2.3f\n', lambda_list(i), rf(1), rf(2),wer_(i)*100);
  end;

  if (toplot),
    figure(1);
    semilogx(lambda_list, log(wer_),'bo-');
    xlabel('\lambda');
    ylabel('log(WER)');
    grid on;
    %set(gca,'xtick', 1:n_lambda);
    %set(gca,'ytick', 1:n_lambda);
    %set(gca,'xticklabel', lambda_list);
    %set(gca,'yticklabel', lambda_list);
    %hold off;
    %plot(lambda_list,wer_ * 100,'bo-');
    %ylabel('EER (%)');
    %xlabel('\lambda');
  end;
  
  %smallest=min(min(wer_));
  %[r,c] = find(wer_ == smallest);
  %lambda(1) = lambda_list(r);
  %lambda(2) = lambda_list(c);

  [tmp, index]=min(wer_);
  rf(1) = NI/(NI+lambda_list(index));
  rf(2) = NC/(NC+lambda_list(index));    
    
  com.relevance_factor = lambda_list(index);
  com.lambda = rf;
  com.wer_validated = wer_;
end;

%standard procedure for training
%data = [expe.dset{1,1}; expe.dset{1,2}];
%labels = [ zeros(size(expe.dset{1,1},1),1);ones(size(expe.dset{1,2},1),1)];
%n_gmm = 1;
%FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
%bayesS = gmmb_create(data, labels + 1, 'FJ', FJ_params{:});
%this gmmbayes accept class labels 1 2 ...

%testing
for d=1:2,for k=1:2,
    %impostor model
    c=1;
    mysigma = rf(c) * bayesS(c).sigma + (1-rf(c)) * sigma_all;
    tmp1 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, mysigma, bayesS(c).weight );
    %client model
    c=2;
    mysigma = rf(c) * bayesS(c).sigma + (1-rf(c)) * sigma_all;
    tmp2 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, mysigma, bayesS(c).weight );
    
    %c=1;tmp1 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
    %mysigma = lambda * bayesS(2).sigma + (1-lambda) * bayesS(1).sigma;
    %c=2;tmp2 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, mysigma, bayesS(c).weight );
    com.dset{d,k} = log(tmp2+realmin)-log(tmp1+realmin);
  end;
end;
  
com.bayesS = bayesS;

fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, ...
					    com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);

%surface fitting
if (draw),
  figure(2);
  [xtesta1,xtesta2]=meshgrid( ...
      linspace(-40,40, 100), ...
      linspace(min(data(:,2)), max(data(:,2)), 100) );
      %linspace(min(data(:,1)), max(data(:,1)), 100), ...
      %linspace(min(data(:,2)), max(data(:,2)), 100) );

  [na,nb]=size(xtesta1);
  xtest1=reshape(xtesta1,1,na*nb);
  xtest2=reshape(xtesta2,1,na*nb);
  xtest=[xtest1;xtest2]';
  
  c=1;
  mysigma = rf(c) * bayesS(c).sigma + (1-rf(c)) * sigma_all;
  tmp1 = gmmb_pdf(xtest, bayesS(c).mu, mysigma, bayesS(c).weight );
  %client model
  c=2;
  mysigma = rf(c) * bayesS(c).sigma + (1-rf(c)) * sigma_all;
  tmp2 = gmmb_pdf(xtest, bayesS(c).mu, mysigma, bayesS(c).weight );
  ypred = log(tmp2+realmin)-log(tmp1+realmin);
  
  ypredmat=reshape(ypred,na,nb);
  hold off;
  contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
  hold on;
  
  thrd = wer(com.dset{1,1}, com.dset{1,2});
  [cs,h]=contour(xtesta1,xtesta2,ypredmat,[thrd],'k');
  clabel(cs,h);colorbar;
  %plot(raw{1}(:,1), raw{1}(:,2), 'ro');
  %plot(raw{2}(:,1), raw{2}(:,2), 'b+');
  %colorbar;

  draw_theory_bayesS(bayesS);
end;

