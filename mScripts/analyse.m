%baseline results
cd /home/learning/norman/VR/Scripts
leg1= {'(FH,MLP)','(DCTs,GMM)','(DCTb,GMM)','(DCTs,MLP)','(DCTb,MLP)','(LFCC,GMM)','(PAC,GMM)','(SSC,GMM)'};
b{1}=[1.875; 
      4.227;    
      1.670;     
      3.359;        
      6.221;    
      1.139;
      6.304;
      2.437];

leg2 = {'(FH,MLP)','(DCTb,GMM)','(LFCC,GMM)','(PAC,GMM)','(SSC,GMM)'};
b{2}=[1.860; 
      0.644;    
      1.300;
      6.642;
      2.013];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% modalities
%fprintf(1, 'Modalities\n'); 
compare{1}=[ 1, 6 %lfcc
	     2, 6
	     3, 6
	     4, 6
	     5, 6
	     1, 7 %pacMFCC
	     2, 7
	     3, 7
	     4, 7
	     5, 7
	     1, 8 %ssc
	     2, 8
	     3, 8
	     4, 8
	     5, 8   
	     ];
compare{2}=[ 1, 3 %lfcc
	     2, 3
	     1, 4 %pacMFCC
	     2, 4
	     1, 5 %ssc
	     2, 5
	     ];
for i=1:5*3,
  mean_modal_b{1}(i) = mean(  [b{1}(compare{1}(i,1)); b{1}(compare{1}(i,2)) ]);
  min_modal_b{1}(i) = min(  [b{1}(compare{1}(i,1)); b{1}(compare{1}(i,2)) ]);
  table{1,i} = sprintf('%s %s', leg1{compare{1}(i,1)}, leg1{compare{1}(i,2)});
end;
for i=1:2*3,
  mean_modal_b{2}(i) = mean(  [b{2}(compare{2}(i,1)); b{2}(compare{2}(i,2)) ]);
  min_modal_b{2}(i) = min(  [b{2}(compare{2}(i,1)); b{2}(compare{2}(i,2)) ]);
  table{2,i} = sprintf('%s %s', leg2{compare{2}(i,1)}, leg2{compare{2}(i,2)});
end;
mlp{1} = [ 0.366 %lfcc
	   0.576
	   0.483
	   0.611
	   0.489
	   0.856 %pacMFCC
	   1.425
	   0.900
	   1.056
	   2.455
	   0.786 %ssc
	   1.175
	   0.704
	   0.829
	   1.176
	   ];
mlp{2} = [ 0.150 %lfcc
	   0.130
	   0.765 %pacMFCC
	   0.222 
	   0.302 %ssc
	   0.162];

svm{1} = [ 0.381 %lfcc
	   0.613
	   0.475
	   0.587
	   0.485
	   0.970 %pacMFCC
	   1.402
	   0.923
	   1.009
	   2.664
	   0.742 %ssc
	   1.213
	   0.742
	   0.850
	   1.121];
svm{2} = [ 0.389 %lfcc
	   0.252
	   0.855 %pacMFCC
	   0.431
	   0.404 %ssc
	   0.383];

ave{1} = [ 0.399 %lfcc
	   0.537
	   0.520
	   0.591
	   0.497
	   1.114 %pacMFCC
	   1.407
	   0.899
	   1.248
	   3.978
	   0.972 %ssc
	   1.028
	   0.756
	   1.167
	   2.986];
ave{2} = [ 0.151 %lfcc
	   0.147
	   1.282 %pacMFCC
	   0.243
	   0.901 %ssc
	   0.049
	   ];
fprintf(1, 'Modalities\n');
for i=1:5*3,
fprintf(1,'LP1 Test & %s & %1.3f & %1.3f & %1.3f & %1.3f & %1.3f\\\\ \\hline\n', table{1,i}, ave{1}(i), mlp{1}(i), svm{1}(i), mean_modal_b{1}(i), min_modal_b{1}(i));
end;
for i=1:2*3,
fprintf(1,'LP2 Test & %s & %1.3f & %1.3f & %1.3f & %1.3f & %1.3f\\\\ \\hline\n', table{2,i}, ave{2}(i), mlp{2}(i), svm{2}(i), mean_modal_b{2}(i), min_modal_b{2}(i));
end;
%for i=1:2,
%fprintf(1,'LP2 Test & %s & %1.3f & %1.3f & %1.3f %1.3f %1.3f\\\\ \\hline\n',table{2,i}, ave{2}(i), mlp{2}(i), svm{2}(i),mean_modal_b{2}(i), min_modal_b{1}(i));
%end;
%fprintf(1, 'mean criterion\n');
%for i=1:5,
%fprintf(1,'LP1 Test & %s & %1.3f & %1.3f & %1.3f\\\\ \\hline\n', table{1,i}, mean_modal_b{1}(i)/ave{1}(i), mean_modal_b{1}(i)/mlp{1}(i), mean_modal_b{1}(i)/svm{1}(i));
%end;
%for i=1:2,
%fprintf(1,'LP2 Test & %s & %1.3f & %1.3f & %1.3f\\\\ \\hline\n', table{2,i}, mean_modal_b{2}(i)/ave{2}(i), mean_modal_b{2}(i)/mlp{2}(i), mean_modal_b{2}(i)/svm{2}(i));
%end;
%fprintf(1, 'min criterion\n');
%for i=1:5,
%fprintf(1,'LP1 Test & %s & %1.3f & %1.3f & %1.3f\\\\ \\hline\n', table{1,i}, min_modal_b{1}(i)/ave{1}(i), min_modal_b{1}(i)/mlp{1}(i), min_modal_b{1}(i)/svm{1}(i));
%end;
%for i=1:2,
%fprintf(1,'LP2 Test & %s & %1.3f & %1.3f & %1.3f\\\\ \\hline\n', table{2,i}, min_modal_b{2}(i)/ave{2}(i), min_modal_b{2}(i)/mlp{2}(i), min_modal_b{2}(i)/svm{2}(i));
%end;

%fprintf(1, 'MLP\n');
clear ratio;
ratio = mean_modal_b{1} ./ mlp{1}';
ratio = [ratio (mean_modal_b{2} ./ mlp{2}') ];
res{2,1} = [mean(ratio); std(ratio)];

%fprintf(1, 'min\n');
clear ratio;
ratio = min_modal_b{1} ./ mlp{1}';
ratio = [ratio (min_modal_b{2} ./ mlp{2}') ];
res{2,2} = [mean(ratio); std(ratio)];

%for i=1:5,
%fprintf(1,'%s & %1.3f & %1.3f & %1.3f &%1.3f & %1.3f\n', table{1,i}, ave{1}(i), mlp{1}(i), svm{1}(i),mean_modal_b{1}(i),min_modal_b{1}(i));
%end;

%fprintf(1, 'SVM\n');
%fprintf(1, 'mean\n');
clear ratio;
ratio = mean_modal_b{1} ./ svm{1}';
ratio = [ratio (mean_modal_b{2} ./ svm{2}') ];
res{3,1} = [mean(ratio); std(ratio)];

%fprintf(1, 'min\n');
clear ratio;
ratio = min_modal_b{1} ./ svm{1}';
ratio = [ratio (min_modal_b{2} ./ svm{2}') ];
res{3,2} = [mean(ratio); std(ratio)];

%fprintf(1, 'Average\n');
%fprintf(1, 'mean\n');
clear ratio;
ratio = mean_modal_b{1} ./ ave{1}';
ratio = [ratio (mean_modal_b{2} ./ ave{2}') ];
res{1,1} = [mean(ratio); std(ratio)];

%fprintf(1, 'min\n');
clear ratio;
ratio = min_modal_b{1} ./ ave{1}';
ratio = [ratio (min_modal_b{2} ./ ave{2}') ];
res{1,2} = [mean(ratio); std(ratio)];

fprintf(1, 'mean\n');
fprintf(1, 'mean: %1.3f & %1.3f & %1.3f \n', res{1,1}(1),res{2,1}(1),res{3,1}(1));
fprintf(1, 'std: %1.3f & %1.3f & %1.3f \n', res{1,1}(2),res{2,1}(2),res{3,1}(2));
fprintf(1, 'min\n');
fprintf(1, 'mean: %1.3f & %1.3f & %1.3f \n', res{1,2}(1),res{2,2}(1),res{3,2}(1));
fprintf(1, 'std: %1.3f & %1.3f & %1.3f \n', res{1,2}(2),res{2,2}(2),res{3,2}(2));

list{1,1} = 1:5;
list{1,2} = 1:2;
list{2,1} = 6:10;
list{2,2} = 3:4;
list{3,1} = 11:15;
list{3,2} = 5:6;
leg = {'LFCC', 'PAC', 'SSC'};
classifier = {'MLP', 'SVM', 'mean'};

for i=1:3,
  clear ratio;
  ratio = mean_modal_b{1}(list{i,1}) ./ mlp{1}(list{i,1})';
  ratio = [ratio (mean_modal_b{2}(list{i,2}) ./ mlp{2}(list{i,2})') ];
  newres{1,1} = [mean(ratio); std(ratio)];
  
  clear ratio;
  ratio = min_modal_b{1}(list{i,1}) ./ mlp{1}(list{i,1})';
  ratio = [ratio (min_modal_b{2}(list{i,2}) ./ mlp{2}(list{i,2})') ];
  newres{1,2} = [mean(ratio); std(ratio)];
  
  clear ratio;
  ratio = mean_modal_b{1}(list{i,1}) ./ svm{1}(list{i,1})';
  ratio = [ratio (mean_modal_b{2}(list{i,2}) ./ svm{2}(list{i,2})') ];
  newres{2,1} = [mean(ratio); std(ratio)];
  
  clear ratio;
  ratio = min_modal_b{1}(list{i,1}) ./ svm{1}(list{i,1})';
  ratio = [ratio (min_modal_b{2}(list{i,2}) ./ svm{2}(list{i,2})') ];
  newres{2,2} = [mean(ratio); std(ratio)];
  
  clear ratio;
  ratio = mean_modal_b{1}(list{i,1}) ./ ave{1}(list{i,1})';
  ratio = [ratio (mean_modal_b{2}(list{i,2}) ./ ave{2}(list{i,2})') ];
  newres{3,1} = [mean(ratio); std(ratio)];
  
  clear ratio;
  ratio = min_modal_b{1}(list{i,1}) ./ ave{1}(list{i,1})';
  ratio = [ratio (min_modal_b{2}(list{i,2}) ./ ave{2}(list{i,2})') ];
  newres{3,2} = [mean(ratio); std(ratio)];

  fprintf(1, '%s: (Gain ratio with mean criterion and std. dev.)\n',leg{i});
  %for j=1:3,
  %  fprintf(1, '%s \t %1.3f %1.3f\n', classifier{j}, newres{j,1});
  %end;
  fprintf(1, 'mean & %1.3f &%1.3f & %1.3f \n', newres{3,1}(1), newres{1,1}(1),newres{2,1}(1));
  fprintf(1, 'std & %1.3f &%1.3f &%1.3f \n', newres{3,1}(2), newres{1,1}(2),newres{2,1}(2));
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% features
fprintf(1, 'Features\n');
clear compare mlp svm mean_modal_b min_modal_b table;

compare{1}=[ 1, 2 %face
	     1, 3
	     1, 4
	     1, 5 
	     6, 8 %speech
	     7, 8
	     ];
compare{2}=[ 1, 2 %face
	     3, 5 %speech
	     4, 5];
for i=1:6,
  mean_modal_b{1}(i) = mean(  [b{1}(compare{1}(i,1)); b{1}(compare{1}(i,2)) ]);
  min_modal_b{1}(i) = min(  [b{1}(compare{1}(i,1)); b{1}(compare{1}(i,2)) ]);
  table{1,i} = sprintf('%s %s', leg1{compare{1}(i,1)}, leg1{compare{1}(i,2)});
end;
for i=1:3,
  mean_modal_b{2}(i) = mean(  [b{2}(compare{2}(i,1)); b{2}(compare{2}(i,2)) ]);
  min_modal_b{2}(i) = min(  [b{2}(compare{2}(i,1)); b{2}(compare{2}(i,2)) ]);
  table{2,i} = sprintf('%s %s', leg2{compare{2}(i,1)}, leg2{compare{2}(i,2)});
end;
mlp{1} = [1.379 %face
	  1.151
	  1.667
	  1.933
	  1.444 %speech
	  2.954];

mlp{2} = [0.670 %face
	  1.034 %speech
	  2.316]; 
	  
svm{1} = [1.393 %face
	  1.528
	  1.476
	  1.938
	  1.142 %speech
	  2.663];

svm{2} = [ 0.488 %face
	   1.063 %speech
	   2.125];

ave{1} = [1.641 %face
	  1.123
	  1.475
	  1.948
	  1.296 %speech
	  3.594];
ave{2} = [0.896 %face
	  1.107 %speech
	  2.614];

for i=1:6,
fprintf(1,'LP1 Test & %s & %1.3f & %1.3f & %1.3f & %1.3f & %1.3f\\\\ \\hline\n', table{1,i}, ave{1}(i), mlp{1}(i), svm{1}(i), mean_modal_b{1}(i), min_modal_b{1}(i));
end;
for i=1:3,
fprintf(1,'LP2 Test & %s & %1.3f & %1.3f & %1.3f & %1.3f & %1.3f\\\\ \\hline\n', table{2,i}, ave{2}(i), mlp{2}(i), svm{2}(i), mean_modal_b{2}(i), min_modal_b{2}(i));
end;

clear res;

clear ratio;
ratio = mean_modal_b{1} ./ mlp{1}';
ratio = [ratio (mean_modal_b{2} ./ mlp{2}') ];
res{2,1} = [mean(ratio); std(ratio)];

clear ratio;
ratio = min_modal_b{1} ./ mlp{1}';
ratio = [ratio (min_modal_b{2} ./ mlp{2}') ];
res{2,2} = [mean(ratio); std(ratio)];

clear ratio;
ratio = mean_modal_b{1} ./ svm{1}';
ratio = [ratio (mean_modal_b{2} ./ svm{2}') ];
res{3,1} = [mean(ratio); std(ratio)];

clear ratio;
ratio = min_modal_b{1} ./ svm{1}';
ratio = [ratio (min_modal_b{2} ./ svm{2}') ];
res{3,2} = [mean(ratio); std(ratio)];

clear ratio;
ratio = mean_modal_b{1} ./ ave{1}';
ratio = [ratio (mean_modal_b{2} ./ ave{2}') ];
res{1,1} = [mean(ratio); std(ratio)];

clear ratio;
ratio = min_modal_b{1} ./ ave{1}';
ratio = [ratio (min_modal_b{2} ./ ave{2}') ];
res{1,2} = [mean(ratio); std(ratio)];

fprintf(1, 'mean\n');
fprintf(1, 'mean: %1.3f & %1.3f & %1.3f \n', res{1,1}(1),res{2,1}(1),res{3,1}(1));
fprintf(1, 'std: %1.3f & %1.3f & %1.3f \n', res{1,1}(2),res{2,1}(2),res{3,1}(2));
fprintf(1, 'min\n');
fprintf(1, 'mean: %1.3f & %1.3f & %1.3f \n', res{1,2}(1),res{2,2}(1),res{3,2}(1));
fprintf(1, 'std: %1.3f & %1.3f & %1.3f \n', res{1,2}(2),res{2,2}(2),res{3,2}(2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% classifiers
fprintf(1, 'Classifiers\n');
clear compare mlp svm mean_modal_b min_modal_b;
compare{1}=[ 2, 4
	     3, 5];
for i=1:2,
  mean_modal_b{1}(i) = mean(  [b{1}(compare{1}(i,1)); b{1}(compare{1}(i,2)) ]);
  min_modal_b{1}(i) = min(  [b{1}(compare{1}(i,1)); b{1}(compare{1}(i,2)) ]);
  table{1,i} = sprintf('%s %s', leg1{compare{1}(i,1)}, leg1{compare{1}(i,2)});
end;
mlp{1} = [2.486
	  1.532];
svm{1} = [2.697
	  1.471];
ave{1} = [2.873
	  2.898];

for i=1:2,
fprintf(1,'LP1 Test & %s & %1.3f & %1.3f & %1.3f & %1.3f & %1.3f\\\\ \\hline\n', table{1,i}, ave{1}(i), mlp{1}(i), svm{1}(i), mean_modal_b{1}(i), min_modal_b{1}(i));
end;
clear ratio;
ratio = mean_modal_b{1} ./ mlp{1}';
res{2,1} = [mean(ratio); std(ratio)];

clear ratio;
ratio = min_modal_b{1} ./ mlp{1}';
res{2,2} = [mean(ratio); std(ratio)];

clear ratio;
ratio = mean_modal_b{1} ./ svm{1}';
res{3,1} = [mean(ratio); std(ratio)];

clear ratio;
ratio = min_modal_b{1} ./ svm{1}';
res{3,2} = [mean(ratio); std(ratio)];

clear ratio;
ratio = mean_modal_b{1} ./ ave{1}';
res{1,1} = [mean(ratio); std(ratio)];

clear ratio;
ratio = min_modal_b{1} ./ ave{1}';
res{1,2} = [mean(ratio); std(ratio)];

fprintf(1, 'mean\n');
fprintf(1, 'mean: %1.3f & %1.3f & %1.3f \n', res{1,1}(1),res{2,1}(1),res{3,1}(1));
fprintf(1, 'std: %1.3f & %1.3f & %1.3f \n', res{1,1}(2),res{2,1}(2),res{3,1}(2));
fprintf(1, 'min\n');
fprintf(1, 'mean: %1.3f & %1.3f & %1.3f \n', res{1,2}(1),res{2,2}(1),res{3,2}(1));
fprintf(1, 'std: %1.3f & %1.3f & %1.3f \n', res{1,2}(2),res{2,2}(2),res{3,2}(2));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%virtual samples
orig = [1.875  1.737];
scr = [1.612  1.518;
       1.667  1.547;
       1.709  1.493;
       1.606  1.559;
       2.186  2.043];
leg ={'Mean', 'Median', 'GMM', 'Global Entropy', 'Local Entropy'};
res = repmat(orig,5,1) ./ scr ;
tmp =mean(res')';
vtmp =std(res')';
for i=1:5,
  fprintf(1, '%s &%1.3f %f\n',leg{i}, tmp(i),vtmp(i));
end;

%optimal virtual sample methods (for any fusion methods)
res(5,:)=[];
out = reshape (res,[prod(size(res)),1]);
mean(out)
std(out)