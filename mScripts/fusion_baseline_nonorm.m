function [com, epc_cost] = fusion_baseline_nonorm(expe)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

%epc curve
fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(expe.dset{1,1}, expe.dset{1,2}, expe.dset{2,1}, expe.dset{2,2}, n_samples,epc_range);

com.dset = expe.dset;