function [out] = fusion_clientd_Fratio(expe, beta)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];
N=1;

%zero-mean unit variance normalisation
model.mean = mean([expe.dset{1,1}; expe.dset{1,2}]);
model.std = std([expe.dset{1,1}; expe.dset{1,2}]);
[expe.dset{1,1}, expe.dset{1,2}] = normalise_scores(expe.dset{1,1}, expe.dset{1,2}, model.mean, model.std);
[expe.dset{2,1}, expe.dset{2,2}] = normalise_scores(expe.dset{2,1}, expe.dset{2,2}, model.mean, model.std);

%this normalisation applies to one dimension only!
chosen = [1];

%get the model label
model_ID = unique(expe.label{1,1});

%go through each id
for d=1:2,for k=1:2,
	out{d,k} = [];
end;end;

%calculate global param
param_global = VR_analysis(expe.dset{1,1},expe.dset{1,2});


%perform client-dependent normalisation
for id=1:size(model_ID,1),
	%get the data set associated to the ID
	for d=1:2,for k=1:2,
	    index{d,k} = find(expe.label{d,k} == model_ID(id));
	    tmp{d,k} = expe.dset{d,k}(index{d,k},chosen);
	end;end;
   
	%Apply F-ratio normalisation on training and test sets, in an ID-dependnent manner
	param_client = VR_analysis(tmp{1,1}, tmp{1,2});
	[clientd{id}.dset{1,1}, clientd{id}.dset{1,2}] = VR_Fnorm(tmp{1,1}, tmp{1,2}, param_client, param_global, 1:N, beta);
	[clientd{id}.dset{2,1}, clientd{id}.dset{2,2}] = VR_Fnorm(tmp{2,1}, tmp{2,2}, param_client, param_global, 1:N, beta);
	%combine the fused scores
	for d=1:2,for k=1:2,
	    out{d,k}(index{d,k},:) = clientd{id}.dset{d,k};
	end;end;
end;

