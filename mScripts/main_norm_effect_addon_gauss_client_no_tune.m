cd /home/learning/norman/xm2vts_fusion/mScripts

%cd c:/cygwin/home/Norman/learning/xm2vts_fusion/mScripts/
load norm_expe_addon_gauss_client.mat com gauss_com mix_com epc_cost
initialise;

%load data
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, label{p}{1,1}, label{p}{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, label{p}{2,1}, label{p}{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      
      %print remarks
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);
      
      %load score files
      for d=1:2,for k=1:2,
	  nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
	end;
      end;
	
      %zero-mean unit variance normalisation
      nexpe{2} = zmuv_norm(nexpe{1});
      
      %use margin scores	
      nexpe{3} = convert2margin_scores(nexpe{1});
      
      %for MLP output
      for d=1:2,for k=1:2,
	  nexpe{4}.dset{d,k} = nexpe{1}.dset{d,k};
	  for ex=1:2,
	    if (mlp{p,s,r}(ex)),
              fprintf(1, 'expert %d is an MLP, so normalised!\n',ex);
              nexpe{4}.dset{d,k}(:,ex) = tanh_inv(nexpe{1}.dset{d,k}(:,ex));
            end;
          end;
        end;
      end;
      %draw_empiric(nexpe{4}.dset{1,1},nexpe{4}.dset{1,2});
      
      %prepare filtered data
      nexpe{5} = nexpe{4};
      for d=1:2,
        for k=1:2,
          for ex=1:2,
            if (mlp{p,s,r}(ex)),
              selected = find(nexpe{5}.dset{d,k}(:,ex)>30);
              nexpe{5}.dset{d,k}(selected,:)=[];
              selected = find(nexpe{5}.dset{d,k}(:,ex)<-30);
              nexpe{5}.dset{d,k}(selected,:)=[];
            end;
          end;
        end;
      end;
      %draw_empiric(nexpe{5}.dset{1,1},nexpe{5}.dset{1,2});
      param = VR_analysis(nexpe{5}.dset{1,1},nexpe{5}.dset{1,2});
      
      %now replace bad values with a reasonable one
      nexpe{6} = nexpe{4};
      for d=1:2,
        for k=1:2,
          for ex=1:2,
            if (mlp{p,s,r}(ex)),
              selected = find(nexpe{6}.dset{d,k}(:,ex)>30);
              nexpe{6}.dset{d,k}(selected,ex)= repmat(param.mu_C(ex),size(selected,1),1);
              selected = find(nexpe{6}.dset{d,k}(:,ex)<-30);
              nexpe{6}.dset{d,k}(selected,ex)= repmat(param.mu_I(ex),size(selected,1),1);
            end;
          end;
        end;
      end;
      %draw_empiric(nexpe{6}.dset{1,1},nexpe{6}.dset{1,2});
      %param2 = VR_analysis(nexpe{5}.dset{1,1},nexpe{5}.dset{1,2});
      
      %second-level fusion
      for d=1:2,for k=1:2,
          new_expe.dset{d,k} = [nexpe{1}.dset{d,k} nexpe{2}.dset{d,k} nexpe{3}.dset{d,k} nexpe{4}.dset{d,k} nexpe{6}.dset{d,k}];
          new_expe.label{d,k} = label{p}{d,k};  
        end;
      end;

      chosen = [1:2];
      [com{1}{p,s,r}] = fusion_gmm(new_expe, chosen, [], com{1}{p,s,r}.bayesS,0);

      chosen = [9:10];
      gauss_com{1}{p,s,r} = fusion_gauss_client(new_expe, chosen, param, 1, 'ca(mu)-qda', 0);
      %gauss_com{2}{p,s,r} = fusion_gauss_client(new_expe, chosen, param, 1, 'ca(mu)-lda', 0);
      
      for d=1:2,for k=1:2,
          texpe.dset{d,k} = [com{1}{p,s,r}.dset{d,k}, gauss_com{1}{p,s,r}.dset{d,k}];
	  %texpe.dset{d,k} = [com{1}{p,s,r}.dset{d,k}, gauss_com{2}{p,s,r}.dset{d,k}];
	end;
      end;

      mix_com{1}{p,s,r} = fusion_svm(texpe);
      while  mean(mix_com{1}{p,s,r}.epc.dev.hter)>0.5,
	mix_com{1}{p,s,r} = fusion_svm(texpe);
      end;
      
      %mix_com{2}{p,s,r} = fusion_svm(texpe);
      mix_com{3}{p,s,r} = fusion_wsum_brute_nonorm(texpe, [1 2],[0.5 0.5]);
      
      %show the possible gain (or loss due to the combination of
      %both global and local into
      m=1;
      hold off;
      plot(epc_cost(:,1), com{1}{p,s,r}.epc.eva.hter_apri*100,'b+-'); hold on;
      plot(epc_cost(:,1), gauss_com{m}{p,s,r}.epc.eva.hter_apri*100,'ro-');
      plot(epc_cost(:,1), mix_com{m}{p,s,r}.epc.eva.hter_apri*100,'gs-');
      plot(epc_cost(:,1), mix_com{m+2}{p,s,r}.epc.eva.hter_apri*100,'g*-');
      legend('global', 'local', 'mix-svm','mix-mean');
      drawnow;
      
      com{m}{p,s,r}.dset ={};
      gauss_com{m}{p,s,r}.dset ={};
      mix_com{m}{p,s,r}.dset ={};
      mix_com{3}{p,s,r}.dset ={};

    end;
  end;
end;

save norm_expe_addon_gauss_client_no_tune.mat com gauss_com mix_com

ncom{1} = com{1};
%qdacom
ncom{2} = gauss_com{1};
ncom{3} = mix_com{1};

%lda
%ncom{4} = gauss_com{2};
%ncom{5} = mix_com{2};

for i=1:size(ncom,2),
  [out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global(ncom{i}); %overall
  [out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global(ncom{i},1); %multimodal
  [out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global(ncom{i},[2 3]); %intramodal
end;

signs = {'g*-','bd-','bs--','rx-.','ro:'};
leg = {'global(gmm)', 'local(qda)', 'mix(qda)','local(lda)', 'mix(lda)'};

list = [1 2 3]; i=1;
plot_all_epc(epc_cost,leg,signs, out.cfg{i}, list);
plot_all_roc(epc_cost,leg,signs, out.cfg{i}, list);

%
for i=1:size(ncom,2),
  [out.cfg{1}.res{i},out.cfg{1}.pNI,out.cfg{1}.pNC] = epc_global_custom(ncom{i},expe.psr_list(expe.isMultiNotFH,:),NI,NC); %multimodal-notFH
  [out.cfg{2}.res{i},out.cfg{2}.pNI,out.cfg{2}.pNC] = epc_global_custom(ncom{i},expe.psr_list(expe.isFH,:),NI,NC); %multimodal-FH
  [out.cfg{3}.res{i},out.cfg{3}.pNI,out.cfg{3}.pNC] = epc_global_custom(ncom{i},expe.psr_list(expe.isIntraFace,:),NI,NC); %intraFace
  [out.cfg{4}.res{i},out.cfg{4}.pNI,out.cfg{4}.pNC] = epc_global_custom(ncom{i},expe.psr_list(expe.isIntraFaceNotFH,:),NI,NC); %intraFaceNotFH
  [out.cfg{5}.res{i},out.cfg{5}.pNI,out.cfg{5}.pNC] = epc_global_custom(ncom{i},expe.psr_list(expe.isIntraSpeech,:),NI,NC); %intraSpeech
  [out.cfg{6}.res{i},out.cfg{6}.pNI,out.cfg{6}.pNC] = epc_global_custom(ncom{i},expe.psr_list(expe.isMultimodal,:),NI,NC); %multimodal
end;

figure(1);
for i=1:6,
  subplot(2,3,i);
  plot_all_epc(epc_cost,leg,signs, out.cfg{i}, list,10,0);
  title(['[' num2str(i) ']']);
end;
figure(2);
for i=1:6
  subplot(2,3,i);
  plot_all_roc(epc_cost,leg,signs, out.cfg{i}, list,10,0);
  title(['[' num2str(i) ']']);
end;