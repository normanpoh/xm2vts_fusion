function res = epc_client_d.m(expe);

%get the model label
model_ID = unique(expe.label{1,1});

out2.fa = [];
out2.fr = [];

%perform client-dependent processing
fprintf(1, 'Processed ID:\n');
for id=1:size(model_ID,1),
	%get the data set associated to the ID
	for d=1:2,for k=1:2,
	    index{d,k} = find(expe.label{d,k} == model_ID(id));
	    tmp{d,k} = expe.dset{d,k}(index{d,k},chosen);
	end;end;
   
	%fprintf(1,'\nCalculating EPC');
	[out2.epc.dev, out2.epc.eva, epc_cost]  = epc(clientd{id}.dset{1,1},clientd{id}.dset{1,2},clientd{id}.dset{2,1},clientd{id}.dset{2,2},n_samples,epc_range);

	Ni = size(clientd{id}.dset{2,1},1);
	Nc = size(clientd{id}.dset{2,2},1);

	out2.fa = [out2.fa; out2.epc.eva.far_apri * Ni];
	out2.fr = [out2.fr; out2.epc.eva.frr_apri * Nc];
end;
NI = size(expe.dset{2,1},1);
NC = size(expe.dset{2,2},1);
com.epc.eva.far_apri = sum(out2.fa) / NI;
com.epc.eva.frr_apri = sum(out2.fr) / NC;
com.epc.eva.hter_apri = (com.epc.eva.far_apri+com.epc.eva.frr_apri)/2;
out2.fa
out2.fr
