t=0;
for r=1:size(xx,1),
for s=1:2,
	t=t+1;
	figure(t);subplot(1,1,1);
	i=1;
	hold off;
	plot( epc_cost(:,1), cres2{s}.mean(:,xx(r,1)),'b'); 
	hold on;	
	plot( epc_cost(:,1), res2{s}.mean(:,xx(r,2)),'r--'); 

	%tmp.p = ones(size(res2{s}.mean(:,1))) * 0.975;
	%tmp.upper = norminv(tmp.p,res2{s}.mean(:,1), res2{s}.std(:,1));
	%tmp.p = ones(size(res2{s}.mean(:,1))) * 0.025;
	%tmp.lower = norminv(tmp.p,res2{s}.mean(:,1), res2{s}.std(:,1));
	%plot( epc_cost(:,1), tmp.lower  ,'r:');
	%plot( epc_cost(:,1), tmp.upper  ,'r:');

	%plot( epc_cost(:,1), cres2{s}.mean(:,xx(r,1)) + 0.1 * cres2{s}.std(:,xx(r,1))  ,'b:');
	%plot( epc_cost(:,1), cres2{s}.mean(:,xx(r,1)) - 0.1 *cres2{s}.std(:,xx(r,1))  ,'b:');
	%plot( epc_cost(:,1), res2{s}.mean(:,xx(r,2)) + 0.1 * res2{s}.std(:,xx(r,2))  ,'r:');
	%plot( epc_cost(:,1), res2{s}.mean(:,xx(r,2)) - 0.1 *res2{s}.std(:,xx(r,2))  ,'r:');

	plot( epc_cost(:,1), ones(size(epc_cost,1),1), 'k:'); 
	ylabel('\beta_{min}');
	xlabel('\alpha');
	title(tit{s});
	legend(leg{r});

	print('-depsc2', sprintf('../Pictures/%s_%d_%d_beta.eps',configf,s,r));
end;end;
