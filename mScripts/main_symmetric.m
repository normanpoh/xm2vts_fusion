cd /home/learning/norman/xm2vts_fusion/mScripts

initialise;

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

clear out;
for k=1:2,
  out.corr.intramodal{k} = [];
  out.corr.multimodal{k} = [];
  out.std{k} = [];
end;

for p=1:2,

  %initliase
  for d=1:2,for k=1:2,
      selected{d,k} = [];
      nexpe.dset{d,k} = [];
    end;
  end;

  %find candidates to filter
  for b=1:size(dat.P{p}.labels,2)
      for d=1:2,for k=1:2,
	  nexpe.dset{d,k}(:,b) = data{p}.dset{d,k}(:,b);
	  if (ismlp{p}(b)),
	    fprintf(1, 'expert %d is an MLP, so normalised!\n',b);
	    nexpe.dset{d,k}(:,b) = tanh_inv(data{p}.dset{d,k}(:,b));
	    selected{d,k} = union(selected{d,k},find(nexpe.dset{d,k}(:,b)>30));
	    selected{d,k} = union(selected{d,k},find(nexpe.dset{d,k}(:,b)<-30));
	  end;
	end;
      end;
  end;

  %filter
  for d=1:2,for k=1:2,
      nexpe.dset{d,k}(selected{d,k},:) = [];
    end;
  end;

  pair_wise = nchoosek(1:size(dat.P{p}.labels,2),2);

  for d=1:2,for k=1:2,

      %find corr
      corr_ = corrcoef(nexpe.dset{d,k});
      %find std dev
      std_ = std(nexpe.dset{d,k}); 
      out.std{k} = [out.std{k} std_];
      
      for i=1:size(pair_wise,1),
	if (dat.P{p}.type(pair_wise(i,1)) == dat.P{p}.type(pair_wise(i,2))),
	  %intramodal
	  out.corr.intramodal{k} = [out.corr.intramodal{k} corr_(pair_wise(i,1),pair_wise(i,2))];
	else
	  out.corr.multimodal{k} = [out.corr.multimodal{k} corr_(pair_wise(i,1),pair_wise(i,2))];
	end;
      end;


     
    end;
  end;

end;

hold off;
plot(out.corr.intramodal{1}, out.corr.intramodal{2}, '+');
hold on;
plot(out.corr.multimodal{1}, out.corr.multimodal{2}, 'ro');
grid on;
x=linspace(0,1,10);
plot(x,x,'r--');
corrcoef([[out.corr.intramodal{1}' out.corr.intramodal{2}']; 
	  [out.corr.multimodal{1}' out.corr.multimodal{2}']])
corrcoef([out.corr.multimodal{1}' out.corr.multimodal{2}'])
	  


hold off;
plot(out.std{1},out.std{2},'+');
hold on;
grid on;
x=linspace(0.2,2.2,10);
plot(x,x,'r--');
xlabel('\sigma_I');
ylabel('\sigma_C');
corrcoef([out.std{1}' out.std{2}']);