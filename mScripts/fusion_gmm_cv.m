function [com] = fusion_gmm_cv(expe, chosen, gmm_cv_list, toplot)

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
  chosen = [1:2];
end;

if (nargin < 3),
  if (length(gmm_cv_list) == 0),
    gmm_cv_list = [1 2:2:20];
  end;
else
  gmm_cv_list = [1 2:2:20];
end;

if (nargin < 4),
  toplot = 0;
end;

%make chosen
for d=1:2,for k=1:2,
	expe.dset{d,k} = expe.dset{d,k}(:,chosen);
end;end;

%tuning by cross-validation
ratio = [0.5 0.5];
tmp{1} = splitdata(expe.dset{1,1}, ratio);
tmp{2} = splitdata(expe.dset{1,2}, ratio);
tdata.dset{1,1} = tmp{1}{1};
tdata.dset{1,2} = tmp{2}{1};
tdata.dset{2,1} = tmp{1}{2};
tdata.dset{2,2} = tmp{2}{2};                

for g=1:size(gmm_cv_list,2),
  fprintf(1, '%d.', gmm_cv_list(g));
  for k=1:2,
    %training on dset=1
    data = tdata.dset{1,k};
    labels = ones(size(tdata.dset{1,k},1),1);
    
    FJ_params = { 'Cmax', gmm_cv_list(g), 'Cmin', gmm_cv_list(g), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
    bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});
    
    %only a single class here!
    
    c=1;llh_list = gmmb_pdf(tdata.dset{2,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
    llh(g,k) = mean(log(llh_list+realmin));
  end;
end;

for k=1:2,
  [tmp_, ind] = max(llh(:,k));
  gcomp(k) = gmm_cv_list(ind(1)); %pick the first one if there are several
  dind(k) = ind(1);
end;

if (toplot),
  hold off;
  plot(gmm_cv_list,llh(:,1), 'r-'); 
  hold on;
  plot(gmm_cv_list,llh(:,2), 'b-');
  plot(gmm_cv_list(dind(1)),llh(dind(1),1), 'ro');
  plot(gmm_cv_list(dind(2)),llh(dind(2),2), 'bo');
  ylabel('Average LL');
  xlabel('Gaussian components');
  legend('impostor', 'client');
end;

fprintf(1, '\nImpostor has %d components\n', gcomp(1));
fprintf(1, 'Client has %d components\n', gcomp(2));

%training
clear bayesS
for k=1:2,
  data = expe.dset{1,k};
  labels = ones(size(expe.dset{1,k},1),1);
  FJ_params = { 'Cmax', gcomp(k), 'Cmin', gcomp(k), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
  bayesS{k} = gmmb_create(data, labels, 'FJ', FJ_params{:});
end;

%testing
for d=1:2,for k=1:2,
    c=1;tmp1 = gmmb_pdf(expe.dset{d,k}, bayesS{c}.mu, bayesS{c}.sigma, bayesS{c}.weight );
    c=2;tmp2 = gmmb_pdf(expe.dset{d,k}, bayesS{c}.mu, bayesS{c}.sigma, bayesS{c}.weight );
    com.dset{d,k} = log(tmp2+realmin)-log(tmp1+realmin);
  end;
end;
  
com.bayesS = bayesS;
com.gcomp = gcomp;

fprintf(1,'\nCalculating EPC');
[com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);