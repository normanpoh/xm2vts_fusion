function out = bootstrap_indep(dset_I, dset_C , mode, varargin)

%constants
n_DET_samples = 5000;
dodisplay = 1;

%process arguments
argc = nargin-3;
for argl = 1:2:(argc-1),
  %fprintf(1,'Processing %s\n', varargin{argl});
  switch lower(varargin{argl})
   case 'width'
    width = varargin{argl+1};
   case 'datapoints',
    xx = varargin{argl+1};
   case 'n_bstrp',
    n_bstrp = varargin{argl+1};
   case 'method'
    mode = varargin{argl+1};
   case 'coef-list'
    coef_list = varargin{argl+1};
   case 'dodisplay'
    dodisplay = varargin{argl+1};
   otherwise
  end
end

%param-checking
if strcmpi(mode,'parzen'),
  if ~exist('width', 'var'), error('kernel width must be defined');end;
end;
if strcmpi(mode,'gmm'),
  if ~exist('width', 'var'), error('kernel width must be defined');end;
  if ~exist('coef_list', 'var'), error('coef-list must be defined');end;
end;

if ~exist('xx', 'var'), 
  mydat=[dset_I; dset_C];
  minmax(1,:) = min(mydat);
  minmax(2,:) = max(mydat);
  clear mydat;
  
  xx=linspace(minmax(1),minmax(2),n_DET_samples)';
end;

%overwrite
%cla; hold on;

%sample the distribution here
out.FAR=[];
out.FRR=[];

switch mode
  
 case 'parzen'
  for s_ = 1:n_bstrp,
    n_dat{1}= parzen_rnd(dset_I, width(1), length(dset_I));
    n_dat{2}= parzen_rnd(dset_C, width(2), length(dset_C));
    [tmp_, tmp_, tmp_, FAR, FRR] = wer(n_dat{1}, n_dat{2});
    if (dodisplay), plot(ppndf(FAR),ppndf(FRR), 'r:'); end;
    out.FAR=[out.FAR; FAR];
    out.FRR=[out.FRR; FRR];
  end;
    
  %plot the original one
  %[pdf{1}]= eva_pdf(xx, dset_I, 0, width(1));
  %[pdf{2}]= eva_pdf(xx, dset_C, 0, width(2));
  %plot(ppndf(1-cumsum(pdf{1})/sum(pdf{1})),ppndf(cumsum(pdf{2})/sum(pdf{2})),'g--','linewidth',2);
  
 case 'bolle'
  for s_ = 1:n_bstrp,
    n_dat{1}= bootstrap_replace(dset_I);
    n_dat{2}= bootstrap_replace(dset_C);
    [tmp_, tmp_, tmp_, FAR, FRR] = wer(n_dat{1}, n_dat{2});
    if (dodisplay), plot(ppndf(FAR),ppndf(FRR), 'r:'); end;
    out.FAR=[out.FAR; FAR];
    out.FRR=[out.FRR; FRR];
  end;
 case 'gmm'
  for coef = 1:length(coef_list),
    [pdf{1}]= eva_pdf(xx, dset_I, 0, coef_list(coef)*width(1));
    [pdf{2}]= eva_pdf(xx, dset_C, 0, coef_list(coef)*width(2));
    if (dodisplay),
      plot(ppndf(1-cumsum(pdf{1})/sum(pdf{1})),ppndf(cumsum(pdf{2})/sum(pdf{2})),'r:');
    end;
  end;
end;

if (dodisplay), Make_DET; end;
%drawnow;
%plot the actual one on top
%[tmp_, tmp_, tmp_, FAR, FRR] = wer(dset_I,dset_C);
%plot(ppndf(FAR),ppndf(FRR),'b-','linewidth',2);

