function [newpoints]= bootstrap_replace(datapoints, n_samples)
if nargin<2,
  n_samples = length(datapoints);
end;
selected_centroids = floor(rand(1,n_samples)*length(datapoints))+1; 
newpoints = datapoints(selected_centroids');
