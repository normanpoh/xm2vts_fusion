function [bayesS, data, cpoints, entropy] = plot_DET_mass(FAR, FRR, varargin)

%default values
filteraway = 4;
conf_list = [0.95 0.8 0.5];
n_samples = 100;
draw_contour = 1;
shade = 1;
plot_points = 0;
sign = 'k';
lwidth = 1;
%process arguments
argc = nargin-2;
for argl = 1:2:(argc-1),
  switch lower(varargin{argl})
   case 'data'
    data = varargin{argl+1};
   case 'conf_list'
    conf_list = varargin{argl+1};
   case 'n_samples',
    n_samples = varargin{argl+1};
   case 'draw_contour',
    draw_contour = varargin{argl+1};
   case 'shade'
    shade = varargin{argl+1};
   case 'plot_points'
    plot_points = varargin{argl+1};
   case 'bayess'
    bayesS = varargin{argl+1};
   case 'color'
    sign = varargin{argl+1};
   case 'linewidth'
    lwidth = varargin{argl+1};
   otherwise
    error('option %s does not exist\n',varargin{argl} );
  end
end

%compute the likelihood

if ~exist('bayesS','var'),
  %filter away bad data

  data = [ppndf(FAR),ppndf(FRR)];
  selected = union(find(data(:,1)<-filteraway),find(data(:,2)<-filteraway));
  data(selected,:)=[];
  selected = union(find(data(:,1)>filteraway),find(data(:,2)>filteraway));
  data(selected,:)=[];

  if (plot_points),
    plot(data(:,1), data(:,2), 'm.','markersize',1);
  end;

  FJ_params = { 'Cmax', 20, 'Cmin', 20, 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
  labels = ones(size(data,1),1);
  bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});
  %Make_DET(0.99);
end;

[xtesta1,xtesta2]=meshgrid( ...
    linspace(ppndf(0.0001), ppndf(0.99), n_samples), ...
    linspace(ppndf(0.0001), ppndf(0.99), n_samples) );
[na,nb]=size(xtesta1);
xtest1=reshape(xtesta1,1,na*nb);
xtest2=reshape(xtesta2,1,na*nb);
xtest=[xtest1;xtest2]';

c=1;tmp1 = gmmb_pdf(xtest, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
ypred = tmp1;
ypred = ypred / sum(ypred);

ypred_prob = ypred;
%get the location
clear index;
tmpx = sort(ypred,1,'descend');
tmpy = cumsum(tmpx);
for t=1:length(conf_list),
  [tmp_, index(t)] =  min(abs(tmpy-conf_list(t)));
end;
ypred = log(ypred+realmin);
cutoff = log(tmpx(index+realmin));

ypredmat=reshape(ypred,na,nb);

%cla; 
hold on;
if (shade),
  contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
end;
if (draw_contour),
  [cpoints,h]=contour(xtesta1,xtesta2,ypredmat,[cutoff'],sign,'linewidth',lwidth);
end;
%clabel(cpoints,h);
%colorbar;
if (shade | draw_contour),
   Make_DET(0.99);
end;

%post-process the cpoints
if nargout>=3,
  selected = find(cpoints(2,:)>6);
  cpoints(:,selected)=[];
  k = convhull(cpoints(1,:),cpoints(2,:));
  cpoints = cpoints(:,k);
  %figure;plot(cpoints(1,:)',cpoints(2,:),'b-');
end;
if nargout>=4,
  entropy = -sum(log(ypred_prob) .* ypred_prob);
end;