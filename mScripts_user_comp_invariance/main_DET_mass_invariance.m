%modified from main_eer_sensitivity_demo01_empVthry.m
cd /home/learning/norman/xm2vts_fusion/mScripts_user_comp_invariance
addpath /home/learning/norman/xm2vts_fusion/mScripts

%compare empirical and theoretical diff
initialise;
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%use the normalised version
load main_baseline_addon_FNorm_rerun baseline
for p=1:2,
  bline{p}.label = data{p}.label;
  for b=1:size(dat.P{p}.labels,2)
    bdat{1}{p,b}.dset = baseline{3}{p,b}.dset; %Z-Norm
    %bdat{2}{p,b}.dset = baseline{6}{p,b}.dset;  %F-Norm
  end;
end;
clear baseline;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% construct the styles for ploting purpose
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
style = {'-','--'};%,':'};
color = {'b', 'g', 'r', 'k'};%, 'm', 'y', 'k'};
i=0;
for c=1:length(color),
  for j=1:2, %repeat twice
    for s =1:length(style),
      i=i+1;
      signs{i}=sprintf('%s%s',color{c},style{s});
      lwidth(i) = j;
    end;
  end;
end;

model_ID = unique(data{p}.label{1,1});

p=1;b=2;
%demonstrate the instability of DET due to the composition of users

%mode
mode = 1;
id_list= {[1:50], [51:100],[101:150],[151:200]};
offset_list = [ 0 50 100 150];
user_size = 50;

mode = 2; %prefered mode
id_list= {[1:100], [101:200]};
offset_list = [ 0 100];
user_size = 100;

%prefered param
%n_user_bstrp=30;
%n_bstrp = 10;
n_user_bstrp=20;
n_bstrp = 4;


%prefered data: orig
t=1;
%t=2;

for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('Experiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    for d=1:2, for k=1:2,
	nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    nexpe{1}.label = data{p}.label;
    
    %try the normalized version
    nexpe{2}.dset = bdat{1}{p,b}.dset; %use Z-Norm
    nexpe{2}.label = bline{p}.label;
    
    %for t=2:2,
      %figure(t);
      for i=1:length(id_list), %for each partition
	
	%figure(1); cla; hold on;
	chosen_ID = id_list{i};
	for n_=1:n_user_bstrp,
	  if n_user_bstrp ==1,
	    rexpe = rndchoose_client_list(nexpe{t}, [], chosen_ID, model_ID);
	    %the original partition
	  else
	    %sample from the original parition
	    chosen_ID = floor(rand(1,user_size)*user_size)+ 1 + offset_list(i);
	    %user-specific bootstrap
	    rexpe = rndchoose_client_list(nexpe{t}, [], chosen_ID, model_ID);
	  end;
	  %indep. bootstrap
	  opt ={'n_bstrp',n_bstrp,'dodisplay',0};
	  %out{t}{i,n_} = bootstrap_indep(rexpe.dset{1,1}, rexpe.dset{1,2}, 'bolle',opt{:});
	  out{n_} = bootstrap_indep(rexpe.dset{1,1}, rexpe.dset{1,2}, 'bolle',opt{:});
	end;
	
	myout.FAR=[];
	myout.FRR=[];
	for n_=1:n_user_bstrp,
	  myout.FAR = [myout.FAR; out{n_}.FAR];
	  myout.FRR = [myout.FRR; out{n_}.FRR];
	end;
	
	opt = {'conf_list', [0.95],'shade',0,'draw_contour',1};
	[res{p,b}{i}.bayesS, tmp_, res{p,b}{i}.cpoints,res{p,b}{i}.entropy] = plot_DET_mass(myout.FAR, myout.FRR,opt{:});
	
      end;  %for each parition i
      %[wer_min, thrd_min, x] = wer(nexpe{t}.dset{1,1}, nexpe{t}.dset{1,2}, [], 2,[],5);
      %legend('set 1 (50)','set 2 (50)','set 3 (50)','set 4 (50)','all (200)');
    
      %plot
      cla; hold on;
      for i=1:length(id_list), %for each partition      
	plot(res{p,b}{i}.cpoints(1,:), res{p,b}{i}.cpoints(2,:),signs{i},'linewidth',lwidth(i));
      end;
      Make_DET(0.99);
      drawnow;

    %end; %for each data set t (orig or Z-Normalised)
    fname = sprintf('main_DET_mass_dat%d_mode%d_%02d_%02d.mat',t,mode, n_user_bstrp,n_bstrp);
    save(fname, 'res');
  end;
end;
save(fname, 'res','n_user_bstrp','n_bstrp','t','mode','id_list');



load('main_DET_mass_dat1_mode1_30_10.mat'); t=1;
load('main_DET_mass_dat2_mode1_30_10.mat'); t=2;
load('main_DET_mass_dat1_mode1_20_04.mat'); t=1;
signs_list = [3 4 15 16];
signs = {'b-','r--','g-.','m-'};
%analyse

clear myout
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    for d=1:2, for k=1:2,
	nexpe{1}.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    nexpe{1}.label = data{p}.label;
    
    %try the normalized version
    nexpe{2}.dset = bdat{1}{p,b}.dset; %use Z-Norm
    nexpe{2}.label = bline{p}.label;
    
    cla; hold on;
    for i=1:length(res{p,b}),
      %plot(res{p,b}{i}.cpoints(1,:),res{p,b}{i}.cpoints(2,:),signs{i});%,'linewidth',lwidth(i));
      opt = {'conf_list', [0.95],'shade',0,'draw_contour',1,'bayesS',res{p,b}{i}.bayesS,'color',signs{i}};
      plot_DET_mass([],[],opt{:});
      chosen_ID = id_list{i};
      rexpe = rndchoose_client_list(nexpe{t}, [], chosen_ID, model_ID);
      
      [tmp_, tmp_, tmp_, FAR, FRR] = wer(rexpe.dset{1,1}, rexpe.dset{1,2});%,[],2,[],signs_list(i));
      plot(ppndf(FAR),ppndf(FRR),signs{i},'linewidth',2);
      pFAR = norminv(FAR);
      pFRR = norminv(FRR); 
      selected = union(find(pFAR<-6), find(pFRR<-6));
      pFAR(selected)=[];
      pFRR(selected)=[];
      out.pFAR{i} = pFAR;
      out.pFRR{i} = pFRR;
     
    end;
    Make_DET(0.99);
    
    for i=1:length(res{p,b}),
      for i_=1:length(res{p,b}),
	k = inpolygon(out.pFAR{i_},out.pFRR{i_},res{p,b}{i}.cpoints(1,:),res{p,b}{i}.cpoints(2,:));
	myout{p,b}.DET_prop(i,i_) = length(find(k==1))/length(k);
      end;
    end;

    fname = sprintf('../Pictures/DET_mass_dat%d_mode%d_%02d_%02d_p%d_b%02d.eps',t,mode, n_user_bstrp,n_bstrp,p,b);
    print('-depsc2', fname);
  end;
end;

clear DET_prop
t=0;
for p=1:1,
  for b=1:size(dat.P{p}.labels,2)   
    t=t+1;
    DET_prop(:,:,t) = myout{p,b}.DET_prop;
  end;
end;
tmp = mean(DET_prop,3) .* (1-eye(2));
tmp = reshape(tmp, [1 prod(size(tmp))]);
tmp(find(tmp <=0))=[];
mean(tmp)


hist(DET_prop(1,2))

%load('main_DET_mass_dat1_mode1_30_10.mat');
0.9638    0.6865    0.6638    0.6935
0.8317    0.9809    0.7457    0.8146
0.5705    0.5416    0.9705    0.4712
0.7743    0.7700    0.4298    0.9701
ave = 0.6661
%averaged over 8 experiments (p=1, b=1...8)

%load('main_DET_mass_dat2_mode1_30_10.mat');
0.9800    0.7303    0.6339    0.8002
0.8376    0.9833    0.7737    0.9045
0.4982    0.4086    0.9796    0.4417
0.8372    0.8113    0.6135    0.9570
ave = 0.6909
%averaged over 8 experiments (p=1, b=1...8)

%load('main_DET_mass_dat1_mode1_20_04.mat');
0.9670    0.8268
0.7523    0.9776
ave = 0.7896
%over all 13 baseline experiments