function [dev, eva, cost]  = epc_bayesS(bayesS, n_samples,range)
if (nargin < 2),
  n_samples =11;
end;
if (nargin < 3),
  range = [0,1];
end;

c_fa = linspace(min(range),max(range),n_samples);
c_fr = 1-c_fa;
cost = [c_fa' c_fr'];

%get some data from bayesS, using 1000 samples
data =[bayesS{1}(1).mu, bayesS{1}(2).mu bayesS{2}(1).mu, bayesS{2}(2).mu];
x = linspace(min(data), max(data), 1000)';

%get the 
%a faster implementation:
for d=1:2, for k=1:2,
  pxomega_pdf(:, (d-1)*2+k) = gmmb_pdf(x, bayesS{d}(k).mu, bayesS{d}(k).sigma, bayesS{d}(k).weight );
  end;
end;
spdf = cumsum(pxomega_pdf);
spdf = spdf ./ repmat(sum(pxomega_pdf),length(spdf),1);

FAR{1} = 1 - spdf(:,1);
FRR{1} = spdf(:,2);
FAR{2} = 1 - spdf(:,3);
FRR{2} = spdf(:,4);

for i=1:n_samples,
  [tmp, min_index] = min( abs(cost(i,1) * FAR{1} - cost(i,2) * FRR{1}) );
  thrd_min = x(min_index);
  dev.wer_apost(i) = cost(i,1) * FAR{1}(min_index) + cost(2) * FRR{1}(min_index);
  dev.thrd_fv(i) = thrd_min;
  
  eva.far_apri(i) = FAR{2}(min_index);
  eva.frr_apri(i) = FRR{2}(min_index); 
  eva.hter_apri(i) = 0.5 * (eva.far_apri(i) + eva.frr_apri(i));
  
  dev.far_apri(i) = FAR{1}(min_index);
  dev.frr_apri(i) = FRR{1}(min_index); 
  dev.hter(i) = 0.5 * (dev.far_apri(i) + dev.frr_apri(i));

end;

