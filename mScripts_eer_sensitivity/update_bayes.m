function nbayesS = update_bayes(bayesS, scale, shift, d_list, k_list, gamma, gbayesS)
%gamma(m,k) where m is moment 1 or 2 and k is class
if nargin<4|length(d_list)==0,
  d_list=[1:2];
end;
if nargin<5|length(k_list)==0,
  k_list=[1:2];
end;
if nargin<6|length(gamma)==0,
  gamma(1,1) = 1;
  gamma(2,1) = 1; %full confidence on impostor param
  gamma(1,2) = 1; %full confidence on client mean
  gamma(2,2) = 0; %zero confidence on client sigma
end;
if nargin<7|length(gbayesS)==0,
  gbayesS=bayesS;
end;

%d=d_list(1);
%k=k_list(1);

Nc = length(bayesS{1}(1).weight);%the number of component

nbayesS = bayesS; %make a copy
for d=d_list,
  for k=k_list,
    for id=1:Nc,
      term = gamma(2,k) * bayesS{d}(k).sigma(1,1,id) + (1-gamma(2,k))* gbayesS{d}(k).sigma(1,1,1);
      nbayesS{d}(k).sigma(1,1,id) = term * scale(d, k);
    end;
    term = gamma(1,k) * bayesS{d}(k).mu + (1-gamma(1,k)) * gbayesS{d}(k).mu ;
    nbayesS{d}(k).mu = term +  shift(d, k);
  end;
end;
