function [scale_value, goodness] = tune_scale(shift, xx, cdf, bayesS, d, k, bound, n_samples, precision,dodisplay,max_round)
if nargin<8|length(n_samples)==0|n_samples<5,
  warning('too few samples');
  n_samples = 5; %must be at least five
end;
if nargin<7|length(bound)==0,
  bound = tan(linspace(pi*10/100,pi/2 - pi/100,n_samples));
else
  bound = linspace(min(bound),max(bound),n_samples);
end;
if nargin<9|length(precision)==0,
  precision=1e-4;
end;
if nargin<10,
  dodisplay=0;
end;
if nargin<11,
  max_round = 3;
end;

Nc = length(bayesS{d}(k).weight);%the number of component

examined.ks=[];
examined.bound=[];
t=0;
done=0;
while ~done,
  nbayesS = bayesS;
  for scale = 1:length(bound),
    for id=1:Nc,
      nbayesS{d}(k).sigma(1,1,id) = bayesS{d}(k).sigma(1,1,id) * bound(scale);
    end;
    nbayesS{d}(k).mu = bayesS{d}(k).mu + shift;
    
    %ks(scale) = bayes2ks(xx,cdf, nbayesS, d, k);
    %evaluate the goodness of fit
    %if (dodisplay),
    %  figure(3);subplot(1,length(bound), scale);
    %  ks(scale) = bayes2ks(xx,cdf, nbayesS, d, k,1);
    %  txt = sprintf('%1.3f',bound(scale)); title(txt);
    %else
    %  ks(scale) = bayes2ks(xx,cdf, nbayesS, d, k,0);
    %end;
    
    %evaluate the goodness of fit
    pxomega_pdf = gmmb_pdf(xx, nbayesS{d}(k).mu, nbayesS{d}(k).sigma, nbayesS{d}(k).weight );
    spdf = cumsum(pxomega_pdf);
    spdf = spdf / sum(pxomega_pdf);

    %filter from 0.2 to 0.8
    selected2 = intersect(find (cdf{d}{k}>0.2),find (cdf{d}{k}<0.8));
    selected1 = intersect(find (spdf>0.2),find (spdf<0.8));
    %selected = union(selected1,selected2);
    my{scale}.param{1} = polyfit(xx(selected1),spdf(selected1),1);
    my{scale}.param{2} = polyfit(xx(selected2),cdf{d}{k}(selected2),1);
    ks(scale) = abs(my{scale}.param{1}(1) - my{scale}.param{2}(1));

    %evaluate the goodness of fit
    if (dodisplay),
      figure(3);subplot(1,length(bound), scale);
      bayes2ks(xx,cdf, nbayesS, d, k,1);
      txt = sprintf('%1.3f',bound(scale)); title(txt);
    end;
  end;
  
  [tmp, index] = sort(ks);

  if (index(1)==1|index(1)==length(index)),
    if t==0,
      warning('no iteration done, your bound may be too narrow!');
      figure(1);
      hold off;
      plot(bound, ks,'bo-');
    end;
    done=1;break;
  end;

  if t>=max_round,
    done=1; break;
  end;
  
  diff = abs(bound(index(1)-1) - bound(index(1)+1));
  if (diff < precision),
    done=1;break;
  end;

  if (dodisplay),
    figure(1);
    subplot(1,2,1);
    hold off;
    plot(bound, ks,'bo-');
    hold on;
    selected = [index(1)-1:index(1)+1];
    plot(bound(selected),ks(selected),'r*');
    
    subplot(1,2,2);
    examined.ks=[examined.ks ks];
    examined.bound=[examined.bound bound];
    [sorted.bound, sorted.index] = sort(examined.bound);
    sorted.ks = examined.ks(sorted.index);
    hold off;
    semilogx(sorted.bound, sorted.ks,'bo-');
    pause;
  end;
  %prepare for next round
  bound = linspace(bound(index(1)-1), bound(index(1)+1), n_samples);
  t=t+1;
  fprintf(1,'.');
end;
scale_value = bound(index(1));
goodness = ks(index(1));