function plot_all_roc(epc_cost,leg,signs, cfg, list, fontsize,legon,plotdet)
%figure(1);
if nargin < 6 | length(fontsize)==0,
  fontsize = 18;
end;
if nargin < 7|length(legon)==0,
  legon = 1;
end;
if nargin < 8,
  plotdet = 0;
end;
set(gca,'Fontsize', fontsize);
maxv=0;
for i=list,
  if (i==list(1)), hold off; end;
  if (plotdet)
    plot(ppndf(cfg.res{i}.eva.far_apri), ppndf(cfg.res{i}.eva.frr_apri),signs{i},'Markersize',14);
  else
    plot(cfg.res{i}.eva.far_apri*100, cfg.res{i}.eva.frr_apri*100,signs{i},'Markersize',14);
  end;
  if (i==list(1)), hold on; end;
  maxv = max([cfg.res{i}.eva.far_apri, cfg.res{i}.eva.frr_apri,maxv]');
end;
axis tight; grid on;
ylabel('FRR(%)');xlabel('FAR(%)');
if legon,
  legend(leg{list});
end;
if (plotdet)
  Make_DET(maxv);
end;
