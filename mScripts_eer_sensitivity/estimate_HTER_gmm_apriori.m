function [HTER, FAR, FRR, FARn, FRRn] = estimate_HTER_gmm_apriori(bayesS, thrd)
% bayesS is the usual bayesS structure (see gmmb_bayes toolbox)

N = 1000; %number of samples
K = size(bayesS,2);
P = [bayesS.apriories];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%get some data from bayesS
for k=1:2,
  var{k} = [];
  tata = [bayesS(k).sigma(1,1,:)];
  tata = reshape(tata, length(tata),1);
  var{k} = tata';
end;
%data =[bayesS(1).mu - max(var{1}), bayesS(2).mu + max(var{2})];
data =[bayesS(1).mu, bayesS(2).mu];
xx = linspace(min(data),max(data), N)';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pxomega_pdf = zeros(N,K);
pxomega = zeros(N,K);
for k = 1:K
  pxomega_pdf(:,k) = gmmb_pdf(xx, bayesS(k).mu, bayesS(k).sigma, bayesS(k).weight );
end
spdf = cumsum(pxomega_pdf);
spdf(:,1) = spdf(:,1) / sum(pxomega_pdf(:,1));
spdf(:,2) = spdf(:,2) / sum(pxomega_pdf(:,2));
spdf(:,1) = 1 - spdf(:,1);

[tmp, index] = min(abs(xx-thrd));
FRR = spdf(index,1);
FAR = spdf(index,2);
HTER = (FAR + FRR)/2;

FARn = spdf(:,1);
FRRn = spdf(:,2);
