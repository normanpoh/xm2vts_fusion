function plot_all_epc(epc_cost,leg,signs, cfg, list,fontsize,legon)
if nargin < 6 | length(fontsize)==0,
  fontsize = 20;
end;
if nargin < 7,
  legon = 1;
end;

%figure(1);
set(gca,'Fontsize', fontsize);
for i=list,
  if (i==list(1)), hold off; end; 
  plot(epc_cost(:,1), cfg.res{i}.eva.hter_apri*100,signs{i}, 'MarkerSize', 14); 
  if (i==list(1)), hold on; end; 
end;
ylabel('HTER(%)');xlabel('\alpha');
%set(gca,'Fontsize', 10);
set(gca, 'Xtick', [0.1:0.1:0.9]);
axis_tmp = axis;
axis_tmp([1 2]) = [0.1 0.9];
axis(axis_tmp);
grid on
if legon,
  legend(leg{list});
end;