function [bdat, bline] = load_data(datatype);
if ~exist('datatype', 'var'),
  error('ver must be defined first');
end;
datatype = lower(datatype);

initialise;
switch datatype
  case 'orig'
    for p=1:2,
      [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}, data{p}.accessID{1}] = load_raw_scores_labels(datafiles{p}.dev);
      [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}, data{p}.accessID{2}] = load_raw_scores_labels(datafiles{p}.eva);
    end;
    for p=1:2,
      bline{p}.label = data{p}.label;
      bline{p}.accessID = data{p}.accessID;
      for b=1:size(dat.P{p}.labels,2),
        for d=1:2,for k=1:2,
            bdat{p,b}.dset{d,k} = data{p}.dset{d,k}(:,b);
          end;
        end;
      end;
    end;

  case 'znorm'
    %load normalised scores by Z-Norm and F-Norm
    load ../mScripts/main_baseline_addon_FNorm_rerun baseline
    for p=1:2,
      bline{p}.label = data{p}.label;
      bline{p}.accessID = data{p}.accessID;
      for b=1:size(dat.P{p}.labels,2)
        bdat{p,b}.dset = baseline{3}{p,b}.dset; %Z-Norm
      end;
    end;

  case 'fnorm'
    %load normalised scores by Z-Norm and F-Norm
    load ../mScripts/main_baseline_addon_FNorm_rerun baseline
    for p=1:2,
      bline{p}.label = data{p}.label;
      bline{p}.accessID = data{p}.accessID;
      for b=1:size(dat.P{p}.labels,2)
        bdat{p,b}.dset = baseline{6}{p,b}.dset; %Z-Norm
      end;
    end;

end;
clear data baseline;
