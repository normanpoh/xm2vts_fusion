for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    for t=1:2,
      tfar = out{t}{p,b}.eva.far_apri .* epc_cost(:,1)';
      tfrr = out{t}{p,b}.eva.frr_apri .* epc_cost(:,2)';
      out{t}{p,b}.eva.wer_apri = tfar' + tfrr';
    end;
  end;
end;

for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    hold off;
    plot(epc_cost(:,1)', out{1}{p,b}.eva.wer_apri);
    hold on;
    plot(epc_cost(:,1)', out{2}{p,b}.eva.wer_apri,'r')
    pause;
  end;
end;



%individual plot
pb_list=[];
t=0;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    t=t+1;
    txt = sprintf('Experiment %d (%s)\n',t,dat.P{p}.labels{b}); fprintf(1,txt);
    pb_list=[pb_list; p b];
    pb_name{t} = dat.P{p}.labels{b};
  end;
end;

isFH = [1 9];
isMLP = [1 4 5 9];
isMLPNotFH = setdiff(isMLP, isFH);
isFace = [1 2 3 4 5 9 10];
isFaceNotMLP = setdiff(isFace,isMLP);
isSpeech = setdiff(1:13, isFace);

[pb_name{isFaceNotMLP}]

for i=1:size(out,2),
  [mycfg{1}.res{i},pNI(1),pNC(1)] = epc_global_baseline_custom(out{i}, pb_list(isFaceNotMLP,:), NI, NC);
  [mycfg{2}.res{i},pNI(2),pNC(2)] = epc_global_baseline_custom(out{i}, pb_list(isMLPNotFH,:), NI, NC);
  [mycfg{3}.res{i},pNI(3),pNC(3)] = epc_global_baseline_custom(out{i}, pb_list(isFH,:), NI, NC);
  [mycfg{4}.res{i},pNI(4),pNC(4)] = epc_global_baseline_custom(out{i}, pb_list(isFace,:), NI, NC);
  [mycfg{5}.res{i},pNI(5),pNC(5)] = epc_global_baseline_custom(out{i}, pb_list(isSpeech,:), NI, NC);
  [mycfg{6}.res{i},pNI(6),pNC(6)] = epc_global_baseline_custom(out{i}, pb_list, NI, NC);
end;

b=[1 2]; tname='Fnorm-Zshift'; list=4:5;
close all

for i=list,    
  figure(i);subplot(1,2,1);
  plot_all_epc(epc_cost,leg,signs, mycfg{i}, b, 14,0);
  subplot(1,2,2);set(gca, 'Fontsize', 14);
  plot_all_roc(epc_cost,leg,signs, mycfg{i}, b, 14,1,1);
  Make_DET(0.4);
  fname = sprintf('../Pictures/%s_%d.eps',tname,i);  
  pause;
  %print('-depsc2', fname);
end;
