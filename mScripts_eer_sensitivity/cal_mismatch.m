function [mismatchf, out] = cal_mismatch(bayesS, dodisplay)

if (nargin<2),
  dodisplay=0;
end;

for d=1:2, %training, test sets
  for k=1:2,
    tata = [bayesS{d}(k).sigma(1,1,:)];
    tata = reshape(tata, length(tata),1);
    out.sigma{d}(k,:) = tata;
    out.mu{d}(k,:) = bayesS{d}(k).mu;
  end;
end;

if(dodisplay==1),
    
  tmpX = [out.mu{1}(1,:), out.mu{1}(2,:)];
  xx = linspace(min(tmpX),max(tmpX),100);

  subplot(1,2,1)
  signs = {'r+', 'bo'};
  signs_curve = {'r--','b--','g-'};
  for k=1:2,
    datX = [out.mu{1}(k,:)];    datY = [out.mu{2}(k,:)];
    out.coef_mu{k} = polyfit(datX,datY,1);    y = polyval(out.coef_mu{k},xx);
    if k==1, hold off; end;
    plot(out.mu{1}(k,:),out.mu{2}(k,:),signs{k});
    if k==1, hold on; end;
    plot(xx,y, signs_curve{k});
  end;
  datX = [out.mu{1}(1,:),out.mu{1}(2,:)];    datY = [out.mu{2}(1,:),out.mu{2}(2,:)];
  out.coef_mu{3} = polyfit(datX,datY,1);    y = polyval(out.coef_mu{3},xx);
  plot(xx,y,signs_curve{3},'linewidth',2);     grid on;
  xlabel('\mu^k_j|train');    ylabel('\mu^k_j|test');
  legend('impostor', 'impostor fit', 'client', 'client fit', 'joint fit');

  %curve fitting individually for sigma
  %figure(4);
  subplot(1,2,2)
  tmpX = [out.sigma{1}(1,:), out.sigma{1}(2,:)];
  xx = linspace(min(tmpX),max(tmpX),100);
  k=1; %for impostor only
  out.coef_sigma{1} = polyfit(out.sigma{1}(k,:), out.sigma{2}(k,:),1);

  %second method: bias is zero
  out.coef_sigma{2} = [mean(out.sigma{2}(k,:)  ./ out.sigma{1}(k,:)), 0];
  %third non-adapted method: bias is zero
  out.coef_sigma{3} = [out.coef_mu{3}(1), 0];
  lw = [1 1 2];
  for i=1:3,
    if i==1, hold off; end;
    y = polyval(out.coef_sigma{i},xx);
    plot(xx,y,signs_curve{i},'linewidth',lw(i));
    if i==1, hold on; end;
  end;
  plot(out.sigma{1}(k,:),out.sigma{2}(k,:),'r+');  grid on; hold on;
  k=2;
  plot(out.sigma{1}(k,:),out.sigma{2}(k,:),'bo');  grid on; hold on;

  xlabel('\sigma^I_j|train');    ylabel('\sigma^I_j|test');
  legend('Impostor fit', 'zero bias', 'none adapted');  
  %tmpX=[out.mu{1}' out.sigma{1}(2,:)'];
  %plot3(tmpX(:,1),tmpX(:,2),tmpX(:,3),'+');
  %xlabel('\mu^I_j');ylabel('\mu^C_j');zlabel('\sigma^I_j');
  %view(2)
  %plot((tmpX(:,1),tmpX(:,2),'+');

  mismatchf = out.coef_mu{3};
else

  datX = [out.mu{1}(1,:),out.mu{1}(2,:)];    
  datY = [out.mu{2}(1,:),out.mu{2}(2,:)];
  mismatchf = polyfit(datX,datY,1);
end;

