function [com] = rndchoose_client_list(expe, client_list, model_ID)

%get the model label
if (nargin < 3),
%use the original order
  model_ID = unique(expe.label{1,1});
end;

%go through each id
for d=1:2,for k=1:2,
    out{d,k} = [];%zeros(,length(chosen));
    label{d,k} = [];%zeros(,length(chosen));
  end;
end;

%perform client-dependent processing
%fprintf(1, 'Processed ID:\n');
for id=1:length(client_list),
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      selected = find(expe.label{d,k} == model_ID(client_list(id)));
      if (size(selected,1)==0),
        error([ 'size is zero for id ', num2str(id)]);
      end;
      tmp{d,k} = expe.dset{d,k}(selected,1);
    end;
  end;

  for d=1:2,for k=1:2,
      out{d,k} = [out{d,k};  tmp{d,k}];
      label{d,k} = [label{d,k}; model_ID(client_list(id)) * ones(size(tmp{d,k},1),1)];
    end;
  end;

  %fprintf(1, '.%d', id);
end;
com.dset = out;
com.label = label;
