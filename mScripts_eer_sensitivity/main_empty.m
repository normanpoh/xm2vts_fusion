%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_draws = 500;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
datatype = {'orig','ZNorm','FNorm'}
method = {'gmm', 'direct'}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dt=1; %datatype
m=2; %method
ver=1; %version
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
initialise;

[bdat, blabel] = load_data(datatype{dt});

txt=sprintf('make_rnd%02d.mat',ver);
load(txt, 'rnd_list', 'n_draws', 'list');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN expe here
%ds=1; %50 client config
n_draws=10;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ds=1;p=1;b=6;n=1;d=1;
signs={'r--','b-','g-'};

tic;
p=1;b=2;
for p=1:2,
  fprintf(1,'Protocol %d\n', p);

  for b=1:size(dat.P{p}.labels,2)
    
    expe.dset = bdat{p,b}.dset;
    expe.label = blabel{p}.label;
    expe.accessID = blabel{p}.accessID;
    
    %loop_adapt_bruteforce
    loop_cal_mismatch
  end;
end;