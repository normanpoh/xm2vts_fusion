function [bayesS] = cal_clientd_stat_accessID(expe, chosen)
% this operation operates on one dimension only!
% check check cal_clientd_stat and fusion_clientd_check for similar coding
if (nargin<2|length(chosen)==0),
  chosen = 1;
end;

%get the model label
model_ID = unique(expe.label{1,1});
access_ID = unique(expe.accessID{1}); %impostor true labels

%perform client-dependent normalisation
for id=1:size(model_ID,1),
  %get the data set associated to the ID
  for d=1:2,
    %impostor processing
    k=1;
    selected_C = find(expe.label{d,k} == model_ID(id)); %choose all impostor associated to this client model
    for idI = 1:length(access_ID),
      
      selected_I = find(expe.accessID{d} == access_ID(idI));
      selected = intersect(selected_C, selected_I);
      %tata(idI) = length(selected);
      %end;
      
      index{d}{k}.index{id, idI} = selected;
    
      %calculate user-specific param in train and test
      tmp = expe.dset{d,k}(index{d}{k}.index{id, idI},chosen);
      myindex = (id-1) * length(access_ID) + idI;
      bayesS{d}(1).mu(myindex) = std(tmp);
      bayesS{d}(1).sigma(1,1,myindex) = mean(tmp);
    end;
    
    %client processing
    k=2;
    index{d}{k}.index{id, 1} = find(expe.label{d,k} == model_ID(id));

    %calculate user-specific param in train and test
    tmp = expe.dset{d,k}(index{d}{k}.index{id, 1},chosen);
    myindex = id;
    bayesS{d}(1).mu(myindex) = std(tmp);
    bayesS{d}(1).sigma(1,1,myindex) = mean(tmp);
    
  end;
end;

%sum of the count
for d=1:2,for k=1:2,
    sumcount(d,k) = sum(count{d,k});
  end;
end;

for d=1:2,
  for k=1:2,
    bayesS{d}(k).weight = count{d,k}/sumcount(d,k);
    %equal priors for each class
    bayesS{d}(k).apriories = 0.5;
  end;
end;
