%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% pre-requisite:
% 1: expe.dset{d,k} is single dimension      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%calculate the empirical FAR and FRR at location xx

[bayesS] = cal_clientd_stat(expe);

[mismatchf] = cal_mismatch(bayesS);
%figure(1);
%[mismatchf, out] = cal_mismatch(bayesS, 1);
%pause;
%txt = sprintf('../Pictures/main_eer_sensitivity_stats_emp_%d_%02d.eps',p,b);
%print('-depsc2', txt);

%now map the scores according to this mismatch function
%figure(2)

nexpe = expe;
for k=1:2,
  nexpe.dset{1,k} = mismatchf(1) * expe.dset{1,k} + mismatchf(2);
end;
if (1==0),

  wer(expe.dset{1,1},expe.dset{1,2},[],2,[],1);
  wer(expe.dset{2,1},expe.dset{2,2},[],2,[],2);
  wer(nexpe.dset{2,1},nexpe.dset{2,2},[],2,[],3);
  legend('dev','eva');
end;

%evaluate by epc

[out{1}{p,b}.dev, out{1}{p,b}.eva, epc_cost]  = epc(expe.dset{1,1}, expe.dset{1,2}, expe.dset{2,1}, expe.dset{2,2}, 21,[0, 1]);
[out{2}{p,b}.dev, out{2}{p,b}.eva, epc_cost]  = epc(nexpe.dset{1,1}, nexpe.dset{1,2}, nexpe.dset{2,1}, nexpe.dset{2,2}, 21,[0, 1]);

signs = {'ro--','bx-'};

if (display==1),
  selected = 2:20;
  hold off;
  plot(epc_cost(selected,1)', out{1}.eva.hter_apri(selected)*100,signs{1});
  hold on;
  plot(epc_cost(selected,1)', out{2}.eva.hter_apri(selected)*100,signs{2});
  legend('orig','trainsformed');
  xlabel('\alpha');
  ylabel('HTER(%)');
  title(dat.P{p}.labels{b});
  %txt = sprintf('../Pictures/loop_cal_mismatch_I_%d_%02d.eps',p,b);
  %txt = sprintf('../Pictures/loop_cal_mismatch_%d_%02d.eps',p,b);
  %print('-depsc2', txt);
end;
if (1==0),
  for k=1:2,
    nexpe.label{2,k} = nexpe.label{1,k};
  end;
  [nbayesS] = cal_clientd_stat(nexpe);


  [tmp, tmp, tmp, tmp, xx, tmp, tmp] = estimate_EER_gmm(expe.dset{d,1}, expe.dset{d,2}, [], 0,bayesS{d});
  for d=1:2,
    [tmp, tmp, tmp, tmp, xxx, cdf{d}{1}, cdf{d}{2}] = estimate_EER_gmm(expe.dset{d,1}, expe.dset{d,2}, [], 0,bayesS{d});
    for k=1:2,
      cdf{d}{k} = interp1(xxx,cdf{d}{k},xx, 'linear');
    end;
    [tmp, tmp, tmp, tmp, xxx, ncdf{d}{1}, ncdf{d}{2}] = estimate_EER_gmm(nexpe.dset{d,1}, nexpe.dset{d,2}, [], 0,bayesS{d});
    for k=1:2,
      ncdf{d}{k} = interp1(xxx,ncdf{d}{k},xx, 'linear');
    end;
    cdf{d}{1} = 1 - cdf{d}{1};
    ncdf{d}{1} = 1 - ncdf{d}{1};
  end;

  %correct
  for d=1:2,
    for k=1:2,
      selected = find(isnan(cdf{d}{k})==1);   cdf{d}{k}(selected) = 0;
      selected = find(isnan(ncdf{d}{k})==1);  ncdf{d}{k}(selected) = 0;
    end;
  end;

  for d=1:2,
    for k=1:2,
      sum(isnan(cdf{d}{k}))
    end;
  end;

  d=2;
  figure(2);
  for k=1:2,
    hold off;
    plot(ppndf(1-cdf{d}{1}),ppndf(cdf{d}{2}),'r'); hold on;
    plot(ppndf(1-ncdf{d}{1}),ppndf(ncdf{d}{2}),'g');
    plot(ppndf(1-cdf{1}{1}),ppndf(ncdf{1}{2}),'b');
  end;
  Make_DET;

    for k=1:2,
    subplot(2,1,k); hold off;
    plot(xx, cdf{2}{k},'r');
    hold on;
    plot(xx, ncdf{2}{k},'g');
    plot(xx, cdf{1}{k},'b');
    ks= max(abs(cdf{2}{k} - ncdf{2}{k}));
    ks2= max(abs(cdf{2}{k} - cdf{1}{k}));
    tata(k) = ks-ks2;
  end;
  legend('test','transformed','train');
end;



%evaluate aposteriori HTER
