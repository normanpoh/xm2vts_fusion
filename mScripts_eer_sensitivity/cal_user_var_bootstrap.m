function out = cal_user_var_bootstrap(expe, rnd_list_ds, list,n_draws)

for n=1:length(list), 
  fprintf(1,'.%d',n);
  %fprintf('.');
  for dd=1:n_draws,

    rexpe = rndchoose_client_list(expe, rnd_list_ds{n}(dd,:));
    [out(dd,n).dev, out(dd,n).eva, epc_cost]  = epc(rexpe.dset{1,1}, rexpe.dset{1,2}, rexpe.dset{2,1}, rexpe.dset{2,2}, 21,[0, 1]);

  end;
end;
fprintf(1,'\n');