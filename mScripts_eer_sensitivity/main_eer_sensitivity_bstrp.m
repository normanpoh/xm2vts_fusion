%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_draws = 500;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
datatype = {'orig','ZNorm','FNorm'}
method = {'gmm', 'direct'}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dt=1; %datatype
m=2; %method
ver=1; %version
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[bdat, blabel] = load_data(datatype{dt});

txt=sprintf('make_rnd%02d.mat',ver);
load(txt, 'rnd_list', 'n_draws', 'list');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN expe here
%ds=1; %50 client config
n_draws=100;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ds=1;p=1;b=6;n=1;d=1;

signs={'ro--','b+--','m*--','gd--'};
signs={signs{:},'ro:','b+:','m*:','gd:'};
signs={signs{:},'ro-','b+-','m*-','gd-'};
  
leg = {'bstrp,small', 'us-gmm,small', 'us-gmm-adapt,small','bstrp,big'};
leg = {leg{:},leg{:}}
epc_cost = linspace(0,1,21)';


time_taken = zeros(1,4);
p=1;b=2;
for p=1:2,
    fprintf(1,'Protocol %d\n', p);
  
    for b=2:size(dat.P{p}.labels,2)
      fprintf(1,'Experiment number %d %d\n', p, b);

      expe.dset = bdat{p,b}.dset;
      expe.label = blabel{p}.label;

      %method 1: empirical, small dataset, datatype is original
      m=1;ds=1;dt=1;tic;
      res{p,b}.out{ds}{dt}{m} = cal_user_var_bootstrap(expe, rnd_list{ds}, list,n_draws);
      time_taken(m) = time_taken(m) + toc;
      
      %method 2: theoretical, small dataset, datatype is original
      m=2;ds=1;dt=1;tic;
      [bayesS] = cal_clientd_stat(expe);
      res{p,b}.out{ds}{dt}{m} = cal_user_var_bayesS(bayesS, rnd_list{ds}, list,n_draws);
      time_taken(m) = time_taken(m) + toc;
      
      %method 3: theoretical, small dataset, datatype is original
      m=3;ds=1;dt=1;tic;
      gamma=[1 1;1 1];
      for d=1:2,
        [tmp, tmp, tmp, tmp, xx, cdf{d}{1}, cdf{d}{2}] = estimate_EER_gmm(expe.dset{d,1}, expe.dset{d,2}, [], 0,bayesS{d});
        cdf{d}{1} = 1 - cdf{d}{1};
      end;
      [scale_,  shift_]= cal_transfer(bayesS, xx,cdf);
      nbayesS = update_bayes(bayesS, scale_, shift_,[1:2], [1:2], gamma);
      res{p,b}.out{ds}{dt}{m} = cal_user_var_bayesS(nbayesS, rnd_list{ds}, list,n_draws);
      
      time_taken(m) = time_taken(m) + toc;
      
      %method 4: empirical, big dataset, datatype is original
      m=4;ds=2;dt=1;tic;
      res{p,b}.out{ds}{dt}{m} = cal_user_var_bootstrap(expe, rnd_list{ds}, list,n_draws);
      time_taken(m) = time_taken(m) + toc;
      
      save main_eer_sensitivity_bstrp res;
    end;
end;

save main_eer_sensitivity_bstrp res time_taken;
load main_eer_sensitivity_bstrp res time_taken;

clear myout;
for p=1:2,
  for b=2:size(dat.P{p}.labels,2)
    for m=1:4,
      if m<4,         ds=1;       else         ds=2;         end;
      for n=1:length(list),
        tmp{1} = [];tmp{2} = [];
        for dd=1:n_draws,
          tmp{1} = [tmp{1}; res{p,b}.out{ds}{dt}{m}(dd,n).eva.far_apri];
          tmp{2} = [tmp{2}; res{p,b}.out{ds}{dt}{m}(dd,n).eva.frr_apri];
          tmp{3} = [tmp{2}; res{p,b}.out{ds}{dt}{m}(dd,n).eva.hter_apri];
        end;
        myout{p,b}{m}{n}{1} = quantil(tmp{1}, [2.5 50 97.5]);
        myout{p,b}{m}{n}{2} = quantil(tmp{2}, [2.5 50 97.5]);
        myout{p,b}{m}{n}{3} = quantil(tmp{3}, [2.5 50 97.5]);
      end;
    end;
    
  end;
end;

clear tmp;
chosen_epc=2:20;
p=1;b=2;
for p=1:2,
  for b=2:size(dat.P{p}.labels,2),
    
    for n=1:length(list),
      subplot(2,3,n);
      seq=[];
      for m=[1 2 4],
        seq=[seq m];seq=[seq m];seq=[seq m];
        
        if m==1, hold off; end;
        %plot(ppndf(myout{p,b}{m}{n}{1}(1,:)),ppndf(myout{p,b}{m}{n}{2}(1,:)),signs{m});
        %plot(myout{p,b}{m}{n}{1}(1,:),myout{p,b}{m}{n}{2}(1,:)),signs{m};
        %tmp = myout{p,b}{m}{n}{1}(1,:)
      
        plot(epc_cost(chosen_epc)',myout{p,b}{m}{n}{3}(1,(chosen_epc)),signs{m});
        if m==1, hold on; end;
        plot(epc_cost(chosen_epc)',myout{p,b}{m}{n}{3}(3,(chosen_epc)),signs{m+4});
        plot(epc_cost(chosen_epc)',myout{p,b}{m}{n}{3}(2,(chosen_epc)),signs{m+8},'linewidth',2);
      end;
      
      %Make_DET();

    end;
    legend(leg{seq});
    title(dat.P{p}.labels{b});
    pause;
    
  end;
end;


%study FAR only
for p=1:2,
  for b=2:size(dat.P{p}.labels,2),
    
    for n=1:length(list),
      subplot(2,3,n);
      seq=[];
      for m=[1 2 4],
        seq=[seq m];seq=[seq m];seq=[seq m];
        
        if m==1, hold off; end;
        plot(epc_cost(chosen_epc)',myout{p,b}{m}{n}{1}(1,(chosen_epc)),signs{m});
        if m==1, hold on; end;
        plot(epc_cost(chosen_epc)',myout{p,b}{m}{n}{1}(3,(chosen_epc)),signs{m+4});
        plot(epc_cost(chosen_epc)',myout{p,b}{m}{n}{1}(2,(chosen_epc)),signs{m+8},'linewidth',2);
      end;
      
    end;
    legend(leg{seq});
    title(dat.P{p}.labels{b});
    pause;
    
  end;
end;

%DET plot
chosen_epc = 1:21;
chosen_method = [1 4];
for p=1:2,
  for b=1:size(dat.P{p}.labels,2),
    
    for n=1:length(list),
      subplot(2,3,n);
      
      t=1;for m=chosen_method,
        if m==1, hold off; end;
        plot(ppndf(myout{p,b}{m}{n}{1}(t,chosen_epc)),ppndf(myout{p,b}{m}{n}{2}(t,chosen_epc)),signs{m});
        if m==1, hold on; end;
      end;
      t=3;for m=chosen_method,
        plot(ppndf(myout{p,b}{m}{n}{1}(t,chosen_epc)),ppndf(myout{p,b}{m}{n}{2}(t,chosen_epc)),signs{m+4});
      end;
      t=2;for m=chosen_method,
        plot(ppndf(myout{p,b}{m}{n}{1}(t,chosen_epc)),ppndf(myout{p,b}{m}{n}{2}(t,chosen_epc)),signs{m+8},'linewidth',2);
      end;
      Make_DET();
    end;
    legend(leg{chosen_method});
    
    fprintf(1,'%s\n',dat.P{p}.labels{b});
    pause;

    txt=sprintf('../Pictures/main_eer_sensitivity_bstrp_DET2_%d_%02d.eps',p,b);
    print('-depsc2 ',txt);
  end;
end;

%debugging only
      ds=1;
      for m=1:3,
        if m==1, hold off; end;
        plot(epc_cost, out{ds}{dt}{m}.res{p,b}(1,1).eva.hter_apri,signs{m}); %axis([0.5 0.95, 0 0.05]);
        if m==1, hold on; end;
      end;
      ds=2; m= 4;
      plot(epc_cost, out{ds}{dt}{m}.res{p,b}(1,1).eva.hter_apri,signs{m}); %axis([0.5 0.95, 0 0.05]);

      legend('bstrp,small', 'us-gmm,small', 'us-gmm-adapt,small','bstrp,big');
      
      for m=1:4,
        if m==1, hold off; end;
        plot(epc_cost, out{ds,dt}{m}{p,b}(1,1).dev.hter,signs{m}); axis([0.5 0.95, 0 0.05]);
        if m==1, hold on; end;
      end;
      legend('bstrp,small', 'us-gmm,small');
      
      
      for n=1:length(list),
        for dd=1:n_draws,
          if n==1 | dd==1, hold off; end;
          plot(out{ds,dt}{1}{p,b}(dd,n).dev.hter, out{ds,dt}{2}{p,b}(dd,n).dev.hter,'+');
          if n==1 | dd==1, hold on; end;
          plot(out{ds,dt}{1}{p,b}(dd,n).dev.hter, out{ds,dt}{3}{p,b}(dd,n).dev.hter,'ro');
        end;
      end;
