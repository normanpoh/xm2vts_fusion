function [nbayesS, transfer] = tune_clientd_stat(expe, bayesS, dodisplay)
if nargin<3,
  dodisplay=0;
end;

%calculate the empirical FAR and FRR at location xx
[tmp, tmp, tmp, tmp, xx, cdf{1}, cdf{2}] = estimate_EER_gmm(expe.dset{1,1}, expe.dset{1,2}, [], 0,bayesS{1});
cdf{1} = 1 - cdf{1};

Nc = length(bayesS{1}(1).weight);%the number of component
%work on dev set only

scale_list = tan(linspace(pi*10/100,pi/2 - pi/100,40)); %0 to 90�

nbayesS = bayesS;
%for d=1:2, %calculate user-specific param in train and test
d=1;
for k=1:2,
  for scale = 1:length(scale_list),
    for id=1:Nc,
      nbayesS{d}(k).sigma(1,1,id) = bayesS{d}(k).sigma(1,1,id) * scale_list(scale);
    end;

    %evaluate the goodness of fit
    pxomega_pdf = gmmb_pdf(xx, nbayesS{d}(k).mu, nbayesS{d}(k).sigma, nbayesS{d}(k).weight );
    spdf = cumsum(pxomega_pdf);
    spdf = spdf / sum(pxomega_pdf);

    %filter from 0.2 to 0.8
    selected2 = intersect(find (cdf{k}>0.2),find (cdf{k}<0.8));
    selected1 = intersect(find (spdf>0.2),find (spdf<0.8));
    %selected = union(selected1,selected2);
    my{scale}.param{1} = polyfit(xx(selected1),spdf(selected1),1);
    my{scale}.param{2} = polyfit(xx(selected2),cdf{k}(selected2),1);
    err(scale) = abs(my{scale}.param{1}(1) - my{scale}.param{2}(1));

    if (dodisplay==1),
      figure(k);
      subplot(6,7,scale);
      hold off;
      plot(xx, spdf,'g'); hold on;
      plot(xx, cdf{k},'b');
      %plot(xx,err*10/3);

      x=linspace(min(xx(selected1)),max(xx(selected1)),4);
      y = polyval(my{scale}.param{1},x);
      plot(x,y,'go--');

      x=linspace(min(xx(selected2)),max(xx(selected2)),4);
      y = polyval(my{scale}.param{2},x);
      plot(x,y,'bo--');

      txt = sprintf('%1.3f, %1.3f',scale_list(scale),err(scale));
      title(txt);
      axis tight;
    end;

  end;

  [tmp, index]=min(err);
  transfer{k}(1) = scale_list(index);

  if (dodisplay==1),
    subplot(6,7,scale+1);
    plot(scale_list, err,'bo-');

    subplot(6,7,index);
    plot(shift,0,'ro');
    plot(shift2,0,'rd');
    %x=linspace(min(xx(selected2)),max(xx(selected2)),6);
    %y = polyval([my{scale}.param{2}(1) my{index}.param{2}(2)],x);
    %plot(x,y,'k*:');
    %axis tight;
  elseif (dodisplay==2),
    figure(k);
    plot(scale_list, err,'bo-');
    %y = polyval(my{scale}.param{2},x);
    shift =  - my{index}.param{1}(2)/my{index}.param{1}(1);
    shift2 =  - my{index}.param{2}(2)/my{index}.param{2}(1);

    end;

end

%update the scaling factor here
tbayesS = bayesS;
for id=1:Nc,
  tbayesS{d}(k).sigma(1,1,id) = bayesS{d}(k).sigma(1,1,id) * transfer{k}(1);
end;

clear pxomega_pdf spdf err;

shift_list = linspace(-2,2,40);
%find the shift
for k=1:2,
  for shift=1:length(shift_list),
    tbayesS{d}(k).mu = bayesS{d}(k).mu +  shift_list(shift);
    %evaluate the goodness of fit
    pxomega_pdf = gmmb_pdf(xx, tbayesS{d}(k).mu, tbayesS{d}(k).sigma, tbayesS{d}(k).weight );
    spdf = cumsum(pxomega_pdf);
    spdf = spdf / sum(pxomega_pdf);

    %filter from 0.2 to 0.8
    %selected2 = intersect(find (cdf{k}>0.2),find (cdf{k}<0.8));
    %selected1 = intersect(find (spdf>0.2),find (spdf<0.8));
    %selected = union(selected1,selected2);
    %err(shift) = sum(abs(spdf(selected)-pxomega_pdf(selected)));
    err(shift) = max(abs(spdf-cdf{k}));

    if (dodisplay==1),

      figure(k+2);
      subplot(6,7,shift);
      hold off;
      plot(xx, spdf,'g'); hold on;
      plot(xx, cdf{k},'b');
      %plot(xx,err*10/3);

      txt = sprintf('%1.3f, %1.3f',shift_list(shift),err(shift));
      title(txt);
      axis tight;

    end;
  end;

  [tmp, index]=min(err);
  transfer{k}(2) = shift_list(index);

  if (dodisplay==1),
    subplot(6,7,shift+1); hold off;
    plot(shift_list, err,'bo-');
  elseif (dodisplay==2),
    figure(k+2);
    plot(shift_list, err,'bo-');
  end;
end;

%update the final bayesS
nbayesS = bayesS;
for d=1:2,
  for k=1:2,
    for id=1:Nc,
      nbayesS{d}(k).sigma(1,1,id) = bayesS{d}(k).sigma(1,1,id) * transfer{k}(1);
    end;
    nbayesS{d}(k).mu = bayesS{d}(k).mu +  transfer{k}(2);
  end;
end;

%evaluate the goodness of fit
for d=1:2,
  figure(d+4),
  [tmp, tmp, tmp, tmp, xx, cdf{1}, cdf{2}] = estimate_EER_gmm(expe.dset{d,1}, expe.dset{d,2}, [], 1,nbayesS{d});
  cdf{1} = 1 - cdf{1};
 
   hold off;
   plot(xx, cdf{1},'r');
   hold on;
   plot(xx, cdf{2},'b');
 
   for k=1:2,
     pxomega_pdf(:,k) = gmmb_pdf(xx, nbayesS{d}(k).mu, nbayesS{d}(k).sigma, nbayesS{d}(k).weight );
   end;
   spdf = cumsum(pxomega_pdf);
   spdf = spdf ./ repmat(sum(pxomega_pdf),length(spdf),1);
 
   plot(xx, spdf(:,1),'r--');
   plot(xx, spdf(:,2),'b--');
 end;