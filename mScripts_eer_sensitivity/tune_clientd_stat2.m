function [transfer,goodness, gbayesS] = tune_clientd_stat2(xx, cdf, bayesS, gamma, n_samples, dodisplay)
%tuning by brute force
if nargin<5,
  n_samples = 10;
end;
if nargin<6,
  dodisplay=0;
end;

%get global information
d=1;k=1;
Nc = length(bayesS{d}(k).weight);%the number of component
for d=1:2,
  for k=1:2,
    tmp=[];
    for id=1:Nc,
      tmp = [tmp bayesS{d}(k).sigma(1,1,id)];
    end;
    %gbayesS has one-component ONLY!
    gbayesS{d}(k).sigma(1,1,1) = tmp * bayesS{d}(k).weight';
    gbayesS{d}(k).mu  = bayesS{d}(k).mu * bayesS{d}(k).weight';
  end;
end;

%parameter to search from
clear goodness;
scale_list = tan(linspace(pi*10/100,pi/2 - pi/100,n_samples));
shift_list = linspace(-2,2,n_samples);
for k = [1:2],
  for d = [1:2],
    fprintf('.');
    for scale_i = 1:length(scale_list),
      for shift_i = 1:length(shift_list),
        shift(k) = shift_list(shift_i);
        scale(k) = scale_list(scale_i);
        shift_(d,k)=shift(k);
        scale_(d,k)=scale(k);
        nbayesS = update_bayes(bayesS, scale_, shift_, d, k, gamma, gbayesS);
        goodness(scale_i, shift_i) = bayes2ks(xx,cdf, nbayesS, d, k);
      end;
    end;

    %find the optimal transfer function
    [tmp, row] = min(goodness);
    [tmp, col] = min(tmp);
    %goodness(row(col), col) equals min(min(goodness))
    transfer{d,k} = [scale_list(row(col)) shift_list(col)];

    if (dodisplay),
      subplot(2,2,(d-1)*2 + k);hold off;
      spectro(goodness);%set(gca,'fontsize',16);
      xlabel('scale index');ylabel('shift index');zlabel('KS-statistics');
      view(2);
      txt = sprintf('d=%d k=%d',d, k);
      title(txt);
    end;
    
  end;
end;
