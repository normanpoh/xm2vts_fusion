%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_draws = 500;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
datatype = {'orig','ZNorm','FNorm'}
method = {'gmm', 'direct'}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dt=1; %datatype
m=2; %method
ver=1; %version
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[bdat, blabel] = load_data(datatype{dt});

txt=sprintf('make_rnd%02d.mat',ver);
load(txt, 'rnd_list', 'n_draws', 'list');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN expe here
%ds=1; %50 client config
n_draws=10;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ds=1;p=1;b=6;n=1;d=1;
signs={'r--','b-','g-'};

tic;
for ds=1:2, %small, big
  fprintf(1,'For dset %d\n', ds);

  for p=1:2,
    fprintf(1,'Protocol %d\n', p);
  
    for b=1:size(dat.P{p}.labels,2)
      expe.dset = bdat{p,b}.dset;
      expe.label = blabel{p}.label;

      switch method{m}
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case 'direct'
          for n=1:length(list), fprintf(1,'.%d',n);
            for dd=1:n_draws,

              rexpe = rndchoose_client_list(expe, rnd_list{ds}{n}(dd,:));
              [out{ds,dt}{m}{p,b}.eer(dd,n),thrd] = wer(rexpe.dset{1,1}, rexpe.dset{1,2});
              [com.hter_apri, com.far_apri, com.frr_apri] = hter_apriori(rexpe.dset{2,1}(:,1), rexpe.dset{2,2}(:,1),thrd);
              out{ds,dt}{m}{p,b}.hter(dd,n) = com.hter_apri;
            
            end;
          end;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case 'gmm'
          %derive some param
          [nbayesS] = cal_clientd_stat(expe);
          stat{p,b} = cal_user_gen_stat(nbayesS);
          
          for n=1:length(list),  fprintf(1,'.%d',n);
            for dd=1:n_draws,
              
              clear tbayesS;
              for d=1:2,for k=1:2,
                  for j=1:length(rnd_list{ds}{n}(dd,:)),
                    tbayesS{d}(k).sigma(1,1,j) = stat{p,b}.nbayesS{d}(k).sigma(1,1,rnd_list{ds}{n}(dd,j));
                  end;
                  tbayesS{d}(k).mu = stat{p,b}.nbayesS{d}(k).mu(rnd_list{ds}{n}(dd,:));
                  tbayesS{d}(k).weight = ones(1,list(n))/list(n);
                  tbayesS{d}(k).apriories = 1;
                end;
              end;
              [out{ds,dt}{m}{p,b}.eer(dd,n), thrd] = estimate_EER_gmm([],[], [], 0, tbayesS{1});

              [out{ds,dt}{m}{p,b}.hter(dd,n), out{ds,dt}{m}{p,b}.far(dd,n), out{ds,dt}{m}{p,b}.frr(dd,n)] = estimate_HTER_gmm_apriori(tbayesS{2}, thrd);
            end;
              
          end;

      end; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end case
      estimate_EER_gmm(rexpe.dset{1,1}, rexpe.dset{1,2}, [], 1, tbayesS{1});

      
      %user-indep fitting
      [value, thrd, tmp, err, xx, FARn, FRRn] = estimate_EER_gmm_cp(rexpe.dset{1,1}, rexpe.dset{1,2}, [], 1);
      out{ds,dt}{1}{p,b}.eer - out{ds,dt}{2}{p,b}.eer
    end;
  end;
end;
time_taken = toc;
save main_eer_sensitivity_replace_direct out time_taken

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_super_draws = 1;
n_draws = 100;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;
for ds=1:2, %150 client config
  fprintf(1,'For dset %d\n', ds);
  for p=1:2,
    fprintf(1,'Protocol %d\n', p);

    for b=1:size(dat.P{p}.labels,2)
      expe.dset = bdat{1}{p,b}.dset; %use Z-Norm
      expe.label = bline{p}.label;

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%% debugging starts here

      
      
      
      
      
      [nbayesS{p,b}, paramj, param] = cal_clientd_stat(expe);
      
      for dd=1:n_super_draws,
        fprintf(1,'�');

        for d=1:n_draws,
          fprintf(1,'.%d',d);

          for i=1:length(list{1}), %force to be the list ds1
            %for i=1:length(list{ds}), %sample the no.-of-user interval

            selected_id = rnd_list{ds}{d}{i};
            for d=1:2,for k=1:2,
                for j=1:length(selected_id),
                  tbayesS{2}{d}(k).sigma(1,1,j) = nbayesS{p,b}{1}(k).sigma(1,1,selected_id(j));
                end;
                tbayesS{2}{d}(k).mu = nbayesS{p,b}{1}(k).mu(selected_id);
                tbayesS{2}{d}(k).weight = ones(1,n_user_list(n))/n_user_list(n);
                tbayesS{2}{d}(k).apriories = 1;
              end;
            end;

            rexpe = rndchoose_client_list(expe, [], , new_expe_id{ds}{dd}{p}.selected(d,:));

            [tmp_,thrd] = wer(rexpe.dset{1,1}(:,1), rexpe.dset{1,2}(:,1));
            [com.hter_apri, com.far_apri, com.frr_apri] = hter_apriori(rexpe.dset{2,1}(:,1), rexpe.dset{2,2}(:,1),thrd);
            out{ds}{dd}{p}{i}(d,b) = com.hter_apri;
          end;
        end;

      end;
    end;
  end;
end;
time_taken = toc;
save main_eer_sensitivity_normalised_replace out time_taken

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%STOP HERE











