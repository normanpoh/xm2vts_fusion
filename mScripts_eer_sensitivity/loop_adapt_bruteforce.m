close all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% pre-requisite:
% 1: expe.dset{d,k} is single dimension
% 2: gamma is set
gamma=[1 1;1 1];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%calculate the empirical FAR and FRR at location xx

[bayesS] = cal_clientd_stat(expe);
for d=1:2,
  [tmp, tmp, tmp, tmp, xx, cdf{d}{1}, cdf{d}{2}] = estimate_EER_gmm(expe.dset{d,1}, expe.dset{d,2}, [], 0,bayesS{d});
  cdf{d}{1} = 1 - cdf{d}{1};
end;

[transfer,goodness, gbayesS] = tune_clientd_stat2(xx, cdf, bayesS, gamma, 10, 1);

%old optimization function; obsoltete
%[nbayesS, tran] = tune_clientd_stat(expe, db_bayesS, 1);
for d=1:2,for k=1:2,
    scale(d,k) = transfer{d,k}(1);
    shift(d,k) = transfer{d,k}(2);
  end;
end;

nbayesS = update_bayes(bayesS, scale, shift, [1:2], [1:2], gamma, gbayesS);
for d=1:2,for k=1:2,
    figure(2);
    subplot(2,2,(d-1)*2 + k);hold off;
    [ks(d,k), spdf{d}{k}] = bayes2ks(xx,cdf, nbayesS, d, k, 1);
    txt = sprintf('d=%d k=%d',d, k);      title(txt);

    figure(3);
    subplot(2,2,(d-1)*2 + k);hold off;
    [ks2(d,k), spdf2{d}{k}] = bayes2ks(xx,cdf, bayesS, d, k, 1);
    txt = sprintf('d=%d k=%d',d, k);      title(txt);

  end;
end;

%show the improvement after additional fitting
ks-ks2

for d=1:2
  figure(3+d);
  %err{1}=cdf{d}{1}-spdf{d}{1};
  %err{2}=cdf{d}{2}-spdf{d}{1};
  hold off;  plot(ppndf(1-cdf{d}{1}), ppndf(cdf{d}{2}), signs{1});
  %hold on;   plot(ppndf(1-(spdf{d}{1}+err{1})), ppndf(spdf{d}{2}+err{2}), signs{2});
  hold on;   plot(ppndf(1-(spdf{d}{1})), ppndf(spdf{d}{2}), signs{2});
  hold on;   plot(ppndf(1-(spdf2{d}{1})), ppndf(spdf2{d}{2}), signs{3});
  Make_DET();
  txt=sprintf('dataset %d', d); title(txt);
  legend('empirical', 'US-gmm,adapted','US-gmm,non-adapted');
end;

figure(4);
    txt = sprintf('../Pictures/loop_adapt_bruteforce_dev_%d_%02d.eps',p,b);
    print('-depsc2', txt);
    figure(5);
    txt = sprintf('../Pictures/loop_adapt_bruteforce_eva_%d_%02d.eps',p,b);
    print('-depsc2', txt);
