%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% pre-requisite:
% 1: expe.dset{d,k} is single dimension      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%calculate the empirical FAR and FRR at location xx

[bayesS] = cal_clientd_stat(expe);
for d=1:2,
  [tmp, tmp, tmp, tmp, xx, cdf{d}{1}, cdf{d}{2}] = estimate_EER_gmm(expe.dset{d,1}, expe.dset{d,2}, [], 0,bayesS{d});
  cdf{d}{1} = 1 - cdf{d}{1};
end;

for d=1:2,for k=1:2,
    
    nbound_scale = tan([pi*1/100,pi/2 - pi/100]);
    nbound_shift= [-6,6];
    shift_value = 0;
    scale_value = 1;
    %[scale_value, nbound] = tune_scale(xx, cdf, bayesS, 1, 1, nbound, 5, [],1);
    
    for i=1:2,
      [scale_value,goodness_scale] = tune_scale(shift_value, xx, cdf, bayesS, d, k, nbound_scale, 5, [],0,20);
      [shift_value,goodness_shift] = tune_shift(scale_value, xx, cdf, bayesS, d, k, nbound_shift, 5, [],0,20);
      %evaluate perf
      %gamma=[1 1;1 1];  shift_(d,k)=shift_value;  scale_(d,k)=scale_value;
      %nbayesS = update_bayes(bayesS, scale_, shift_, d, k, gamma);
      %figure(1);
      %goodness(i)= bayes2ks(xx,cdf, nbayesS, d, k,0);
      %pause;
      fprintf(1,'\n');
    end;
    %goodness
    %transfer{d,k} = [scale_value shift_value];
    %evaluate perf
    %gamma=[1 1;1 1];  shift_(d,k)=shift_value;  scale_(d,k)=scale_value;
    %nbayesS = update_bayes(bayesS, scale_, shift_, d, k, gamma);
    %subplot(2,2,(d-1)*2+k);
    %bayes2ks(xx,cdf, nbayesS, d, k,1)
    %transfer{d,k}
    %pause;
  end;
end;

for d=1:2,for k=1:2,
    scale_(d,k) = transfer{d,k}(1);
    shift_(d,k) = transfer{d,k}(2);
  end;
end;
nbayesS = update_bayes(bayesS, scale_, shift_,[1:2], [1:2], gamma);

for d=1:2,for k=1:2,
    gamma=[1 1;1 1];
    figure(1);
    subplot(2,2,(d-1)*2+k);
    [ks(d,k), spdf{d}{k}] = bayes2ks(xx,cdf, nbayesS, d, k, 1);
    
    figure(2);
    subplot(2,2,(d-1)*2+k);
    [ks2(d,k), spdf2{d}{k}] = bayes2ks(xx,cdf, bayesS, d, k, 1);
    %transfer{d,k}
    %pause;
  end;
end;


for d=1:2
  figure(2+d);
  %err{1}=cdf{d}{1}-spdf{d}{1};
  %err{2}=cdf{d}{2}-spdf{d}{1};
  hold off;  plot((1-cdf{d}{1}), (cdf{d}{2}), signs{1});
  hold on;   plot((1-(spdf{d}{1})), (spdf{d}{2}), signs{2});
  hold on;   plot(ppndf(1-(spdf2{d}{1})), ppndf(spdf2{d}{2}), signs{3});
  Make_DET();
  txt=sprintf('dataset %d', d); title(txt);
  legend('empirical', 'US-gmm,adapted','US-gmm,non-adapted');
end;


for d=1:2
  figure(2+d);
  err{1}=cdf{d}{1}-spdf{d}{1};
  err{2}=cdf{d}{2}-spdf{d}{1};
  hold off;  plot(ppndf(1-cdf{d}{1}), ppndf(cdf{d}{2}), signs{1});
  %hold on;   plot(ppndf(1-(spdf{d}{1}+err{1})), ppndf(spdf{d}{2}+err{2}), signs{2});
  hold on;   plot(ppndf(1-(spdf{d}{1})), ppndf(spdf{d}{2}), signs{2});
  hold on;   plot(ppndf(1-(spdf2{d}{1})), ppndf(spdf2{d}{2}), signs{3});
  Make_DET();
  txt=sprintf('dataset %d', d); title(txt);
  legend('empirical', 'US-gmm,adapted','US-gmm,non-adapted');
end;

