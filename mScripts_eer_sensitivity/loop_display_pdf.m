for d=1:2,
  [tmp, tmp, cbayesS{d}, est_err, xx, cdf{d}{1}, cdf{d}{2}] = estimate_EER_gmm(expe.dset{d,1}, expe.dset{d,2}, [], 0);
  cdf{d}{1} = 1 - cdf{d}{1};
end;
[bayesS] = cal_clientd_stat(expe);

for d=1:2,for k=1:2,
    [ks(d,k), spdf{d}{k}] = bayes2ks(xx,cdf, cbayesS, d, k, 1);
    [ks2(d,k), spdf2{d}{k}] = bayes2ks(xx,cdf, bayesS, d, k, 1);
    [ks2(d,k), spdf3{d}{k}] = bayes2ks(xx,cdf, nbayesS, d, k, 1);
  end;
end;

t=[0 1];
for d=1:2,for k=1:2,
    subplot(2,2,(d-1)*2+k);
    hold off;
    plot(cdf{d}{k}, spdf{d}{k}, '+');
    hold on;
    plot(cdf{d}{k}, spdf2{d}{k}, 'ro');
    plot(cdf{d}{k}, spdf3{d}{k}, 'gx');
    plot(t,t,'b');
    txt = sprintf('%d %d',d,k);title(txt);
  end;
end;
legend('gmm', 'us-gmm','us-gmm,adapt');



k=1; hold off;
plot(cdf{1}{k}, cdf{2}{k}, 'ro');
k=2; hold on;
plot(cdf{1}{k}, cdf{2}{k}, 'g+');



for d=1:2
  figure(2+d);
  %err{1}=cdf{d}{1}-spdf{d}{1};
  %err{2}=cdf{d}{2}-spdf{d}{1};
  hold off;  plot(ppndf(1-cdf{d}{1}), ppndf(cdf{d}{2}), signs{1});
  %hold on;   plot(ppndf(1-(spdf{d}{1}+err{1})), ppndf(spdf{d}{2}+err{2}), signs{2});
  hold on;   plot(ppndf(1-(spdf{d}{1})), ppndf(spdf{d}{2}), signs{2});
  hold on;   plot(ppndf(1-(spdf2{d}{1})), ppndf(spdf2{d}{2}), signs{3});
  Make_DET();
  txt=sprintf('dataset %d', d); title(txt);
  legend('empirical', 'gmm','us-gmm');
end;

pause;
%mysigns{'r-','r--','b-','b--'};
for d=1:2,
  figure(d+1);
  for k=1:2,
    cpxomega_pdf = gmmb_pdf(xx, cbayesS{d}(k).mu, cbayesS{d}(k).sigma, cbayesS{d}(k).weight );
    cpxomega_pdf = cpxomega_pdf / sum(cpxomega_pdf);

    pxomega_pdf = gmmb_pdf(xx, bayesS{d}(k).mu, bayesS{d}(k).sigma, bayesS{d}(k).weight );
    pxomega_pdf = pxomega_pdf / sum(pxomega_pdf);

    npxomega_pdf = gmmb_pdf(xx, nbayesS{d}(k).mu, nbayesS{d}(k).sigma, bayesS{d}(k).weight );
    npxomega_pdf = npxomega_pdf / sum(npxomega_pdf);
    
    if k==1, hold off; end;
    plot(xx,pxomega_pdf,'b');
    if k==1, hold on; end;
    hold on;  plot(xx,cpxomega_pdf,'b--');
    plot(xx,npxomega_pdf,'r--');
  end;
end;

%plot from bayesS
figure(2); hold on;
for k=1:2,id_cumpdf{k}=zeros(length(xx),1);end;
for id=1:length(bayesS{1}(1).weight),
  for k=1:2,
    id_pdf = cmvnpdf(xx, bayesS{d}(k).mu(id), bayesS{d}(k).sigma(1,1,id));
    id_cumpdf{k} = id_cumpdf{k} + id_pdf * bayesS{d}(k).weight(id);
    plot(xx,id_cumpdf{k}, signs{k});
  end;
  drawnow;
end;

