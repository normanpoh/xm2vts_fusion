function [bayesS, paramj, param] = cal_clientd_stat(expe, chosen)
%this operation operates on one dimension only!
% check fusion_clientd_check for similar coding
if (nargin<2|length(chosen)==0),
  chosen = 1;
end;

%get the model label
model_ID = unique(expe.label{1,1});

%calculate global param in train and test
param{1} = VR_analysis(expe.dset{1,1},expe.dset{1,2});
param{2} = VR_analysis(expe.dset{1,1},expe.dset{1,2});

%perform client-dependent normalisation
for id=1:size(model_ID,1),
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      tmp{d,k} = expe.dset{d,k}(index{d,k},chosen);
      count{d,k}(id) = size(tmp{d,k},1);
    end;
  end;

  for d=1:2, %calculate user-specific param in train and test
    myparamj = VR_analysis(tmp{d,1}, tmp{d,2});
    paramj{d}{id} = myparamj;
    %convert into Bayes structure
    bayesS{d}(1).mu(id) = myparamj.mu_I;
    bayesS{d}(2).mu(id) = myparamj.mu_C;
    bayesS{d}(1).sigma(1,1,id) = myparamj.sigma_I;
    bayesS{d}(2).sigma(1,1,id) = myparamj.sigma_C;
  end;
end;

%sum of the count
for d=1:2,for k=1:2,
    sumcount(d,k) = sum(count{d,k});
  end;
end;

for d=1:2,
  for k=1:2,
    bayesS{d}(k).weight = count{d,k}/sumcount(d,k);
    %equal priors for each class
    bayesS{d}(k).apriories = 0.5;
  end;
end;
