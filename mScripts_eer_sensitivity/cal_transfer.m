function [scale_,  shift_]= cal_transfer(bayesS, xx,cdf, dodisplay)
%see loop_adapt_iter
if nargin<4,
  dodisplay=0;
end;
for d=1:2,for k=1:2,
    nbound_scale = tan([pi*1/100,pi/2 - pi/100]);
    nbound_shift= [-6,6];
    shift_value = 0;
    scale_value = 1;
    %[scale_value, nbound] = tune_scale(xx, cdf, bayesS, 1, 1, nbound, 5, [],1);

    for i=1:2,
      [scale_value,goodness_scale] = tune_scale(shift_value, xx, cdf, bayesS, d, k, nbound_scale, 5, [],0,20);
      [shift_value,goodness_shift] = tune_shift(scale_value, xx, cdf, bayesS, d, k, nbound_shift, 5, [],0,20);
      %evaluate perf
      %gamma=[1 1;1 1];  shift_(d,k)=shift_value;  scale_(d,k)=scale_value;
      %nbayesS = update_bayes(bayesS, scale_, shift_, d, k, gamma);
      %figure(1);
      %goodness(i)= bayes2ks(xx,cdf, nbayesS, d, k,0);
      %pause;
      fprintf(1,'\n');
    end;
    %goodness
    transfer{d,k} = [scale_value shift_value];
    %evaluate perf
    %gamma=[1 1;1 1];  shift_(d,k)=shift_value;  scale_(d,k)=scale_value;
    %nbayesS = update_bayes(bayesS, scale_, shift_, d, k, gamma);
    %subplot(2,2,(d-1)*2+k);
    %bayes2ks(xx,cdf, nbayesS, d, k,1)
    %transfer{d,k}
    %pause;
  end;
end;

for d=1:2,for k=1:2,
    scale_(d,k) = transfer{d,k}(1);
    shift_(d,k) = transfer{d,k}(2);
  end;
end;