function [ks, spdf]= bayes2ks(xx,cdf, nbayesS, d, k, dodisplay)
if nargin<6,
  dodisplay=0;
end;

%evaluate the goodness of fit
pxomega_pdf = gmmb_pdf(xx, nbayesS{d}(k).mu, nbayesS{d}(k).sigma, nbayesS{d}(k).weight );
spdf = cumsum(pxomega_pdf);
spdf = spdf / sum(pxomega_pdf);
ks = max(abs(spdf-cdf{d}{k}));

if (dodisplay),
  hold off;
  plot(xx, cdf{d}{k},'r--'); hold on;
  plot(xx, spdf,'b-');
  legend('empirical', 'US-gmm');
end;