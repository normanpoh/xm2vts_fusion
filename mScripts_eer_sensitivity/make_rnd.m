%function make_rnd(n_draws, list)
initialise;

dat = dlmread('../Data/multi_modal/PI/dev.label', ' ');
model_ID = unique(dat(:,2));
clear dat;

if ~exist('ver', 'var'),
  error('ver must be defined first');
end;
if (ver==2),
  n_draws=100;
  dset_size = [50 150];
  list = [20 40 60 100 200 300 600 1000];
end;

if (ver==1),
  n_draws=100;
  dset_size = [50 150];
  list = [20 40 60 80 100];
end;

offset = [0 50];

clear rnd_list;
for p=1:2,
  for d=1:n_draws,
    for ds=1:length(dset_size),
      for n=1:length(list),
        selected_id = floor(rand(1,list(n))*dset_size(ds))+1; %randomly choose list(n) users from dset_size(ds) users
        rnd_list{ds}{n}(d,:) = selected_id + offset(ds);
      end;
    end;
  end;
end;
txt=sprintf('make_rnd%02d.mat',ver);
save(txt, 'rnd_list', 'n_draws', 'list');