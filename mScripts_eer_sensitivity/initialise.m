%the MLPs have tanh output [-1, 1]
%all GMMs use Log-likelihood ratios
dat.P{1}.labels = {'(FH,MLP)', '(DCTs,GMM)', '(DCTb,GMM)', '(DCTs,MLP)', '(DCTb,MLP)', '(LFCC,GMM)', '(PAC,GMM)', '(SSC,GMM)'};
dat.P{2}.labels = {'(FH,MLP)', '(DCTb,GMM)', '(LFCC,GMM)', '(PAC,GMM)', '(SSC,GMM)'};

%Protocol 1
% VR via modalities
expe.P{1}.seq{1} = [1 6; 1 7; 1 8;
    2 6; 2 7; 2 8;
    3 6; 3 7; 3 8;
    4 6; 4 7; 4 8;
    5 6; 5 7; 5 8];
% VR via extractors
expe.P{1}.seq{2} = [1 2;1 3;1 4; 1 5; %for face
	6 8; 7 8]; % for speech

% VR via classifiers
expe.P{1}.seq{3} = [2 4; 3 5];

expe.P{2}.seq{1} = [1 3; 1 4; 1 5; 2 3; 2 4; 2 5];
expe.P{2}.seq{2} = [1 2; 3 5; 4 5];
expe.P{2}.seq{3} = [];

datafiles{1}.dev = '../Data/multi_modal/PI/dev.label';
datafiles{1}.eva = '../Data/multi_modal/PI/eva.label';
datafiles{2}.dev = '../Data/multi_modal/PII/dev.label';
datafiles{2}.eva = '../Data/multi_modal/PII/eva.label';

remark = {'VR via modalities', 'VR via extractors', 'VR via classifiers' };

for p=1:2,
	for s=1:3,
		tmp = {};
		for i=1:size(expe.P{p}.seq{s},1),
		    tmp{i} = [dat.P{p}.labels{expe.P{p}.seq{s}(i,:)}];
		end;
		txt = sprintf('%s for protocol %d\n', remark{s}, p);
%		fprintf(1,txt);
		expe_labels.P{p}.seq{s}.row =tmp';
	end;
end;

clear i p s tmp txt;

%access the labels this way:
%expe_labels.P{1}.seq{1}(2,:)

NC = 400;
NI = 111800;
