function out = cal_user_gen_stat(nbayesS)

out.nbayesS = nbayesS;
for d=1:2, %training, test sets
  for k=1:2,
    tata = [nbayesS{d}(k).sigma(1,1,:)];
    tata = reshape(tata, length(tata),1);
    out.sigma{d}(k,:) = tata;
    out.mu{d}(k,:) = nbayesS{d}(k).mu;
  end;

  %calculate second-level statistics
  tmp=[out.mu{d}' out.sigma{d}'];
  labels = zeros(size(tmp,1),1);
  n_gmm = [1 10];
  FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
  hbayesS{d} = gmmb_create(tmp, labels + 1, 'FJ', FJ_params{:});
  
  n_gmm = [1];
  FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
  sbayesS{d} = gmmb_create(tmp, labels + 1, 'FJ', FJ_params{:});
end;
%find the transformation function using mu�train by stacking them
%together
datX = [out.mu{1}(1,:),out.mu{1}(2,:)];
datY = [out.mu{2}(1,:),out.mu{2}(2,:)];
out.transferf = polyfit(datX,datY,1);
datX = [out.sigma{1}(1,:),out.mu{1}(2,:)];

%y = polyval(coef_mu,xx);
%plot(xx,y,signs_curve{3});     grid on;
%xlabel('\mu^k_j|train');    ylabel('\mu^k_j|test');

%warning: don't know how to model sigma on test given sigma on
%training
%[f,x]=ksdensity(out.sigma{2}');
