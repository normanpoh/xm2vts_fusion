function out = cal_user_var_bayesS(bayesS, rnd_list_ds, list,n_draws)

for n=1:length(list), 
  fprintf(1,'.%d',n);
  for dd=1:n_draws,

    chosen_ID = rnd_list_ds{n}(dd,:);
    
    %update the bayesS
    for d=1:2, for k=1:2,
        for id=1:length(chosen_ID),
          nbayesS{d}(k).sigma(1,1,id) = bayesS{d}(k).sigma(1,1,chosen_ID(id));
        end;
        nbayesS{d}(k).mu = bayesS{d}(k).mu(chosen_ID);
        nbayesS{d}(k).weight = ones(1,length(chosen_ID))/length(chosen_ID);
        nbayesS{d}(k).apriories = 1;
      end;
    end;
    %run the epc
    [out(dd,n).dev, out(dd,n).eva, epc_cost]  = epc_bayesS(nbayesS, 21,[0 1]);
  end;
end;
fprintf(1,'\n');