
for dd=1:size(out,1),
  fprintf(1,'.');
  for i=1:2,
    myout{dd,i} = [];
    for ddb=1:size(out{i},2),
      switch mode
       case  'hter'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.hter_apri];
       case  'far'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.far_apri];
       case  'frr'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.frr_apri];
       otherwise
	error('no such option');
      end;
    end;
  end;
end;
epc_cost= linspace(0,1,21);

fprintf(1,'\nAnalysing\n');

%first row is the true-user set
for myalpha = 1:21,
  fprintf(1,'.');
  true_user = myout{1,1}(:,myalpha) - myout{1,2}(:,myalpha);
    bound_true = [min(true_user), max(true_user)];
    for dd=2:size(out,1),%n_draws,
      chimeric_user = myout{dd,1}(:,myalpha) - myout{dd,2}(:,myalpha);
      bound_chimeric = [min(chimeric_user), max(chimeric_user)];
      bound_lower = max(bound_chimeric(1), bound_true(1));
      bound_upper = min(bound_chimeric(2), bound_true(2));
      %plot(chimeric_user); hold on; plot(true_user,'r');
      tot1 = size(intersect(find(chimeric_user>=bound_lower),find(chimeric_user<=bound_upper)),1);
      tot2 = size(intersect(find(true_user>=bound_lower),find(true_user<=bound_upper)),1);
      prop_intersected = (tot1+tot2) / (length(chimeric_user)+length(true_user));
      myout2.prop(dd,myalpha) = prop_intersected;
    end;
end;

signs = {'bo-','rx--'};

subplot(1,3,m);
set(gca,'fontsize',12);
myout2.quan = quantil(myout2.prop(2:end,:), [2.5, 50 97.5]);
e = myout2.quan([1,3],:) - repmat(myout2.quan(2,:),2,1);
errorbar(epc_cost, myout2.quan(2,:)'*100, e(1,:)'*100, e(2,:)'*100,signs{1});
%axis_tmp = axis; axis_tmp([1 2]) = [0 1]; axis(axis_tmp);
axis([0 1 0 100]); set(gca,'ytick', [0:25:100]);
set(gca, 'xtick',[0:0.25:1]);
grid on;

subplot(1,3,1); title('HTER');
xlabel('\alpha');
ylabel('% of overlap');
