clear prop;

for i=1:2
  %process true user and its bootstrap
  dd=1;
  true_stat{i} = [];
  for ddb=1:size(out{i},2),
    switch mode
     case  'hter'
      true_stat{i} = [true_stat{i}; out{dd,i}(ddb).eva.hter_apri];
     case  'far'
      true_stat{i} = [true_stat{i}; out{dd,i}(ddb).eva.far_apri];
     case  'frr'
      true_stat{i} = [true_stat{i}; out{dd,i}(ddb).eva.frr_apri];
    end;
  end;
  
  %process the chimeric user without bootstrap
  for dd=1:size(out_simple,1),
    switch mode
     case  'hter'
      chim_stat{i}(dd,:) = out_simple{dd,i}.epc.eva.hter_apri;
     case  'far'
      chim_stat{i}(dd,:) = out_simple{dd,i}.epc.eva.far_apri;
     case  'frr'
      chim_stat{i}(dd,:) = out_simple{dd,i}.epc.eva.frr_apri;
    end;
  end;
end;
epc_cost= linspace(0,1,11);

%filter the true_stat
selected = 1:2:21;
for i=1:size(true_stat,2),
  true_stat{i} = true_stat{i}(:,selected);
end;

fprintf(1,'\nAnalysing\n');

%first row is the true-user set
clear res;
for i=1:2,
  %find 
  interval = quantil(true_stat{i}, [2.5, 97.5]);
  for dd=1:size(out_simple,1),
    for j=1:size(epc_cost,2),
      thealpha(j) = chim_stat{i}(dd,j) >= interval(1,j) & chim_stat{i}(dd,j) <= interval(2,j);
    end;
    res{i}(dd,:) = thealpha;
  end;
end;

selected = 2:10;
for i=1:2,
  prop{i} = sum(res{i}(2:end,:)) / (size(res{i},1)-1); 
  prop{i} = 1 - prop{i};
  subplot(2,3,(i-1)*3+m);
  set(gca, 'fontsize',12);
  plot(epc_cost(selected),prop{i}(selected)*100);
  axis([0 1 0 100]);grid on;
  set(gca, 'xtick',[0:0.25:1]);
  set(gca, 'ytick',[0:25:100]);
end;

subplot(2,3,1); title('(a) HTER');
subplot(2,3,2); title('(b) FAR');
subplot(2,3,3); title('(c) FRR');

subplot(2,3,4); title('(d) HTER');
xlabel('\alpha');
ylabel('% supporting H_1');
subplot(2,3,5); title('(e) FAR');
subplot(2,3,6); title('(f) FRR');
