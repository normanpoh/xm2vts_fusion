
%process true user and its bootstrap
dd=1;
true_stat = [];
for ddb=1:size(out{1},2),
  switch mode
   case  'hter'
    true_stat = [true_stat; out{dd,1}(ddb).eva.hter_apri-out{dd,2}(ddb).eva.hter_apri];
   case  'far'
    true_stat = [true_stat; out{dd,1}(ddb).eva.far_apri-out{dd,2}(ddb).eva.far_apri];
   case  'frr'
    true_stat = [true_stat; out{dd,1}(ddb).eva.frr_apri-out{dd,2}(ddb).eva.frr_apri];
  end;
end;
%process the chimeric user without bootstrap
for dd=1:size(out_simple,1),
  switch mode
   case  'hter'
      chim_stat(dd,:) = out_simple{dd,1}.epc.eva.hter_apri-out_simple{dd,2}.epc.eva.hter_apri;
     case  'far'
      chim_stat(dd,:) = out_simple{dd,1}.epc.eva.far_apri-out_simple{dd,2}.epc.eva.far_apri;
     case  'frr'
      chim_stat(dd,:) = out_simple{dd,1}.epc.eva.frr_apri-out_simple{dd,2}.epc.eva.frr_apri;
  end;
end;

epc_cost= linspace(0,1,11);

%filter the true_stat
selected = 1:2:21;
true_stat = true_stat(:,selected);

size(chim_stat)
size(true_stat)


%plot the density
if (1==0),
  for i=1:length(epc_cost),
    [f,x] = ksdensity(true_stat(:,i));
    subplot(3,5,i);
    plot(x,f);
  end;
end;
  
fprintf(1,'\nAnalysing\n');

%first row is the true-user set
clear res;
%find 
interval = quantil(true_stat, [2.5, 97.5]);
for dd=1:size(out_simple,1),
  for j=1:size(epc_cost,2),
    thealpha(j) = chim_stat(dd,j) >= interval(1,j) & chim_stat(dd,j) <= interval(2,j);
  end;
    res(dd,:) = thealpha;
end;

selected = 2:10;

prop = sum(res(2:end,:)) / (size(res,1)-1); 
prop = 1 - prop;
subplot(1,3,m);
set(gca, 'fontsize',12);
plot(epc_cost(selected),prop(selected)*100);
axis([0 1 0 100]);grid on;
set(gca, 'xtick',[0:0.25:1]);
set(gca, 'ytick',[0:25:100]);


subplot(1,3,1); title('(a) HTER');
xlabel('\alpha');
ylabel('% supporting H_1');
subplot(1,3,2); title('(b) FAR');
subplot(1,3,3); title('(c) FRR');


