
for dd=1:size(out,1),
  fprintf(1,'.');
  for i=1:2,
    myout{dd,i} = [];
    for ddb=1:size(out{i},2),
      switch mode
       case  'hter'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.hter_apri];
       case  'far'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.far_apri];
       case  'frr'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.frr_apri];
       otherwise
	error('no such option');
      end;
    end;
  end;
end;
epc_cost= linspace(0,1,21);

fprintf(1,'\nAnalysing\n');

%first row is the true-user set
for myalpha = 1:21,
  fprintf(1,'.');
  for i=1:2,
    true_user = myout{1,i}(:,myalpha);
    for dd=2:size(out,1),%n_draws,
      chimeric_user = myout{dd,i}(:,myalpha);

      [H,P,KSstat] = kstest2(true_user, chimeric_user);
      myout2.KSstat(dd,myalpha,i) = KSstat;
      myout2.P(dd,myalpha,i) = P;
    end;
  end;
end;

signs = {'bo-','rx--'};
thealpha= 0.05;

if (1==0),
  selected=2:length(epc_cost)-1;
  figure(1); cla; hold on;
  for i=1:2,
    prop{i} = sum(thealpha >= myout2.P(2:end,:,i)) / (length(myout2.P)-1);
    set(gca, 'fontsize',12);
    plot(epc_cost(selected),prop{i}(selected)*100, signs{i});
    axis([0 1 0 100]);grid on;
    set(gca, 'xtick',[0:0.25:1]);
    set(gca, 'ytick',[0:25:100]);
  end;
end;

for i=1:2,
  subplot(2,3,(i-1)*3+m);cla; hold on;
  set(gca,'fontsize',12);
  myout2.quan{i} = quantil(myout2.KSstat(2:end,:,i), [2.5, 50 97.5]);
  e = myout2.quan{i}([1,3],:) - repmat(myout2.quan{i}(2,:),2,1);
  errorbar(epc_cost, myout2.quan{i}(2,:)', e(1,:)', e(2,:)',signs{i});
  %axis_tmp = axis; axis_tmp([1 2]) = [0 1]; axis(axis_tmp);
  grid on;
  axis([0 1 0 1]); set(gca,'ytick', [0:0.25:1]);
  set(gca,'xtick', [0:0.25:1]);

  prop{i} = sum(thealpha >= myout2.P(2:end,:,i)) / (length(myout2.P)-1);
  plot(epc_cost(selected),prop{i}(selected), signs{i},'linewidth',2);
end;
%axis([0 1 0 100]); set(gca,'ytick', [0:25:100]);
%set(gca, 'xtick',[0:0.25:1]);
xlabel('\alpha');
ylabel('prop supporting H_1');


%txt=sprintf('no. of chimeric bstrap = %d', size(out,1));
%text(0.45, 0.25,txt,'fontsize',16);
%txt=sprintf('no. of bstrap for conf.= %d', size(out,1));
%text(0.6, 0.1,txt,'fontsize',16);

subplot(2,3,1); title('(a) HTER');
subplot(2,3,2); title('(b) FAR');
subplot(2,3,3); title('(c) FRR');

subplot(2,3,4); title('(d) HTER');
xlabel('\alpha');
ylabel('% of overlap');
subplot(2,3,5); title('(e) FAR');
subplot(2,3,6); title('(f) FRR');
