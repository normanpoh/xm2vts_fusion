%pre-requisite: p and r defined
s=1;
%print remarks


b = expe.P{p}.seq{s}(r,:);
%load score files
for d=1:2,for k=1:2,
    nexpe.dset{d,k} = data{p}.dset{d,k}(:,b);
  end;
end;

nexpe.label = data{p}.label;

%save memory
%clear data;

model_ID = unique(nexpe.label{1,1});
n_users = length(model_ID);

[tmp_,hostname]=unix('hostname');

txt = sprintf('\nRunning Protocol %d Experiment %d (%s)', p,r,expe_labels.P{p}.seq{s}.row{r});
txt = sprintf('%s on %s\n', txt, hostname);
fprintf(1,txt);

for dd=draw_from:n_draws,
  %get the randomized data set
  myexpe = rndcombine_client(nexpe, [1 2], 200,expe_id{p}.selected(dd,:));
  
  com{1} = fusion_gmm(myexpe, [1 2], [2 6],[],0);
  com{2} = fusion_wsum_brute(myexpe, [1 2], [0.5 0.5]);
  fprintf(1,'%d.',dd);
	     
  for i=1:2,
    %clear memory
    com{i}.dset={};
    out{dd,i} = com{i};
  end;

  %save for each dd completed
  %fname = sprintf('main_scalable_EER_simple_%d_%02d.mat',p,r);
  if (floor(dd/10) == dd/10),
    %save every 10 cycles
    save(fname, 'out');
  end;
end;
