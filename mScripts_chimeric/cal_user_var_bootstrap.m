function out = cal_user_var_bootstrap(expe, n_draws, rnd_list_ds)

model_ID = unique(expe.label{1,1});
n_users = length(model_ID);

%generate the random sampling on the fly if necessary
if nargin<3,
  for dd=1:n_draws,
    rnd_list_ds{dd} = floor(rand(1,n_users)*n_users)+1;         
    %randomly choose list(n) users from dset_size(ds) users
  end;
end;

for dd=1:n_draws,

  fprintf(1,'.');
  rexpe = rndchoose_client_list(expe, 1, [1:n_users], model_ID(rnd_list_ds{dd}));
  %bootstrap with replacement
  [out(dd).dev, out(dd).eva, epc_cost]  = epc(rexpe.dset{1,1}, rexpe.dset{1,2}, rexpe.dset{2,1}, rexpe.dset{2,2}, 21,[0, 1]);
end;
fprintf(1,'\n');