cd /home/learning/norman/xm2vts_fusion/mScripts_chimeric
addpath /home/learning/norman/xm2vts_fusion/mScripts
initialise;
p=1;s=1;r=1;



mode_list = {'hter','far','frr'};
s=1; %over multimodal config. only
for p=1:2, %over two protocols

  for r=1:size(expe.P{p}.seq{s},1), 
    %init. the output variable is myout{p}{r}
    
    %load simple data
    fname = sprintf('main_scalable_EER_simple_%d_%02d.mat',p,r);
    fprintf(1,'loading %s\n',fname);
    load(fname, 'out');
    out_simple = out;
    
    %load bootstrapped data
    fname = sprintf('main_scalable_EER_conf_%d_%d.mat',p,r);
    fprintf(1,'loading %s\n',fname);
    load(fname, 'out');
    
    %close all;
    for m=1:1,%3,
      mode= mode_list{m};
      %txt = sprintf('%s for protocol %d\n', remark{s}, p);      fprintf(1,txt);
      loop_analysis_chimeric_simple; %on relax19
      %loop_analysis_chimeric_simple_diff; on relax14
      myout{p}{r}{m} = prop;
    end;
    
    drawnow;
    %fname = sprintf('../Pictures/main_scalable_EER_prop_H1_%d_%02d.eps',p,r);
    %fname = sprintf('../Pictures/main_scalable_EER_prop_H1_simple_%d_%02d.eps',p,r);
    %fname = sprintf('../Pictures/main_scalable_EER_prop_H1_simple_diff_%d_%02d.eps',p,r);
    %print('-depsc2', fname);
    
  end;
end;

save('analysis_scalable_EER_simple.mat', 'myout');
save('analysis_scalable_EER_simple_diff.mat', 'myout');