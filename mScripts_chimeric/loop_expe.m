%pre-requisite: p and r defined
s=1;
%print remarks

b = expe.P{p}.seq{s}(r,:);
%load score files
for d=1:2,for k=1:2,
    nexpe.dset{d,k} = data{p}.dset{d,k}(:,b);
  end;
end;

nexpe.label = data{p}.label;

%save memory
clear data;

model_ID = unique(nexpe.label{1,1});
n_users = length(model_ID);

[tmp_,hostname]=unix('hostname');
for dd=draw_from:n_draws,
  txt = sprintf('\nRunning Protocol %d Experiment %d (%s)', p,r,expe_labels.P{p}.seq{s}.row{r});
  txt = sprintf('%s on %s\n', txt, hostname);
  fprintf(1,txt);

  %get the randomized data set
  myexpe = rndcombine_client(nexpe, [1 2], 200,expe_id{p}.selected(dd,:));
  
  com{1}{dd}{p,r} = fusion_gmm(myexpe, [1 2], [2 6],[],0);
  %com{2}{dd}{p,r} = fusion_gmm_indep(myexpe, [1 2], [2 6],[],0);
  
  com{2}{dd}{p,r} = fusion_wsum_brute(myexpe, [1 2], [0.5 0.5]);
  %calculate KL-distance, relative entropy
  %out{dd}{p,r} = cal_KL_dep(myexpe, com{1}{dd}{p,r}.bayesS, com{2}{dd}{p,r}.bayesS,0);
  fprintf(1,'%d',dd);
	     
  for ddb=1:n_draws_bstrp,
    rnd_list_ds{ddb} = floor(rand(1,n_users)*n_users)+1;         
    %randomly choose list(n) users from dset_size(ds) users
    %but common for com{1} and com{2}, so that they are directly comparable
  end;

  for i=1:2,
    tata = com{i}{dd}{p,r};
    tata.label = nexpe.label;

    %calculate confidence by bootstrap
    out{dd,i} = cal_user_var_bootstrap(tata, n_draws_bstrp, rnd_list_ds);
    
    %plot
    if 1==0,
      for j=1:10,%n_draws_bstrp,
	if j==1, hold off; end;
	plot(ppndf(out(j).eva.far_apri),ppndf(out(j).eva.frr_apri),'ro--');
	if j==1, hold on; end;
      end;
      plot(ppndf(com{i}{dd}{p,r}.eva.far_apri),ppndf(com{i}{dd}{p,r}.eva.frr_apri),'b*-');
      Make_DET(0.1);
    end;
    %clear memory
    com{i}{dd}{p,r}.dset={};
  end;

  %save for each dd completed
  fname = sprintf('main_scalable_EER_conf_%d_%d.mat',p,r);
  save(fname, 'out');

end;
