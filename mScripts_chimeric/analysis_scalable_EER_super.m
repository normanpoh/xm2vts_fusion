cd /home/learning/norman/xm2vts_fusion/mScripts_chimeric
addpath /home/learning/norman/xm2vts_fusion/mScripts
initialise;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%load Test 2
load('analysis_scalable_EER_simple.mat', 'myout');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%make a big matrix
s=1;
for i=1:2,
  out{i} = [];
  for p=1:2, %over two protocols
    for r=1:size(expe.P{p}.seq{s},1), 
      out{i} = [out{i}; myout{p}{r}{1}{i}];
    end;
  end;
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%load Test 2, diff
load('analysis_scalable_EER_simple_diff.mat', 'myout');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%make a big matrix
s=1;
i=3;
out{i} = [];
for p=1:2, %over two protocols
  for r=1:size(expe.P{p}.seq{s},1), 
    out{i} = [out{i}; myout{p}{r}{1}];
  end;
end;

signs = {'bo-','rx--','gd-.'};
lwidth = [3 2 1];
epc_cost= linspace(0,1,11);
selected = 2:10;
shift = [-0.01,0, 0.01];
figure(1);cla; hold on;
for i=1:3,
  set(gca,'fontsize', 20);
  tmp = quantil(out{i}, [2.5, 50 97.5]);
  e = tmp([1,3],:) - repmat(tmp(2,:),2,1);
  errorbar(epc_cost(selected)+shift(i), tmp(2,selected)'*100, e(1,selected)'*100, e(2,selected)'*100,signs{i},'linewidth',lwidth(i));
  xlabel('\alpha');
  ylabel('% supporting H_1');
  grid on;
  axis([0 1 0 80])
end;
legend('GMM', 'mean','diff');
print('-depsc2', '../Pictures/analysis_scalable_EER_super_all.eps');

clear tata;
figure(1);cla; hold on;
for i=1:3,
  set(gca,'fontsize', 20);
  tmp = sum(out{i}*200) /(size(out{i},1)*200);
  plot(epc_cost(selected), tmp(selected)*100,signs{i},'linewidth',lwidth(i));
  xlabel('\alpha');
  ylabel('% supporting H_1');
  grid on;
  %axis([0 1 0 80])
  tata(:,i)=tmp(selected);
end;
legend('GMM', 'mean','diff');
print('-depsc2', '../Pictures/analysis_scalable_EER_super_all_pooled.eps');

min(tata(:,[1 2]))
max(tata(:,[1 2]))
min(tata(:,[3]))
max(tata(:,[3]))


%display expe per expe
lwidth = [1 1 1];
t=0;
for p=1:2, %over two protocols
  for r=1:size(expe.P{p}.seq{s},1), 
    t=t+1;
    subplot(3,7,t);
    cla; hold on;
    for i=1:3,
      %set(gca,'fontsize', 20);
      plot(epc_cost(selected)+shift(i),out{i}(t,selected)*100,signs{i},'linewidth',lwidth(i));
      grid on;
      axis([0 1 0 80])
      tit = sprintf('[%d]',t);  title(tit);
      if t==15,
	xlabel('\alpha');
	ylabel('% supporting H_1');
      else
	%clear axis
	set(gca,'yticklabel', cell(1,length(get(gca,'yticklabel'))));
      	set(gca,'xticklabel', cell(1,length(get(gca,'xticklabel'))));
      end;
    end;
  end;
end;
legend('GMM', 'mean','diff');
print('-depsc2', '../Pictures/analysis_scalable_EER_super_each.eps');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%load Test 3
load('analysis_scalable_EER_conf_diff.mat', 'myout');
myout_diff = myout;
load('analysis_scalable_EER_conf.mat', 'myout');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

signs = {'bo-','rx--','gd-.'};
lwidth = [1 1 1];
epc_cost= linspace(0,1,21);
selected = 2:2:20;
shift = [-0.01,0, 0.01];
figure(1);cla; hold on;
t=0;
tmp2{1}=[];tmp2{2}=[];tmp2{3}=[];
for p=1:2, %over two protocols
  for r=1:size(expe.P{p}.seq{s},1), 
    t=t+1;
    subplot(3,7,t); cla; hold on;
    for i=1:3,
      if i==3,
	tmp = quantil(myout_diff{p}{r}{1}, [2.5, 50 97.5]);
      else
	tmp = quantil(myout{p}{r}{1}(:,:,i), [2.5, 50 97.5]);
      end;
      e = tmp([1,3],:) - repmat(tmp(2,:),2,1);
      errorbar(epc_cost(selected)+shift(i), tmp(2,selected)'*100, e(1,selected)'*100, e(2,selected)'*100,signs{i},'linewidth',lwidth(i));
      grid on;
      axis([0 1 0 100]); 
      if t==15,
	xlabel('\alpha');
	ylabel('% of overlap');
      else
	%clear axis
	set(gca,'yticklabel', cell(1,length(get(gca,'yticklabel'))));
      	set(gca,'xticklabel', cell(1,length(get(gca,'xticklabel'))));
	%set(gca,'ytick', [0:25:100]);
	%set(gca, 'xtick',[0:0.25:1]);
	%axis([0 1 0 80])
      end;
    end;
    tit = sprintf('[%d]',t);  title(tit);    
    tmp2{1} = [tmp2{1};  myout{p}{r}{1}(:,:,1)];
    tmp2{2} = [tmp2{2}; myout{p}{r}{1}(:,:,2)];
    tmp2{3} = [tmp2{3}; myout_diff{p}{r}{1}];
  
  end;
end;

legend('GMM', 'mean','diff');
print('-depsc2', '../Pictures/analysis_scalable_EER_super_test3_each.eps');

for i=1:2,
  tmp = quantil(myout{p}{r}{1}(2:end,:,i), [2.5, 50 97.5]);
  e = tmp([1,3],:) - repmat(tmp(2,:),2,1);
  errorbar(epc_cost(selected)+shift(i), tmp(2,selected)'*100, e(1,selected)'*100, e(2,selected)'*100,signs{i},'linewidth',lwidth(i));
  %axis_tmp = axis; axis_tmp([1 2]) = [0 1]; axis(axis_tmp);
end;

close all;cla; hold on;
lwidth = [3 2 1];
set(gca, 'fontsize', 20);
for i=1:3,
  
  tmp = quantil(tmp2{i}, [2.5, 50 97.5]);
  e = tmp([1,3],:) - repmat(tmp(2,:),2,1);
  errorbar(epc_cost(selected)+shift(i), tmp(2,selected)'*100, e(1,selected)'*100, e(2,selected)'*100,signs{i},'linewidth',lwidth(i));
  grid on;
  axis([0 1 0 100]); 
  xlabel('\alpha');
  ylabel('% of overlap');

  set(gca, 'xtick',[0:0.2:1]);
end;
legend('GMM', 'mean','diff');
print('-depsc2', '../Pictures/analysis_scalable_EER_super_test3_all.eps');


%%%%
% another plot
thealpha = 0.90;
figure(1);cla; hold on;
lwidth = [3 2 1];
set(gca, 'fontsize', 20);
for i=1:3,
  set(gca,'fontsize', 20);
  tmp = sum(thealpha >= tmp2{i}) / length(tmp2{i});
  plot(epc_cost(selected),tmp(selected)*100, signs{i},'linewidth',2);

  xlabel('\alpha');
  %ylabel('% supporting H_1');
  txt = sprintf('overlap < %d',thealpha*100);
  ylabel(['% ' txt '%']);
  grid on;
  [min(tmp) max(tmp)]
end;
legend('GMM', 'mean','diff');
print('-depsc2', '../Pictures/analysis_scalable_EER_super_test3_all_less95.eps');
