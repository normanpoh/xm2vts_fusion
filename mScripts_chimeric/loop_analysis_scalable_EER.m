
for dd=1:size(out,1),
  fprintf(1,'.');
  for i=1:2,
    myout{dd,i} = [];
    for ddb=1:size(out{i},2),
      switch mode
       case  'hter'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.hter_apri];
       case  'far'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.far_apri];
       case  'frr'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.frr_apri];
       otherwise
	error('no such option');
      end;
    end;
  end;
end;
epc_cost= linspace(0,1,21);

fprintf(1,'\nAnalysing\n');

%first row is the true-user set
for myalpha = 1:21,
  fprintf(1,'.');
  for i=1:2,
    true_user = myout{1,i}(:,myalpha);
    bound_true = [min(true_user), max(true_user)];
    for dd=2:size(out,1),%n_draws,
      chimeric_user = myout{dd,i}(:,myalpha);
      bound_chimeric = [min(chimeric_user), max(chimeric_user)];
      bound_lower = max(bound_chimeric(1), bound_true(1));
      bound_upper = min(bound_chimeric(2), bound_true(2));
      %plot(chimeric_user); hold on; plot(true_user,'r');
      tot1 = size(intersect(find(chimeric_user>=bound_lower),find(chimeric_user<=bound_upper)),1);
      tot2 = size(intersect(find(true_user>=bound_lower),find(true_user<=bound_upper)),1);
      prop_intersected = (tot1+tot2) / (length(chimeric_user)+length(true_user));
      myout2.prop(dd,myalpha,i) = prop_intersected;
    end;
  end;
end;
signs = {'bo-','rx--'};

for i=1:2,
  subplot(2,3,(i-1)*3+m);
  set(gca,'fontsize',12);
  myout2.quan{i} = quantil(myout2.prop(2:end,:,i), [2.5, 50 97.5]);
  e = myout2.quan{i}([1,3],:) - repmat(myout2.quan{i}(2,:),2,1);
  errorbar(epc_cost, myout2.quan{i}(2,:)'*100, e(1,:)'*100, e(2,:)'*100,signs{i});
  %axis_tmp = axis; axis_tmp([1 2]) = [0 1]; axis(axis_tmp);
  axis([0 1 0 100]); set(gca,'ytick', [0:25:100]);
  set(gca, 'xtick',[0:0.25:1]);
  grid on;
end;

thealpha = 0.95;
for i=1:2,
  subplot(2,3,(i-1)*3+m);cla;
  set(gca,'fontsize',12);
  prop{i} = sum(thealpha >= myout2.prop(2:end,:,i)) / (length(myout2.prop)-1);
  plot(epc_cost(selected),prop{i}(selected), signs{i},'linewidth',2);

  grid on;
  axis([0 1 0 1]); set(gca,'ytick', [0:0.25:1]);
  set(gca,'xtick', [0:0.25:1]);
  xlabel('\alpha');
  ylabel('% with overlap<95%');
end;


%txt=sprintf('no. of chimeric bstrap = %d', size(out,1));
%text(0.45, 0.25,txt,'fontsize',16);
%txt=sprintf('no. of bstrap for conf.= %d', size(out,1));
%text(0.6, 0.1,txt,'fontsize',16);

subplot(2,3,1); title('(a) HTER');
subplot(2,3,2); title('(b) FAR');
subplot(2,3,3); title('(c) FRR');

subplot(2,3,4); title('(d) HTER');
xlabel('\alpha');
ylabel('% of overlap');
subplot(2,3,5); title('(e) FAR');
subplot(2,3,6); title('(f) FRR');
