%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts_chimeric
addpath /home/learning/norman/xm2vts_fusion/mScripts

initialise;
%load main_norm_effect_addon_wsum_user.mat com epc_cost

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%load the fusion data sets and RUN the multimodal fusion
load('../mScripts_chimeric_old/main_scalable_EER', 'expe_id');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run with confidence by bootstrap subset technique
n_draws=200;
n_draws_bstrp = 200;

p=2;r=6; %last launched process

fname = sprintf('main_scalable_EER_conf_%d_%d.mat',p,r);
load(fname, 'out');
draw_from = size(out,1)+1;

draw_from = 1;
%clear out;
tic; loop_expe;
time_taken = toc;
save(fname, 'out', 'time_taken','com');

%run analysis_scalable_EER_conf

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run without confidence by bootstrap subset technique

initialise;
n_draws = 201;
s=1;

for p=1:2, %over two protocols
  
  for r=1:size(expe.P{p}.seq{s},1), 
    
    fname = sprintf('main_scalable_EER_simple_%d_%02d.mat',p,r);
    if exist(fname, 'file')
      %load(fname, 'out');
      %draw_from = size(out,1)+1;
      fprintf(1,'The file %s exists already yet\n', fname);
    else
      fprintf(1,'The file %s does not exist yet\n', fname);
      draw_from = 1;
      save(fname, 'draw_from'); %reserve the name
    
      tic; loop_expe_simple;
      time_taken = toc;
      save(fname, 'out', 'time_taken');

    end;
    %if exist('out', 'var'),
    %  clear out;
    %end;
  end;
  
end;

%run analysis_scalable_EER_simple
