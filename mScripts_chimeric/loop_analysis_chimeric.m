
for dd=1:size(out,1),
  fprintf(1,'.');
  for i=1:2,
    myout{dd,i} = [];
    for ddb=1:size(out{i},2),
      switch mode
       case  'hter'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.hter_apri];
       case  'far'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.far_apri];
       case  'frr'
	myout{dd,i} = [myout{dd,i}; out{dd,i}(ddb).eva.frr_apri];
      end;
    end;
  end;
end;
epc_cost= linspace(0,1,21);

fprintf(1,'\nAnalysing\n');

%first row is the true-user set
clear res;
for i=1:2,
  for dd=1:size(out,1),%n_draws,
    val = quantil(myout{dd,i},[50]);
    for j=1:21,
      thealpha(j) = size(find(myout{1,i}(:,j) > val(j)),1) / size(myout{1,i}(:,j),1);
    end;
    %post-process
    too_small_var = log(std(myout{dd,i}) + eps);
    selected = find(too_small_var < -12 );
    thealpha(selected)=0.5;
    res{i}(dd,:) = thealpha;
  end;
end;

for i=1:2,
  for myalpha=1:21,
    %subplot(3,7,myalpha);
    %[N,X] = hist(res{i}(:,myalpha),20);
    %plot(X,N);
    selected1 = find(res{i}(:,myalpha)<=0.025);
    selected2 = find(res{i}(:,myalpha)>=0.975);
    prop(myalpha) = length(union(selected1, selected2)) / length(res{i}(:,myalpha));
    %tit = sprintf('%1.2f',prop(myalpha));%epc_cost(myalpha));
    %title(tit);
  end;
  %signs = {'bo-','rx--'};
  
  subplot(2,3,(i-1)*3+m);
  set(gca, 'fontsize',12);
  plot(epc_cost,prop*100);
  axis([0 1 0 100]);grid on;
  set(gca, 'xtick',[0:0.25:1]);
  set(gca, 'ytick',[0:25:100]);
end;

subplot(2,3,1); title('(a) HTER');
subplot(2,3,2); title('(b) FAR');
subplot(2,3,3); title('(c) FRR');

subplot(2,3,4); title('(d) HTER');
xlabel('\alpha');
ylabel('% supporting H_1');
subplot(2,3,5); title('(e) FAR');
subplot(2,3,6); title('(f) FRR');
