%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts_zoo
addpath /home/learning/norman/xm2vts_fusion/mScripts

initialise;
%load main_norm_effect_addon_wsum_user.mat com epc_cost

for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

p=1; b=6;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    txt = sprintf('Experiment %d (%s)\n',b,dat.P{p}.labels{b}); fprintf(1,txt);
    
    for d=1:2, for k=1:2,
	expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    expe.label = data{p}.label;
    
    [param{p,b},size_dset{p}] = cal_clientd_param(expe);
    for d=1:2,
      param{p,b}.fratio{d} = (param{p,b}.mu{d,2} - param{p,b}.mu{d,1}) ./ (param{p,b}.sigma{d,2} + param{p,b}.sigma{d,1});
    end;
  
  end;
end;

%for p=1:2,
%  for b=1:size(dat.P{p}.labels,2)
%  end;
%end;

figure(1);
t=0;
for p=1:2,
  for b=2:size(dat.P{p}.labels,2)
    t=t+1;
    subplot(3,4,t);
    cla; hold on;
    plot(param{p,b}.mu{1,2},param{p,b}.mu{2,2},'bo')
    plot(param{p,b}.mu{1,1},param{p,b}.mu{2,1},'r+')

    axis tight;
    title(dat.P{p}.labels{b});
  end;
end;

figure(2);
t=0;
for p=1:2,
  for b=2:size(dat.P{p}.labels,2)
    t=t+1;
    subplot(3,4,t);
    cla; hold on;
    plot(param{p,b}.sigma{1,2},param{p,b}.sigma{2,2},'bo')
    plot(param{p,b}.sigma{1,1},param{p,b}.sigma{2,1},'r+')

    axis tight;
    title(dat.P{p}.labels{b});
  end;
end;

figure(3);
t=0;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    t=t+1;
    subplot(3,5,t);
    cla; hold on;
    plot(param{p,b}.fratio{1},param{p,b}.fratio{2},'r+');
    corr1 = corrcoef(param{p,b}.fratio{1},param{p,b}.fratio{2});
    out.fratio(t) = corr1(1,2);
    axis tight;
    title(dat.P{p}.labels{b});
  end;
end;

out.corr=[];
out.sig_corr=[];
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    cla; hold on;
    [corr1,p1] = corrcoef(param{p,b}.mu{1,1},param{p,b}.mu{2,1});
    [corr2,p2] = corrcoef(param{p,b}.mu{1,2},param{p,b}.mu{2,2});
    [corr3,p3] = corrcoef(param{p,b}.sigma{1,1},param{p,b}.sigma{2,1});
    [corr4,p4] = corrcoef(param{p,b}.sigma{1,2},param{p,b}.sigma{2,2});
    out.corr=[out.corr; corr1(1,2) corr2(1,2) corr3(1,2) corr4(1,2)];
    out.sig_corr=[out.sig_corr; p1(1,2) p2(1,2) p3(1,2) p4(1,2)];
  end;
end;


%out.sig_corr
%out.thrd = zeros(size(out.sig_corr));
%selected = find(out.sig_corr<0.05);
%out.thrd(selected)=1;

%relevance factor
rfactor = [ 0 1 10 100];

%apriori
apri=[0 0.5 1];
t=0;
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    t=t+1;
    
    for r = 1:length(rfactor),

      %adjusted
      for d=1:2,
	k=1; term{k} = size_dset{p}{d,k} ./ (size_dset{p}{d,k} + rfactor(r) );
	k=2; term{k} = size_dset{p}{d,k} ./ (size_dset{p}{d,k} + 10*rfactor(r) );
	for k=1:2,	
	  mu_adjusted{k} = param{p,b}.mu{d,k} .* term{k} + param{p,b}.all.mu(d,k) .* (1-term{k});
	  sigma_adjusted{k} = param{p,b}.sigma{d,k} .* term{k} + param{p,b}.all.sigma(d,k) .* (1-term{k});
	end;
	myout.fratio{1}{d,t}(:,r) = (mu_adjusted{2} - mu_adjusted{1}) ./  (sigma_adjusted{2} + sigma_adjusted{1});
      end;
      
      %a priori      
      for d=1:2,
	for  a= 1:length(apri),
	  mu_adjusted{2} = param{p,b}.mu{d,2} .* apri(a) + param{p,b}.all.mu(d,2) .* (1-apri(a));
	  myout.fratio{2}{d,t}(:,a) = (mu_adjusted{2} - param{p,b}.mu{d,1}) ...
	      ./  (param{p,b}.all.sigma(d,2) + param{p,b}.sigma{d,1});
	end;
      end;
      
    end;
    
  end;
end;

m_list = [length(rfactor),length(apri)];
for m=1:2,
  figure(m);
  t_=0;
  for t=1:size(myout.fratio{1},2),
    for r = 1:m_list(m),
      
      corr1 = corrcoef(myout.fratio{m}{1,t}(:,r), myout.fratio{m}{2,t}(:,r));
      
      myout.corr{m}(t,r) = corr1(1,2);
      t_=t_+1;
      subplot(13,m_list(m),t_);
      plot(myout.fratio{m}{1,t}(:,r), myout.fratio{m}{2,t}(:,r),'b+');
      axis tight;
  end;
  myout.ave_fratio{m}(t,:) = mean([mean(myout.fratio{m}{1,t}) ; mean(myout.fratio{m}{2,t})]);
  end;
end;

figure;
plot(myout.corr');
legend(num2str([1:13]'));


