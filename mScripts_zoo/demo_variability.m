%run baseline
n_samples = 11;
epc_range = [0.1 0.9];

%make protocols

clear cfg

cfg.p{1}=[];
for t1= [ 1 2 7 9],
  for t2 = [3 4 5],
    cfg.p{1} = [cfg.p{1}; t1 t2];
  end;
end;
cfg.p{2} = [1 2; 1 3; 1 4];

%create fdata
get_fnorm_scores

%global config
user_seq = [1:10];

for d=1:2, for k=1:2,
    expe.dset{d,k} = fdata{p}.dset{d,k}(:,chosen);
    orig.dset{d,k} = data{p}.dset{d,k}(:,chosen);
  end;
end;
expe.label = data{p}.label;
orig.label = data{p}.label;


param = VR_analysis(orig.dset{1,1}, orig.dset{1,2});
tmp_orig = filter_users(orig, [1 2], user_seq);


%draw original data
figure;set(gca,'fontsize',20);
draw_empiric(tmp_orig.dset{1,1}, tmp_orig.dset{1,2}, 1);
print('-depsc2','demo_var_01_data.eps');
hold on;
draw_theory([1 2], param);
print('-depsc2','demo_var_02_gauss.eps');


figure;set(gca,'fontsize',20);
com = fusion_gmm(tmp_orig, [1 2], [1 3],[],0);
cla; hold off;
draw_empiric(tmp_orig.dset{1,1}, tmp_orig.dset{1,2}, 1);
draw_theory_bayesS(com.bayesS);
print('-depsc2','demo_var_05_gmm.eps');

%draw user-gauss
figure;set(gca,'fontsize',20);
draw_empiric(tmp_orig.dset{1,1}, tmp_orig.dset{1,2}, 1);
hold on;
get_user_bayesS(tmp_orig, [1 2], 2, 1);
print('-depsc2','demo_var_03_user_gauss.eps');

%draw desired result
%using f-normalized data
tmp_expe = filter_users(expe, [1 2], user_seq);
param = VR_analysis(tmp_expe.dset{1,1}, tmp_expe.dset{1,2});
figure;set(gca,'fontsize',20);
draw_empiric(tmp_expe.dset{1,1}, tmp_expe.dset{1,2}, 1);
hold on;
get_user_bayesS(tmp_expe, [1 2], 2, 1);
print('-depsc2','demo_var_04_fnorm.eps');
  
[com, epc_cost ] = fusion_gmm(expe, [1 2], 1);
com.epc.eva.hter_apri(6)
[com2, epc_cost ] = fusion_gmm(data{p}, chosen, 1);
com2.epc.eva.hter_apri(6)

figure;set(gca,'fontsize',20);
plot(ppndf(com2.epc.eva.far_apri),ppndf(com2.epc.eva.frr_apri),'rd-');
hold on;
plot(ppndf(com.epc.eva.far_apri),ppndf(com.epc.eva.frr_apri),'bo--');
Make_DET;

figure;set(gca,'fontsize',20);
wer(com.dset{2,1},com.dset{2,2},[],2,[],1);
wer(com2.dset{2,1},com2.dset{2,2},[],2,[],2);
Make_DET(0.02);
legend('After normalization', 'Before normalization');
com.epc.eva.hter_apri(6)/com2.epc.eva.hter_apri(6) - 1
print('-depsc2','demo_var_05_det.eps');
