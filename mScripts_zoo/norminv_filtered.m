function [nfar, nfrr] = norminv_filtered(far, frr, use_ppndf)
cutting_point = ppndf(0.0005);

if length(use_ppndf)==0,
  use_ppndf = 0;
end;

if use_ppndf,
  nfar = ppndf(far); 
  nfrr = ppndf(frr); 
else
  nfar = norminv(far); 
  nfrr = norminv(frr); 
end;

index_far = find(nfar< cutting_point);
index_frr = find(nfrr< cutting_point);
nfar(index_far)=cutting_point;
nfrr(index_frr)=cutting_point;

