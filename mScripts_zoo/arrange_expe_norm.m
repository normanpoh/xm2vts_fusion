function ndat = arrange_expe_norm(dat, chosen, param, method)
% expecting the following field
% dat.dset{d,k}, param.mu{d,k}, param.sigma{d,k}
for d=1:2, for k=1:2,
    mu_C = us_stat2scores(dat.label{d,k}, param.mu{1,2}');
    mu_I = us_stat2scores(dat.label{d,k}, param.mu{1,1}');
    sigma_I = us_stat2scores(dat.label{d,k}, param.sigma{1,1}');
    
    switch lower(method)
      case {'orig'}
        ndat.dset{d,k} = dat.dset{d,k}(:,chosen);
      case {'z-norm-lr'}        
        % [y/sigma^I -mu^I/sigma^I]
        col1 = dat.dset{d,k}(:,chosen) ./ sigma_I;
        col2 = - mu_I ./ sigma_I;
        ndat.dset{d,k} = [col1 col2];
      case {'f-norm-lr'}        
        % [y/sigma^I -mu^I/sigma^I]
        term = mu_C * .5 + mean(param.mu{1,2}) * .5 - mu_I;
        col1 = dat.dset{d,k}(:,chosen) ./ term;
        col2 = - mu_I ./ term;
        ndat.dset{d,k} = [col1 col2];
      case {'z-norm-gen_norm-lr'}
        % [y/sigma^I, -mu^I/sigma^I, mu^C/sigma^I]
        col1 = dat.dset{d,k}(:,chosen) ./ sigma_I;
        col2 = - mu_I ./ sigma_I;
        col3 = mu_C ./ sigma_I;
        ndat.dset{d,k} = [col1 col2 col3];
      case {'z-norm-[y,p]-lr'}
        %[y 1/sigma^I mu^I mu^C]
        ndat.dset{d,k} = [dat.dset{d,k}(:,chosen), 1 ./sigma_I, mu_I, mu_C];
      case {'z-norm-[y,y*p]-lr'}
        %[y 1/sigma^I mu^I mu^C]
        tmp = [dat.dset{d,k}(:,chosen), 1 ./sigma_I, mu_I, mu_C];
        ndat.dset{d,k} = [dat.dset{d,k}(:,chosen), tensor_prod_scores(tmp, [1], [2 3 4])];
      case {'z-norm-[y,p,y*p]-lr'}
        %p = [1/sigma^I mu^I mu^C] 
        %[y p y*p]
        tmp = [dat.dset{d,k}(:,chosen), 1 ./sigma_I, mu_I, mu_C];
        ndat.dset{d,k} = [dat.dset{d,k}(:,chosen), tmp, tensor_prod_scores(tmp, [1], [2 3 4])];
      case {'fz-norm-lr'}        
        % [y^F, 1/sigma^FI, y^F/sigma^FI]
        ndat.dset{d,k} = [dat.dset{d,k}(:,chosen), 1 ./ sigma_I, dat.dset{d,k}(:,chosen) ./ sigma_I];
      otherwise
        disp('Unknown method.')
    end
  end;
end;