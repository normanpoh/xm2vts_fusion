taddpath ../mScripts_zoo

n_samples = 11;
epc_range = [0.1 0.9];

initialise;

%load and use only the speech data
for p=1:2,
  [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
  [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
  nexpe{p}.label = data{p}.label;

  speech_ = find(dat.P{p}.type==0);
  for d=1:2,
    for k=1:2,
      syslabel{p} = { dat.P{p}.labels{speech_} };
      nexpe{p}.dset{d,k}= data{p}.dset{d,k}(:,speech_);
    end;
  end;
end;

%get f-normalised data
clear data expe
for p=1:2,
  for d=1:2, for k=1:2,
      fdata{p}.dset{d,k} = zeros(size(nexpe{p}.dset{d,k}));
    end;
  end;
end;

for p=1:2,
  fdata{p}.label = nexpe{p}.label;
  for b=1:size(nexpe{p}.dset{1,1},2),
    
    for d=1:2, for k=1:2,
        expe.dset{d,k} = nexpe{p}.dset{d,k}(:,b);
      end;
    end;
    expe.label = nexpe{p}.label;

    [tmp, epc_cost] = fusion_clientd_check(expe, 1, 'f-norm-simple');

    for d=1:2, for k=1:2,
        fdata{p}.dset{d,k}(:,b) = tmp.dset{d,k};
      end;
    end;
    fprintf(1,'.');
  end;
end;

clear expe
for p=1:2,
  expe{1,p} = nexpe{p};
  expe{2,p} = fdata{p};
end;
clear fdata nexpe

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%expe{1,p} contains the original scores;
%expe{2,p} contains the F-normalised scores;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%build epxe_config
for i=1:3,
  expe_config.seq{i} = nchoosek(1:3,i);
end;

for p=1:2,
  for s=1:length(expe_config.seq),
    for r=1:size(expe_config.seq{s},1),
      for m=1:2,
        chosen = expe_config.seq{s}(r,:);
        [com{1,m}] =  fusion_gmm(expe{m,p}, chosen, [2 6]);
        [com{2,m}] =  fusion_lr(expe{m,p}, chosen);
        fprintf(1,'.');
      end;
      
      [out.eer_(p,s,r,t,m),tmp2, x, FAR, FRR] = wer(com{t,m}.dset{2,1},com{t,m}.dset{2,2});

    end;
  end;
end;
 

