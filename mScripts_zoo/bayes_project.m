function nbayesS = bayes_project(bayesS, vec)
%vec is a column vector

for k=1:2,
  nbayesS(k).weight  = bayesS(k).weight;
  nbayesS(k).apriories  = bayesS(k).apriories;
  nbayesS(k).mu  = (bayesS(k).mu' * vec)';
  for c=1:length(bayesS(k).weight),
    nbayesS(k).sigma(1,1,c)=0;
    for i=1:length(vec),
      for j=1:length(vec),
        nbayesS(k).sigma(1,1,c) = nbayesS(k).sigma(1,1,c) + bayesS(k).sigma(i,j,c) * vec(i) * vec(j);
      end;
    end;
  end;
end;