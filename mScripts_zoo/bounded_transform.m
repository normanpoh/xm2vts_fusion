function y = bounded_transform(x, min_, max_)
% y = bounded_transform(x, min_, max_)
if nargin<2,
  min_ = min(x);
end;
if nargin<3,
  max_ = max(x);
end;
y = log( (x - min_) ./ (max_ - x));