addpath ../mScripts

n_samples = 11;
epc_range = [0.1 0.9];

initialise;

%load and use a modified version of the default data
for p=1:2,
  [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
  [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
  if p==1,
    for d=1:2,for k=1:2,
        data{p}.dset{d,k} = [ data{p}.dset{d,k} ...
         bounded_transform(data{p}.dset{d,k}(:,4), -1 ,1) ...
         bounded_transform(data{p}.dset{d,k}(:,5),-1 ,1)];
        order = [1 2 3 6 7 8 4 9 5 10];
        data{p}.dset{d,k}(:,[1:10])= data{p}.dset{d,k}(:, order);
        
      end;
    end;
    %rearrange
  end;
  %delete the first one
  for d=1:2,for k=1:2,
      data{p}.dset{d,k}(:,1) = [];
    end;
  end;  
end;

%the new systems are as follows
syslabel={'GMM,F','GMM,F','GMM,S','GMM,S','GMM,S',...
	  'MLP,F','MLPi,F','MLP,F','MLPi,F','GMM,F',...
	  'GMM,S','GMM,S','GMM,S'}; 
syslabel={'P1:1(F)','P1:2(F)','P1:3(S)','P1:4(S)','P1:5(S)',...
	  'P1:6(F)','P1:7(F)','P1:8(F)','P1:9(F)','P2:1(F)',...
    'P2:2(S)','P2:3(S)','P2:4(S)'};
  
%%
  
  
  
%between 6 and 7
p=1;
for d=1:2,for k=1:2,

    temp.dset{d,k}(:,1) = data{p}.dset{d,k}(:,6);
    temp.dset{d,k}(:,2) = bounded_transform(data{p}.dset{d,k}(:,6), -1 ,1);
    temp.dset{d,k}(:,3) = tanh_inv(data{p}.dset{d,k}(:,6));

    temp.dset{d,k}(:,4) = data{p}.dset{d,k}(:,8);
    temp.dset{d,k}(:,5) = bounded_transform(data{p}.dset{d,k}(:,8), -1 ,1);
    temp.dset{d,k}(:,6) = tanh_inv(data{p}.dset{d,k}(:,8));

  end;
end;
temp.label = data{p}.label;
clear data;
clear skew kurt lilie
for c=1:6,
  t=0;
  for d=1:2,
    for k=1:2,
      t=t+1;
      skew(t,c) = skewness(temp.dset{d,k}(:,c));
      kurt(t,c) = kurtosis(temp.dset{d,k}(:,c));
      [tmp,tmp, lilie(t,c)] =lillietest(temp.dset{d,k}(:,c));
    end;
  end;
end;

lilie(:,[1 2 4 5])
skew(:,[1 2 4 5])
kurt(:,[1 2 4 5])-3


tmp{1}=lilie(:,[1 2]);
tmp{1}=[tmp{1}; lilie(:,[4 5])];
tmp{2}=skew(:,[1 2]);
tmp{2}=[tmp{2}; skew(:,[4 5])];
tmp{3}=kurt(:,[1 2]);
tmp{3}=[tmp{3}; kurt(:,[4 5])];

tit = {'KS','skewness','kurtosis'};
for t=1:3,
  subplot(2,3,t);
  barh(tmp{t});
  leg={'dev,I,1','dev,C,1','eva,I,1','eva,C,1','dev,I,2','dev,C,2','eva,I,2','eva,C,2'};
  set(gca,'ytick',1:8);
  set(gca,'yticklabel',leg);
  legend('before','after');
  xlabel(tit{t});
  axis_ = axis;
  axis_(4)=11; axis(axis_);
end;
print('-depsc2','Pictures/main_arun_global_transform_stat.eps');


out = cal_US_moments(temp,[1 2 4 5]);

set(gca,'fontsize',20);
boxplot(out{2,1}.skew)
leg={'before [1]','after [1]','before [2]','after [2]'};
set(gca,'xticklabel',leg);
ylabel('user-specific skewness');
xlabel('');
print('-depsc2','Pictures/main_arun_global_transform_us_skew.eps');

boxplot(out{2,1}.kurt)
leg={'before [1]','after [1]','before [2]','after [2]'};
set(gca,'xticklabel',leg);
ylabel('user-specific kurtosis');
xlabel('');
print('-depsc2','Pictures/main_arun_global_transform_us_kurt.eps');

boxplot(out{2,1}.lillie)
leg={'before [1]','after [1]','before [2]','after [2]'};
set(gca,'xticklabel',leg);
ylabel('user-specific KS value');
xlabel('');
print('-depsc2','Pictures/main_arun_global_transform_us_ks.eps');


figure(1);
subplot(2,1,1);c=1; 
wer(temp.dset{2,1}(:,c),temp.dset{2,2}(:,c),[],4)
title('before');
subplot(2,1,2);c=2;
wer(temp.dset{2,1}(:,c),temp.dset{2,2}(:,c),[],4)
title('after');
print('-depsc2','Pictures/main_arun_global_transform_density_1.eps');

figure(2);
subplot(2,1,1);c=4; 
wer(temp.dset{2,1}(:,c),temp.dset{2,2}(:,c),[],4)
title('before');
subplot(2,1,2);c=5;
wer(temp.dset{2,1}(:,c),temp.dset{2,2}(:,c),[],4)
title('after');
print('-depsc2','Pictures/main_arun_global_transform_density_2.eps');
% 
% for c=6:9, 
%   for d=1:2,
%     for k=1:2,
%       [t,t, tmp(d,k,c)] =(data{p}.dset{d,k}(:,c));
%     end;
%   end;
% end;
% 
% for c=6:9, 
%   for k=1:2,
%     t(c,k) = mean(tmp(:,k,c));
%   end;
% end;