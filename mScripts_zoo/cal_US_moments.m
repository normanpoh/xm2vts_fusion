function [out] = cal_US_moments(expe, chosen)

if nargin<2|length(chosen)==0,
  chosen = 1;
end;

model_ID = unique(expe.label{1,1});

%get the data set associated to the ID
for c = 1:length(chosen),
  for id=1:size(model_ID,1),
    for d=1:2,for k=1:2,
        index = find(expe.label{d,k} == model_ID(id));
        out{d,k}.skew(id,c) = skewness(expe.dset{d,k}(index,chosen(c)));
        out{d,k}.kurt(id,c) = kurtosis(expe.dset{d,k}(index,chosen(c)));
        if length(index)<4,
          out{d,k}.lillie(id,c)=nan;
        else
          [tmp, tmp, out{d,k}.lillie(id,c)] = lillietest(expe.dset{d,k}(index,chosen(c)));
        end;
      end;
    end;
  end;
end;