function dat = us_stat2scores(label, us_stat)

dat = zeros(length(label), size(us_stat,2));

model_ID = unique(label);
for id=1:size(model_ID,1),
  
  %get the data set associated to the ID
  index = find(label == model_ID(id));
  dat(index,:) = us_stat(id,:);
end;