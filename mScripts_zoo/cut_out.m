    %myout{t}.criterion(1,:) = out{p}.fratio{1}(3,:);
    %myout{t}.criterion(2,:) = out{p}.fratio{2}(3,:);
    
    %check consistency in terms of correct ordering
    %for d=1:2,
    %  [tmp, index{d}]=sort(out{p}.fratio{d});
    %  for t_=1:size(index{d},2),%over all users
    %    pos(d,t_) = index{d}(3,t_); %find the thrid row is the best
                                    %one
    %  end;
    %end;
    %myout{t}.pos = pos;
    %usage(t,:) = [length(find(pos(1,:)==1))
    %		  length(find(pos(1,:)==2))
    %		  length(find(pos(1,:)==3))];

    %correct(t) = length(find(pos(1,:)-pos(2,:)==0))/size(pos,2);

    %prepare to append back
    %debug
    
    for d=1:2,
      for i=1:3,
	for c=1:length(cut_rate),
	  usage(d,i,c,t) = length(user_seq_{d,i,c});
	end
      end;
    end;
    if (1==0)
      for i=1:3,
	if length(texpe{i}.label{1,2})==0,
	  fprintf(1,'skipped %d\n',i);
	else
	  wer(texpe{i}.dset{2,1},texpe{i}.dset{2,2})
	end;
      end;
      for i=1:3,
	wer(gmmf{i}.dset{2,1},gmmf{i}.dset{2,2})
      end;
      wer(gmm.dset{2,1},gmm.dset{2,2})
      wer(gexpe.dset{2,1},gexpe.dset{2,2})
      wer(gexpe_oracle.dset{2,1},gexpe_oracle.dset{2,2})

    end;
    
    %plot DET
    %orig
    figure(1);set(gca,'fontsize',16);
    eer_(t,1) = wer(data{p}.dset{2,1}(:,chosen(1)), data{p}.dset{2,2}(:,chosen(1)),[],2,[],1);
    eer_(t,2) = wer(data{p}.dset{2,1}(:,chosen(2)), data{p}.dset{2,2}(:,chosen(2)),[],2,[],2);
    eer_(t,3) = wer(gmm.dset{2,1},gmm.dset{2,2},[],2,[],3); %gmm

    eer_(t,4) = wer(gmmf{1}.dset{2,1},gmmf{1}.dset{2,2},[],2,[],9); %f-norm 1
    eer_(t,5) = wer(gmmf{2}.dset{2,1},gmmf{2}.dset{2,2},[],2,[],10); %f-norm 2
    eer_(t,6) = wer(gmmf{3}.dset{2,1},gmmf{3}.dset{2,2},[],2,[],11); %f-norm gmm
    eer_(t,7) = wer(gexpe.dset{2,1},gexpe.dset{2,2},[],2,[],12); %OR switcher(f-norm gmm)
    eer_(t,8) = wer(gexpe_oracle.dset{2,1},gexpe.dset{2,2},[],2,[],13); %OR switcher-oracle(f-norm gmm)

    eer_(t,:)
    
    legend(leg{:}); Make_DET(0.201);
    fname = sprintf('Pictures/main_fusion_DET_%02d.eps',t);
    print('-depsc2',fname);

    %evaluate in terms of EPC
    %baseline
    for i=1:2,
      [orig{i}.epc.dev, orig{i}.epc.eva, epc_cost]  = epc(data{p}.dset{1,1}(:,chosen(i)),data{p}.dset{1,2}(:,chosen(i)),data{p}.dset{2,1}(:,chosen(i)),data{p}.dset{2,2}(:,chosen(i)), n_samples,epc_range);
      myout{t}.res{i} = orig{i}.epc;
    end;
    [gexpe.epc.dev, gexpe.epc.eva, epc_cost]  = epc(gexpe.dset{1,1}, gexpe.dset{1,2}, gexpe.dset{2,1}, gexpe.dset{2,2}, n_samples,epc_range);
    [gexpe_oracle.epc.dev, gexpe_oracle.epc.eva, epc_cost]  = epc(gexpe_oracle.dset{1,1}, gexpe_oracle.dset{1,2}, gexpe_oracle.dset{2,1}, gexpe_oracle.dset{2,2}, n_samples,epc_range);
    
    myout{t}.res{3} = gmm.epc;
    myout{t}.res{4} = gmmf{1}.epc;
    myout{t}.res{5} = gmmf{2}.epc;
    myout{t}.res{6} = gmmf{3}.epc;
    myout{t}.res{7} = gexpe.epc;
    myout{t}.res{8} = gexpe_oracle.epc;

    %figure(2);cla;
    %plot_all_epc(epc_cost(:,1),leg,signs, myout{t}, 1:8,lwidth,14,1,1);
    %legend(leg{:});
    %fname = sprintf('Pictures/main_fusion_epc_%02d.eps',t);
    %print('-depsc2',fname);

    %plot_all_epc(epc_cost(:,1),leg,signs, myout{t}, 1:8,lwidth,14,1,0);
    %legend(leg{:});
    %fname = sprintf('Pictures/main_fusion_det_%02d.eps',t);
    %fprintf(1,'.');
  end;
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
