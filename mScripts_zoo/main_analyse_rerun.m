%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts_zoo
%addpath /home/learning/norman/xm2vts_fusion/mScripts
addpath ../mScripts

%cd C:\cygwin\home\Norman\learning\xm2vts_fusion\mScripts_zoo
%addpath C:\cygwin\home\Norman\learning\xm2vts_fusion\mScripts

initialise;
%load main_norm_effect_addon_wsum_user.mat com epc_cost

for p=1:2,
  [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
  [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
  if p==1,
    for d=1:2,for k=1:2,
        data{p}.dset{d,k} = [ data{p}.dset{d,k} ...
          tanh_inv(data{p}.dset{d,k}(:,4)) tanh_inv(data{p}.dset{d,k}(:,5))];
        order = [1 2 3 6 7 8 4 9 5 10];
        data{p}.dset{d,k}(:,[1:10])= data{p}.dset{d,k}(:, order);

      end;
    end;
    %rearrange
  end;
  %delete the first one
  for d=1:2,for k=1:2,
      data{p}.dset{d,k}(:,1) = [];
    end;
  end;
end;
syslabel={'GMM,F','GMM,F','GMM,S','GMM,S','GMM,S',...
	  'MLP,F','MLPi,F','MLP,F','MLPi,F','GMM,F',...
	  'GMM,S','GMM,S','GMM,S'}; 

for p=1:2,
  for b=1:size(data{p}.dset{1,1},2)
    txt = sprintf('Experiment P%d:%d',p,b);fprintf(1,'%s\n',txt);

    for d=1:2, for k=1:2,
        expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    expe.label = data{p}.label;

    [param{p,b},size_dset{p}] = cal_clientd_param(expe);
    for d=1:2,
      param{p,b}.fratio{d} = (param{p,b}.mu{d,2} - param{p,b}.mu{d,1}) ./ (param{p,b}.sigma{d,2} + param{p,b}.sigma{d,1});
    end;
  
  end;
end;

figure(1);
t=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2)
    t=t+1;
    subplot(3,5,t);
    cla; hold on;
    plot(param{p,b}.mu{1,2},param{p,b}.mu{2,2},'bo')
    plot(param{p,b}.mu{1,1},param{p,b}.mu{2,1},'r+')

    axis tight;
    txt = sprintf('P%d:%d %s',p,b,syslabel{t});    title(txt);
  end;
end;
%print('-depsc2','../Pictures/main_analyse_rerun_mu.eps');

figure(2);
t=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2)
    t=t+1;
    subplot(3,5,t);
    cla; hold on;
    plot(param{p,b}.sigma{1,2},param{p,b}.sigma{2,2},'bo')
    plot(param{p,b}.sigma{1,1},param{p,b}.sigma{2,1},'r+')

    axis tight;
    txt = sprintf('P%d:%d %s',p,b,syslabel{t});
    title(txt);
  end;
end;
%print('-depsc2','../Pictures/main_analyse_rerun_sigma.eps');

out.corr=[];
out.sig_corr=[];
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2)
    cla; hold on;
    [corr1,p1] = corrcoef(param{p,b}.mu{1,1},param{p,b}.mu{2,1});
    [corr2,p2] = corrcoef(param{p,b}.mu{1,2},param{p,b}.mu{2,2});
    [corr3,p3] = corrcoef(param{p,b}.sigma{1,1},param{p,b}.sigma{2,1});
    [corr4,p4] = corrcoef(param{p,b}.sigma{1,2},param{p,b}.sigma{2,2});
    out.corr=[out.corr; corr1(1,2) corr2(1,2) corr3(1,2) corr4(1,2)];
    out.sig_corr=[out.sig_corr; p1(1,2) p2(1,2) p3(1,2) p4(1,2)];
  end;
end;

figure(3);
set(gca,'fontsize',16);
boxplot(out.corr)
a={'mean,I','mean,C','std,I','std,C'};
set(gca,'xticklabel',a);
ylabel('correlation');
xlabel('');
print('-depsc2','../Pictures/main_analyse_rerun_hist_mu_sigma.eps');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%demo plot
figure;
p=1;b=1;
subplot(2,2,1); set(gca,'fontsize',16);
cla; hold on;
plot(param{p,b}.mu{1,2},param{p,b}.mu{2,2},'bo')
plot(param{p,b}.mu{1,1},param{p,b}.mu{2,1},'r+')
xlabel('\mu^I_j|dev');
ylabel('\mu^I_j|eva')
txt = sprintf('corr: I=%1.3f, C=%1.3f', out.corr(1,1), out.corr(1,2));
title(txt); axis tight;

subplot(2,2,2); set(gca,'fontsize',16);
cla; hold on;
plot(param{p,b}.sigma{1,2},param{p,b}.sigma{2,2},'bo')
plot(param{p,b}.sigma{1,1},param{p,b}.sigma{2,1},'r+')
xlabel('\sigma^I_j|dev');
ylabel('\sigma^I_j|eva')
txt = sprintf('corr: I=%1.3f, C=%1.3f', out.corr(1,3), out.corr(1,4));
title(txt); axis tight;
print('-depsc2','Pictures/main_analyse_rerun_demo_1.eps');

figure;
subplot(2,1,1);
set(gca,'fontsize',16);
boxplot(out.corr)
a={'mean,I','mean,C','std,I','std,C'};
set(gca,'xticklabel',a);
ylabel('correlation');
xlabel('');
print('-depsc2','Pictures/main_analyse_rerun_hist_mu_sigma_short.eps');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%figure(3);cla; hold on;
%signs = {'r+-','bo--','rd-','bs--'};
%for i=1:4,
%  [f,x] = ksdensity(out.corr(:,i));
%  plot(x,f,signs{i});
%end;
%axis([0 1 0 12])
%set(gca,'fontsize',20);
%legend('\mu^I','\mu^C','\sigma^I','\sigma^C');
%xlabel('correlation');
%ylabel('count');


%get f-normalized  score
get_fnorm_scores

%calucate param on f-normalized domain

for p=1:2,
  for b=1:size(data{p}.dset{1,1},2),
    [paramf{p,b},size_dset{p}] = cal_clientd_param(fdata{p},b);
  end;
end;

%apriori
apri=[0 0.25 0.5 0.75 1];

t=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2),

    t=t+1;
    mytxt{t} = sprintf('P%d:%d %s',p,b,syslabel{t});
    %criterion 1: f-ratio as is given    
    c=1;
    for d=1:2,    
      myout{c}.fratio{d,t}(:,1) = param{p,b}.fratio{d};
    end;
    
    %criterion 2: a priori
    c=2;
    for d=1:2,
      for  a= 1:length(apri),
        mu_adjusted = param{p,b}.mu{d,2} .* apri(a) + param{p,b}.all.mu(d,2) .* (1-apri(a));
        myout{c}.fratio{d,t}(:,a) = (mu_adjusted - param{p,b}.mu{d,1}) ...
          ./  (param{p,b}.all.sigma(d,2) + param{p,b}.sigma{d,1});
      end;
    end;
    
    %criterion 3: f-norm
    c=3;
    for d=1:2,
      myout{c}.fratio{d,t}(:,1) = 1 ./ (paramf{p,b}.all.sigma(d,2) + paramf{p,b}.sigma{d,1});
    end;

    %criterion 4: f-norm
    c=4;
    for d=1:2,
      myout{c}.fratio{d,t}(:,1) = 1 ./ ( paramf{p,b}.sigma{d,1});
    end;

  end;
end;

%analyse errors
for c=1:length(myout),
  for t=1:13,
    for r = 1:size(myout{c}.fratio{1,1},2),
      corr1 = corrcoef(myout{c}.fratio{1,t}(:,r), myout{c}.fratio{2,t}(:,r));
      myout{c}.corr(t,r) = corr1(1,2);
      myout{c}.mean_fratio(t,r) = mean([myout{c}.fratio{1,t}(:,r); myout{c}.fratio{2,t}(:,r)]);
      myout{c}.mean_error(t,r) = mean(myout{c}.fratio{1,t}(:,r) -  myout{c}.fratio{2,t}(:,r));
      myout{c}.std_error(t,r) = std(myout{c}.fratio{1,t}(:,r) -  myout{c}.fratio{2,t}(:,r));
    end;
  end;
end;

%plot all 4 criteria
chosen =[1 3 1 1]; %method-2 has a few possibilities
for c=1:length(myout),
  figure(c);
  r=chosen(c);
  for t=1:size(myout{1}.fratio,2),
    subplot(3,5,t);cla; hold on;
    %plot(f_eer(myout{c}.fratio{1,t}(:,r)), f_eer(myout{c}.fratio{2,t}(:,r)),'b.');
    %x=[min(f_eer(myout{c}.fratio{1,t}(:,r))), max(f_eer(myout{c}.fratio{2,t}(:,r)))];
    plot((myout{c}.fratio{1,t}(:,r)), (myout{c}.fratio{2,t}(:,r)),'b.');
    x=[min((myout{c}.fratio{1,t}(:,r))), max((myout{c}.fratio{2,t}(:,r)))];
    axis tight;
    hold on; plot(x,x,'r-');
    a=get(gca,'xticklabel'); a=cell(1,length(a)); set(gca,'xticklabel',a);
    a=get(gca,'yticklabel'); a=cell(1,length(a)); set(gca,'yticklabel',a);
    title(mytxt{t});
  end;
end;
for c=1:length(myout),
  figure(c);
  fname = sprintf('../Pictures/main_analyse_rerun_cri%d.eps',c);
  print('-depsc2',fname);
end;

%just check
tata=[];
for c=1:length(myout),
  tata=[tata;mean(abs(myout{c}.mean_error(:,chosen(c)))), mean(myout{c}.corr(:,chosen(c)))];
end;


%bias and corr analysis
%low bias (around zero) and high correlation are good properties
a={'F-ratio', 'c. F-ratio','F-norm ratio','c. F-norm ratio'};

subplot(2,1,1);set(gca,'fontsize',14);
boxplot([myout{1}.corr myout{2}.corr(:,3), myout{3}.corr myout{4}.corr])
ylabel('correlation');
xlabel('');
set(gca,'xticklabel',a);
title('(a)');

subplot(2,1,2);set(gca,'fontsize',14);cla; hold on;
boxplot([myout{1}.mean_error myout{2}.mean_error(:,3), myout{3}.mean_error myout{4}.mean_error])
set(gca,'xticklabel',a);
ylabel('bias');
xlabel('');
title('(b)');
plot([0;4.5], [0;0],'k--');
print('-depsc2','../Pictures/main_analyse_rerun_fratio-corr-bias.eps');
