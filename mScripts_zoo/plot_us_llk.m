function plot_us_llk(data, param, t, d, k, n)
%data of protocol p and expert b
%param{t} t  is one of 1:13
%data set d
%# of users: n

t_=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2)
    t_=t_+1;
    cfg(t_,:) = [p b];
  end;
end;
p=cfg(t,1)
b=cfg(t,2)

min_ = min( [data{p}.dset{d,1}(:,b);data{p}.dset{d,2}(:,b)]);
max_ = max( [data{p}.dset{d,2}(:,b);data{p}.dset{d,2}(:,b)]);
axis_ = [min_ max_];

scores = linspace(min_,max_,1000)';

%mov = avifile('example.avi','quality',90);
hold on;
signs = {'r-','-b'};
for j=1:n,
  pdf_(:,k) = cmvnpdf(scores,param{t}.mu{d,k}(j),param{t}.sigma{d,k}(j) .^2 );
  %pdf_(:,2) = cmvnpdf(scores,param{t}.mu{d,}(j),param{t}.sigma{d,2}(j)  );
  %plot(scores,pdf_(:,1),'r-');
  plot(scores,pdf_(:,k),signs{k});
end;  
xlabel('scores');
ylabel('likelihood');
%pdf_(:,1) = cmvnpdf(scores,param{t}.all.mu(d,1),param{t}.all.sigma(d,1) );
%pdf_(:,2) = cmvnpdf(scores,param{t}.all.mu(d,2),param{t}.all.sigma(d,2) );
%plot(scores,pdf_(:,1),'m-','linewidth',2);
%plot(scores,pdf_(:,2),'k-','linewidth',2);

