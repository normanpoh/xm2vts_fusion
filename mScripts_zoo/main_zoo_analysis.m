%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% load data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath ../mScripts

initialise;
for p=1:2,
  [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
  [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
  if p==1,
    for d=1:2,for k=1:2,
        data{p}.dset{d,k} = [ data{p}.dset{d,k} ...
          tanh_inv(data{p}.dset{d,k}(:,4)) tanh_inv(data{p}.dset{d,k}(:,5))];
        order = [1 2 3 6 7 8 4 9 5 10];
        data{p}.dset{d,k}(:,[1:10])= data{p}.dset{d,k}(:, order);

      end;
    end;
    %rearrange
  end;
  %delete the first one
  for d=1:2,for k=1:2,
      data{p}.dset{d,k}(:,1) = [];
    end;
  end;
end;
systype = [1 1 2 2 2  1 1 1 1 1  2 2 2];
syslabel={'(F,DCTs,GMM)', '(F,DCTb,GMM)', '(S,LFCC,GMM)', '(S,PAC,GMM)', '(S,SSC,GMM)',...
	  '(F,DCTs,MLP)','(F,DCTs,iMLP)','(F,DCTb,MLP)','(F,DCTb,iMLP)', '(F,DCTb,GMM)',...
    '(S,LFCC,GMM)', '(S,PAC,GMM)', '(S,SSC,GMM)'};
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% calculate param
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
t=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2)
    txt = sprintf('Experiment P%d:%d',p,b);fprintf(1,'%s\n',txt);
    t=t+1;
    protocol(t)=p;

    [param{t},size_dset{p}] = cal_clientd_param(data{p}, b);
  end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% draw US llk (optional)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
d=2; k=2;
for t=3:4,
  figure(t);cla;
  plot_us_llk(data, param, t, d, k, 200);
end;

d=2; 
t=1;
t=3;
t=4;
figure(t);cla;set(gca,'fontsize',12);
for k=1:2,
  plot_us_llk(data, param, t, d, k, 200);
end;
fname = sprintf('Pictures/main_zoo_analysis_t%d_d%d.eps',t,d);
print('-depsc2',fname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% process parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
%mean( (param{t}.all.mu(d,k) - param{t}.mu{d,k}).^2)
%mean(param{t}.sigma{d,k} .^ 2)/200
%param{t}.all.sigma(d,k) ^2 

for t=1:13,
  for d=1:2,
    for k=1:2,
      
      tmp =[param{t}.sigma{d,k}' .^ 2 ... %intra-client variance
        (param{t}.all.mu(d,k) -param{t}.mu{d,k})' .^2 ... %inter-client variance
        param{t}.sigma_wrt_global_mu{d,k}' .^ 2 %sum of two
        ];

      %check
      %[tmp(:,1)/2 + tmp(:,2)  tmp(:,3)]
      %[tmp(:,1)/2  tmp(:,3) - tmp(:,2)]
      %N_ = 2;
      %tmp_p_prime = (N_ * tmp_p -1) ./ (N_ -1);
      mystat.BMI{d,k}(:,t) = [tmp(:,2) ./ tmp(:,3)];
      mystat.V_B{d,k}(:,t) = [tmp(:,2)];
      mystat.V_tot{d,k}(:,t) = [tmp(:,3)];
    end;
  end;
end;

% for p=1:2,
%   for d=1:2, for k=1:2,
%       N(d,k,p) = length(find(data{p}.label{d,k}==371))
%     end;
%   end;
% end;
%selected = [find(systype==1) find(systype==2)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% plot BMI charts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

selected = 1:13;

% N_ = zeros(13,1);
% for d=1:2, for k=1:2,
%     N_(find(protocol==1)) = N(d,k,1);
%     N_(find(protocol==2)) = N(d,k,2);
%     subplot(2,2,k);
%     set(gca,'fontsize',14);
%     p = stat{d,k}(selected,1) ./ stat{d,k}(selected,2);
%     p_prime = (N_ .* p -1) ./ (N_ -1);
%     barh(p_prime*100)
%     set(gca,'yticklabel',{syslabel{selected}});
%     axis tight;
%     axis_=axis;    axis_(1:2) = [0 100]; axis(axis_);
%     xlabel('zoo index [%]');
%     ylabel('Data sets');
%     fname = sprintf('Pictures/main_zoo_analysis_zoo_index_d%d_k%d.eps',d,k);
%     %print('-depsc2',fname);
%   end;
% end;


N_ = zeros(13,1);
for d=1:2, for k=1:2,
    %subplot(2,2,k);
    tmp_ = [
      mean(mystat.BMI{d,k});
      mean(mystat.BMI{d,k}) + std(mystat.BMI{d,k});
      mean(mystat.BMI{d,k}) - std(mystat.BMI{d,k}) ];
    %boxplot(tmp_ * 100, 'notch', 'on','orientation', 'horizontal')
    %boxplot(mystat.BMI{d,k}* 100, 'notch', 'on','orientation', 'horizontal')
    boxplot(tmp_* 100, 'notch', 'on','orientation', 'horizontal')
    set(gca,'ydir','reverse')
    set(gca,'yticklabel',{syslabel{selected}});
    %axis tight;
    axis_=axis;    axis_(1:2) = [0 100]; axis(axis_);
    xlabel('zoo index [%]');
    ylabel('Data sets');
    fname = sprintf('Pictures/main_zoo_analysis_zoo_index_d%d_k%d_boxplot.eps',d,k);
    %print('-depsc2',fname);
  end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% plot correlation under mismatched
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k=1:2,
  figure(k); set(gca,'fontsize',16);
  %plot(median(mystat{1,k}),median(mystat{2,k}),'+','markersize',10);
  plot(mean(mystat.BMI{1,k})*100,mean(mystat.BMI{2,k})*100,'+','markersize',10);
  xlabel('zoo index (dev) [%]');
  ylabel('zoo index (eva) [%]');
  text(mean(mystat.BMI{1,k})*100,mean(mystat.BMI{2,k})*100,syslabel);
  fname = sprintf('Pictures/main_zoo_analysis_zoo_index_%d_corr.eps',k);
  print('-depsc2',fname);
end;

for k=1:2,
  tmp = [mean(mystat{1,k}); mean(mystat{2,k})];
  corrcoef(tmp')
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% plot stability analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k=1:2,
  figure(k); set(gca,'fontsize',16);
  cla; hold on;
  %plot(median(mystat{1,k}),median(mystat{2,k}),'+','markersize',10);
  plot(mean(mystat.V_B{1,k}),mean(mystat.V_B{2,k}),'+','markersize',10);
  %plot(mean(mystat.V_tot{1,k}),mean(mystat.V_tot{2,k}),'ro','markersize',10);
  xlabel('Between-client variance(dev)');
  ylabel('Between-client variance(eva)');
  %text(mean(mystat.V_B{1,k})*100,mean(mystat.V_B{2,k})*100,syslabel);
  fname = sprintf('Pictures/main_zoo_analysis_Vb_%d_corr.eps',k);
  print('-depsc2',fname);
end;

for k=1:2,
  figure(k+2); set(gca,'fontsize',16);
  %plot(median(mystat{1,k}),median(mystat{2,k}),'+','markersize',10);
  plot(mean(mystat.V_tot{1,k}),mean(mystat.V_tot{2,k}),'+','markersize',10);
  xlabel('Total variance(dev)');
  ylabel('Total variance(eva)');
  %text(mean(mystat.V_tot{1,k})*100,mean(mystat.V_tot{2,k})*100,syslabel);
  fname = sprintf('Pictures/main_zoo_analysis_Vtot_%d_corr.eps',k);
  print('-depsc2',fname);
end;

for k=1:2,
  corrcoef([mean(mystat.V_B{1,k})' mean(mystat.V_B{2,k})'])
  corrcoef([mean(mystat.V_tot{1,k})' mean(mystat.V_tot{2,k})'])
end;

%selected = [find(systype==1) find(systype==2)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fnorm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

get_fnorm_scores;

for d=1:2,
  t=0;
  for p=1:2,
    for b=1:size(data{p}.dset{1,1},2),
      t=t+1;
      eer_(t,1,d) = wer(fdata{p}.dset{d,1}(:,b),fdata{p}.dset{d,2}(:,b));
      eer_(t,2,d) = wer(data{p}.dset{d,1}(:,b),data{p}.dset{d,2}(:,b));
    end;
  end;
end;
% 
% [eer_f(:,1) eer_b(:,1)]

t=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2)
    txt = sprintf('Experiment P%d:%d',p,b);fprintf(1,'%s\n',txt);
    t=t+1;
    protocol(t)=p;

    [fparam{t},size_dset{p}] = cal_clientd_param(fdata{p}, b);
  end;
end;

for t=1:13,
  for d=1:2,
    for k=1:2,
      
      tmp =[fparam{t}.sigma{d,k}' .^ 2 ... %intra-client variance
        (fparam{t}.all.mu(d,k) -fparam{t}.mu{d,k})' .^2 ... %inter-client variance
        fparam{t}.sigma_wrt_global_mu{d,k}' .^ 2 %sum of two
        ];

      mystatf.BMI{d,k}(:,t) = [tmp(:,2) ./ tmp(:,3)];
      mystatf.V_B{d,k}(:,t) = [tmp(:,2)];
      mystatf.V_tot{d,k}(:,t) = [tmp(:,3)];
    end;
  end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plot BMI chart for fnorm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
selected = 1:13;
for d=1:2, for k=1:2,
    %subplot(2,2,k);
    figure; set(gca,'fontsize',16);
    tmp_ = [
      mean(mystatf.BMI{d,k});
      mean(mystatf.BMI{d,k}) + std(mystatf.BMI{d,k});
      mean(mystatf.BMI{d,k}) - std(mystatf.BMI{d,k}) ];
    %boxplot(tmp_ * 100, 'notch', 'on','orientation', 'horizontal')
    %boxplot(mystat.BMI{d,k}* 100, 'notch', 'on','orientation', 'horizontal')
    boxplot(tmp_* 100, 'notch', 'on','orientation', 'horizontal')
    set(gca,'ydir','reverse');
    set(gca,'yticklabel',{syslabel{selected}});
    %axis tight;
    axis_=axis;    axis_(1:2) = [0 100]; axis(axis_);
    xlabel('zoo index [%]');
    ylabel('Data sets');
    fname = sprintf('Pictures/main_zoo_analysis_zoo_index_d%d_k%d_boxplot_fnorm.eps',d,k);
    print('-depsc2',fname);
  end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% compare before and after fnorm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k=1:2,
  figure(k); set(gca,'fontsize',14);
  barh([mean(mystat.BMI{1,k})' mean(mystatf.BMI{2,k})']*100)
  set(gca,'yticklabel',{syslabel{selected}});
  set(gca,'ydir','reverse');
  axis_=axis;    axis_(1:2) = [0 100]; axis(axis_);
  xlabel('zoo index [%]');
  ylabel('Data sets');
  legend('before Fnorm', 'after Fnorm');
  set(gca,'ydir','reverse')
  fname = sprintf('Pictures/main_zoo_analysis_zoo_index_%d_corr_orig_fnorm.eps',k);
  print('-depsc2',fname);
end;

%no obvious relationship
plot(eer_(:,1,2) ./eer_(:,2,2) - 1, mean(mystatf.BMI{2,1}) +  mean(mystatf.BMI{2,2}),'+');

