function nscores = tensor_prod_scores(scores, col_index1, col_index2)

nscores = zeros(size(scores,1), length(col_index1)*length(col_index2));
max_= max(max(col_index1), max(col_index2));
if max_ > size(scores,2),
  error('index out of bound');
end;
t=0;
for i = col_index1,
  for j = col_index2,
    t=t+1;
    nscores(:,t) = scores(:,i) .* scores(:,j);
  end;
end;