function local = get_user_bayesS(expe, chosen, data_set, toplot)

if (nargin < 2),
  chosen = [1:2];
end;

if (nargin < 3|length(data_set)==0),
  data_set = 1; %by default, local bayesS is derived from the dev
                %data set
                
end;

if (nargin < 4),
  toplot = 0;
end;

%go through each id
for d=1:2,for k=1:2,
    com.dset{d,k} = zeros(size(expe.dset{d,k},1),1);
  end;
end;

%get the model label
model_ID = unique(expe.label{1,1});

%get client independent cov
for id=1:size(model_ID,1),
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      index{d,k} = find(expe.label{d,k} == model_ID(id));
      if (size(index{d,k},1)==0),
        warning([ 'size is zero for id ', num2str(id)]);
      end;
      tmp{id}.dset{d,k} = expe.dset{d,k}(index{d,k},chosen);
    end;
  end;
end;

%collect global info:
param = VR_analysis(expe.dset{1,1},expe.dset{1,2});

common.bayesS(1).mu = param.mu_I';
common.bayesS(2).mu = param.mu_C';
common.bayesS(1).sigma = param.cov_I;
common.bayesS(2).sigma = param.cov_C;

for id=1:size(model_ID,1),
  d=data_set;
  for k=1:2,
    if (size(tmp{id}.dset{d,k},1)==0),
      %no info available at all
      local{id}.bayesS(k).sigma = zeros(2);
      local{id}.bayesS(k).mu = [0 0]';
      local{id}.status(k) = 0;
    elseif (size(tmp{id}.dset{d,k},1)==1),
      %with one example, we cannot calculate cov
      %but we can calculate the mean at least!
      local{id}.bayesS(k).sigma = zeros(2);
      %mu is the example itself!
      local{id}.bayesS(k).mu = tmp{id}.dset{d,k}';
      local{id}.status(k) = 1;
    elseif (size(tmp{id}.dset{d,k},1)==2),
      %with two examples, we cannot calculate cov but we can calculate
      %the mean at least AND variance!
      local{id}.bayesS(k).sigma = cov(tmp{id}.dset{1,k});
      %reset the covariance elements
      local{id}.bayesS(k).sigma(1,2) = 0;
      local{id}.bayesS(k).sigma(2,1) = 0;
      local{id}.bayesS(k).mu = mean(tmp{id}.dset{1,k})';
      local{id}.status(k) = 2;
    else
      %with 3 or more examples, we can estimate cov
      local{id}.bayesS(k).sigma = cov(tmp{id}.dset{1,k});
      local{id}.bayesS(k).mu = mean(tmp{id}.dset{1,k})';
      local{id}.status(k) = 3;
    end;
    local{id}.bayesS(k).weight = 1;
  end;
end;

%visualise
hold on;
for id=1:size(model_ID,1),
  %if (local{id}.status(2) == 3),
    for k=1:2,
      common.bayesS(k).sigma = local{id}.bayesS(k).sigma;
    end;
    %if (size(tmp{id}.dset{1,2},1)>2),
    draw_theory_bayesS(local{id}.bayesS);
    %pause;
  %else
  %  fprintf(1,'not enough genuine data to plot for the %d-th user\n',id);
  %end;
end;
%draw_empiric(expe.dset{1,1}, expe.dset{1,2});
