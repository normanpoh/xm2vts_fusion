cd /home/learning/norman/xm2vts_fusion/mScripts_zoo

load_scores

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% load protocols
clear cfg

%define the experimental protocols
cfg.p{1}=[];
for t1= [ 1 2 7 9],
  for t2 = [3 4 5],
    cfg.p{1} = [cfg.p{1}; t1 t2];
  end;
end;
cfg.p{2} = [1 2; 1 3; 1 4];

for p=1:2,
  for row = 1:length(cfg.p{p}),
    cfg.p{p}(row,:)
  end;
end;
%% create labels
t=0;
for p=1:2,
  for r = 1:size(cfg.p{p},1),
    
    t=t+1;
    chosen = cfg.p{p}(r,:);
    if p==1,
      sys_bline{t} = {syslabel{chosen}};
    else
      sys_bline{t} = {syslabel{chosen+9}};
    end;
  end;
end;
%% load fscores
get_fnorm_scores

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%most important data are:
% - data 
% - fdata

signs = {'bx-','bd--','b*-','r+-','rs--','r^-','kh--','kv-'};
lwidth = [ 1 1 2 1 1 2 2 2];
leg={'orig_1', 'orig_2', 'orig_{com}', 'fnorm_1', 'fnorm_2', 'fnorm_{com}', 'fnorm_{OR}','fnorm_{OR}^{apost}'};

%% now perform fusion using OR-switcher
clear param out nexpe myout eer_;
t=0;
for p=1:2,
  for r = 1:size(cfg.p{p},1),

    t=t+1;
    chosen = cfg.p{p}(r,:);

    for d=1:2, for k=1:2,
        expe.dset{d,k} = fdata{p}.dset{d,k}(:,chosen);
        expe.dset{d,k}(:,3) =  mean(expe.dset{d,k},2);
      end;
    end;
    expe.label = data{p}.label;

    %myparam = VR_analysis(expe.dset{1,1},expe.dset{1,2})
    %VR_draw(expe.dset{1,1},expe.dset{1,2},1,myparam)

    %orig_COM
    [gmm] =  fusion_gmm(data{p}, chosen, [2 6],[],0);
    %fnorm_COM
    [gmmf_] =  fusion_gmm(expe, [1 2], [2 6],[],0);
    %fnorm_1
    nbayesS = bayes_project(gmmf_.bayesS, [1 0]');
    [gmmf{1}] = fusion_gmm(expe, 1, [], nbayesS);
    %fnorm_2
    nbayesS = bayes_project(gmmf_.bayesS, [0 1]');
    [gmmf{2}] = fusion_gmm(expe, 2, [], nbayesS);
    %fnorm_COM
    gmmf{3} = gmmf_;

    %calculate the US-param
    for i=1:3,
      [param{i}] = cal_clientd_param(expe,i);
    end;
    gmmf{3}.label = data{p}.label;
    [tmp, epc_cost] = fusion_clientd_check(gmmf{3}, 1, 'f-norm-simple');
    tmp.label =  data{p}.label;
    [param{4}] = cal_clientd_param(tmp,1);
    
    %if(1==0),
    %  i=3;
    %  for d=1:2,for k=1:2,
    %	  param{i}.mu{d,k} = (param{1}.mu{d,k} + param{2}.mu{d,k})/2;
    %	  param{i}.sigma{d,k} = sqrt((param{1}.sigma{d,k}.^ 2 + param{2}.sigma{d,k}.^2)/4);
    %	end;
    %  end;
    %end;
    
    %i=3;
    %%apply f-norm
    %gmmf{i}.label = fdata{p}.label;
    %[tmp, epc_cost] = fusion_clientd_check(gmmf{i}, 1, 'f-norm-simple');
    %%then calculate criterion
    %tmp.label = fdata{p}.label;
    %[param{i}] = cal_clientd_param(tmp,1);
      
    %calculate the F-ratio
    clear tmp
    for i=1:3,
      for d=1:2,
        out{p}.fratio{d}(i,:) = ...
          1 ./  (param{i}.sigma{d,1});
        %(param{i}.mu{d,2} - param{i}.mu{d,1}) ...
        %    ./  (param{i}.sigma{d,1});% + param{i}.all.sigma(d,2));
        tmp(d,i) = 	1 ./  (param{i}.all.sigma(d,1)); %(param{i}.all.mu(d,2) - param{i}.all.sigma(d,1)) / (param{i}.all.sigma(d,1) + param{i}.all.sigma(d,2));
      end;
    end;

    f_eer(mean(out{p}.fratio{1}'))

    %check consistency in terms of correlation
    if (1==0),
      figure(1);
      for i=1:4,
        subplot(2,2,i);cla; hold on;
        plot(out{p}.fratio{1}(i,:),out{p}.fratio{2}(i,:),'.');
        x=linspace(min([out{p}.fratio{1}(i,:),out{p}.fratio{2}(i,:)]), max([out{p}.fratio{1}(i,:),out{p}.fratio{2}(i,:)]),3);
        plot(x,x); grid on;
      end;
      
      %debug
      for t_1=1:2,
        for t_2=1:2,
          subplot(2,2,(t_1-1)*2+t_2); cla; hold on;
          plot(out{p}.fratio{t_1}(3,:),out{p}.fratio{t_2}(4,:),'+')
          plot([3 11], [3 11],'-');
        end;
      end;
    
      %plot distribution
      cla; hold on;
      for i=1:3,
        [f,x]=ksdensity(out{p}.fratio{d}(i,:));
        plot(x,f,signs{i});
      end;
      legend('face','speech', 'com (theo)', 'com (emp)');
      
      tmp = corrcoef([out{p}.fratio{1}(3:4,:)',out{p}.fratio{2}(3:4,:)'])
      
      cla; hold on;
      plot(out{p}.fratio{2}(1,:),out{p}.fratio{2}(2,:),'+');
      axis([2 12 2 12]);
      plot([2 12], [2 12]);
    end;

    %determine users to combine or not
    cut_rate = [eps .1:.1:1] * 100;
    user_seq_=cell(2,3,length(cut_rate));
    for d=1:2,
      sorted_fratio = sort(out{p}.fratio{d}(3,:));
      cut_fratio_list = prctile( sorted_fratio,cut_rate);
      cut_fratio_list(end) = cut_fratio_list(end) + 1;
      for c=1:length(cut_rate),
        cut_fratio = cut_fratio_list(c);
        %(round((cut_rate(c))*200));
        
        %users that use multimodal biometrics
        user_seq_{d,3,c} = find(out{p}.fratio{d}(3,:) < cut_fratio );
        %users that use a single one
        users_left = find(out{p}.fratio{d}(3,:) >= cut_fratio );
        tmp = out{p}.fratio{d}(1:2,users_left);
        user_seq_{d,1,c} = users_left(find(tmp(1,:) > tmp(2,:)));
        user_seq_{d,2,c} = setdiff(users_left,user_seq_{d,1});
      end;
    end;

    clear gexpe gexpe_oracle 
    for c=1:length(cut_rate),
      %reset
      for d=1:2,for k=1:2,
          gexpe{c}.dset{d,k}=[];
          %gexpe_oracle{c}.dset{d,k}=[];
        end;
      end;
      
      %append data back
      for i=1:3,
        for d=1,
          gmmf{i}.label = data{p}.label;
          texpe{d,i} = filter_users(gmmf{i}, 1, user_seq_{d,i,c});
        end;
        for d=1:2,for k=1:2,
            gexpe{c}.dset{d,k}=[gexpe{c}.dset{d,k}; texpe{1,i}.dset{d,k}];%gmmf{i}.dset{d,k}];
            %gexpe_oracle{c}.dset{d,k}=[gexpe_oracle{c}.dset{d,k}; texpe{2,i}.dset{d,k}];
          end;
        end;
      end;
      fprintf(1,'.');
    end;
    
    figure(1);set(gca,'fontsize',12);
    for c=1:length(cut_rate),
      eer_(t,c) = wer(gexpe{c}.dset{2,1},gexpe{c}.dset{2,2},[],2,[],c);
    end;
    eer_(t, 12) = wer(gmm.dset{2,1},gmm.dset{2,2},[],2,[],13);
    eer_(t, 13) = wer(gmmf{3}.dset{2,1},gmmf{3}.dset{2,2},[],2,[],14);
    eer_(t, 14) = wer(data{p}.dset{2,1}(:,chosen(1)),data{p}.dset{2,2}(:,chosen(1)),[],2,[],15);
    eer_(t, 15) = wer(data{p}.dset{2,1}(:,chosen(2)),data{p}.dset{2,2}(:,chosen(2)),[],2,[],16);
    
    myleg=cellstr(num2str(  (100-round(cut_rate'))/100 ));
    if p==1,
      sys_ = {syslabel{chosen}};
    else
      sys_ = {syslabel{chosen+9}};
    end;
    myleg={myleg{:}, 'orig_{com}','fnorm_{com}',sys_{:}};
    legend(myleg);
    
    %Make_DET(0.1);
    
    fname = sprintf('Pictures_/main_fusion_DET_%02d.eps',t);
    print('-depsc2',fname);
    
    %evaluate in terms of EPC
    %baseline
    for i=1:2,
      [orig{i}.epc.dev, orig{i}.epc.eva, epc_cost]  = epc(data{p}.dset{1,1}(:,chosen(i)),data{p}.dset{1,2}(:,chosen(i)),data{p}.dset{2,1}(:,chosen(i)),data{p}.dset{2,2}(:,chosen(i)), n_samples,epc_range);
      myout{t}.res{i} = orig{i}.epc;
    end;
    for c=1:length(cut_rate),
      [gexpe{c}.epc.dev, gexpe{c}.epc.eva, epc_cost]  = epc(gexpe{c}.dset{1,1}, gexpe{c}.dset{1,2}, gexpe{c}.dset{2,1}, gexpe{c}.dset{2,2}, n_samples,epc_range);
    end;
    
    myout{t}.res{3} = gmm.epc;
    myout{t}.res{4} = gmmf{1}.epc;
    myout{t}.res{5} = gmmf{2}.epc;
    myout{t}.res{6} = gmmf{3}.epc;

    for c=1:length(cut_rate),
      myout{t}.res{6+c} = gexpe{c}.epc;
    end;
    myout{t}.user_seq = user_seq_;


  end;
end;

save main_fusion_rerun myout epc_cost eer_
%% for replotting purpose
load main_fusion_rerun myout epc_cost eer_ sys_bline

bline = eer_(:,12);
mean_ = mean(eer_(:,[14 15]),2);
min_ = min(eer_(:,[14 15])')';

rel_error_reduction =  [ eer_(:,[1:11]), mean_, min_  ] ./ repmat(bline,1,13)  -1;
boxplot(rel_error_reduction)
axis([0.5 13.5 -1.2 8.6])
myleg=cellstr(num2str(  (100-round(cut_rate'))/100 ));
set(gca,'xtick',1:13);
set(gca,'xticklabel', {myleg{:}, 'mean', 'min'});

%%
%tmp = [ eer_(:,[1:11]), mean_, min_  ];
cut_rate = 0:10:100;
clf;
for t=1:15,
  subplot(3,5,t);hold on;
  x=(100-round(cut_rate'))/100;
  plot(x, eer_(t,[1:11])*100,'bo-');
  plot([0 1], [eer_(t,12) eer_(t,12)]*100,'r--','linewidth',2);
  %plot([0 1], [eer_(t,13) eer_(t,13)]*100,'g--');
  plot([0 1], [eer_(t,14) eer_(t,14)]*100,'k--');
  plot([0 1], [eer_(t,15) eer_(t,15)]*100,'k-');
  title(sprintf('(%s) %s+%s',char(96+t), sys_bline{t}{1},sys_bline{t}{2}));
  if t==11,
    xlabel('Pruning rate (r)');
    ylabel('EER(%)');
  end;
end;
legend('Client-specific(r)','Fusion', 'Face','Speech');
fname = sprintf('Pictures/main_fusion_table.eps');
print('-depsc2',fname);
%% text
tmp = [eer_ bline] *100;
for i=1:length(tmp),
  txt=sprintf('%d',i);
  for t=1:5,
    if tmp(i,6) > tmp(i,t),
      txt=sprintf('%s & {\\bf %1.2f}',txt,tmp(i,t));
    else
      txt=sprintf('%s & %1.2f',txt,tmp(i,t));
    end;
  end;
  fprintf(1,'%s & %1.2f \\\\ \n',txt, tmp(i,6));
end;


set(gca,'fontsize',16);
plot(bline*100, eer_(:,5)*100,'+','markersize',10);
xlabel('Baseline EER (%)');
ylabel('F-normalized EER (%)');
hold on;
plot([0 2.5],[0 2.5],'k');
grid on;
axis square;
print('-depsc2','Pictures/EER_improvement.eps');

[f,x] = ksdensity(rel_error_reduction(:,5));
plot(x,f);


signs = {'bx-','bd--','b*-','r+-','rs--','r^-','kh--','kv-','k^--','ks-','o--'};
lwidth = [ 1 1 2, 1 1 2, 2 2 2 2];
leg={'orig_1', 'orig_2', 'orig_{com}', 'fnorm_1', 'fnorm_2', ...
     'fnorm_{com}', 'fnorm_{OR}^{0.6}','fnorm_{OR}^{0.7}','fnorm_{OR}^{0.8}','fnorm_{OR}^{0.9}'};
     

expe_list= 1:15;
mylist=1:10,
for m=mylist,
  [cfg.res{m},pNI,pNC] = epc_global_custom(myout, m, expe_list);
end;

mylist=[1:3];
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1,0);
legend('face','speech','combined');
fname = sprintf('Pictures/main_fusion_demo.eps');
print('-depsc2',fname);

mylist=[3 6 7:10];
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1,0);
legend(leg{mylist})

fname = sprintf('../Pictures/main_fusion_DET_all_noOR.eps');
fname = sprintf('../Pictures/main_fusion_DET_all.eps');
print('-depsc2',fname);

rel_error_reduction * 100
tmp = rel_error_reduction<0

tmp= sum(tmp')

%plot grouped curve
mylist=1:10;
tlist=[3 6 7:10];
for i=1:5,
  myexpe_list = find(tmp==i);
  clear tcfg
  for m=mylist,
    [tcfg.res{m},pNI,pNC] = epc_global_custom(myout, m, myexpe_list);
  end;
  figure(i);
   plot_all_epc(epc_cost(:,1),leg,signs, tcfg, tlist,lwidth, 14,1,0);
  legend(leg{tlist})
  Make_DET(0.05)
end;

for i=1:5,
  length(find(tmp==i))
end;

[(1:15)' tmp']

%computational savings
(1+cut_rate)/2*100