get_fnorm_scores
%% try
clear BMI;
for p=1:2,
  
  [out,size_dset] = cal_clientd_param_multivariate(fdata{p});
  
  for d=1:2,for k=1:2,
      fBMI{p}{d,k} = zeros(size(out.cov_B{d,k},1),size(out.cov_B{d,k},2),200);
      for t=1:200,
        fBMI{p}{d,k}(:,:,t) = out.cov_B{d,k}(:,:,t) ./ out.cov_tot{d,k}(:,:,t);
      end;
    end;
  end;
end;

for d=1:2,for k=1:2,
    mean_fBMI{d,k} = [diag(mean(fBMI{1}{d,k},3)) ; diag(mean(fBMI{2}{d,k},3))];
    std_fBMI{d,k} = [diag(std(fBMI{1}{d,k},1,3)) ; diag(std(fBMI{2}{d,k},1,3))];
  end;
end;

%% 
for p=1:2,
  for d=1:2,for k=1:2,
      figure;
      imagesc(mean(fBMI{p}{d,k},3))
    end;
  end;
end;


%% plot cross-session
signs = {'b+','r+'};
for k=1:2,
  figure(k);
  set(gca,'fontsize',16); cla; hold on;
  for p=1:2,
    plot(diag(mean(fBMI{p}{1,k},3)),diag(mean(fBMI{p}{2,k},3)),signs{p})
  end;
end;


%% plot BMI + var(BMI)
t=4;
for d=2:2, %for d=1:2,
  for k=1:2,
    t=t+1;    figure(t); set(gca,'fontsize',14);
    tmp = [mean_fBMI{d,k} mean_fBMI{d,k} + std_fBMI{d,k} mean_fBMI{d,k} - std_fBMI{d,k}];
    boxplot(tmp','orientation','horizontal');
    axis_ = axis;
    axis_(1:2) = [0, 1]; axis(axis_);
    xlabel('zoo index [%]');
    ylabel('Data sets');
    set(gca,'yticklabel',{syslabel_new{1}{:} syslabel_new{2}{:}});
    set(gca,'ydir','reverse');
  end;
  fname = sprintf('Pictures/main_zoo_mvar_analysis_fdata_BMI_%d_%d.eps',d,k);
  print('-depsc2',fname);
end;
 
%%
figure(9); set(gca,'fontsize',14);
barh([mean_BMI{2,2} mean_fBMI{2,2}])
legend('before','after');
set(gca,'yticklabel',{syslabel_new{1}{:} syslabel_new{2}{:}});
set(gca,'ydir','reverse');
fname = sprintf('Pictures/main_zoo_mvar_analysis_fdata_C.eps',t,d);
print('-depsc2',fname);


figure(10); set(gca,'fontsize',14);
barh([mean_BMI{2,1} mean_fBMI{2,1}])
legend('before','after');
set(gca,'yticklabel',{syslabel_new{1}{:} syslabel_new{2}{:}});
set(gca,'ydir','reverse');
fname = sprintf('Pictures/main_zoo_mvar_analysis_fdata_I.eps',t,d);
print('-depsc2',fname);



