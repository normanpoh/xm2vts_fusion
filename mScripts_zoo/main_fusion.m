%run baseline
n_samples = 11;
epc_range = [0.1 0.9];

%make protocols

clear cfg

cfg.p{1}=[];
for t1= [ 1 2 7 9],
  for t2 = [3 4 5],
    cfg.p{1} = [cfg.p{1}; t1 t2];
  end;
end;
cfg.p{2} = [1 2; 1 3; 1 4];

%create fdata
get_fnorm_scores

if 1==0,
  t=0;
  for p=1:2,
    for b=1:size(data{p}.dset{1,1},2),
      t=t+1;
      subplot(3,5,t);
      myeer(t,1) = wer(data{p}.dset{2,1}(:,b),data{p}.dset{2,2}(:,b));%,[],2,[],1)
      myeer(t,2) = wer(fdata{p}.dset{2,1}(:,b),fdata{p}.dset{2,2}(:,b));%,[],2,[],2)
      drawnow;
    end;
  end;
  fname = sprintf('../Pictures/main_analyse_rerun_DET_all.eps');
  print('-depsc2',fname);

  %eer analysis
  figure;
  cla; hold on;
  plot(myeer(:,1)*100,myeer(:,2)*100,'+');
  x=[0, 7];hold on; plot(x,x);
  text(myeer(:,1)*100,myeer(:,2)*100,num2str([1:13]'));
  xlabel('original EER(%)');
  ylabel('EER of F-norm (%)');
end;

%wer(fdata{p}.dset{1,1}(:,b),fdata{p}.dset{1,2}(:,b),[],1)
%plot(epc_cost(:,1), out{1}{t}.epc.eva.hter_apri)
%hold on; plot(epc_cost(:,1), out{2}{t}.epc.eva.hter_apri,'r')

%filter away bad users
clear param myout;
nlist = 200:-20:20;
t=0;

n_samples = 100;
epc_range = [0.025 0.975];
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2),
    t=t+1;

    [param] = cal_clientd_param(fdata{p},b);
    d=1;
    fratio = 1 ./  (param.sigma{d,1});

    [tmp,user_seq] = sort(-fratio); %sort in ascending order
    %select users
    for n=1:length(nlist),
      nexpe = filter_users(fdata{p}, b, user_seq(1:nlist(n)));
      %apply LLR transformation to ensure better generalization
      [nexpe] = fusion_gmm(nexpe, 1);
      %wer(nexpe.dset{2,1},nexpe.dset{2,2},[],2,[],n);
      %drawnow;
      [tmp.epc.dev, tmp.epc.eva, epc_cost]  = epc(nexpe.dset{1,1}, nexpe.dset{1,2}, nexpe.dset{2,1}, nexpe.dset{2,2}, n_samples,epc_range);
      myout{t}.res{n} = tmp.epc;
      fprintf(1,'.');
    end;
    %legend(num2str(nlist'));
    %fname = sprintf('../Pictures/main_fusion_filtered_DET_%d.eps',t);
    %print('-depsc2',fname);
    fprintf(1,'|');
  end;
end;

style = {'-','--'};%,':'};
color = {'b', 'g', 'r', 'k'};%, 'm', 'y', 'k'};
i=0;
for c=1:length(color),
  for j=1:2, %repeat twice
    for s =1:length(style),
      i=i+1;
      signs{i}=sprintf('%s%s',color{c},style{s});
      lwidth(i) = j;
    end;
  end;
end;

use_ppndf=1;
for dev=[0 1];
  t_=0; figure;
  for t=1:13,
    %subplot(5,3,t);
    t_=t_+1;
    if t_>4,
      legend(cellstr(num2str(nlist')))
      t_=1;
      figure;
  end;
  subplot(2,2,t_);
  cla; hold on;
  for n=1:10,
    if dev
      [nfar, nfrr] = norminv_filtered(myout{t}.res{n}.dev.far, myout{t}.res{n}.dev.frr,use_ppndf);
    else
      [nfar, nfrr] = norminv_filtered(myout{t}.res{n}.eva.far_apri, myout{t}.res{n}.eva.frr_apri,use_ppndf);
    end;
    plot(nfar,nfrr,signs{n},'linewidth',lwidth(n));
  end;
  Make_DET;
  axis tight;
  axis square;
  txt = sprintf('[%d] %s',t,syslabel{t});
  title(txt);
  end;
  legend(cellstr(num2str(nlist')))
end;

for t=1:8,
  figure(t);
  fname = sprintf('Pictures/main_fusion_DET_filtered_%d.eps',t);
  print('-depsc2',fname);
end;

%custom plot
t=4;
for dev=[1 0];
  subplot(2,2,2-dev);
  cla; hold on;
  for n=1:10,
    if dev
      [nfar, nfrr] = norminv_filtered(myout{t}.res{n}.dev.far, myout{t}.res{n}.dev.frr,use_ppndf);
    else
      [nfar, nfrr] = norminv_filtered(myout{t}.res{n}.eva.far_apri, myout{t}.res{n}.eva.frr_apri,use_ppndf);
    end;
    plot(nfar,nfrr,signs{n},'linewidth',lwidth(n));
  end;
  Make_DET;
  axis tight;
  axis square;
  txt = sprintf('[%d] %s',t,syslabel{t});
  title(txt);
end;
subplot(2,2,1);title('dev');
subplot(2,2,2);title('eva');
legend(cellstr(num2str(nlist')))
fname = sprintf('Pictures/main_fusion_filtered_det_demo_4.eps');
print('-depsc2',fname);

















expe_list= 1:13;
for m=1:10,
  [fbline.res{m},pNI,pNC] = epc_global_custom(myout, m, expe_list);
end;

mysigns = {'bx-','bo--','r.-','rd--'};

leg =cellstr(num2str([1:10]'));

plot_all_epc(epc_cost(:,1),leg,signs, fbline, 1:10, lwidth,14,1,1); 
legend(num2str(nlist(1:3)'));
fname = sprintf('../Pictures/main_fusion_filtered_epc_all.eps');
print('-depsc2',fname);

%plot_all_epc(epc_cost(:,1),leg,mysigns, fbline, 1:3,14,0,1); legend(num2str(nlist(1:3)'));
fname = sprintf('../Pictures/main_fusion_filtered_DET_all.eps');
print('-depsc2',fname);

for t=1:13,
  subplot(3,5,t);
  plot_all_epc(epc_cost(:,1),leg,mysigns, myout{t}, 1:3,10,0,1); 
end;
legend(num2str(nlist(1:3)'));





%now perform fusion using OR-switcher
clear param out nexpe;
t=0;
for p=1:2,
  for r = 1:size(cfg.p{p},1),
    t=t+1;
    chosen = cfg.p{p}(r,:);
    for d=1:2, for k=1:2,
        expe.dset{d,k} = fdata{p}.dset{d,k}(:,chosen);
        expe.dset{d,k}(:,3) =  mean(expe.dset{d,k},2);
      end;
    end;
    expe.label = data{p}.label;

    for i=1:3,
      [param{i}] = cal_clientd_param(expe,i);
    end;
    
    for i=1:3,
      for d=1:2,
        %criterion 1
        %mu_adjusted = param{i}.mu{d,2} .* beta + param{i}.all.mu(d,2) .* (1-beta);
        %out{p}.fratio{d}(i,:) = (mu_adjusted - param{i}.mu{d,1}) ...
        %  ./  (param{i}.all.sigma(d,2) + param{i}.sigma{d,1});
        %criterion 2
        %out{p}.fratio{d}(i,:) = 1 ./  (param{i}.all.sigma(d,2) + param{i}.sigma{d,1});
        %criterion 3
        out{p}.fratio{d}(i,:) = 1 ./  (param{i}.sigma{d,1});
      end;
    end;

    %check consistency in terms of correlation
    %figure(1);
    %for i=1:3,
    %  subplot(1,3,i);cla; hold on;
    %  plot(out{p}.fratio{1}(i,:),out{p}.fratio{2}(i,:),'.');
    %  x=linspace(min([out{p}.fratio{1}(i,:),out{p}.fratio{2}(i,:)]), max([out{p}.fratio{1}(i,:),out{p}.fratio{2}(i,:)]),3);
    %  plot(x,x); grid on;
    %end;
    myout{t}.criterion(1,:) = out{p}.fratio{1}(3,:);
    myout{t}.criterion(2,:) = out{p}.fratio{2}(3,:);
    
    %check consistency in terms of correct ordering
    for d=1:2,
      [tmp, index{d}]=sort(out{p}.fratio{d});
      for t_=1:size(index{d},2),%over all users
        pos(d,t_) = find(index{d}(:,t_)==3); %find the thrid
                                             %position; the best one
      end;
    end;
    myout{t}.pos = pos;
    
    correct(t) = length(find(pos(1,:)-pos(2,:)==0))/size(pos,2);
    
    [gmm] =  fusion_gmm(data{p}, chosen, [2 6],[],0);

    [gmmf_] =  fusion_gmm(expe, [1 2], [2 6],[],0);

    nbayesS = bayes_project(gmmf_.bayesS, [1 0]');
    [gmmf{1}] = fusion_gmm(expe, 1, [], nbayesS);
    nbayesS = bayes_project(gmmf_.bayesS, [0 1]');
    [gmmf{2}] = fusion_gmm(expe, 2, [], nbayesS);
    gmmf{3} = gmmf_;
    
    %now, based on correctness, output the
    for d=1:2,for k=1:2,
        gexpe.dset{d,k}=[];
	gexpe_oracle.dset{d,k}=[];
      end;
    end;
    
    for i=1:3, 
      gmmf{i}.label = fdata{p}.label;
      %OR-switcher
      user_seq = find(pos(1,:)==i);
      texpe{i} = filter_users(gmmf{i}, 1, user_seq);

      usage(t,i) = length(unique(texpe{i}.label{1,2}));

      %oracle of OR-switcher
      user_seq = find(pos(2,:)==i);
      texpe_oracle{i} = filter_users(gmmf{i}, 1, user_seq);
    end;

    %append data back
    for i=1:3, 
      for d=1:2,for k=1:2,
          gexpe.dset{d,k}=[gexpe.dset{d,k}; texpe{i}.dset{d,k}];%gmmf{i}.dset{d,k}];
	  gexpe_oracle.dset{d,k}=[gexpe_oracle.dset{d,k}; texpe_oracle{i}.dset{d,k}];
	end;
      end;
    end;

    %plot DET
    %orig
    figure(1);
    wer(data{p}.dset{2,1}(:,chosen(1)), data{p}.dset{2,2}(:,chosen(1)),[],2,[],1)
    wer(data{p}.dset{2,1}(:,chosen(2)), data{p}.dset{2,2}(:,chosen(2)),[],2,[],2)
    wer(gmm.dset{2,1},gmm.dset{2,2},[],2,[],3) %gmm

    %%i=3; %f-norm with mean
    %%wer(expe.dset{2,1}(:,i),expe.dset{2,2}(:,i),[],2,[],4)

    wer(gmmf{1}.dset{2,1},gmmf{1}.dset{2,2},[],2,[],9) %f-norm 1
    wer(gmmf{2}.dset{2,1},gmmf{2}.dset{2,2},[],2,[],10) %f-norm 2
    wer(gmmf{3}.dset{2,1},gmmf{3}.dset{2,2},[],2,[],11) %f-norm gmm
    wer(gexpe.dset{2,1},gexpe.dset{2,2},[],2,[],12) %OR switcher(f-norm gmm)
    wer(gexpe_oracle.dset{2,1},gexpe.dset{2,2},[],2,[],13) %OR switcher-oracle(f-norm gmm)
    legend(leg{:});
    fname = sprintf('../Pictures/main_fusion_DET_%02d.eps',t);
    print('-depsc2',fname);

    %evalutate in terms of EPC
    %baseline
    for i=1:2,
      [orig{i}.epc.dev, orig{i}.epc.eva, epc_cost]  = epc(data{p}.dset{1,1}(:,chosen(i)),data{p}.dset{1,2}(:,chosen(i)),data{p}.dset{2,1}(:,chosen(i)),data{p}.dset{2,2}(:,chosen(i)), n_samples,epc_range);
      myout{t}.res{i} = orig{i}.epc;
    end;
    [gexpe.epc.dev, gexpe.epc.eva, epc_cost]  = epc(gexpe.dset{1,1}, gexpe.dset{1,2}, gexpe.dset{2,1}, gexpe.dset{2,2}, n_samples,epc_range);
    [gexpe_oracle.epc.dev, gexpe_oracle.epc.eva, epc_cost]  = epc(gexpe_oracle.dset{1,1}, gexpe_oracle.dset{1,2}, gexpe_oracle.dset{2,1}, gexpe_oracle.dset{2,2}, n_samples,epc_range);
    
    myout{t}.res{3} = gmm.epc;
    myout{t}.res{4} = gmmf{1}.epc;
    myout{t}.res{5} = gmmf{2}.epc;
    myout{t}.res{6} = gmmf{3}.epc;
    myout{t}.res{7} = gexpe.epc;
    myout{t}.res{8} = gexpe_oracle.epc;

    figure(2);
    plot_all_epc(epc_cost(:,1),leg,signs, myout{t}, 1:8,lwidth,14,1,1);
    legend(leg{:});
    fname = sprintf('../Pictures/main_fusion_epc_%02d.eps',t);
    print('-depsc2',fname);
    fprintf(1,'.');
  end;
end;

save main_fusion myout epc_cost
load main_fusion myout epc_cost

signs = {'bx-','bd--','b*-','r+-','rs--','r^-','kh--','kv-'};
lwidth = [ 1 1 2 1 1 2 2 2];
leg={'orig_1', 'orig_2', 'orig_{com}', 'fnorm_1', 'fnorm_2', 'fnorm_{com}', 'fnorm_{OR}','fnorm_{OR}^{apost}'};

expe_list= 1:15;
mylist=1:7
for m=mylist,
  [cfg.res{m},pNI,pNC] = epc_global_custom(myout, m, expe_list);
end;

plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1);
fname = sprintf('../Pictures/main_fusion_epc_all.eps',t);

print('-depsc2',fname);

mylist=[1:6];
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1,0);
fname = sprintf('../Pictures/main_fusion_DET_all_noOR.eps');
fname = sprintf('../Pictures/main_fusion_DET_all.eps');
print('-depsc2',fname);

b = [3 7];
hter_significant_plot(cfg.res{b(1)}, cfg.res{b(2)}, pNI, pNC,epc_cost(:,1));
subplot(2,1,1);legend(leg{b})
fname = sprintf('../Pictures/main_fusion_sigtest_3-7.eps',t);
print('-depsc2',fname);

b = [6 7];
hter_significant_plot(cfg.res{b(1)}, cfg.res{b(2)}, pNI, pNC,epc_cost(:,1));
subplot(2,1,1);legend(leg{b})
fname = sprintf('../Pictures/main_fusion_sigtest_6-7.eps',t);
print('-depsc2',fname);

b = [3 6];
hter_significant_plot(cfg.res{b(1)}, cfg.res{b(2)}, pNI, pNC,epc_cost(:,1));
subplot(2,1,1);legend(leg{b})
fname = sprintf('../Pictures/main_fusion_sigtest_3-6.eps',t);
print('-depsc2',fname);


%plot only F-norm
mylist=[1 2 4 5];
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist,lwidth, 14,1);
fname = sprintf('../Pictures/main_fusion_epc_uni.eps');
print('-depsc2',fname);
plot_all_epc(epc_cost(:,1),leg,signs, cfg, mylist, lwidth ,14,1,0);
fname = sprintf('../Pictures/main_fusion_DET_uni.eps');
print('-depsc2',fname);

%check the number of hits on 1 2 or 3
tata=[];
for t=1:15,
  for i=1:3,
    tata(i,t) = length(find(myout{t}.pos(1,:)==i));
  end;
end;
sum(tata')
[[cfg.p{1};cfg.p{2}] tata']
tata =tata';

subplot(2,1,1);
bar(tata(:,2:3))
xlabel('fusion tasks');
ylabel('usage frequency')
legend('speech', 'face+speech')
fname = sprintf('../Pictures/main_fusion_freq_mode2-3.eps',t);
print('-depsc2',fname);

sum(tata)/sum(sum(tata))*100