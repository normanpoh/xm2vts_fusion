%load and process data according to main_analyse_rerun
get_fnorm_scores

t=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2),
    t=t+1;

    [param] = cal_clientd_param(fdata{p},b);
    d=1;
    fratio = 1 ./  (param.sigma{d,1});

    [tmp,user_seq] = sort(-fratio); %sort in ascending order

    n_samples=100;
    x=linspace(-2,2,n_samples);
    
    cla; hold on;
    for i=1:200,
      f=draw_us_DET(x,[param.mu{d,1}(user_seq(i)) param.mu{d,2}(user_seq(i))], [param.sigma{d,1}(user_seq(i)) param.sigma{d,2}(user_seq(i))]);
      plot(norminv(f(:,1)),norminv(f(:,2)));
      Make_DET;
      %pause;
    end;
    
  end;
end;