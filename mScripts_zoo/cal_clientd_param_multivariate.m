function [out,size_dset] = cal_clientd_param_multivariate(expe)

%get the model label
model_ID = unique(expe.label{1,1});
for d=1:2,
  for k=1:2,
    out.all.mu{d,k} = mean(expe.dset{d,k});
    out.all.cov{d,k} = cov(expe.dset{d,k});
  end;
end;

%perform client-dependent normalisation
id_index = 0;
for id=1:size(model_ID,1),
  
  id_index = id_index + 1;
  
  %get the data set associated to the ID
  for d=1:2,for k=1:2,
      index = find(expe.label{d,k} == model_ID(id));
      tmp{d,k} = expe.dset{d,k}(index,:);
      size_dset{d,k}(id) = size(tmp{d,k},1);

      out.mu{d,k}(id_index,:) = mean(tmp{d,k}); %mu_j

      %within-class cov
      tmp_ = zeros(size(tmp{d,k},2),size(tmp{d,k},2),size(tmp{d,k},1));
      for t=1:size(tmp{d,k},1),
        tmp__ = tmp{d,k}(t,:) - out.mu{d,k}(id_index,:);
        tmp_(:,:,t) = tmp__' * tmp__;
      end;
      out.cov_W{d,k}(:,:,id_index) = mean(tmp_,3);

      %between-class cov
      tmp__ = out.mu{d,k}(id_index,:) - out.all.mu{d,k};
      out.cov_B{d,k}(:,:,id_index) =  tmp__' * tmp__;
      
      %total cov
      tmp_ = zeros(size(tmp_));
      for t=1:size(tmp{d,k},1),
        tmp__ = tmp{d,k}(t,:) - out.all.mu{d,k};
        tmp_(:,:,t) = tmp__' * tmp__;
      end;
      out.cov_tot{d,k}(:,:,id_index) = mean(tmp_,3);
    end;
  end;
  
end;

