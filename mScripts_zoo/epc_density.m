function [eva, dev, cost] = epc_density(FAR, FRR, x, n_samples,range);
%FAR and FRR are each Nx2 matrice where 
%FAR(:,1) is the first system and FAR(:,2) is the second system
%x is Nx1 matrice

% range : a vector of the form [a,b]
%         It analyses the range from a to b
%         default is [0,1]
% n_samples : the number of samples to take from a to b
if (nargin < 4),
  n_samples =11;
end;
if (nargin < 5),
  range = [0 1];
end;

c_fa = linspace(min(range),max(range),n_samples);
c_fr = 1-c_fa;
cost = [c_fa' c_fr'];

for i=1:n_samples,
  [tmp, min_index] = min( abs(cost(i,1) * FAR(:,1) - cost(i,2) * FRR(:,1)) );
  thrd_min = x(min_index);
  dev.wer_apost(i) = cost(i,1) * FAR(min_index,1) + cost(i,2) * FRR(min_index,1);
  dev.thrd_fv(i) = thrd_min;
  eva.hter_apri(i) = (FAR(min_index,2) +  FRR(min_index,2))/2;
  dev.hter_apri(i) = (FAR(min_index,1) +  FRR(min_index,1))/2;
end;
