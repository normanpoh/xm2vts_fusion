%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load data from main_analyse_rerun
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p=1;b=1;

for d=1:2, for k=1:2,
    expe.dset{d,k} = data{p}.dset{d,k}(:,b);
  end;
end;
expe.label = data{p}.label;

[param{p,b},size_dset{p}] = cal_clientd_param(expe);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot US densities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

min_ = min( [expe.dset{1,1};expe.dset{1,2}]);
max_ = max( [expe.dset{1,1};expe.dset{1,2}]);
axis_ = [min_ max_];

scores = linspace(min_,max_,1000)';
n=200;
%mov = avifile('example.avi','quality',90);
frames = [1:5 50 100 150 200];
F =  figure(1);
cla; hold on;
for j=1:200,

  pdf_(:,1) = cmvnpdf(scores,param{1}.mu{d,1}(j),param{1}.sigma{d,1}(j));
  pdf_(:,2) = cmvnpdf(scores,param{1}.mu{d,2}(j),param{1}.sigma{d,2}(j));
  plot(scores,pdf_(:,1),'r-');
  plot(scores,pdf_(:,2),'b-');
  xlabel('scores');
  ylabel('likelihood');

  axis([min_ max_, 0 5]);
  if length(find(frames==j))>0,
    fname = sprintf('Pictures/US_llk%03d.eps',j);
    print('-depsc2', fname);
  end;
  %mov = addframe(mov,F);
end;
%mov = close(mov);
figure(1);
[f,x] = ksdensity(expe.dset{1,1});
plot(x,f*10,'r','linewidth',2);
[f,x] = ksdensity(expe.dset{1,2});
plot(x,f*10,'b','linewidth',2);
axis([min_ max_, 0 5]);

set(gca, 'fontsize', 16);
xlabel('scores');
ylabel('likelihood');
fname = sprintf('Pictures/US_llk300.eps',j);
print('-depsc2', fname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate cdf from model and from data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

samples = [expe.dset{2,1}(:,b); expe.dset{2,2}(:,b)];
samples = linspace(min(samples), max(samples), 1000)';

for k=1:2,
  cdf_{k} = cal_cdf(param{1}.mu{2,k},param{1}.sigma{2,k},samples);
end;
[deg_list,radius{1}] = DET2polar(1-cdf_{1},cdf_{2});

[tmp, tmp, x, FAR, FRR] = wer(expe.dset{2,1}(:,b), expe.dset{2,2}(:,b));
[deg_list,radius{2}] = DET2polar(FAR,FRR);

%plot pdf and cdfs
subplot(2,1,1); cla; hold on; set(gca, 'fontsize', 16);
k=1; cal_cdf(param{1}.mu{2,k},param{1}.sigma{2,k},expe.dset{2,k},1,{'r--','b--'});
k=2; cal_cdf(param{1}.mu{2,k},param{1}.sigma{2,k},expe.dset{2,k},1,{'r-','b-'});
axis tight;axis_ = axis;
xlabel('likelihood'); ylabel('scores');
subplot(2,1,2); set(gca, 'fontsize', 16);
cla; hold on;
plot(samples, 1-cdf_{1},'r--')
plot(x, FAR,'b--','linewidth',2);
plot(samples, cdf_{2}, 'r-')
plot(x, FRR,'b-','linewidth',2);
xlabel('Scores');
ylabel('FRR, FAR');
legend('impostor (model)', 'impostor (data)','client (model)', 'client (data)');
axis tight; axis__ = axis; axis__([1 2]) = axis_(1:2);
axis(axis__);
fname = sprintf('Pictures/FAR_FRR_thrd.eps');
print('-depsc2', fname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot ROC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
signs = {'b-', 'r--'};
set(gca, 'fontsize', 16); cla; hold on;
loglog(FAR,FRR,signs{1});
loglog(1-cdf_{1}, cdf_{2},signs{2},'linewidth',2);
%semilogx(FAR,FRR,signs{1});
%semilogx(1-cdf_{1}, cdf_{2},signs{2});
%semilogx(1-cdf_{1},1-cdf_{2},signs{1});
%semilogx(1-FAR,FRR,signs{2});
xlabel('FAR');
ylabel('FRR');
legend('data','model');
fname = sprintf('Pictures/ROC_MSCC_vs_emp.eps');
print('-depsc2', fname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot DET
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
signs = {'bo-', 'rx-'};
cla; hold on;
leg = {'data','model'};
for m=1:2,
  [FAR_,FRR_] = polar2DET(radius{m}, deg_list);
  plot(FAR_,FRR_,signs{m});
end;
Make_DET; legend({leg{:}});%,'Location','North');
fname = sprintf('Pictures/DET_MSCC_vs_emp.eps');
print('-depsc2', fname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot EPC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_samples =11;
epc_range = [0,1];

[com{1}.epc.dev, com{1}.epc.eva, epc_cost]  = epc(expe.dset{1,1}, expe.dset{1,2}, expe.dset{2,1}, expe.dset{2,2}, n_samples,epc_range);
[com_{1}.epc.dev, com_{1}.epc.eva, epc_cost]  = epc(expe.dset{2,1}, expe.dset{2,2}, expe.dset{1,1}, expe.dset{1,2}, n_samples,epc_range);

for k=1:2,
  for d=1:2,
    cdf__{k}(:,d) = cal_cdf(param{1}.mu{d,k},param{1}.sigma{d,k},samples);
  end;
  cdf___{k} = cdf__{k}(:, [2 1]);
end;


[com{2}.epc.dev, com{2}.epc.eva, epc_cost]  = epc_density(1-cdf__{1}, cdf__{2}, samples, n_samples, epc_range);
[com_{2}.epc.dev, com_{2}.epc.eva, epc_cost]  = epc_density(1-cdf___{1}, cdf___{2}, samples, n_samples, epc_range);

lw = [1 2];
chosen=2:10;
subplot(2,1,1);
set(gca, 'fontsize', 16); cla; hold on;
for t=1:2;
  plot(epc_cost(chosen,1), com{t}.epc.eva.hter_apri(chosen)*100,signs{t},'linewidth',lw(t));
end;
ylabel('HTER (%)');
xlabel('\beta');
axis([0.1 0.9 4 10]);

subplot(2,1,2);
close all;
set(gca, 'fontsize', 16); cla; hold on;
for t=1:2;
  plot(epc_cost(chosen,1), com_{t}.epc.eva.hter_apri(chosen)*100,signs{t},'linewidth',lw(t));
end;
ylabel('HTER (%)');
xlabel('\beta');
axis([0.1 0.9 4 10]);
legend('data','model');
fname = sprintf('Pictures/EPC_MSCC_vs_emp.eps');
print('-depsc2', fname);
