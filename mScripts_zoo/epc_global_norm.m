function [res,pNI,pNC] = epc_global_norm(res_com, m, chosen)
%
%[far_apri, frr_apri, hter_apri] = epc_global(res_com, config_list)
% group by method m
initialise;

if (nargin < 2),
	config_list = [1:3];
end;

out.fa = [];
out.fr = [];
count = 0;

for t=chosen    %over all expe
    %txt = sprintf('%s for protocol %d\n', remark{s}, p);
    %fprintf(1,txt);
    count = count + 1;
    out.fa = [out.fa; res_com{t,m}.epc.eva.far_apri * NI];
    out.fr = [out.fr; res_com{t,m}.epc.eva.frr_apri * NC];
end;

out.NC = count * NC;
out.NI = count * NI;

res.eva.far_apri = sum(out.fa) / out.NI;
res.eva.frr_apri = sum(out.fr) / out.NC;
res.eva.hter_apri = (res.eva.far_apri+res.eva.frr_apri)/2;

pNC = out.NC;
pNI = out.NI;