t=0;
for p=1:2,
  fdata{p}.label = data{p}.label;
  for b=1:size(data{p}.dset{1,1},2),
    t=t+1;
    for d=1:2, for k=1:2,
        expe.dset{d,k} = data{p}.dset{d,k}(:,b);
      end;
    end;
    expe.label = data{p}.label;
    
    [out{1}{t}, epc_cost] = fusion_clientd_check(expe, 1, 'orig');
    [out{2}{t}, epc_cost] = fusion_clientd_check(expe, 1, 'z-norm');
    [out{3}{t}, epc_cost] = fusion_clientd_check(expe, 1, 'z-shift');
    [out{4}{t}, epc_cost] = fusion_clientd_check(expe, 1, 'f-norm');
    [out{5}{t}, epc_cost] = fusion_clientd_check(expe, 1, 'f-norm-simple');    

    for m=1:5,
      out{m}{t}.dset={};
    end;
    
  end;
end;

for t=1:13,
  for m=1:5,
    myout{t}.res{m} = out{m}{t}.epc;
  end;
end;

signs = {'bx-','bd--','b*-','r+-','rs--','r^-','kh--'};
lwidth = [ 1 1 1 2 2];
leg={'orig', 'z-norm', 'z-shift', 'f-norm', 'f-norm-variant'};

expe_list= setdiff(1:13,[7 9]);
face_list= setdiff([1 2 6:10],[7,9]);
speech_list = [ 3:5 11:13];

for m=1:5,
  [cfg{1}.res{m},pNI,pNC] = epc_global_custom(myout, m, expe_list);
  [cfg{2}.res{m},pNI,pNC] = epc_global_custom(myout, m, face_list);
  [cfg{3}.res{m},pNI,pNC] = epc_global_custom(myout, m, speech_list);
end;

for m=1:3,
  figure(m*2-1);
  plot_all_epc(epc_cost(:,1),leg,signs, cfg{m}, 1:5,lwidth, 14,1);
  fname = sprintf('../Pictures/main_baseline_epc_%d.eps',m);
  pause;
  print('-depsc2',fname);
  
  figure(m*2);
  plot_all_epc(epc_cost(:,1),leg,signs, cfg{m}, 1:5,lwidth,14,1,0);
  fname = sprintf('../Pictures/main_baseline_DET_%d.eps',m);
  pause;
  print('-depsc2',fname);

end;