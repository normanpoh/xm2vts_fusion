%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% load data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath ../mScripts

initialise;
for p=1:2,
  [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
  [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
  if p==1,
    for d=1:2,for k=1:2,
        data{p}.dset{d,k} = [ data{p}.dset{d,k} ...
          tanh_inv(data{p}.dset{d,k}(:,4)) tanh_inv(data{p}.dset{d,k}(:,5))];
        order = [1 2 3 6 7 8 4 9 5 10];
        data{p}.dset{d,k}(:,[1:10])= data{p}.dset{d,k}(:, order);
        
      end;
    end;
    %rearrange
  end;
  %delete the first one
  for d=1:2,for k=1:2,
      data{p}.dset{d,k}(:,1) = [];
    end;
  end;
end;
systype = [1 1 2 2 2  1 1 1 1 1  2 2 2];
syslabel={'(F,DCTs,GMM)', '(F,DCTb,GMM)', '(S,LFCC,GMM)', '(S,PAC,GMM)', '(S,SSC,GMM)',...
  '(F,DCTs,MLP)','(F,DCTs,iMLP)','(F,DCTb,MLP)','(F,DCTb,iMLP)', '(F,DCTb,GMM)',...
  '(S,LFCC,GMM)', '(S,PAC,GMM)', '(S,SSC,GMM)'};

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% reorder the data by protocols
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2)
    txt = sprintf('Experiment P%d:%d',p,b);fprintf(1,'%s\n',txt);
    t=t+1;
    protocol(t)=p;
  end;
end;

for p=1:2,
  order = [find(systype==1 & protocol==p) ...
    find(systype==2 & protocol==p)];
  
  syslabel_new{p} = {syslabel{order}};
  
  if p==2,
    order = [1:4]; %use the original order F S S S
  end;
  for d=1:2, for k=1:2,
      data{p}.dset{d,k} = data{p}.dset{d,k}(:,order);
    end;
  end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% multivariate zoo analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear BMI;
for p=1:2,
  
  [out,size_dset] = cal_clientd_param_multivariate(data{p});
  
  for d=1:2,for k=1:2,
      BMI{p}{d,k} = zeros(size(out.cov_B{d,k},1),size(out.cov_B{d,k},2),200);
      for t=1:200,
        BMI{p}{d,k}(:,:,t) = out.cov_B{d,k}(:,:,t) ./ out.cov_tot{d,k}(:,:,t);
      end;
    end;
  end;
end;
% 
% for d=1:2,for k=1:2,
%     mean_BMI{d,k} = [diag(mean(BMI{1}{d,k},3)) ; diag(mean(BMI{2}{d,k},3))];
%     std_BMI{d,k} = [diag(std(BMI{1}{d,k},1,3)) ; diag(std(BMI{2}{d,k},1,3))];
%   end;
% end;

%% plot cross-session
signs_{1} = {'b+','ro'};
signs_{2} = {'gx','kd'};
klabel={'impostor','client'};

clear txt;
clf; set(gca,'fontsize',16); cla; hold on;
t=0;
for k=1:2,
  for p=1:2,
    t=t+1;
    txt{t} = sprintf('%s, LP%d',klabel{k},p);
    plot(100*diag(mean(BMI{p}{1,k},3)),100*diag(mean(BMI{p}{2,k},3)),signs_{k}{p},'markersize',10)
    corrcoef([diag(mean(BMI{p}{1,k},3)),diag(mean(BMI{p}{2,k},3))])
  end;
end;
xlabel('zoo index [%], dev set');
ylabel('zoo index [%], eva set');
legend(txt,'location','southeast')
fname = sprintf('Pictures/main_zoo_mvar_analysis_data_BMI_corr.eps');
%print('-depsc2',fname);

%% plot BMI + var(BMI)
t=0;
for d=1:2,
  for k=1:2,
    t=t+1;    figure(t); set(gca,'fontsize',14);
    tmp = [mean_BMI{d,k} mean_BMI{d,k} + std_BMI{d,k} mean_BMI{d,k} - std_BMI{d,k}];
    boxplot(tmp','orientation','horizontal');
    axis_ = axis;
    axis_(1:2) = [0, 1]; axis(axis_);
    xlabel('zoo index [%]');
    ylabel('Data sets');
    set(gca,'yticklabel',{syslabel_new{1}{:} syslabel_new{2}{:}});
    set(gca,'ydir','reverse');
    fname = sprintf('Pictures/main_zoo_mvar_analysis_data_BMI_%d_%d.eps',d,k);
    print('-depsc2',fname);
  end;
end;

%% cross system
%% reshape the data
for p=1:2,
  tot = size(syslabel_new{p},2);
  for k=1:2,
    tmp=zeros(200*2,tot);
    for n=1:tot,
      tmp(1:200,n) = BMI{p}{1,k}(n,n,:);
      tmp(201:400,n) = BMI{p}{2,k}(n,n,:);
    end;
    [bmig{p,k}]= gauss_copula_transform(tmp);
    bmi{p,k}=tmp;
  end;
end;

set(gca,'fontsize',20);
for p=1:2,
  tot = size(syslabel_new{p},2);
  for k=1:2,
    bmig_corr{p,k} =corrcoef(bmig{p,k});
    imagesc(bmig_corr{p,k});
    colorbar; colorbar('fontsize',12)
    set(gca,'ytick',[1:tot]);
    set(gca,'yticklabel',syslabel_new{p});
    tmp = get(gca,'xticklabel');   set(gca,'xticklabel',cell(size(tmp,1),1));
    fname = sprintf('Pictures/main_zoo_mvar_analysis_data_BMI_xcorr_summary_p%d_k%d.eps',p, k);
    print('-depsc2',fname);
  end;
end;
%% 

set(gca,'fontsize',20);
type = 'bmig';
%type = 'bmi';
for p=1:2,
  tot = size(syslabel_new{p},2);
  list=nchoosek([1:tot],2);
  for k=1:2,
    for i=1:length(list);
      m=list(i,1); n=list(i,2);
      
      if strcmp(type,'bmig'),
        m_ = bmig{p,k}(:,m);
        n_ = bmig{p,k}(:,n);
      else
        m_ = bmi{p,k}(:,m);
        n_ = bmi{p,k}(:,n);
      end;
      %       m_ = reshape(BMI{p}{d,k}(m,m,:),1,200);
      %       n_ = reshape(BMI{p}{d,k}(n,n,:),1,200);
      %       m_= log(m_ ./ (1-m_));
      %       n_= log(n_ ./ (1-n_));
      %
      plot(m_,n_,'+');
      xlabel(syslabel_new{p}{m});
      ylabel(syslabel_new{p}{n});
      %tmp = get(gca,'xticklabel');   set(gca,'xticklabel',cell(size(tmp,1),1));
      %tmp = get(gca,'yticklabel');   set(gca,'yticklabel',cell(size(tmp,1),1));
      %tmp = corrcoef([m_',n_']);
      
      title(sprintf('Corr = %1.2f',bmig_corr{p,k}(m,n)));
      axis tight;
      fname = sprintf('Pictures/main_zoo_mvar_analysis_data_BMI_xcorr_%s_p%d_k%d_%d_%d.eps',type,p,k,m,n);
      print('-depsc2',fname);
      fname = strrep(fname,'.eps','.jpg');
      fname = strrep(fname,'Pictures','Pictures_');
      print('-djpeg95',fname);
    end;
  end;
end;
%




%% stop here

%% cross systems
dlabel = {'dev','eva'};
klabel = {'impostor','client'};
for p=1:2,
  figure(p); clf;
  t=0;
  for d=1:2, for k=1:2,
      t=t+1;
      subplot(2,2,t);
      imagesc(mean(BMI{p}{d,k},3)); colorbar;
      title(sprintf('%s, %s',dlabel{d},klabel{k}));
    end;
  end;
end;

%% 
%selected{p}{m} where, p = protocol, m = modality
selected{1}{1} = [1 2 4 6];
selected{1}{2} = [7:9];
selected{2}{1} = [1];
selected{2}{2} = [2:4];

for k=1:2,
  for m=1:2,
    bmi_{k,m} = [];
    for p=1:2,
      for d=1:2,
        tmp = triu(mean(BMI{p}{d,k},3)) .* (1-eye(size(BMI{p}{1},1)));
        tmp = tmp(selected{p}{m},:);
        tmp = tmp';
        bmi_{k,m} = [bmi_{k,m}; tmp(find (tmp~=0))];
      end;
    end;
  end;
end;

for m=1:2,
  subplot(1,2,m);
  boxplot([bmi_{1,m} bmi_{2,m}]);
end;

%%

for k=1:2,
  bmi_x{k}=[];
  for p=1:2,
    for d=1:2,
      tmp = triu(mean(BMI{p}{d,k},3)) .* (1-eye(size(BMI{p}{1},1)));
        if p==1,
          tmp_= tmp([1 2 4 6],[7:9]);
        else
          tmp_ = tmp(1,[2:4]);
        end;
        bmi_x{k}= [bmi_x{k} tmp_];
      end;
    end;
  end;
end;

%% x
for m=1:2,
  subplot(1,2,m);
  boxplot([bmi_{1,m} bmi_{2,m}]);
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%intramodal face
save main_zoo_mvar_analysis_data.mat mycfg protocol systype BMI

for p=1:2,
  mycfg.face{p} = intersect(find(systype==1), find(protocol==p));
  mycfg.speech{p} = intersect(find(systype==2), find(protocol==p));
end;

for p=1:2,
  mycfg.face_pair{p} = nchoosek(mycfg.face{p},2);
  mycfg.speech_pair{p} = nchoosek(mycfg.speech{p},2);

  t=0;
  for t1=1:length(mycfg.face{p}),
    for t2=1:length(mycfg.speech{p}),
    t=t+1;
    mycfg.mm_pair{p}(t,:) = [mycfg.face{p}(t1) mycfg.speech{p}(t2)];
    end;
  end;
end;

%% comparing BMI of different systems
sys = mycfg.face_pair{1}(2,:)
sys = mycfg.mm_pair{1}(5,:)

d=1;
cla; hold on;
tmp(:,1) = BMI{1}{d,1}(sys(1),sys(1),:);
tmp(:,2) = BMI{1}{d,1}(sys(2),sys(2),:);
tmp = log(tmp ./ (1 - tmp));
plot(tmp(:,1), tmp(:,2),'ro');
corr_ = corrcoef(tmp);
leg_{1} = sprintf('impostor (%1.3f)',corr_(1,2));

tmp(:,1) = BMI{1}{d,2}(sys(1),sys(1),:);
tmp(:,2) = BMI{1}{d,2}(sys(2),sys(2),:);
tmp = log(tmp ./ (1 - tmp));
plot(tmp(:,1), tmp(:,2),'b+');
corr_ = corrcoef(tmp);
leg_{2} = sprintf('client (%1.3f)',corr_(1,2));
xlabel(syslabel{sys(1)});
ylabel(syslabel{sys(2)});
legend(leg_);

%% comparing BMI of the same system in a zoo plot
t=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2)
    sys = b;
    
    %clf; hold on;
    for d=1:2,  subplot(2,2,d);
      tmp(:,1) = BMI{p}{d,1}(sys(1),sys(1),:);
      tmp(:,2) = BMI{p}{d,2}(sys(1),sys(1),:);
      tmp = log(tmp ./ (1 - tmp));
      plot(tmp(:,1), tmp(:,2),'b+');
      corr_ = corrcoef(tmp);
      xlabel('impostor BMI');
      ylabel('client BMI');
    end;
    for k=1:2,  subplot(2,2,k+2);
      tmp(:,1) = BMI{p}{1,k}(sys(1),sys(1),:);
      tmp(:,2) = BMI{p}{2,k}(sys(1),sys(1),:);
      tmp = log(tmp ./ (1 - tmp));
      plot(tmp(:,1), tmp(:,2),'b+');
      corr_ = corrcoef(tmp);
      xlabel('dev BMI');
      ylabel('eva BMI');
    end;
    subplot(2,2,1); title('dev');
    subplot(2,2,2); title('eva');
    subplot(2,2,3); title('impostor');
    subplot(2,2,4); title('client');
  end;
end;
%%
for d=1:2,
  for k=1:2,
    for p=1:2,
      logBMI{p}{d,k} = log ((BMI{p}{d,k}+400) ./ (400-BMI{p}{d,k}));
    end;
  end;
end;

%%
figure(4);
subplot(2,2,1);
imagesc(mean(logBMI{1}{d,1},3));
colorbar;
subplot(2,2,2);
imagesc(mean(logBMI{1}{d,2},3))
colorbar;