%% get scores
load_scores;
get_fnorm_scores;
%get_znorm_scores

for p=1:2,
  for b=1:size(data{p}.dset{1,1},2),
    [param{p,b},size_dset{p}] = cal_clientd_param(data{p},b);
    [paramf{p,b},size_dset{p}] = cal_clientd_param(fdata{p},b);
    %[paramz{p,b},size_dset{p}] = cal_clientd_param(zdata{p},b);
  end;
end;

%% calculate the f-ratios
t=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2),

    t=t+1;
    mytxt{t} = sprintf('P%d:%d %s',p,b,syslabel{t});


    for d=1:2,    
      %f-ratio
      myout{1}.fratio{d,t} = (param{p,b}.mu{d,2} - param{p,b}.mu{d,1}) ...
        ./ (param{p,b}.sigma{d,1} + param{p,b}.sigma{d,2});

      %compensated f-ratio
      param_ = .5 * param{p,b}.sigma{d,2} + .5 * mean(param{p,b}.sigma{d,2});
      myout{2}.fratio{d,t} = (param{p,b}.mu{d,2} - param{p,b}.mu{d,1}) ...
        ./ (param{p,b}.sigma{d,1} + param_);

      %simplified f-ratio
      myout{3}.fratio{d,t} = (param{p,b}.mu{d,2} - param{p,b}.mu{d,1}) ...
        ./ param{p,b}.sigma{d,1};

      %f-ratio in f-norm
      myout{4}.fratio{d,t} = 1 ./ (paramf{p,b}.sigma{d,1} + paramf{p,b}.sigma{d,2});

      %compensated f-ratio in f-norm
      param_ = .5 * paramf{p,b}.sigma{d,2} + .5 * mean(paramf{p,b}.sigma{d,2});
      myout{5}.fratio{d,t} = 1 ./ (paramf{p,b}.sigma{d,1} + param_);

      %simplified f-ratio in f-norm
      myout{6}.fratio{d,t} = 1 ./ (paramf{p,b}.sigma{d,1});

      %       %f-ratio in z-norm
      %       myout{3}.fratio{d,t} = (paramz{p,b}.mu{d,2}) ./ paramz{p,b}.sigma{d,2};
      %
      %       %constrained f-ratio in z-norm
      %       myout{4}.fratio{d,t} = (paramz{p,b}.mu{d,2});
      %
      
    end;

  end;
end;
%% 
for c=1:length(myout),
  for t=1:13,
    corr1 = corrcoef(myout{c}.fratio{1,t}', myout{c}.fratio{2,t}');
    myout{c}.corr(t,:) = corr1(1,2);
    myout{c}.bias(t,:) = mean((myout{c}.fratio{1,t}' -  myout{c}.fratio{2,t}'));
  end;
end;
%% plot
for c=1:length(myout),
  figure(c);

  for t=1:size(myout{1}.fratio,2),
    subplot(3,5,t);cla; hold on;
    plot((myout{c}.fratio{1,t}), (myout{c}.fratio{2,t}),'b.','markersize',6);
    x=[min((myout{c}.fratio{1,t})), max((myout{c}.fratio{2,t}))];
    axis tight;
    hold on; plot(x,x,'r-');
    a=get(gca,'xticklabel'); a=cell(1,length(a)); set(gca,'xticklabel',a);
    a=get(gca,'yticklabel'); a=cell(1,length(a)); set(gca,'yticklabel',a);
    title(mytxt{t});
  end;
end;
print('-depsc2','../Pictures/main_analyse_rerun2_fratio-corr-bias.eps');

%% bias and corr analysis
%low bias (around zero) and high correlation are good properties
a={'F-ratio (orig)', 'comp. F-ratio (orig)','B-ratio (orig)','F-ratio (F-norm)', 'comp. F-ratio (F-norm)','B-ratio (F-norm)'};
fnames_=strrep(a,' ','-');fnames_=strrep(fnames_,'.','');fnames_=strrep(fnames_,' ','-');fnames_=strrep(fnames_,'(','');fnames_=strrep(fnames_,')','');
for c=1:length(myout),
  figure(c);
  fname = sprintf('Pictures/main_analyse_rerun3_arun_%s.eps', fnames_{c});
  print('-depsc2',fname);
end;
 
%% plot
figure(1);
set(gca,'fontsize',14);
boxplot([myout{1}.corr myout{4}.corr, myout{2}.corr myout{4}.corr, ...
	 myout{3}.corr myout{6}.corr],'orientation','horizontal');
xlabel('correlation');
ylabel('');
set(gca,'yticklabel',{a{[1 4 2 5 3 6]}});
fname = sprintf('Pictures/main_analyse_rerun3_arun_fratio_corr_corr.eps');
print('-depsc2',fname);

figure(2);
set(gca,'fontsize',14);cla; hold on;
boxplot([myout{1}.bias myout{4}.bias, myout{2}.bias, ...
	 myout{5}.bias, myout{3}.bias, myout{6}.bias],'orientation','horizontal');
set(gca,'yticklabel',{a{[1 4 2 5 3 6]}});
xlabel('bias');
ylabel('');
%title('(b)');
plot([0;0],[6.5;0],'g-');
axis([  -0.7489    0.7489 0.5    6.5000    ]);
fname = sprintf('Pictures/main_analyse_rerun3_arun_fratio_corr_bias.eps');
print('-depsc2',fname);
