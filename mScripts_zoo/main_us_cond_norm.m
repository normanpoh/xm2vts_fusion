%% step function
% we shall experiment with the following step function
% where p = P(C|y)
max_ = .5;
cla; hold on;
p = linspace(0,1, 100);
conf = .5;
beta_ = max(0 , p-(conf)) * 1/(1-conf) * max_;
plot(p,beta_)
conf = 0.75;
beta_ = max(0 , p-(conf)) * 1/(1-conf) * max_;
plot(p,beta_,'r-')
conf = 0.9;
beta_ = max(0 , p-(conf)) * 1/(1-conf) * max_;
plot(p,beta_,'g-')

%% function 2
% cla; hold on;
% p = linspace(0,1, 100);
% conf = .5;
% beta_ = min(1-min(conf , 1-p), conf)
% plot(p,beta_)
% conf = 0.75;
% beta_ = min(1-min(conf , 1-p), conf)
% plot(p,beta_,'r-')
% conf = 0.9;
% beta_ = min(1-min(conf , 1-p), conf)
%plot(p,beta_,'g-')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% load the scores
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load_scores;
get_znorm_scores;
clear data;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% calculate param
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath ../mScripts_compen

conf = 1-eps;
conf = .85;
max_ = .5;

conf_list = linspace(0,1-eps,3);

t=0;
for p=1:2,
  for b=1:size(zdata{p}.dset{1,1},2)
    txt = sprintf('Experiment P%d:%d',p,b);fprintf(1,'%s\n',txt);
    t=t+1;
    protocol(t)=p;

    [param{t},size_dset{p}] = cal_clientd_param(zdata{p}, b);
    
    com = fusion_lr(zdata{p}, b);
    
    out{t}.mu_C = mean(param{t}.mu{1,2});
    out{t}.mu_Cj = param{t}.mu{1,2};

    for c = 1:length(conf_list),
      for d=1:2, for k=1:2,
          adjust_param = max(0 , com.dset{d,k}-conf_list(c)) * 1/(1-conf_list(c)) * max_;
          us_mu_Cj = us_stat2scores(zdata{p}.label{d,k}, out{t}.mu_Cj');
          beta_{d,k} = out{t}.mu_C - (adjust_param .* us_mu_Cj + (1-adjust_param) .* out{t}.mu_C);
          tmp.dset{d,k} = zdata{p}.dset{d,k}(:,b) + beta_{d,k};
        end;
      end;
      [tmp.epc.dev, tmp.epc.eva, epc_cost]  = epc(tmp.dset{1,1}, tmp.dset{1,2}, tmp.dset{2,1}, tmp.dset{2,2}, n_samples,epc_range);
      %dygnosis
      %[beta_{1,2}, com.dset{1,2}]
      eer_(c) = wer(tmp.dset{2,1},tmp.dset{2,2});
      fprintf(1,'.');
    end;
    
    %[tmp_.epc.dev, tmp_.epc.eva]  = epc(zdata{p}.dset{1,1}(:,b), zdata{p}.dset{1,2}(:,b), zdata{p}.dset{2,1}(:,b), zdata{p}.dset{2,2}(:,b), n_samples,epc_range);
    plot(conf_list,eer_*100);
  end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% run normalization experiment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



