  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% multivariate analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear V_B V_tot;

for p=1:2,
  order = [find(systype==1 & protocol==p) ...
    find(systype==2 & protocol==p)];
  
  syslabel_new{p} = {syslabel{order}};
  
  if p==2,
    order = [1:4]; %use the original order F S S S
  end;

  for d=1:2, for k=1:2,
      
      V_tot{p,d,k} = cov(data{p}.dset{d,k}(:,order));
      
      %calculate mu
      mu = [];      mu_global = mean(data{p}.dset{d,k}(:,order))';
      for t=order,
        mu = [mu;param{t}.mu{d,k}];
      end;
      tmp = [];
      for i=1:200,
        tmp(:,:,i) =(mu(:,i) - mu_global) * (mu(:,i) - mu_global)';
      end;
      V_B{p,d,k} = mean(tmp,3);
    end;
  end;
end;

for p=1:2,
  for d=1:2, for k=1:2,
      V_W{p,d,k} = V_tot{p,d,k} - V_B{p,d,k};
    end;
  end;
end;

%% plot
dlabel = {'dev','eva'};
klabel = {'impostor','client'};
for p=1:2,
  figure(p); clf;
  t=0;
  for d=1:2, for k=1:2,
      t=t+1;
      subplot(2,2,t);
      imagesc(abs(V_B{p,d,k}) ./ ( abs(V_B{p,d,k}) + abs(V_W{p,d,k}) ));
      colorbar;
      title(sprintf('%s, %s',dlabel{d},klabel{k}));
      sys_ = find(protocol==p);
      set(gca,'xtick',1:length(sys_));
      set(gca,'xticklabel',cell(1,length(sys_)));
      if k==1,
        set(gca,'ytick',1:length(sys_));
        %set(gca,'yticklabel',{syslabel{sys_}});
        set(gca,'yticklabel',syslabel_new{p});
      else
        set(gca,'ytick',1:length(sys_));
        set(gca,'yticklabel',cell(1,length(sys_)));
        %set(gca,'yticklabel',{syslabel_new{p}});
      end;
    end;
  end;
  fname = sprintf('Pictures/main_zoo_mvar_analysis_zoo_index_%d.eps',p);
  print('-depsc2',fname);
end;

% figure(p);
% fname = sprintf('Pictures/main_zoo_mvar_analysis_zoo_index_%d.eps',p);
% print('-depsc2',fname);

%% try
clear BMI;
for p=1:2,
  
  [out,size_dset] = cal_clientd_param_multivariate(data{p});
  
  for d=1:2,for k=1:2,
      for t=1:200,
        BMI{p}{d,k}(:,:,t) = out.cov_B{d,k}(:,:,t) ./ out.cov_tot{d,k}(:,:,t);
      end;
    end;
  end;
end;

for d=1:2,for k=1:2,
    mean_BMI{d,k} = [diag(mean(BMI{1}{d,k},3)) ; diag(mean(BMI{2}{d,k},3))];
    std_BMI{d,k} = [diag(std(BMI{1}{d,k},1,3)) ; diag(std(BMI{2}{d,k},1,3))];
  end;
end;

%% plot the matrix
for p=1:2,
  for d=1:2,for k=1:2,
      figure;
      imagesc(mean(BMI{p}{d,k},3))
    end;
  end;
end;

%% plot cross-session
signs_{1} = {'b+','ro'};
signs_{2} = {'gx','kd'};
klabel={'impostor','client'};

set(gca,'fontsize',16); cla; hold on;
t=0;
for k=1:2,
  for p=1:2,
    t=t+1;
    txt{t} = sprintf('%s, LP%d',klabel{k},p);
    plot(100*diag(mean(BMI{p}{1,k},3)),100*diag(mean(BMI{p}{2,k},3)),signs_{k}{p},'markersize',10)
  end;
end;
xlabel('zoo index [%], dev set');
ylabel('zoo index [%], eva set');
legend(txt,'location','southeast')

%% plot BMI + var(BMI)
t=0;
for d=1:2,for k=1:2,
    t=t+1;    figure(t); set(gca,'fontsize',14);
    tmp = [mean_BMI{d,k} mean_BMI{d,k} + std_BMI{d,k} mean_BMI{d,k} - std_BMI{d,k}];
    boxplot(tmp','orientation','horizontal');
    axis_ = axis;
    axis_(1:2) = [0, 1]; axis(axis_);
    xlabel('zoo index [%]');
    ylabel('Data sets');
    set(gca,'yticklabel',{syslabel_new{1}{:} syslabel_new{2}{:}});
    set(gca,'ydir','reverse');
    fname = sprintf('Pictures/main_zoo_mvar_analysis_BMI_%d_%d.eps',d,k);
    print('-depsc2',fname);
  end;
end;


