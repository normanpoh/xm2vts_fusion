function f = draw_us_DET(x, mu, sigma)
for k=1:2,
  f(:,k) = mvnpdf(x',mu(k), sigma(k));
end;
f = f ./ repmat(sum(f),size(f,1),1);
f=cumsum(f);
f(:,1)=1-f(:,1);
