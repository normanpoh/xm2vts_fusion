%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts_svm_feature_select
addpath /home/learning/norman/xm2vts_fusion/mScripts
initialise;

%Start experiment
p=1;
for p=1:2, %over two protocols

  %load score files
  [nexpe{1}.dset{1,1}, nexpe{1}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
  [nexpe{1}.dset{2,1}, nexpe{1}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
  N = size(nexpe{1}.dset{1,1},2);

  for i=1:N
    combinations = nchoosek([1:8],i);
    for i2=1:length(combinations),
      chosen = combinations(i2,:);
      for d=1:2,for k=1:2,
	nexpe{2}.dset{d,k} = nexpe{1}.dset{d,k}(:,chosen);
	end;
      end;
    
      %estimate performance using standard svm
      
      %estimate performance using parametric-svm
      
      
    end;
  end;

end;