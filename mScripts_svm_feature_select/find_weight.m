function [weight, time_taken, bayesS, WER_weight_explored] = find_weight(expe, method, dodisplay, debug, n_gmm, bayesS, prop_data_at_boundary, n_thrd)

chosen=[1 2];

if (nargin < 3 | length(dodisplay)==0),
	dodisplay = 0;
end;
if (nargin < 4 | length(debug)==0),
	debug = 0;
end;
if (nargin < 5 | length(n_gmm)==0),
	n_gmm = 30;
end;
if (nargin < 6 | length(bayesS)==0),
	bayesS = [];
end;
if (nargin < 7 | length(prop_data_at_boundary)==0),
	prop_data_at_boundary = 0.1;
end;
if (nargin < 8 | length(n_thrd)==0),
	n_thrd = 101;
end;

param = VR_analysis(expe.dset{1,1}(:,chosen), expe.dset{1,2}(:,chosen));
tic;
switch method
case 'fisher'
	weight = cal_weight_fisher([1 2], param);
case 'brute_gauss'
	[weight, alpha, eer] = cal_weight_brute([1 2], param, 101);
	signs = 'bo:';
case 'brute'
	alpha = linspace(0,1,101)';
	weight_ = [alpha 1-alpha];
	for i=1:size(weight_,1),
		w = expe.dset{1,1} * weight_(i,:)';
		s = expe.dset{1,2} * weight_(i,:)';
		wer_(i) = wer(w, s,[1 1]);
	end;
	[v, index]= min(wer_);
	weight = weight_(index,:);
	signs = 'rd--'; eer = wer_;
case 'gmm'
 fprintf(1, 'finding weight using gmm option (with brute-force search ...\n');
 data = [expe.dset{1,1}; expe.dset{1,2}];
 labels = [ ones(size(expe.dset{1,1},1),1);2*ones(size(expe.dset{1,2},1),1)];
 FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',1};%diagonal
 bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});

 %[weight, alpha, eer] = cal_WER_bayesS(bayesS,expe, debug,1); %criterion 1
 alpha = linspace(0,1,101)';
 weight_ = [alpha 1-alpha];
 for i=1:size(weight_,1),
   %evaluate EER
   nbayesS = com_bayesS(bayesS, weight_(i,:));
   [wer_(i)] = cal_WER_theory(nbayesS, [], n_thrd);
 end;
 [v, index]= min(wer_);
 weight = weight_(index,:);
 
 %switch n_gmm
 %	case 2
 %		signs = 'k:';
 %	case 5
 %		signs = 'k-.';
 %	case 10
 %		signs = 'k--';
 %	otherwise
 signs = 'k-';
 %end;
case 'gmm-em'
	data = [expe.dset{1,1}; expe.dset{1,2}];
	labels = [ ones(size(expe.dset{1,1},1),1);2*ones(size(expe.dset{1,2},1),1)];
	bayesS = gmmb_create(data, labels, 'EM', 'components', max(n_gmm), 'thr', 1e-8, 'verbose', 1);
	[weight, alpha, eer] = cal_WER_bayesS(bayesS,expe, debug,1); 

case 'gmm-log'
	data = [expe.dset{1,1}; expe.dset{1,2}];
	labels = [ ones(size(expe.dset{1,1},1),1);2*ones(size(expe.dset{1,2},1),1)];
	FJ_params = { 'Cmax', 5, 'Cmin', 2, 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
	bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});

	[weight, alpha, eer] = cal_WER_bayesS(bayesS,expe, debug, 2);
	signs = 'k--';

case 'svm'
    X=[expe.dset{1,1};expe.dset{1,2}];
    Y=[-1*ones(size(expe.dset{1,1},1),1);ones(size(expe.dset{1,2},1),1)];

    net = svm(size(X, 2), 'linear', [], 10);   
    net = svmtrain(net, X, Y);
    weight=net;

case 'gmm-eer'
	data = [expe.dset{1,1}; expe.dset{1,2}];
	labels = [ ones(size(expe.dset{1,1},1),1);2*ones(size(expe.dset{1,2},1),1)];
	FJ_params = { 'Cmax', n_gmm, 'Cmin', n_gmm, 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
	bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});

	weight_top = bayesS(2).mu * bayesS(2).weight - bayesS(1).mu * bayesS(1).weight;
	for k=1:2,
		sigma{k}= zeros(2);
		for c=1:size(bayesS(k).weight,1),
			sigma{k} = sigma{k} + bayesS(k).sigma(:,:,c) * (bayesS(k).weight(c) ^ 2);
		end;
	end;
	weight_bottom = sigma{1} + sigma{2};
	weight = weight_top' * inv(weight_bottom);
	weight = weight /sum(weight);

case 'gmm-pairing'
	if (length(bayesS) == 0),
		data = [expe.dset{1,1}; expe.dset{1,2}];
		labels = [ ones(size(expe.dset{1,1},1),1);2*ones(size(expe.dset{1,2},1),1)];
		FJ_params = { 'Cmax', n_gmm, 'Cmin', n_gmm, 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
		bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});
	end;

	%calculate the discriminative hyperplane locally
	for c1 =1:size(bayesS{1}.weight,1),
		for c2 =1:size(bayesS{2}.weight,1),
			weight_top = bayesS{2}.mu(:,c2) - bayesS{1}.mu(:,c1);
			weight_bottom = bayesS{2}.sigma(:,:,c2) + bayesS{1}.sigma(:,:,c1);
			out.weight{c1,c2} = weight_top' * inv(weight_bottom);
			out.prior(c1,c2) = (bayesS{1}.weight(c1) + bayesS{2}.weight(c2))/2;
		end;
	end;

	%weighted hyperplane
	weight = zeros(1, 2);
	for c1 =1:size(bayesS{1}.weight,1),
		for c2 =1:size(bayesS{2}.weight,1),
			weight = [weight + out.weight{c1,c2} * out.prior(c1,c2)];
			%could also evaluate EER for each weight
			%could also evaluate EER along old weight and new weight direction, 
			%each time keeping the smallest one
		end;
	end;
	weight = weight /sum(weight);

case 'gmm-gradient'
	if (length(bayesS) == 0),
		data = [expe.dset{1,1}; expe.dset{1,2}];
		labels = [ ones(size(expe.dset{1,1},1),1);2*ones(size(expe.dset{1,2},1),1)];
		FJ_params = { 'Cmax', n_gmm, 'Cmin', n_gmm, 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
		bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});
	end;

	weight = bayesS2weight(bayesS);

case 'gmm-discrim'
	%train gmm if necessary
	if (length(bayesS) == 0),
		data = [expe.dset{1,1}; expe.dset{1,2}];
		labels = [ ones(size(expe.dset{1,1},1),1);2*ones(size(expe.dset{1,2},1),1)];
		FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
		bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});
	end;
	d=1;for k=1:2,
	%for gmm
		c=1;tmp1 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
		c=2;tmp2 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
		%out.dset{d,k} = log(tmp2+realmin)-log(tmp1+realmin);
		out.dset{d,k} = log(tmp2+realmin)-log(tmp1+realmin);
	end;

	%[wer_ thrd_min] = wer(out.dset{1,1},out.dset{1,2});
	%k=1;	out2.dset{d,k} = expe.dset{d,k}( find(out.dset{d,k} >= thrd_min),: );
	%k=2;	out2.dset{d,k} = expe.dset{d,k}( find(out.dset{d,k} < thrd_min),: );

	d=1;for k=1:2,
		[f,x] = ecdf(abs(out.dset{d,k}));%plot(x,f);
		[tmp, index] = min(abs(f-prop_data_at_boundary));
		thrd = x(index);
		out2.dset{d,k} = expe.dset{d,k}( find(abs(out.dset{d,k}) <= thrd),: );
	end;

	%wer(out.dset{1,1},out.dset{1,2}, [0.5 0.5], 1);
	%draw_empiric(out2.dset{1,1},out2.dset{1,2});

	%calculate the distribution on data laying on the boundary
	data = [out2.dset{1,1}; out2.dset{1,2}];
	labels = [ ones(size(out2.dset{1,1},1),1);2*ones(size(out2.dset{1,2},1),1)];
	FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',1};
	nbayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});

	[weight, WER_weight_explored] = bayesS2weight(nbayesS, bayesS, n_thrd);

	%figure(2);draw_empiric(expe.dset{1,1},expe.dset{1,2});

 case 'gmm-svm'
  %train gmm if necessary
  if (length(bayesS) == 0),
    data = [expe.dset{1,1}; expe.dset{1,2}];
    labels = [ ones(size(expe.dset{1,1},1),1);2*ones(size(expe.dset{1,2},1),1)];
    FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
    bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});
  end;
  d=1;for k=1:2,
    c=1;tmp1 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
    c=2;tmp2 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
    out.dset{d,k} = log(tmp2+realmin)-log(tmp1+realmin);
  end;

  %segment the data out
  %cut = [0.1 0.2 0.3 0.4];
  cut = [0.2 0.2 0.2 0.4];
  for i=1:4, sum_cut(i) = sum(cut(1:i)); end;
  sum_cut = [ 0 sum_cut];
  d=1;for k=1:2,
    [f,x] = ecdf(abs(out.dset{d,k}));
    %hold off;plot(x,f); hold on;
    for i=1:4,
      [tmp, index] = min(abs(f-sum_cut(i))); thrd_lower = x(index);
      [tmp, index] = min(abs(f-sum_cut(i+1))); thrd_upper = x(index);
      %plot(thrd_upper, f(index),'o');
      if (i==1),
	selected1=[];
      else
        selected1 = find(abs(out.dset{d,k}) <= thrd_lower);
      end;
      selected2 = find(abs(out.dset{d,k}) <= thrd_upper);
      selected_exp = setdiff(selected2, selected1);
      out2{i}.dset{d,k} = expe.dset{d,k}( selected_exp,: );
    end;
    %pause;
  end;

  %================================
  %if (1==0),
  %calculate the distribution of data laying on the boundary
  n_gmm = 15;
  for i=1:4,
    fprintf(1,'Estimating the distribution for data set %d\n', i);
    data = [out2{i}.dset{1,1}; out2{i}.dset{1,2}];
    labels = [ ones(size(out2{i}.dset{1,1},1),1);2*ones(size(out2{i}.dset{1,2},1),1)];
    FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',1};
    nbayesS{i} = gmmb_create(data, labels, 'FJ', FJ_params{:});
  end;

  %form a big bayesS
  for k=1:2,
      nnbayesS(k).mu=[];
      nnbayesS(k).sigma=[];
      nnbayesS(k).weight=[];
      t=0;
      for i=1:4,
      nnbayesS(k).mu = [nnbayesS(k).mu nbayesS{i}(k).mu];
      for comp =1:size(nbayesS{i}(k).weight,1)
	t=t+1;
        nnbayesS(k).sigma(:,:,t) = nbayesS{i}(k).sigma(:,:,comp);
      end;
      nnbayesS(k).weight = [nnbayesS(k).weight; nbayesS{i}(k).weight * cut(i)];
    end;
  end;
  
  %end;
  %================================  
  %draw_theory_bayesS(nnbayesS);
  
  %for i=1:4,
  %  figure(1);subplot(2,2,i); hold off;
  %  draw_theory_bayesS(nbayesS{i});
  %end;
  %print('-depsc2', '../Pictures/segmental_1_1_1.eps');	
  %figure(2);
  %i=1;
  %draw_theory_bayesS(nbayesS{i});
  %draw_empiric(out2{i}.dset{1,1}, out2{i}.dset{1,2});
  %print('-depsc2', '../Pictures/segmental2_1_1_1.eps');	

  %[weight, WER_weight_explored] = bayesS2weight(nbayesS, nnbayesS,
  %n_thrd);
  
  %run the svm algo:
  %fprintf(1,'Training weights with svm\n');
  %i=1;
  %X=[out2{i}.dset{1,1};out2{i}.dset{1,2}];
  %Y=[-1*ones(size(out2{i}.dset{1,1},1),1);ones(size(out2{i}.dset{1,2},1),1)];
  
  %net = svm(size(X, 2), 'linear', [], 10);   
  %net = svmtrain(net, X, Y);
  %weight=net.normalw;

  [weight, WER_weight_explored] = bayesS2weight(nbayesS{1}, nnbayesS, n_thrd);
  bayesS = nnbayesS;
 otherwise
	error('Unknown method');
end

time_taken = toc;

if (dodisplay),
	plot(alpha(:,1), eer, signs);
end;

function [weight, WER_weight_explored] = bayesS2weight(bayesS, orig_bayesS, n_thrd);
if (nargin < 2),
  %the no. of search step when calling cal_WER_theory
  n_thrd = 101;
end;

%for i=1:2,
%	nbayesS(i).mu = bayesS{i}.mu;
%	nbayesS(i).sigma = bayesS{i}.sigma;
%	nbayesS(i).weight = bayesS{i}.weight;
%end;

%calculate the discriminative hyperplane locally
for c1 =1:size(bayesS(1).weight,1),
  for c2 =1:size(bayesS(2).weight,1),
    weight_top = bayesS(2).mu(:,c2) - bayesS(1).mu(:,c1);
    weight_bottom = bayesS(2).sigma(:,:,c2) + bayesS(1).sigma(:,:,c1);
    out.weight{c1,c2} = weight_top' * inv(weight_bottom);
    out.weight{c1,c2} = out.weight{c1,c2} / sum(out.weight{c1,c2});
    out.prior(c1,c2) = (bayesS(1).weight(c1) + bayesS(2).weight(c2))/2;
  end;
end;

%evaluate EER along old weight and new weight direction, 
%each time keeping the smallest one

n_steps = 5;

%5 cycle iteration for stable solution

WER_weight_explored = [];
%evaluating:
fprintf(1, 'evaluating %d client components and %d impostor components on:\n', size(bayesS(2).weight,1), size(bayesS(1).weight,1));
orig_bayesS(1)
orig_bayesS(2)	
%for t=1:5,	

%to visualise the weight space
%w=linspace(0,1,101);
%w=[w' 1-w'];
%for i=1:101,
%      nbayesS = com_bayesS(orig_bayesS, w(i,:));
%      [min_WER(i)] = cal_WER_theory(nbayesS, [], n_thrd);
%end;
%plot(w(:,1),min_WER)

for c1 =1:size(bayesS(1).weight,1),
  for c2 =c1+1:size(bayesS(2).weight,1),
    fprintf(1, '.');
    %initialisation: initial weight
    weight_top = bayesS(2).mu(:,c2) - bayesS(1).mu(:,c1);
    weight_bottom = bayesS(2).sigma(:,:,c2) + bayesS(1).sigma(:,:,c1);
    weight = weight_top' * inv(weight_bottom);
    weight = weight / sum(weight);
    
    if (c1==1 & c2==2),
      old_weight = weight;
      nbayesS = com_bayesS(orig_bayesS, weight);
      [old_min_WER] = cal_WER_theory(nbayesS, [], n_thrd);
    else
      myw{1} = old_weight;
      min_WER(1) = old_min_WER;
      myw{2} = weight;
      nbayesS = com_bayesS(orig_bayesS, weight);
      min_WER(2) = cal_WER_theory(nbayesS, [], n_thrd);

      %switch 1 and 2 if necessary
      if min_WER(1) < min_WER(2),
	%1 is better
	%swith 1 and 2
	tmp = myw{1}; myw{1} = myw{2}; myw{2} = tmp;
	tmp = min_WER(1); min_WER(1) = min_WER(2); min_WER(2)=tmp;
      end;

      %min_WER
      %pause;
      
      done=0;
      while (~done),
	%min_WER
	fprintf(1,'*');
	myw{3} = mean([myw{1};myw{2}]);
	nbayesS = com_bayesS(orig_bayesS, myw{3});
	[min_WER(3)] = cal_WER_theory(nbayesS, [], n_thrd);

	[tmp, index] = sort(min_WER);

	if (index(3)==3), %the last element is the worse
	  done = 1;
	else
	  if abs(min_WER(index(1)) - min_WER(index(2))) <0.0001,
	    done=1;
	  else
	    %continue
	    %3 to 2; 2 to 1
	    myw_{1}=myw{index(2)};min_WER_(1)=min_WER(index(2));
	    myw_{2}=myw{index(1)};min_WER_(2)=min_WER(index(1));
	    myw=myw_; min_WER = min_WER_;
	  end;
	end;
      end;
      
      %record the best element
      old_min_WER = min_WER(2);
      old_weight = myw{2};      
      
      %for debugging purpose
      WER_weight_explored = [WER_weight_explored;  old_min_WER'];
     
    end;
    %fprintf(1,'current minimum value %1.5f\n',old_min_WER);
  end;
end;

fprintf(1, '\n');

WER_weight_explored 

%t=1;
%fprintf(1, 'At cycle %d c1=%d and c2=%d\n',t, c1, c2);
%[weight_search min_WER'*100]
%old_weight;
%pause;

%end;

%hold off; plot(test(:,1), test(:,2), '+'); 
%axis([0 1 0 1]);
%grid on;pause;
%corrcoef(test)

weight = old_weight;