%load configurations
cd /home/learning/norman/xm2vts_fusion/mScripts_svm_feature_select
addpath /home/learning/norman/xm2vts_fusion/mScripts

initialise;
load_data; %load the variable data

%Start experiment
p=1;s=1;r=1;
for p=1:2, %over two protocols
  for s=1:3, %over 3 different configurations
    txt = sprintf('%s for protocol %d\n', remark{s}, p);
    fprintf(1,txt);
    for r=1:size(expe.P{p}.seq{s},1),
      %print remarks
      txt = sprintf('\nExperiment %d (%s)\n',r,[expe_labels.P{p}.seq{s}.row{r}]); fprintf(1,txt);
      
      b = expe.P{p}.seq{s}(r,:);
      %load score files
      for d=1:2,for k=1:2,
	  nexpe.dset{d,k} = data{p}.dset{d,k}(:,b);
	end;
      end;
      
      cla;
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      %calculate the GMM parameters
      %bayesS = scores2bayesS(nexpe{1}); 
      %cla;
      %draw_theory_bayesS(nnbayesS);
      draw_empiric(nexpe.dset{1,1}, nexpe.dset{1,2});
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      pause;
    end;
  end;
end;