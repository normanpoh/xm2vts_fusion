function nnbayesS = scores2bayesS(expe, n_gmm, bayesS, cut)

if (nargin<2),
  n_gmm = [5 20];
end;

if (nargin<3),
  bayesS=[];
end;

if (nargin<4),
  cut = [0.2 0.2 0.2 0.4];
end;

if (length(bayesS) == 0),
  data = [expe.dset{1,1}; expe.dset{1,2}];
  labels = [ ones(size(expe.dset{1,1},1),1);2*ones(size(expe.dset{1,2},1),1)];
  FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',0};
  bayesS = gmmb_create(data, labels, 'FJ', FJ_params{:});
end;

%calculate the LLR
d=1;for k=1:2,
  c=1;tmp1 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
  c=2;tmp2 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
  out.dset{d,k} = log(tmp2+realmin)-log(tmp1+realmin);
end;

sum_cut = [ 0 cumsum(cut)];
d=1;for k=1:2,
  [f,x] = ecdf(abs(out.dset{d,k}));
  %hold off;plot(x,f); hold on;
  for i=1:4,
    [tmp, index] = min(abs(f-sum_cut(i))); thrd_lower = x(index);
    [tmp, index] = min(abs(f-sum_cut(i+1))); thrd_upper = x(index);
    %plot(thrd_upper, f(index),'o');
    if (i==1),
      selected1=[];
    else
      selected1 = find(abs(out.dset{d,k}) <= thrd_lower);
    end;
    selected2 = find(abs(out.dset{d,k}) <= thrd_upper);
    selected_exp = setdiff(selected2, selected1);
    out2{i}.dset{d,k} = expe.dset{d,k}( selected_exp,: );
  end;
  %pause;
end;

%calculate the distribution of data laying on the boundary
%n_gmm = 15;
for i=1:4,
  fprintf(1,'Estimating the distribution for data set %d\n', i);
  data = [out2{i}.dset{1,1}; out2{i}.dset{1,2}];
  labels = [ ones(size(out2{i}.dset{1,1},1),1);2*ones(size(out2{i}.dset{1,2},1),1)];
  FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 1, 'covtype',1};
  nbayesS{i} = gmmb_create(data, labels, 'FJ', FJ_params{:});
end;

%form a big bayesS
for k=1:2,
  nnbayesS(k).mu=[];
  nnbayesS(k).sigma=[];
  nnbayesS(k).weight=[];
  t=0;
  for i=1:4,
    nnbayesS(k).mu = [nnbayesS(k).mu nbayesS{i}(k).mu];
    for comp =1:size(nbayesS{i}(k).weight,1)
      t=t+1;
      nnbayesS(k).sigma(:,:,t) = nbayesS{i}(k).sigma(:,:,comp);
    end;
    nnbayesS(k).weight = [nnbayesS(k).weight; nbayesS{i}(k).weight * cut(i)];
  end;
end;
