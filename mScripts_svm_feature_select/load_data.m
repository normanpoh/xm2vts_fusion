for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, expe.label{1,1}, expe.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, expe.label{2,1}, expe.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%normalize the data
for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    if ismlp{p}(b),
      for d=1:2,for k=1:2,
	  data{p}.dset{d,k}(:,b) = tanh_inv(data{p}.dset{d,k}(:,b));
	end;
      end;
    end;
  end;
end;
