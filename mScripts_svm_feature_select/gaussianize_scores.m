for p=1:2,
  for b=1:size(dat.P{p}.labels,2)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %for MLP output
    if ismlp{p}(b),
      for d=1:2,for k=1:2,
	  dat
          nexpe{1}.dset{d,k} = expe.dset{d,k};
          ex=1;
          nexpe{1}.dset{d,k}(:,ex) = tanh_inv(expe.dset{d,k}(:,ex));
        end;
      end;
      
      %filter the bad ones
      nexpe{2} = nexpe{1};
      for k=1:2,
        d=1;ex=1;
        selected = find(nexpe{2}.dset{d,k}(:,ex)>30);
        nexpe{2}.dset{d,k}(selected)=[];
        selected = find(nexpe{2}.dset{d,k}(:,ex)<-30);
        nexpe{2}.dset{d,k}(selected)=[];
      end;
      
      param = VR_analysis(nexpe{2}.dset{1,1},nexpe{2}.dset{1,2});      
      
      %now replace bad values with a reasonable one
      nexpe{3} = nexpe{1};
      
      for d=1:2, for k=1:2,
          ex=1;
          selected = find(nexpe{3}.dset{d,k}(:,ex)>30);
          nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_C(ex),size(selected,1),1);
          selected = find(nexpe{3}.dset{d,k}(:,ex)<-30);
          nexpe{3}.dset{d,k}(selected,ex)= repmat(param.mu_I(ex),size(selected,1),1);
        end;
      end;
    
      myexpe = nexpe{3};  
      myexpe.label = data{p}.label;
    end;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%