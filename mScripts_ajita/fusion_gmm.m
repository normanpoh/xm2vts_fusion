function [com, epc_cost ] = fusion_gmm(expe, chosen, n_gmm, bayesS,draw, noepc)
% use [com, com_gmm] = fusion_rbf(expe, chosen, 'nonorm') for no normalisation

%default config:
n_samples = 11;
epc_range = [0.1 0.9];

if (nargin < 2),
  chosen = [1:2];
end;

if (nargin < 3 | length(n_gmm)==0),
  n_gmm = [1 6];
end;

if (nargin < 4 | length(bayesS)==0),
  bayesS= [];
end;

if (nargin < 5 | length(draw)==0),
  draw = 0;
end;

if (nargin < 6),
  noepc = 0;
  epc_cost=[];
end;
%make chosen
for d=1:2,for k=1:2,
    expe.dset{d,k} = expe.dset{d,k}(:,chosen);
end;end;

%standard procedure
data = [expe.dset{1,1}; expe.dset{1,2}];
labels = [ zeros(size(expe.dset{1,1},1),1);ones(size(expe.dset{1,2},1),1)];

if (length(bayesS)==0),
  FJ_params = { 'Cmax', max(n_gmm), 'Cmin', min(n_gmm), 'thr', 1e-2, 'animate', 0, 'verbose', 0, 'covtype',0};
  bayesS = gmmb_create(data, labels + 1, 'FJ', FJ_params{:});
  %this gmmbayes accept class labels 1 2 ...
end;

%calculate likelihoods

for d=1:2,for k=1:2,
    c=1;tmp1 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
    c=2;tmp2 = gmmb_pdf(expe.dset{d,k}, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
    com.dset{d,k} = log(tmp2+realmin)-log(tmp1+realmin);
  end;
end;

com.bayesS = bayesS;

if noepc,
  %do nothing
else
  [com.epc.dev, com.epc.eva, epc_cost]  = epc(com.dset{1,1}, com.dset{1,2}, com.dset{2,1}, com.dset{2,2}, n_samples,epc_range);
end;

%surface fitting
if (draw),
  [xtesta1,xtesta2]=meshgrid( ...
      linspace(min(data(:,1)), max(data(:,1)), 100), ...
      linspace(min(data(:,2)), max(data(:,2)), 100) );
  [na,nb]=size(xtesta1);
  xtest1=reshape(xtesta1,1,na*nb);
  xtest2=reshape(xtesta2,1,na*nb);
  xtest=[xtest1;xtest2]';
  
  c=1;tmp1 = gmmb_pdf(xtest, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
  c=2;tmp2 = gmmb_pdf(xtest, bayesS(c).mu, bayesS(c).sigma, bayesS(c).weight );
  ypred = log(tmp2+realmin)-log(tmp1+realmin);
  
  ypredmat=reshape(ypred,na,nb);
  hold off;
  contourf(xtesta1,xtesta2,ypredmat,50);shading flat;
  hold on;
  
  thrd = wer(com.dset{1,1}, com.dset{1,2});
  [cs,h]=contour(xtesta1,xtesta2,ypredmat,[thrd],'k');
  clabel(cs,h);colorbar;
  %plot(raw{1}(:,1), raw{1}(:,2), 'ro');
  %plot(raw{2}(:,1), raw{2}(:,2), 'b+');
  %colorbar;

  draw_theory_bayesS(bayesS);
end;

