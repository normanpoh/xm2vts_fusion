addpath ../mScripts

n_samples = 11;
epc_range = [0.1 0.9];

initialise;

%load and use a modified version of the default data
for p=1:2,
  [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}] = load_raw_scores_labels(datafiles{p}.dev);
  [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}] = load_raw_scores_labels(datafiles{p}.eva);
  if p==1,
    for d=1:2,for k=1:2,
        data{p}.dset{d,k} = [ data{p}.dset{d,k} ...
          tanh_inv(data{p}.dset{d,k}(:,4)) tanh_inv(data{p}.dset{d,k}(:,5))];
        order = [1 2 3 6 7 8 4 9 5 10];
        data{p}.dset{d,k}(:,[1:10])= data{p}.dset{d,k}(:, order);
        
      end;
    end;
    %rearrange
  end;
  %delete the first one
  for d=1:2,for k=1:2,
      data{p}.dset{d,k}(:,1) = [];
    end;
  end;  
end;
%the new systems are as follows
syslabel={'GMM,F','GMM,F','GMM,S','GMM,S','GMM,S',...
	  'MLP,F','MLPi,F','MLP,F','MLPi,F','GMM,F',...
	  'GMM,S','GMM,S','GMM,S'}; 
syslabel={'P1:1(F)','P1:2(F)','P1:3(S)','P1:4(S)','P1:5(S)',...
	  'P1:6(F)','P1:7(F)','P1:8(F)','P1:9(F)','P2:1(F)',...
	  'P2:2(S)','P2:3(S)','P2:4(S)'}; 
