function [cdf_, cdf_data]= cal_cdf(mu,var_,samples, draw)
if nargin<4,
  draw=0;
end;

samples = sort(samples);

%assuming equal priors across users
prior = ones(1, length(mu))  ./ length(mu);
pdf_ = zeros(length(samples),1);  
for i=1:length(mu),
  %size(cmvnpdf(samples, mu(i), var_(i)))
  pdf_ = pdf_ + cmvnpdf(samples, mu(i), var_(i)) * prior(i);
  %pdf_ = pdf_ + normpdf(samples, mu(i), var_(i)) * prior(i);
end;

cdf_ = cumsum(pdf_)/sum(pdf_);

if draw~=0,
  [f,x]= ksdensity(samples);
  cdf_data = cumsum(f)/sum(f);
end;
if (draw==1),
  %subplot(1,2,1);
  cla; hold on;
  plot(samples, pdf_,'b-');
  plot(x,f,'r--','linewidth',2);
  %legend('predicted', 'from data');
end;
if (draw==3),
  %subplot(1,2,1);
  cla; hold on;
  plot(samples, cdf_,'b-');
  plot(x,cdf_data,'r--','linewidth',2);
  %legend('predicted', 'from data');
end;

if (draw==2),
  subplot(1,2,1);
  cla; hold on;
  plot(samples, pdf_,'b-');
  plot(x,f,'r-','linewidth',2);
  legend('predicted', 'from data');
  
  subplot(1,2,2); cla; hold on;
  plot(samples, cdf_,'b-');
  plot(x,cdf_data,'r-','linewidth',2);
  
  legend('predicted', 'from data');
  
  %check what ecdf does exactly! I suspect that it is wrong!
  %[f,x] = ecdf(samples);
  %plot(x,f,'g-');
  %legend('US gauss', 'ksdensity','ecdf');
  
end;