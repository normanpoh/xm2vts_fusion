function [deg_list, radius] = get_DET_conf(gauss_param, n_bootstraps, samples)

n_users = length(gauss_param);
for n_=1:n_bootstraps,
  chosen_users = floor(rand(1,n_users) * n_users)+1;
  cdf_gen = cal_cdf( gauss_param(chosen_users,1), gauss_param(chosen_users,2), samples);
  cdf_imp = cal_cdf( gauss_param(chosen_users,3), gauss_param(chosen_users,4), samples);
  [deg_list, radius(n_,:)] = DET2polar(1-cdf_imp,cdf_gen);
end;