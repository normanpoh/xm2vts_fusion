function [out, param] = remove_user_dep_com(expe, impID, chosen, method)

if nargin<4,
  method = 'single impostor';
end;

for d=1:2,for k=1:2,
    out.dset{d,k} = zeros(size(expe.dset{d,k},1),1);
  end;
end;

%get the model label
model_ID = unique(expe.label{1,1});

for id=1:size(model_ID,1),

  switch method
    case 'impostor specific'
    
      %get the data set associated to the ID
      for d=1:2,
        for k=1,
          selected = find(expe.label{d,k} == model_ID(id));
          %index{d,k} = selected;
          scores = expe.dset{d,k}(selected,chosen);
          size_dset{d,k}(id) = length(scores);
          
          impID_ = impID(selected);
          imp_list = unique(impID_);
          for i=1:length(imp_list),
            selected_ = find(impID_==imp_list(i));
            scores_ = scores(selected_);
            param.mu{d,k}(id,i)=mean(scores_);
            param.var{d,k}(id,i)=var(scores_);            
          end;
          out.dset{d,k}(selected(selected_),1) = scores_ - param.mu{d,k}(id,i);
        end;
        
        for k=2,
          selected = find(expe.label{d,k} == model_ID(id));
          scores = expe.dset{d,k}(selected,chosen);
          size_dset{d,k}(id) = length(scores);
          param.mu{d,k}(id,1)=mean(scores);
          param.var{d,k}(id,1)=var(scores);          
          out.dset{d,k}(selected,1) = scores - param.mu{d,k}(id,1);
        end;
      end;

    case 'single impostor'
      for d=1:2,
        for k=1:2
          selected = find(expe.label{d,k} == model_ID(id));
          scores = expe.dset{d,k}(selected,chosen);
          size_dset{d,k}(id) = length(scores);
          param.mu{d,k}(id,1)=mean(scores);
          param.var{d,k}(id,1)=var(scores);          
          out.dset{d,k}(selected,1) = scores - param.mu{d,k}(id,1);
        end;
      end;
  end;% end case
end;
