%cd /home/learning/norman/xm2vts_fusion/mScripts_
cd F:\Norman\learning\xm2vts_fusion\mScript_trained_kernel
addpath ../mScripts

%compare empirical and theoretical diff
initialise;
for p=1:2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}, data{p}.impID] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}, data{p}.impID] = load_raw_scores_labels(datafiles{p}.eva);
end;

p=1;
chosen = 2;
method = 'single impostor';
%method = 'impostor specific';
[out, param] = remove_user_dep_com(data{p}, data{p}.impID, chosen, method);

t=0;
for d=1:2; 
  for k=1:2,
    t=t+1;
    [f,x, width] = ksdensity(out.dset{d,k},'kernel','triangle');
    
    [n,x_] = hist(out.dset{d,k});
    n=n/sum(n);

    subplot(2,2,t);
    cla; hold on;
    bar(x_,n);
    plot(x,f,'r--');
    
    [param.phat{d,k}, pci] =mle(out.dset{d,k}, 'distribution', 'normal');
    n_rows = length(x);
    llh = pdf('normal', x', ones(n_rows,1)*phat(1), ones(n_rows,1)*phat(2));
    plot(x,llh,'g-');
  end;
end;

% dist = {'normal', 'poiss','Rayleigh', 'Beta','Bernoulli','gamma'};
% for d_=1:length(dist),
%   [phat, pci] =mle(out.dset{d,k}, 'distribution', dist{d_});
%   n_rows = length(out.dset{d,k});
%   switch length(phat)
%     case 1
%       llh(d_) = sum(log(pdf(dist{d_}, out.dset{d,k}, ones(n_rows,1)*phat(1))));
%     case 2
%       llh(d_) = sum(log(pdf(dist{d_}, out.dset{d,k}, ones(n_rows,1)*phat(1), ones(n_rows,1)*phat(2))));
%     case 3
%       llh(d_) = sum(log(pdf(dist{d_}, out.dset{d,k}, ones(n_rows,1)*phat(1), ones(n_rows,1)*phat(2), ones(n_rows,1)*phat(3))));
%   end;
% end;

figure;
plot(param.mu{1,1},param.mu{1,2},'+');

t=0;
for d=1:2; 
  for k=1:2,
    t=t+1;
    subplot(2,2,t);
    [f,x, width] = ksdensity(param.mu{d,k},'kernel','box');
    
    [n,x_] = hist(param.mu{d,k});
    n=n/sum(n);
    cla; bar(x_,n);axis tight;
  end;
end;


chosen=2;method ='single impostor';
[out, param] = remove_user_dep_com(data{p}, data{p}.impID, chosen, method);
d=1;
min_ = min([data{p}.dset{d,1};data{p}.dset{d,2}]);
max_ = max([data{p}.dset{d,1};data{p}.dset{d,2}]);
samples=linspace(min_(chosen),max_(chosen),1000)';

%calculate the common covariance
for d=1:2,
  for k=1:2,
    param.var_common(d,k) = var(out.dset{d,k});
  end;
  gauss_param = [param.mu{d,2},ones(n_rows,1) * param.var_common(d,2), ...
    param.mu{d,1},ones(n_rows,1) * param.var_common(d,1)];
  [deg_list, radius{d,1}] = get_DET_conf(gauss_param, 100, samples);
  gauss_param = [param.mu{d,2},param.var{d,2}, ...
    param.mu{d,1},param.var{d,1}];
  [deg_list, radius{d,2}] = get_DET_conf(gauss_param, 100, samples);
end;

signs_={'rd--','bo:'};
cla; hold on;
for d=1:2,
  for t=1:2,
    conf = quantile(radius{d,t},[2.5 97.5]/100);
    for i=1:2,
      [FAR,FRR] = polar2DET(conf(i,:),deg_list);
      plot(FAR,FRR,signs_{t},'linewidth', d);
    end;
  end;
end;
Make_DET;
%legend('');
%print('-depsc2', '../Pictures/true_random_imp_svc2004.eps');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k=1:2,
  subplot(2,2,k);
  n_rows = size(param.mu{d,k},1);
  [cdf_, cdf_data]= cal_cdf(param.mu{d,k},ones(n_rows,1) * param.var_common(d,k),data{p}.dset{d,k}(:,chosen), 3);
  
  subplot(2,2,k+2);
  %[cdf_, cdf_data]= cal_cdf(param.mu{d,k},param.var{d,k},data{p}.dset{d,k}(:,chosen), 1);
  [cdf_, cdf_data]= cal_cdf(param.mu{d,k},param.var{d,k},data{p}.dset{d,k}(:,chosen), 3);
end;

wer(data{p}.dset{d,1}(:,chosen),data{p}.dset{d,2}(:,chosen),[],1);