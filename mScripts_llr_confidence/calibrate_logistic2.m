function [imp_calib, gen_calib, com ] = calibrate_logistic2(imp_, gen_, chosen, debug)
% [imp_calib, gen_calib, com ] = calibrate_logistic2(imp, gen)
% Given a pair of inputs (imp, gen) scores, each of which must be one 
% column, this funciton gives a calibrated output.
%
% [imp_calib, gen_calib, com ] = calibrate_logistic2(imp, gen, chosen)
% chosen specifies the column of data to use
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date: 31 Jan 2015

if nargin<3||isempty(chosen),
  chosen=1;
end;

if nargin<4||isempty(debug),
  debug=0;
end;

% define weight
data_ = [imp_(:,chosen); gen_(:,chosen)];
label_ = [ zeros(size(imp_(:,chosen))); ones(size(gen_(:,chosen)))];
w_ = [ ones(size(imp_(:,chosen))) ./ numel(imp_(:,chosen)); 
  ones(size(gen_(:,chosen)))  ./ numel(gen_(:,chosen)) ];

com.b = glmfit(data_, label_, 'binomial','weight',w_);
Prob_ = glmval(com.b, data_, 'identity');

% for the output
imp_calib= Prob_(label_==0);
gen_calib= Prob_(label_==1);

if debug,
  subplot(2,1,1);
  wer(imp_(:,chosen), gen_(:,chosen),[],4,[],1);
  subplot(2,1,2);
  wer(imp_calib,gen_calib,[],4,[],1);
end
