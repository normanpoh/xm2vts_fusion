clear;
load main_llr_confidence_subject_level_bstrp.mat out_subject
load main_llr_confidence_sample_level_bstrp.mat out_sample
load main_llr_confidence.mat syslabel

%% plot all of them
tit = {'sample, LR', 'sample, LLR', 'subject, LR', 'subject, LLR'};
for b=1:13,
  figure(b);
  for t=1:4,
    subplot(2,2,t);
    switch t
      case 1
        boxplot(out_sample{b,1}(1:5:end,:)','outliersize', 1);
      case 2
        boxplot(out_sample{b,2}(1:5:end,:)','outliersize', 1);
      case 3
        boxplot(out_subject{b,1}(1:5:end,:)','outliersize', 1);
      case 4
        boxplot(out_subject{b,2}(1:5:end,:)','outliersize', 1);
    end;
    title(tit{t});
    %title(syslabel{b});
  end;
end;

%% compare them
%prctile(out_sample{b,1}', [2.5 25 50 75 97.5])
for b=1:13,
  for m=1:2,
    subplot(2,1,m);
    cla;
    conf_sample = prctile(out_sample{b,m}', [2.5 50 97.5]);
    plot(conf_sample','--')
    hold on;
    conf_subject = prctile(out_subject{b,m}', [2.5 50 97.5]);
    plot(conf_subject','-');
    xlabel('index of range of E with equal sampling');
    ylabel('log likelihood ratio (E)');
    title(syslabel{b});
    grid on;
  end;
  legend('low (sample)','med (sample)','upper (sample)','low (subject)','med (subject)','upper (subject)','location','northwest')
  fname = sprintf('Pictures/main_llr_confidence_analyse__%02d.png',b);
  print('-dpng', fname);
  %pause;
end;
  
  
