%% begin
addpath ../mScripts_zoo
addpath H:\Norman\learning\myPrograms\Matlab\VR-EER
%for fusion_lr
load_scores;

%% get 30 subject-level bootstraps for each system b (which goes from 1 to 13)
% create the user list
clear model*
b_=0;
for p=1:2,
  user_seq = unique(data{p}.label{1,1});
  for b=1:size(data{p}.dset{1,1},2),
    b_=b_+1;
    
    fprintf(1,'Experiment %d of 13\n',b_);
    for s = 1:30,
      selected_user = randi(200,1,200);
      nexpe = filter_users(data{p}, b, selected_user);
      
      % METHOD 1
      [~, ~,com] = calibrate_logistic2(nexpe.dset{1,1}, nexpe.dset{1,2},1);
      model{b_}.param(:,s) = com.b;
      
      % METHOD 2
      [~, ~,com2] = calibrate_LLR(nexpe.dset{1,1}, nexpe.dset{1,2},1);
      
      fprintf(1,'.');
    end;
    fprintf(1,'\n');
  end;
end;

%% test the 30 bootstraps for system b
clear out;
b_=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2),
    b_=b_+1;
    dat_ = [ data{p}.dset{2,1}(:,b);data{p}.dset{2,2}(:,b)]; 
    xlist_ = linspace(min(dat_), max(dat_), 50); %sample all values in the range
    out{b_}=zeros(50,30);
    for s=1:30,
      out{b_}(:,s)= glmval(model{b_}.param(:,s), xlist_, 'identity');
    end;
  end;
end;

%% plot the confidence intervals for all 13 systems
for b=1:13,
  subplot(3,5,b);
  boxplot(out{b}(1:3:end,:)','outliersize', 1);
  title(syslabel{b});
end;
%
print('-depsc2','Pictures/main_llr_confidence__13_expe_logistics.eps');

%% NEXT: We need to compare the confidence intervals of subject-level bootstrapping with the conventional bootstrapping

%% METHOD 2

% [imp_ca