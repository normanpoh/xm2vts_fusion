function [imp_calib, gen_calib, com ] = calibrate_logistic2_FAR_FRR(imp_, gen_, chosen, debug)
% [imp_calib, gen_calib, com ] = calibrate_logistic2(imp, gen)
% Given a pair of inputs (imp, gen) scores, each of which must be one 
% column, this funciton gives a calibrated output.
%
% [imp_calib, gen_calib, com ] = calibrate_logistic2(imp, gen, chosen)
% chosen specifies the column of data to use
%
% Author: Norman Poh (normanpoh@ieee.org)
% Date: 31 Jan 2015

if nargin<3||isempty(chosen),
  chosen=1;
end;

if nargin<4||isempty(debug),
  debug=0;
end;

% get the FAR and FRR
[eer_, thrd_, com.x, com.FAR, com.FRR] = wer(imp_(:,chosen),gen_(:,chosen));

left_= com.FRR ./eer_ * 0.5;
right_= 1-com.FAR ./eer_ * 0.5;

%%
f_=zeros(size(com.x));
range = [min(find(com.FRR~=0 )) find(com.FAR==0, 1 )];

% f_(1:range(1)) = 0;
f_(1:range(1)) = left_(range(1));

%f_(range(2):end) = 1;
f_(range(2):end) = right_(range(2)-1);

mid_ = find(com.x==thrd_);

f_(range(1):mid_) = left_(range(1):mid_);
f_(mid_:range(2)-1) = right_(mid_:range(2)-1);
com.f = f_;

% for the output
imp_calib = interp1(com.x,com.f,imp_(:,chosen));
gen_calib = interp1(com.x,com.f,gen_(:,chosen));

% transform back to logit
imp_calib = log (imp_calib  ./ (1-imp_calib ));
gen_calib = log (gen_calib  ./ (1-gen_calib ));


if debug,
  subplot(3,1,1);
  cla; hold on;
  plot(com.x, f_,'k:');
  plot(com.x, com.FAR,'r--');
  plot(com.x, com.FRR,'b--');
  grid on;
  
  subplot(3,1,2);
  wer(imp_(:,chosen),gen_(:,chosen),[],4)

  subplot(3,1,3);
  wer(imp_calib(:,chosen),gen_calib(:,chosen),[],4)
  %axis_=axis;
  %x_liveness=linspace(-30,20,100);
  %y=glmval(com.b,x_liveness,'logit');
  %plot(x_liveness,y/10,'k--');
  
end
