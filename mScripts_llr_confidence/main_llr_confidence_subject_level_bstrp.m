%% load the data
if 1==0,
  addpath ../mScripts_zoo
  addpath H:\Norman\learning\myPrograms\Matlab\VR-EER
  %for fusion_lr
  load_scores;
  save main_llr_confidence.mat data syslabel
else
  clear;
  load main_llr_confidence.mat
end;

%% get 30 subject-level bootstraps for each system b (which goes from 1 to 13)
% create the user list
clear model*
b_=0;
for p=1:2,
  user_seq = unique(data{p}.label{1,1});
  for b=1:size(data{p}.dset{1,1},2),
    b_=b_+1;
    
    fprintf(1,'Experiment %d of 13\n',b_);
    for s = 1:30,
      selected_user = randi(200,1,200);
      nexpe = filter_users(data{p}, b, selected_user);
      
      % METHOD 1 training
      [~, ~,com] = calibrate_logistic2(nexpe.dset{1,1}, nexpe.dset{1,2},1);
      model_lr{b_}.param(:,s) = com.b;
       
      % METHOD 2 training
      [~, ~,com_llr] = calibrate_LLR(nexpe.dset{1,1}, nexpe.dset{1,2},1);
      model_llr{b_}{s} = com_llr;
      fprintf(1,'.');
    end;
    fprintf(1,'\n');
  end;
end;

%% test the 30 bootstraps for system b
clear out_subject
b_=0;
for p=1:2,
  for b=1:size(data{p}.dset{1,1},2),
    b_=b_+1;
    
    dat_ = [ data{p}.dset{2,1}(:,b);data{p}.dset{2,2}(:,b)]; 
    xlist_ = linspace(min(dat_), max(dat_), 50); %sample all values in the range
    out_subject{b_,1}=zeros(50,30);
    out_subject{b_,2}=zeros(50,30);
    
    for s=1:30,
      %method 1 inference
      out_subject{b_}(:,s)= glmval(model_lr{b_}.param(:,s), xlist_, 'identity');
      
      %method 2 inference
      com = model_llr{b_}{s};
      f_{1} = interp1(com.x,com.pdf{1},xlist_,'linear');
      f_{2} = interp1(com.x,com.pdf{2},xlist_,'linear');
      out_subject{b_,2}(:,s)  = log( f_{2}+realmin) - log (f_{1}+realmin);
      
    end;
  end;
end;
%%
save main_llr_confidence_subject_level_bstrp.mat out_subject

%% debug
for i=1:13,
  figure(i);
  for b=1:2,
    subplot(2,1,b);
    boxplot(out_subject{i,b}(1:5:end,:)','outliersize', 1);
  end;
end;
%%

%% plot the confidence intervals for all 13 systems
for b=1:13,
  subplot(3,5,b);
  boxplot(out{b}(1:5:end,:)','outliersize', 1);
  title(syslabel{b});
end;
%
print('-depsc2','Pictures/main_llr_confidence__13_expe_logistics.eps');

%% plot the confidence intervals for all 13 systems
close all;
figure(1);
for b=1:13,
  subplot(3,5,b);
  boxplot(out{b,1}(1:5:end,:)','outliersize', 1);
  title(syslabel{b});
end;
%
print('-dpng','Pictures/main_llr_confidence_subject_level_bstrp__13_expe_LR.png');

%%
figure(2);
for b=1:13,
  subplot(3,5,b);
  boxplot(out{b,2}(1:5:end,:)','outliersize', 1);
  title(syslabel{b});
end;
print('-dpng','Pictures/main_llr_confidence_subject_level_bstrp__13_expe_LLR.png');
