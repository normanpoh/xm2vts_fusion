addpath ../mScripts/
initialise;

for p=2,
    [data{p}.dset{1,1}, data{p}.dset{1,2}, data{p}.label{1,1}, data{p}.label{1,2}, data{p}.imp_id{1}] = load_raw_scores_labels(datafiles{p}.dev);
    [data{p}.dset{2,1}, data{p}.dset{2,2}, data{p}.label{2,1}, data{p}.label{2,2}, data{p}.imp_id{2}] = load_raw_scores_labels(datafiles{p}.eva);
end;

%export nist format
%claim id, true id, scores
d=1;
chosen = [2 4];
tmp{1}=[data{p}.imp_id{d} data{p}.label{d,1} data{p}.dset{d,1}(:,chosen)];
tmp{2}=[data{p}.label{1,2} data{p}.label{d,2} data{p}.dset{d,2}(:,chosen)];

txt = {'imp.scores', 'gen.scores'};
for k=1:2,
  fid = fopen(txt{k}, 'w')
  for i=1:length(tmp{k}),
    fprintf(fid, '%d %d %f %f\n', tmp{k}(i,1),tmp{k}(i,2), tmp{k}(i,3), tmp{k}(i,4));
  end;
  fclose(fid);
end;

